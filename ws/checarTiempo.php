<?php
	// el argumento es el id de la operación
	include_once "php/funciones2.php";

	global $mysqli;
	$idOperacion = $argv[1];

	// checamos si hay operaciones
	if ( $idOperacion!=null )
	{
		// checamos si la operación sogue en estado "en recepción"
		$q = 'select count(*) as c, id_usuario from f_dc_operaciones where id_operacion='.$idOperacion.' and status='.ESTADO_ENSALIDA;
		$rs = $mysqli->query($q);
		if ( $rs!=false )
		{
			$row = $rs->fetch_assoc();
			if ($row["c"] > 0)
			{
				// checamos cuanto tiempo le queda
				$q2 = 'select hora_estimada_entrega as hee from f_dc_operaciones where id_operacion='.$idOperacion;
				$rs2 = $mysqli->query($q2);
				
				if ( $rs2!=false )
				{
					$row2 = $rs2->fetch_assoc();
					$hora_estimada_entrega = new DateTime($row2["hee"]);
					$ahora = new DateTime("now");
					$segundosOffset = intval($hora_estimada_entrega->format("s"))-1;
					mail("gabriel@funktionell.com.mx", "testing", $row["id_usuario"]."->".$hora_estimada_entrega->format("Y-m-d H:i:s")." :: ".$ahora->format("Y-m-d H:i:s")." --- offsetsecs: ".$segundosOffset);
					
					// $ahora siempre tiene segundos=0. necesitamos esperar adicionalmente el numero de segundos en $hora_estimada_entrega
					@mysqli_close($mysqli);		// cerramos la conexión para no ocuparla mientras esperamos
					sleep($segundosOffset);		// esperamos
					$mysqli = mysqli_connect(DB_URL, DB_USER, DB_PASS, DB_SCHEMA);
					
					if ( $ahora->diff($hora_estimada_entrega)->i >= 2 )
					{
						// notificar que quedan dos minutos
						mail("gabriel@funktionell.com.mx", "notificación 2", "se envió la notificación de que quedan 2 minutos ".$row["id_usuario"]);
						enviarNotificacion($row["id_usuario"], "Quedan dos minutos para recoger tu auto.");
						
						// calendarizar el siguiente chequeo en 2 minutos
						exec('echo "/usr/bin/php /var/www/html/ws/checarTiempo.php '.$idOperacion.'" | sudo at now + 2 minutes');
					}
					else
					{
						// reingresar
						enviarNotificacion($row["id_usuario"], "El tiempo para recoger tu auto ha expirado y tu auto será reingresado.");
						reingresarAuto($idOperacion);
					}
				}
			}
			else
			{
				// la operación ya no está en "entrega" -> ya no es necesario enviar notificación
			}
		}
	} // fin del if si hay un id_operacion valido