<?php
	header('Content-type: text/plain; charset=utf-8');
	date_default_timezone_set('America/Mexico_City');
	
	// _____________________________________________________________________________________________________ CONFIGURACIÓN
	include_once("php/funciones2.php");

	$method = $_SERVER['REQUEST_METHOD'];
	if ( isset($_SERVER['PATH_INFO']) )
		$request = explode('/', trim($_SERVER['PATH_INFO'],'/'));
	else
		$request = array();
		
	global $mensaje;
	$mensaje = "";
	
	global $wsVersion;
	$wsVersion = 2;

	// _____________________________________________________________________________________________________ VALIDACIÓN
	if ( count($request) < 2 )
	{
		// 2do: error: no se especificó un comando
		// 1. reportar error en la bd
		// 2. regresar un mensaje de error
		echo "error";
		exit(1);
	}

	// _____________________________________________________________________________________________________ PROCESAMIENTO
	$res = null;		// es la respuesta que regresará el webservice
	$jsonparams = isset($request[2]) ? urldecode($request[2]) : null;	// el tercer parámetro son los parámetros del comando
	//$jsonparams = isset($request[2]) ? $request[2] : null;	// el tercer parámetro son los parámetros del comando

	// el primer comando de $request es la categoría de la operación: umanagement
	switch( strtolower($request[0]) )
	{
		// __________________________________________________________________________ admin de usuarios
		case "umanagement":		// atiene peticiones de manejo de usuarios
			// ahora procesamos el segundo param: comando
			switch( strtolower($request[1]) )
			{
				case "altametodopago":
					$res = ejecutarMetodo("altaMetodoPago", $jsonparams);
					break;

				case "bajametodopago":
					$res = ejecutarMetodo("bajaMetodoPago", $jsonparams);
					break;

				case "cambiometodopago":
					$res = ejecutarMetodo("cambioMetodoPago", $jsonparams);
					break;
					
				case "getoperadores":
					$res = ejecutarMetodo("getOperadores", $jsonparams);
					break;

				case "getusuario":
					if ( strpos($jsonparams, "idUsuario") !== false )
						$res = ejecutarMetodo("getUsuarioById", $jsonparams);
					else
						$res = ejecutarMetodo("getUsuario", $jsonparams);
					break;

				case "getusuariobyidoperacion":
					$res = ejecutarMetodo("getUsuarioByIdOperacion", $jsonparams);
					break;

				case "login":
					$res = ejecutarMetodo("validarUsuario", $jsonparams);
					break;

				case "perfileditarnombre":
					if ( strpos($jsonparams, "idUsuario") !== false )
						$res = ejecutarMetodo("perfilEditarNombreById", $jsonparams);
					else
						$res = ejecutarMetodo("perfilEditarNombre", $jsonparams);
					break;

				case "perfileditartelefono":
					if ( strpos($jsonparams, "idUsuario") !== false )
						$res = ejecutarMetodo("perfilEditarTelefonoById", $jsonparams);
					else
						$res = ejecutarMetodo("perfilEditarTelefono", $jsonparams);
					break;

				case "perfileditarcontrasenha":
					if ( strpos($jsonparams, "idUsuario") !== false )
						$res = ejecutarMetodo("perfilEditarPasswordById", $jsonparams);
					else
						$res = ejecutarMetodo("perfilEditarPassword", $jsonparams);
					break;

				case "perfileditarnip":
					if ( strpos($jsonparams, "idUsuario") !== false )
						$res = ejecutarMetodo("perfilEditarNipById", $jsonparams);
					else
						$res = ejecutarMetodo("perfilEditarNip", $jsonparams);
					break;

				case "recover":
					$res = ejecutarMetodo("recuperarContrasenha", $jsonparams);
					break;

				case "recovernip":
					$res = ejecutarMetodo("recuperarNip", $jsonparams);
					break;

				case "register":
					$res = ejecutarMetodo("altaUsuario", $jsonparams);
					break;

				case "registerfb":
					$res = ejecutarMetodo("altaUsuarioFB", $jsonparams);
					break;
			} // fin del switch umanagement:command
			break;

		// __________________________________________________________________________ datos genéricos (sin necesidad de una operación)
		case "data":
			switch( strtolower($request[1]) )
			{
				case "altaauto":
					$res = ejecutarMetodo("altaAuto", $jsonparams);
					break;

				case "bajaauto":
					$res = ejecutarMetodo("bajaAuto", $jsonparams);
					break;

				case "cambiarauto":
					$res = ejecutarMetodo("cambioAuto", $jsonparams);
					break;

				case "getautos":
					$res = ejecutarMetodo("getAutos", $jsonparams);
					break;

				case "getestacionamientos":
					$res = ejecutarMetodo("getEstacionamientos", $jsonparams);
					break;
					
				case "getestacionamiento":
					$res = ejecutarMetodo("getEstacionamiento", $jsonparams);
					break;
				
				case "getestpensioncompartida":
					$res = ejecutarMetodo("getEstPensionCompartida", $jsonparams);
					break;

				case "getoperacionespendientes":
					$res = ejecutarMetodo("getOperacionesPendientes", $jsonparams);
					break;

				case "getpensiones":
					$res = ejecutarMetodo("getPensiones", $jsonparams);
					break;

				case "getstatususuario":
					$res = ejecutarMetodo("getStatusUsuario", $jsonparams);
					break;

				case "gettienemetodopago":
					$res = ejecutarMetodo("getTieneMetodoPago", $jsonparams);
					break;
				
				case "gettienemetodopagooperacion":
					$res = ejecutarMetodo("getTieneMetodoPagoOperacion", $jsonparams);
					break;

				case "perfilgetcelular":
					$res = ejecutarMetodo("perfilGetCelular", $jsonparams);
					break;
					
				case "gethistorial":
					$res = ejecutarMetodo("getHistorial", $jsonparams);
					break;
					
				case "registerpushtoken":
					$res = ejecutarMetodo("registerPushToken", $jsonparams);
					break;

				case "registerparkingpushtoken":
					$res = ejecutarMetodo("registerParkingPushToken", $jsonparams);
					break;
			} // fin del switch dara:command
			break;


		// __________________________________________________________________________ operaciones
		case "operations":
			switch( strtolower($request[1]) )
			{
				case "aceptarsolicitudsalida":
					$res = ejecutarMetodo("aceptarSolicitudSalida", $jsonparams);
					break;

				case "aceptarsolicitudsalidaplaca":					// se mantiene por compatibilidad
					$res = ejecutarMetodo("aceptarSolicitudSalidaPlaca", $jsonparams);
					break;
					
				case "aceptarticket":
					$res = ejecutarMetodo("aceptarTicket", $jsonparams);
					break;
					
				case "altaoperacion":
					$res = ejecutarMetodo("altaOperacion", $jsonparams);
					break;

				case "altafotooperacion":
					$res = ejecutarMetodo("altaFotoOperacion", $jsonparams);
					break;

				case "cancelarpension":
					$res = ejecutarMetodo("cancelarPension", $jsonparams);
					break;

				case "completaralta":
					$res = ejecutarMetodo("completarAlta", $jsonparams);
					break;

				case "contratarpension":
					$res = ejecutarMetodo("contratarPension", $jsonparams);
					break;
				
				case "contratarpensioncompartida":
					$res = ejecutarMetodo("contratarPensionCompartida", $jsonparams);
					break;

				case "getdummieautoentregado":
					$res = ejecutarMetodo("getDummieAutoEntregado", $jsonparams);
					break;

				case "getoperacion":
					$res = ejecutarMetodo("getOperacion", $jsonparams);
					break;

				case "getreserva":
					$res = ejecutarMetodo("getReserva", $jsonparams);
					break;

				case "getservicioactivo":
					$res = ejecutarMetodo("getServicioActivo", $jsonparams);
					break;

				case "getservicioactivobyid":
					$res = ejecutarMetodo("getServicioActivoById", $jsonparams);
					break;

				case "getstatusestacionamiento":
					$res = ejecutarMetodo("getStatusEstacionamiento", $jsonparams);
					break;

				case "registrarsalida":
					$res = ejecutarMetodo("registrarSalida", $jsonparams);
					break;

				case "reingresarauto":
					$res = ejecutarMetodo("reingresarAuto", $jsonparams);
					break;

				case "reservarestacionamiento":
					$res = ejecutarMetodo("reservarEstacionamiento", $jsonparams);
					break;

				case "setasistencia":
					$res = ejecutarMetodo("setAsistencia", $jsonparams);
					break;

				case "setasistencias":
					$res = ejecutarMetodo("setAsistencias", $jsonparams);
					break;

				case "solicitarauto":
					$res = ejecutarMetodo("solicitarAuto", $jsonparams);
					break;

				case "finalizaroperacion":
					$res = ejecutarMetodo("finalizarOperacion", $jsonparams);
					break;
			} // fin del switch umanagement:command
			break;
			
		case "dev":
			switch( strtolower($request[1]) )
			{
				case "logerror":
					$res = ejecutarMetodo("logErrorApp", $jsonparams);
					break;
					
				case "reportexception":
					$res = ejecutarMetodo("reportException", $jsonparams);
					break;
					
				case "reporterror":
					$res = ejecutarMetodo("reportError", $jsonparams);
					break;
			} // fin del switch umanagement:command
			break;

		default:
			// 2do: error -> la categoria no es reconocida
			// 1. reportar error en la bd
			// 2. regresar un mensaje de error
			$res = '{"error":"Categoría no reconocida","data":"'.$request[0].'"}';
			break;
	}


	// ________________________________________________________________________________ RESPUESTA
	@mysqli_close($mysqli);

	$r = array(
		"respuesta"	=> $res===null ? "error" : "ok",
		"datos" 	=> $res,
		"mensaje"	=> $mensaje
	);
	echo json_encode((object)$r);




