<?php
	// el argumento es el id de la operación

	include_once "php/funciones.php";
	global $mysqli;
	$idOperacion = $argv[1];

	// checamos si ya fue aceptada la operacion
	if ( $idOperacion!=null )
	{
		$q = 'select count(*) as cuenta, id_usuario from f_dc_operaciones where status = 3 and id_operacion='.$idOperacion;
		$rs = $mysqli->query($q);
		if ( $rs!=false )
		{
			$row = $rs->fetch_assoc();
			if ( $row["cuenta"] > 0 )
			{
				// aún no se acepta -> aceptra automáticamente
				$mysqli->query('update f_dc_operaciones set status=4 where id_operacion='.$idOperacion);
				enviarNotificacion($row["id_usuario"], "Tu servicio ha sido aceptado automáticamente. Disfruta tu estancia.");
			}
			else
			{
				// ya se aceptó -> no hacer nada
			}
		}
		else
	} // fin del if si hay un id_operacion valido

