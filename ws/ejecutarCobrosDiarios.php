<?php
	$currdir=getcwd();
	chdir("/var/www/html/ws/");
	include_once "php/funciones2.php";

	global $mysqli;

	// obtener una lista de todos los cobros a cobrarse hoy
	$q = "select * from f_dc_pagos_programados where status=1 and DATE(fecha_vencimiento) = CURDATE()";		// status=1 son los pendientes por cobrar
	$rs = $mysqli->query($q);
	if ($rs!=false)
	{
		while($row = $rs->fetch_assoc())
		{
			$token = getTokenMetodoPago( $row["id_usuario"] );
			$monto = $row["monto"];

			$referencia = "".date("Y-m-d H:i:s", time());
			$referencia = str_ireplace(" ", "", $referencia);
			$referencia = str_ireplace("-", "", $referencia);
			$referencia = str_ireplace(":", "", $referencia);
			$referencia .= $row["id_operacion"];

			$res = pagar($referencia, $token, $monto, $row["tipo"], $row["id_operacion"]); // $referencia, $token, $monto, $tipo, $idMovimiento=-1
			
			if ( $res==true )
			{
				$mysqli->query("update f_dc_pagos_programados set status=3 where id_pago_programado=".$row["id_pago_programado"]);
			}
			else
			{
				// todo: no se pudo realizar el pago -> notificar al usuario (y crear un log)
			}
			
			sleep(5);	// metemos una pausa entre cada procesamiento de un pago para no saturar al dbms en caso de tener muchos pagos programados (pueden ser miles y no está del todo optimizado)
		}
	}
	else
	{
	}
	
	// obtener una lista de todos los cobros a cuya fecha de vencimiento pasó hace 3 días o más (ya están retrasados)
	$q = "select * from f_dc_pagos_programados where status=1 and DATE(fecha_vencimiento) <= DATE_SUB(CURDATE(), INTERVAL 3 DAY)";		// status=1 son los pendientes por cobrar
	$rs = $mysqli->query($q);
	if ($rs!=false)
	{
		while($row = $rs->fetch_assoc())
		{
			$token = getTokenMetodoPago( $row["id_usuario"] );
			
			$monto = $row["monto"];
			
			$referencia = "".date("Y-m-d H:i:s", time());
			$referencia = str_ireplace(" ", "", $referencia);
			$referencia = str_ireplace("-", "", $referencia);
			$referencia = str_ireplace(":", "", $referencia);
			$referencia .= $row["id_operacion"];

			$res = pagar($referencia, $token, $monto, $row["tipo"], $row["id_operacion"]); // $referencia, $token, $monto, $tipo, $idMovimiento=-1
			
			if ( $res==true )
			{
				$mysqli->query("update f_dc_pagos_programados set status=3 where id_pago_programado=".$row["id_pago_programado"]);
			}
			
			sleep(5);	// metemos una pausa entre cada procesamiento de un pago para no saturar al dbms en caso de tener muchos pagos programados (pueden ser miles y no está del todo optimizado)
		}
	}
	else
	{
	}


	// desactivar las pensiones que ya cumplieron su plazo
	$q = "SELECT * FROM db_veepe.f_dc_pensionados pensionados inner join f_dc_pensiones pensiones on pensionados.id_pension = pensiones.id_pension where pensionados.status=1";
	$rs = $mysqli->query($q);
	if ( $rs!=false )
	{
		while($row = $rs->fetch_assoc())
		{
			$fechaFinal = new DateTime($row["fecha_contratacion"]);
			$fechaFinal->add(new DateInterval("P".$row["periodo"]."M"));
			$ahora = new DateTime('now');

			if ( $fechaFinal < $ahora )
			{
				$mysqli->query("update f_dc_pensionados set status=-1 where id_pensionado=".$row["id_pensionado"]);
			}
		}
	}



?>