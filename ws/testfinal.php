<?php
	$currdir=getcwd();
	chdir("/var/www/html/ws/");
	include_once "php/funciones2.php";

	global $mysqli;

	// desactivar las pensiones que ya cumplieron su plazo
	$q = "SELECT * FROM db_veepe.f_dc_pensionados pensionados inner join f_dc_pensiones pensiones on pensionados.id_pension = pensiones.id_pension where pensionados.status=1";
	$rs = $mysqli->query($q);
	if ( $rs!=false )
	{
		while($row = $rs->fetch_assoc())
		{
			$fechaFinal = DateTime::createFromFormat('Y-m-d', $row["fecha_contratacion"]);
			$fechaFinal->add(new DateInterval("P".$row["periodo"]."M"));
			$ahora = new DateTime('now');
			echo $fechaFinal;
			echo "<br>";
			echo $ahora;

			if ( $fechaFinal < $ahora )
			{
				$mysqli->query("update f_dc_pensionados set status=-1 where id_pensionado=".$row["id_pensionado"]);
			}
		}
	}
?>