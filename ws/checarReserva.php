<?php
	// el argumento es el id de la operación
	include_once "php/funciones.php";
	global $mysqli;
	$idOperacion = $argv[1];

	// checamos si hay operaciones
	if ( $idOperacion!=null )
	{
		// checamos si la reservación ya expiró
		$q = 'select id_usuario from f_dc_operaciones where id_operacion='.$idOperacion.' and status=1';
		$rs = $mysqli->query($q);
		
		if ( $rs!=false )
		{
			if ( $rs->num_rows > 0 )
			{
				$row = $rs->fetch_assoc();
				
				// aun existe -> ya expiró
				// eliminar el registro1
				$q2 = 'update f_dc_operaciones set status=-1 where id_operacion='.$idOperacion;
				$mysqli->query($q2);
				
				// todo: notificar al usuario
				enviarNotificacion($row["id_usuario"], "Tu reserva de estacionamiento expiró.");	// el mensaje debe contener las palabras "expiró" y "reserva" para que sea identificado por la app
			}
			else
			{
				// ya fue eliminado u ocupado. no hacer nada mas
			}
		}
	}

