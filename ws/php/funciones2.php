<?php

include_once "settings.php";

// _____________________________________________________________________________________________________ CONFIGURACIÓN
// !Configuración
global $mysqli;
$mysqli = mysqli_connect(DB_URL, DB_USER, DB_PASS, DB_SCHEMA);

if ( !$mysqli )
{
	notificarDev("Error crítico Veepe", "El módulo no se está conectando a la bd!");
	exit(0);
}

mysqli_set_charset($mysqli,'utf8');



// _____________________________________________________________________________________________________ FUNCIONES GENERALES Y MISC
// !Funciones generales y misc

/**
 * Carga el bootstrap de Drupal
 */
function loadDrupalBootstrap()
{
	$currdir=getcwd();
	chdir(ROOT_DRUPAL);
	define('DRUPAL_ROOT', getcwd());
	require_once("./includes/bootstrap.inc");
	drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
	//drupal_bootstrap(DRUPAL_BOOTSTRAP_SESSION);
	chdir($currdir);
}


/**
 * Ejecuta un método de php, a través de su nombre
 *
 * @param string $metodo El nombre del método que se quiere ejecutar
 * @param string $jsonparams Cadena json con los parámetros que se pasarán al método
 *
 * @return mixed Lo que el método invocado haya regresado, o *null* si no se pudo ejecutar el método
 */
function ejecutarMetodo($metodo, $jsonparams)
{
	if ( $jsonparams != null )
	{
		$params = json_decode($jsonparams, true);

		if ( sizeof($params)==0 )
		{
			$res = call_user_func($metodo);
		}
		else
		{
			$res = call_user_func_array($metodo, $params);
		}

		return $res;
	}
	else
	{
		return call_user_func($metodo);
	}
}


/**
 * Envía un mail usando un tenplate html.
 *
 * El template debe tener las etiquetas #TITULO#, #CUERPO# Y #FOOTER# las cuales serán
 * reemplazdas por la info que se pase al método
 *
 * @param string $to La dirección de correo
 * @param string $asunto El asunto del mail
 * @param string $titulo El título dentro del cuerpo del mail
 * @param string $cuerpo El cuerpo de texto dentro del cuerpo del mail
 * @param string $pie El texto del footer
 *
 * @return bool *true* si se pudo enviar el mail, *false* en caso contrario
 */
function enviarMailConFormato($to, $asunto, $titulo, $cuerpo, $pie="")
{
	// obtenemos el formato de correo
	if ( strpos($_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'], 'ws') == false )
	{
		// estamos invocando desde drupal
		$html = file_get_contents("plantillas/mail.html");
	}
	else
	{
		$html = file_get_contents("../plantillas/mail.html");
	}

	// sustituimos valores
	$html = str_ireplace("#TITULO#", $titulo, $html);
	$html = str_ireplace("#CUERPO#", $cuerpo, $html);
	$html = str_ireplace("#FOOTER#", $pie, $html);

	$html = str_ireplace("á", "&aacute;", $html);
	$html = str_ireplace("é", "&eacute;", $html);
	$html = str_ireplace("í", "&iacute;", $html);
	$html = str_ireplace("ó", "&oacute;", $html);
	$html = str_ireplace("ú", "&uacute;", $html);
	$html = str_ireplace("Á", "&Aacute;", $html);
	$html = str_ireplace("É", "&Eacute;", $html);
	$html = str_ireplace("Í", "&Iacute;", $html);
	$html = str_ireplace("Ó", "&Oacute;", $html);
	$html = str_ireplace("Ú", "&Uacute;", $html);
	$html = str_ireplace("Ñ", "&Ntilde;", $html);
	$html = str_ireplace("ñ", "&ntilde;", $html);

	/*$headers = "From: " . strip_tags("info@veepe.mx") . "\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=UTF-8\r\n";*/

	// enviamos el mail
	//return mail($to, $asunto, $html, $headers);
	require 'PHPMailer/PHPMailerAutoload.php';

	$mail = new PHPMailer();
	//$mail->SMTPDebug = 3;
	$mail->isSMTP();
	$mail->Host= 'email-smtp.us-west-2.amazonaws.com'; // Amazon SES
	$mail->SMTPAuth = true; // SMTP authentication

	$mail->Username = "AKIAJUQG4GE5PN47VWZQ";  // SMTP  Username
	$mail->Password = "AphTRxB+QSMu6Idn/UdGsddMmGigP+Hd5oqMDUvf/FBa";  // SMTP Password
	$mail->CharSet = "UTF-8";

	$mail->SMTPSecure = 'tls';
	$mail->Port = 25; //465;  // SMTP Port
	$from = "info@veepe.mx";
	$mail->SetFrom($from, 'Veepe');
	$address = $to;
	$mail->AddAddress($address, $to);

	$mail->isHTML(true);
	$mail->Subject = $asunto;
	$mail->Body = $html;

	if(!$mail->send())
		return false;
	else
		return true;
}


/**
 * Envía un mail de notificación a una cuenta de desarrollo
 *
 * @param string $titulo El asunto del mail
 * @param string $mensaje El cuerpo del mail
 */
function notificarDev($titulo, $mensaje)
{
	@mail("gabriel@funktionell.com.mx", "ws2:".$titulo, $mensaje);
}


// ___________________________________________________________________ Usuario
// !Usuario

/**
 * Da de alta un usuario
 *
 * @param string $u El nombre de usuario (puede ser un email)
 * @param string $p La contraseña NO encriptada
 *
 * @return int 1 si todo salió bien, 0 si el usuario ya existe, 3 en caso de otro error
 */
function altaUsuario($u=null, $p=null)
{
	loadDrupalBootstrap();

	$res = 3;
	global $mensaje;
	
	if ($u==null || $p==null)
	{
		watchdog_exception("altaUsuario::Algún parámetro es nulo: u: ".$u.", p: ".$p, new Exception(""));
		return 3;
	}

	// primero checamos si ya existe
	if ( db_query("select count(*) from users where name = :nu", array(":nu"=>$u))->fetchField() > 0 )
	{
		// el usuario ya existe
		$res = 0;
	}
	else
	{
		$transaction = db_transaction();
		try
		{
			$roles[8] = "usuario";
	
			$fields = array(
				"name" => $u,
				"mail" => $u,
				"pass" => $p,
				"status" => 1,
				"init" => $u,
				"roles" => $roles
			);
	
			// the first parameter is left black so a new user is created
			$account = user_save("", $fields);
	
			// Creamos su registro en la base de datos
			$token = substr(md5(microtime()),rand(0,26),10);
			db_insert('f_dc_perfilusuario')->fields(array(
				"uid"				=> $account->uid,
				"nombre_usuario"	=> $u,
				"email" 			=> $u,
				"token"				=> $token,
				"status"			=> -1
			))->execute();
	
			if (!enviarMailConFormato($u, "Bienvenid@ a Veepe", "Bienvenid@ a Veepe", "Tu cuenta ha sido creada y sólo falta que termines tu registro visitando la siguiente liga y completando el formulario:<br><a href=\"http://veepe.mx/uf_confirmar?t=".$token."\">http://veepe.mx/uf_confirmar?t=".$token."</a>", ""))
			{
				watchdog_exception("altaUsuario::Error al enviar el mail usuario ".$u, new Exception(""));
			}

			$res = 1;
		}
		catch (Exception $e)
		{
			$transaction->rollback();
			watchdog_exception("Excepción altaUsuario", $e);
		}
	}

	return $res;
}


// regresa 1=todo bien, 0=ya existe->loggear, 3=otro error, 5=existe pero no es el login (esto nunca debería pasar)
/**
 * Da de alta un usuario a través de facebook
 *
 * @param string $u El nombre de usuario (puede ser un email)
 * @param string $p La contraseña NO encriptada
 * @param string $nombre El nombre del usuario
 * @param string $apellidoPaterno El apellido paterno
 * @param string $apellidoMaterno El apellido materno
 *
 * @return int 1 si todo salió bien, 0 si el usuario ya existe, 3 en caso de otro error
 */
function altaUsuarioFB($u=null, $p=null, $nombre="", $apellidoPaterno="", $apellidoMaterno="", $token, $plataforma)
{
	global $mensaje;
	$res = array("respuesta"=>3, "usuario"=>null);
	
	if ($u==null || $p==null)
	{
		watchdog_exception("altaUsuarioFB::Algún parámetro es nulo: u: ".$u.", p: ".$p, new Exception(""));
		return array("respuesta"=>3, "usuario"=>null);
	}

	loadDrupalBootstrap();
	
	$row = db_query("select count(*) as c, ur.rid as rid from users u inner join users_roles ur on u.uid=ur.uid where name = :nu group by ur.rid", array(":nu"=>$u))->fetchAssoc();
	if ( $row["c"] > 0 )
	{
		// ya existe, pero es un usuario válido
		if ( $row["rid"] == ROL_USUARIOFINAL )
		{
			if ($uid = user_authenticate($u, $p))
			{
				$user_obj = user_load_by_name($u);
				$veepeId = db_query("select veepeid from f_dc_perfilusuario where uid=".$user_obj->uid)->fetchField();
				registerPushToken($user_obj->uid, $token, $plataforma);
				$res = array("respuesta"=>0, "usuario"=>getUsuario($veepeId));
			}
			else
			{
				$mensaje = "Ya existe una cuenta con el mismo correo. Por favor usa la cuenta existente para iniciar sesión.";
			}
		}
		else
		{
			$mensaje = "El correo que estás intentando registrar ya está en uso en una cuenta administrativa. Por favor usa otro correo.";
		}
	}
	else
	{
		// no existe -> crear
		$v = generarVeepeId($nombre, $apellidoPaterno, $apellidoMaterno);

		$transaction = db_transaction();
		try
		{
			// insertamos en users
			$new_user = array(
				'name' => $u,
				'pass' => $p, // note: do not md5 the password
				'mail' => $u,
				'status' => 1,
				'init' => $u,
				'roles' => array(
					DRUPAL_AUTHENTICATED_RID => 'authenticated user',
					8 => 'usuario',
				)
			);
			$account = user_save('', $new_user);
			$idUsuario = $account->uid;
			$nip = rand(1000, 9000);		// nos reservamos algunos nips para usos especiales

			// insertamos en perfil_usuario
			$n1 = db_insert("f_dc_perfilusuario")
				->fields(array(
					"uid" 				=> $idUsuario,
					"veepeid" 			=> $v,
					"nombre_usuario" 	=> $u,
					"nombre" 			=> ucfirst($nombre),
					"apellido_paterno"	=> ucfirst($apellidoPaterno),
					"apellido_materno"	=> ucfirst($apellidoMaterno),
					"nip" 				=> $nip."",
					"email" 			=> $u,
					"status" 			=> 1,
					"tipo"				=> 3
				))
				->execute();

			notificarDev("Copia del mail de registro con facebook: ".$u, "Tu cuenta ha sido creada con tus datos de Facebook. <br><br>Te hemos asignado el siguiente NIP: ".$nip.". Te recomendamos que lo cambies lo antes posible.");
			if (!enviarMailConFormato($u, "Bienvenid@ a Veepe", "Bienvenid@ a Veepe", "Tu cuenta ha sido creada con tus datos de Facebook. <br><br>Te hemos asignado el siguiente NIP: ".$nip.". Te recomendamos que lo cambies lo antes posible.", ""))
			{
				watchdog_exception('Error al enviar el mail. usuario '.$u, new Exception(''));
			}
			
			registerPushToken($idUsuario, $token, $plataforma);

			$res = array("respuesta"=>1, "usuario"=>getUsuarioById($idUsuario));
		}
		catch (Exception $e)
		{
			$transaction->rollback();
			watchdog_exception('Funk', $e);
			$mensaje = "".$e;
			$res = array("respuesta"=>3, "usuario"=>null);
		}
	}

	return $res;
}


function getHistorial($veepeId)
{
	global $mysqli;
	$res = null;
	
	$idu = getIdByVeepeId($veepeId);
	
	/*$q = 'select o.id_operacion as idOperacion, o.id_estacionamiento as idEstacionamiento, o.hora_entrada as horaEntrada, o.hora_salida as horaSalida, o.monto as Monto, '.
			'o.rating as Rating, e.nombre as nombreEstacionamiento '.
			'from f_dc_operaciones o inner join f_dc_estacionamientos e on o.id_estacionamiento=e.id_estacionamiento '.
			'where o.status=11 and id_usuario='.$idu;*/
	$q = "select o.id_operacion as idOperacion, o.id_estacionamiento as idEstacionamiento, id_usuario as idUsuario, ".
		"id_auto as idAuto, hora_entrada as FechaIngreso, hora_salida as HoraSalida, hora_estimada_entrega as HoraEstimadaEntrega, recibio as Recibio, Monto as monto, ".
		"fotoIzquierda as _fotoIzquierda, fotoDelantera as _fotoDelantera, fotoDerecha as _fotoDerecha, fotoTrasera as _fotoTrasera, ".
		"deja_lentes as dejaLentes, deja_laptop as dejaLaptop, ".
		"deja_tablet as dejaTablet, deja_celular as dejaCelular, otros_objetos as otrosObjetos, comentarios, o.status as Status, rating as Rating ".
		"from f_dc_operaciones o inner join f_dc_perfilusuario p on o.id_usuario=p.uid ".
		"where o.id_usuario = ".$idu." and o.status = ".ESTADO_ENTREGADO." order by id_operacion desc";
	$rs = $mysqli->query($q);

	if ( $rs!=false )
	{
		$res = array();
		
		while ($rowOperacion = $rs->fetch_assoc())
		{
			// iteramos cada operacion
			$auxRow = array();
			$auxRow["operacion"] = $rowOperacion;
			
			// ahora obtenemos los datos del auto
			$auxRow["auto"] = $mysqli->query("select id_automovil as idAutomovil, marca, submarca, modelo, placa, color from f_dc_automoviles where id_automovil = ".$rowOperacion["idAuto"])->fetch_assoc();

			// los datos del estacionamiento
			$auxRow["estacionamiento"] = $mysqli->query("select id_estacionamiento as idEstacionamiento, nombre, horario_servicio as horarioServicio, terminos, tarifa_hora as TarifaHora from f_dc_estacionamientos where id_estacionamiento=".$rowOperacion["idEstacionamiento"])->fetch_assoc();
			
			$res[] = $auxRow;
		}
	}
	
	return $res;
}


function getIdByVeepeId($veepeId)
{
	global $mysqli;

	$res = null;
	$q = 'select uid from f_dc_perfilusuario where veepeid="'.$veepeId.'"';
	$rs = $mysqli->query($q);

	if ( $rs!=false && $rs->num_rows > 0 )
	{
		$row = $rs->fetch_assoc();
		$res = $row["uid"];
	}
	else
		notificarDev("veepe -> Consulta mal hecha", "Hay una consulta que regresó false: \n".$q."\n".print_r($rs, true));

	return $res;
}


function getNombreById($uid)
{
	global $mysqli;
	$res = "--";

	$q = 'select concat(nombre, " ", apellido_paterno) as nombre from f_dc_perfilusuario where uid='.intval($uid);
	$rs = $mysqli->query($q);

	if ( $rs!=false )
	{
		if ( $rs->num_rows > 0 )
		{
			$row = $rs->fetch_assoc();

			$res = $row["nombre"];
		}
	}

	return $res;
}


/**
 * Obtiene el estado del usuario, es decir, en qué paso del proceso se encuentra
 *
 * @param string $veepeid El id del usuario
 * @return int $res -1->usuario no existe, 0->perfil incompleto, 1->reservado, 3->se registró entrada y se espera aceptación,
 	5->servicio activo, 7->solicitó su auto, 11->solicitud aprobada por el operador, 13->calificacion de servicio, cualquier otro->sin proceso
 */
function getStatusUsuario($veepeId)
{
	global $mysqli;
	global $mensaje;
	$res = 20;

	$uid = getIdByVeepeId($veepeId);
	
	if ( $uid == null )
	{
		// ni siquiera se encontró -> notificar app para que envie a pantalla de login
		return -1;
	}
	
	$q = 'select id_operacion, status, fecha_reserva from f_dc_operaciones where status > 0 and status < 11 and id_usuario='.$uid;
	$rs = $mysqli->query($q);

	if ( $rs!=false )
	{
		if ( $rs->num_rows > 0 )
		{
			$row = $rs->fetch_assoc();
			
			switch( $row["status"] )
			{
				case 1:
					// todo: checar si ya expiró la reserva
					$now = new DateTime();
					$fechaFin = new DateTime($row["fecha_reserva"]);
					$fechaFin->add(new DateInterval('PT30M'));

					if ( $now < $fechaFin )
					{
						$mensaje = $row["fecha_reserva"];
						$res = 1;
					}
					else
					{
						// ya expiró
						$mysqli->query('update f_dc_operaciones set status=-1 where id_operacion='.$row["id_operacion"]);
						$res = 20;
					}
					break;

				case 3:
					$res = 3;
					break;
					
				case 4:
					$res = 5;
					break;

				case 5:
					$res = 7;
					break;
					
				case 7:
					$res = 11;
					break;

				case 10:
					$res = 13;
					break;
			}
		}
	}
	
	// requiere actualizar el pushtoken?
	$q = 'select token from f_dc_pushtokens where id_usuario='.$uid;
	$rs = $mysqli->query($q);
	
	$debeActualizar = false;
	if ( $rs!=false )
	{
		while( $row = $rs->fetch_assoc() )
		{
			if ( "".$row["token"] == "" )
				$debeActualizar = true;
		}
	}
	if ( $debeActualizar )
		$mensaje .= "actualizarpush";

	return $res;
}


/**
 * Regresa información sobre si tiene o no método de pago registrado
 *
 * @param string $uid El id del usuario
 *
 * @return bool $res *true* si tiene método de pago, *false* en caso contrario
 */
function getTieneMetodoPago($uid)
{
	global $mysqli;
	$res = false;
	
	$q = 'select count(*) as c from f_r_usuario_metodopago where id_usuario='.$uid.' and status=1';
	$rs = $mysqli->query($q);
	
	if ($rs!=false)
	{
		$row = $rs->fetch_assoc();
		if ($row["c"] > 0)
			$res = true;
	}
	
	return $res;
}


/**
 * Regresa información sobre si tiene o no método de pago registrado, dada una operación
 *
 * @param string $uid El id de la operación
 *
 * @return bool $res *true* si tiene método de pago, *false* en caso contrario
 */
function getTieneMetodoPagoOperacion($idOperacion)
{
	global $mysqli;
	$uid = -1;
	
	$q = "select id_usuario from f_dc_operaciones where id_operacion=".$idOperacion;
	$rs = $mysqli->query($q);
	if ($rs != false)
	{
		$row = $rs->fetch_assoc();
		$uid = $row["id_usuario"];
	}
	
	return getTieneMetodoPago($uid);
}


function getUsuarioById($idUsuario)
{
	global $mensaje;
	$res = null;

	loadDrupalBootstrap();

	$transaction = db_transaction();
	try {
		$rs = db_query( "select p.nombre, apellido_paterno as apellidoPaterno, apellido_materno as apellidoMaterno, nombre_usuario as correo, 
				p.uid as idUsuario, veepeid as veepeId, rid as rol, celular, p.id_estacionamiento as idEstacionamiento, 
				e.nombre as nombreEstacionamiento, tipo as Tipo 
				from f_dc_perfilusuario p inner join users_roles r on p.uid=r.uid 
				left join f_dc_estacionamientos e on e.id_estacionamiento=p.id_estacionamiento
				where p.uid = :param", array(":param"=>$idUsuario) );

		if ($rs != false)
		{
			if ( $rs->rowCount() < 1 )
			{
				$mensaje .= " No se encontró el usuario -".$idUsuario."-";
				$res = null;
			}
			else
			{
				// ***** datos generales
				$res = $rs->fetchAssoc();
	
				// ***** autos
				$rs = db_query("select id_automovil as idAuto, marca as Marca, submarca as Submarca, modelo as Modelo, placa as Placa, color as Color 
						from f_dc_automoviles a inner join f_r_usuario_auto ua on a.id_automovil=ua.id_auto 
						where ua.status=1 and ua.id_usuario = :param", array(":param"=>$idUsuario) );
				if ($rs!=false)
				{
					$auxAutos = array();
					while($row = $rs->fetchAssoc())
					{
						$auxAutos[] = $row;
					}
					$res["Autos"] = $auxAutos;
				}
	
				// ***** métodos de pago
				$rs = db_query("select id_metodospago as idMetodoPago, nombre_cuenta as nombreCuenta, nombre_tarjeta as nombreTarjeta, tipo as Tipo, 
						pseudonumero as Pseudonumero, token as Token 
						from f_dc_metodospago m inner join f_r_usuario_metodopago um on m.id_metodospago=um.id_metodopago 
						where um.status=1 and um.id_usuario = :param", array(":param"=>$idUsuario) );
				if ($rs!=false)
				{
					$auxMetodosPago = array();
					while($row = $rs->fetchAssoc())
					{
						$auxMetodosPago[] = $row;
					}
					$res["MetodosPago"] = $auxMetodosPago;
				}

				// ***** pensiones
				$res["PensionesContratadas"] = getPensiones($idUsuario);
				
				// ***** tiene servicio activo?
				$ns = db_query("select count(*) from f_dc_operaciones ".
					"where status < ". ESTADO_EVALUACION . " and status >= " . ESTADO_PORAPROBAR . " and id_usuario = ".$idUsuario)->fetchField();
				$res["tieneOperacionActiva"] = $ns==0 ? false : true;

				// ***** tiene pagos atrasados?
				$res["tienePagosAtrasados"] = debePagos($idUsuario);
			}
		}
		else
		{
			$mensaje .= $q;
			$res = null;
		}
	}
	catch (Exception $e)
	{
		$transaction->rollback();
		watchdog_exception('Funk', $e);
		global $mensaje;
		$mensaje = "".$e;
	}

	return $res;
}


function getUsuario($veepeId)
{
	return getUsuarioById(getIdByVeepeId($veepeId));
}


function getUsuarioByIdOperacion($idOperacion)
{
	global $mysqli;
	global $mensaje;
	
	$res = null;
	
	$q = 'select id_usuario from f_dc_operaciones where id_operacion='.$idOperacion;
	$mensaje = $q;
	$rs = $mysqli->query($q);
	
	if ( $rs!=false && $rs->num_rows > 0 )
	{
		$row = $rs->fetch_assoc();
		
		$res = getUsuarioById($row["id_usuario"]);
	}
	
	return $res;
}


function getVeepeidById($uid)
{
	global $mysqli;
	$res = "--";

	$q = 'select veepeid from f_dc_perfilusuario where uid='.intval($uid).' and status=1';
	$rs = $mysqli->query($q);

	if ( $rs!=false )
	{
		if ( $rs->num_rows > 0 )
		{
			$row = $rs->fetch_assoc();

			$res = $row["veepeid"];
		}
	}

	return $res;
}


/**
 * Edición del nombre en el perfil de usuario
 *
 * @param String $veepeid
 * @param String $nombre
 * @param String $apellidoPaterno
 * @param String $apellidoMaterno
 *
 * @return boolean true si fue exitoso, false en caso contrario
 */
function perfilEditarNombre($veepeId, $nombre, $apellidoPaterno, $apellidoMaterno)
{
	return perfilEditarNombreById(getIdByVeepeId($veepeId), $nombre, $apellidoPaterno, $apellidoMaterno);
}


/**
 * Edición del nombre en el perfil de usuario
 *
 * @param String $veepeid
 * @param String $nombre
 * @param String $apellidoPaterno
 * @param String $apellidoMaterno
 *
 * @return boolean true si fue exitoso, false en caso contrario
 */
function perfilEditarNombreById($idUsuario, $nombre, $apellidoPaterno, $apellidoMaterno)
{
	global $mysqli;
	$res = false;

	$sql = 'update f_dc_perfilusuario SET nombre="'.$nombre.'", apellido_paterno="'.$apellidoPaterno.'", apellido_materno="'.$apellidoMaterno.'" WHERE uid="'.$idUsuario.'"';
	if ($mysqli->query($sql) != FALSE){
		$res = true;
	}

	return $res;
}


/**
 * Edición del número telefónico en el perfil de usuario
 *
 * @param String $veepeId 		uid del usuarrio a modificar
 * @param String $telefono 		Nuevo teléfono del usuario
 *
 * @return boolean $res 			Operación exitosa
 */
function perfilEditarTelefono($veepeId, $telefono)
{
	return perfilEditarTelefonoById(getIdByVeepeId($veepeId), $telefono);
}


/**
 * Edición del número telefónico en el perfil de usuario
 *
 * @param String $veepeId 		uid del usuarrio a modificar
 * @param String $telefono 		Nuevo teléfono del usuario
 *
 * @return boolean $res 			Operación exitosa
 */
function perfilEditarTelefonoById($idUsuario, $telefono)
{
	global $mysqli;
	$res = false;

	$sql = 'update f_dc_perfilusuario SET celular="' . $telefono . '" WHERE uid="' . $idUsuario . '"';
	if ($mysqli->query($sql) != FALSE){
		$res = true;
	}

	return $res;
}


/**
 * Edición de la contraseña en el perfil de usuario
 *
 * @param string $veepeId 		veepeid del usuarrio a modificar
 * @param string $c1 			Vieja contraseña
 * @param string $c2 			Nueva contraseña a registrar
 *
 * @return boolean $res 			Operación exitosa
 */
function perfilEditarPassword($veepeId, $c1, $c2)
{
	global $mensaje;
	return perfilEditarPasswordById(getIdByVeepeId($veepeId), $c1, $c2);
}


/**
 * Edición de la contraseña en el perfil de usuario
 *
 * @param string $veepeId 		veepeid del usuarrio a modificar
 * @param string $c1 			Vieja contraseña
 * @param string $c2 			Nueva contraseña a registrar
 *
 * @return boolean $res 			Operación exitosa
 */
function perfilEditarPasswordById($idUsuario, $c1, $c2)
{
	global $mysqli;
	$res = false;
	
	loadDrupalBootstrap();
	$name = db_query("select u.name from users u inner join f_dc_perfilusuario p on p.uid=u.uid where p.uid=:v", array(":v" => $idUsuario))->fetchField();
	
	if ( user_authenticate($name, $c1) )
	{
		$res = actualizarPassword($idUsuario, $c2);
		if($res)
		{
			$email = db_query("select nombre_usuario from f_dc_perfilusuario where uid=".$idUsuario)->fetchField();
			enviarMailConFormato($email, "Tu contraseña de Veepe ha cambiado", "Cambio de contraseña", "Tu contraseña fue recientemente actualizada.<br>Si no modificaste tu contraseña por favor visita la siguiente liga y resetea tu contraseña: <a href=\"http://veepe.mx/inicio_recuperar\">http://veepe.mx/inicio_recuperar</a>", "");
		}
	}

	return $res==false?false:true;
}


/**
 * Edición de la contraseña en el perfil de usuario
 *
 * @param string $veepeId 	veepeid del usuarrio a modificar
 * @param int $n1 			Vieajo nip
 * @param int $n2 			Nuevo nip
 *
 * @return boolean $res 		Operación exitosa
 */
function perfilEditarNip($veepeId, $n1, $n2)
{
	return perfilEditarNipById(getIdByVeepeId($veepeId), $n1, $n2);
}


/**
 * Edición de la contraseña en el perfil de usuario
 *
 * @param string $veepeId 	veepeid del usuarrio a modificar
 * @param int $n1 			Vieajo nip
 * @param int $n2 			Nuevo nip
 *
 * @return boolean $res 		Operación exitosa
 */
function perfilEditarNipById($idUsuario, $n1, $n2)
{
	global $mysqli;
	$res = false;

	// primero checamos si el nip viejo coincide
	$q = 'select uid from f_dc_perfilusuario where uid="'.$idUsuario.'" and nip='.$n1.' and status=1';
	$rs = $mysqli->query($q);

	if ( $rs!=false )
	{
		if ( $rs->num_rows > 0 )
		{
			// es válido -> editar
			$sql = 'update f_dc_perfilusuario SET nip=' . $n2 . ' WHERE uid="' . $idUsuario . '"';
			if ($mysqli->query($sql) != FALSE){
				$res = true;
			}
		}
	}

	return $res;
}


/**
 * Recupera la contraseña de un usuario
 *
 * @param string $email El email de la cuenta de la que se requiere recuperar
 * @return int $res La respuesta: 1=se envio mail, 0=la cuenta no existe, 3=error
 */
function recuperarContrasenha($email)
{
	global $mysqli;
	$res = 3;

	// validamos que el mail exista
	$q = "select uid from users where mail = \"".$email."\"";
	$rs = $mysqli->query($q);

	if ( $rs!=false )
	{
		if ( $rs->num_rows > 0 )
		{
			$row = $rs->fetch_assoc();
			$uid = $row["uid"];

			// enviamos el correo de recuperación
			loadDrupalBootstrap();

			$account = user_load($uid);
			// Invoke the email. It will be queued along with other system mail to be sent during cron
			_user_mail_notify('password_reset', $account);

			$res = 1;
		}
		else
			$res = 0;	// la cuenta no existe
	}

	return $res;
}


/**
 * Recupera el nip de un usuario
 *
 * @param string $veepeId El email de la cuenta de la que se requiere recuperar
 * @return int $res La respuesta: 1=se envio mail, 0=la cuenta no existe, 3=error
 */
function recuperarNip($veepeId)
{
	$res = false;
	loadDrupalBootstrap();
	
	$email = db_query("select nombre_usuario from f_dc_perfilusuario where veepeid='".$veepeId."'")->fetchField();
	
	if ( $email!=null )
	{
		// si existe -> actualizamos y enviamos el mail
		$token = substr(md5(microtime()),rand(0,26),10);

		db_update("f_dc_perfilusuario")->fields(array(
			"token_nip"	=> $token,
		))
		->condition('veepeid', $veepeId, '=')
		->execute();
		
		$res = enviarMailConFormato($email, "Recuperación de NIP", "", "Has solicitado recuperar tu NIP. Para ello, haz clic en la siguiente liga y completa el formulario:<br><a href=\"http://veepe.mx/recuperar_nip?t=".$token."\">http://veepe.mx/recuperar_nip?t=".$token."</a>", "");
	}

	return $res;
}


/**
 * Registra un token para notificaciones al usuario especificado
 *
 * @param int $idu El id del usuario
 * @param string $token El token
 * @param string $plataforma La plataforma en la que está el usuario (para saber a qué servicio enviarle el mensaje)
 *
 * @return bool *true* si se pudo registrar, *false* en caso contrario 
 */
function registerPushToken($idu, $token, $plataforma=null)
{
	global $mysqli;
	$res = false;
	
	if ($plataforma==null)
	{
		notificarDev("Error al registrar push token", "Se registró un token sin plataforma: idu:".$idu.", token:".$token.", plataforma:".$plataforma);
		return $res;
	}
	
	// lo registramos en la bd local. existe?
	$q = "select * from f_dc_pushtokens where plataforma='".strtolower($plataforma)."' and id_usuario = ".$idu;
	$rs = $mysqli->query($q);

	if ( $rs!=false )
	{
		if ( $rs->num_rows < 1 )
		{
			// el token no existe -> agregar
			$q = "insert into f_dc_pushtokens (id_usuario, token, plataforma) values (".$idu.", '".$token."', '".strtolower($plataforma)."')";
			$rs = $mysqli->query($q);
		}
		else
		{
			// ya existe -> lo actualiza
			$q = "update f_dc_pushtokens set token='".$token."' where plataforma='".strtolower($plataforma)."' and id_usuario=".$idu;
			$rs = $mysqli->query($q);
		}
		$res = true;
	}

	return $res;
}


function registerParkingPushToken($idEstacionamiento, $token)
{
	global $mysqli;
	global $mensaje;
	$res = false;
	
	// vemos si ya existe
	$q = 'select * from f_dc_pushtokens_estacionamientos where id_estacionamiento='.$idEstacionamiento;
	$rs = $mysqli->query($q);
	
	if ( $q!=false )
	{
		if ( $rs->num_rows < 1 )
		{
			// el token no existe -> agregar
			$q = "insert into f_dc_pushtokens_estacionamientos (id_estacionamiento, token) values (".$idEstacionamiento.", '".$token."')";
			$rs = $mysqli->query($q);
		}
		else
		{
			// el token ya existe -> actualizar
			$q = "update f_dc_pushtokens_estacionamientos set token='".$token."' where id_estacionamiento=".$idEstacionamiento;
			$rs = $mysqli->query($q);
		}
		$res = true;
	}
	else
	{
		$mensaje .= $q;
	}
	
	return $res;
}


/**
 * Desasocia todos los pushtokens del usuario especificado
 *
 * @param int $idu El id del usuario
 *
 * @return bool *true* si se pudo desregistrar, *false* en caso contrario 
 */
function unregisterPushToken($idu)
{
	global $mysqli;
	$res = false;
	
	$q = 'delete from f_dc_pushtokens where id_usuario='.$idu;
	$rs = $mysqli->query($q);

	if ( $rs!=false )
	{
		$res = true;
	}

	return $res;
}


/**
 * Valida un inicio de sesión, para cualquier usuario del sisteama
 *
 * @param string $u El correo del usuario
 * @param string $c La contraseña (no encriptada) del usuario
 * @param string $t El token del dispositivo
 * @param string $p La plataforma del dispositivo ("android" o "ios")

 */
function validarUsuario($u, $c, $t, $p)
{
	global $mysqli;
	global $mensaje;
	$res = null;
	
	loadDrupalBootstrap();

	try {
		$token = db_query("select token from f_dc_perfilusuario pu inner join users u on u.uid=pu.uid where pu.status=-1 and pu.nombre_usuario=:nu", 
					array(":nu" => $u))->fetchField();
		if ( $token != false )
		{
			// sí está desactivado
			global $mensaje;
			$mensaje = "desactivado";
		}
		else
		{
			// existe?
			$uid = db_query("select uid from f_dc_perfilusuario where nombre_usuario=:u", array(":u"=>$u))->fetchField();
			if ( $uid )
			{
				if(user_authenticate($u, $c))
				{
					//$veepeId = db_query("select veepeid from f_dc_perfilusuario where uid=".$uid)->fetchField();
					//$res = getUsuario($veepeId);
					$res = getUsuarioById($uid);
					
					if ( $res["idEstacionamiento"] == "-1" )
					{
						// es un usuario
						registerPushToken($uid, $t, $p);
					}
					else
					{
						// es un operador/je
						registerParkingPushToken($res["idEstacionamiento"], $t);
					}
				}
			}
			else
			{
				// noexiste
				$mensaje = "noexiste";
			}
		}
	}
	catch (Exception $e) {
		$transaction->rollback();
		watchdog_exception('Funk', $e);
		$res=null;
	}

	return $res;
}


function validarNip($uid, $nip)
{
	global $mysqli;
	$res = false;

	$q = 'select count(*) as cuenta from f_dc_perfilusuario where uid='.$uid.' and nip='.$nip;
	$rs = $mysqli->query($q);

	if ( $rs!=false )
	{
		$row = $rs->fetch_assoc();
		if ( $row["cuenta"] > 0 )
			$res = true;
	}

	return $res;
}



// ___________________________________________________________________ Autos
// !Autos

function getAutos($veepeId)
{
	global $mysqli;
	$res = null;

	try {
		$uid = getIdByVeepeId($veepeId);

		$q = "select id_automovil as idAuto, marca as Marca, submarca as Submarca, modelo as Modelo, placa as Placa, color as Color ".
				"from f_dc_automoviles a inner join f_r_usuario_auto ua on a.id_automovil=ua.id_auto ".
				"where ua.status=1 and ua.id_usuario=".$uid;
		$rs = $mysqli->query($q);

		if ($rs!=false)
		{
			$res = array();
			while($row = $rs->fetch_assoc())
			{
				$res[] = $row;
			}
		}
	} catch (Exception $e) {}

	return $res;
}

function altaAuto($veepeId, $marca, $submarca, $modelo, $placa, $color)
{
	global $mysqli;

	$res = false;

	//$mysqli->begin_transaction();
	$q = 'insert into f_dc_automoviles (marca, submarca, modelo, placa, color) values ("'.$marca.'", "'.$submarca.'", "'.$modelo.'", "'.$placa.'", "'.$color.'")';
	$mysqli->query($q);

	$idAuto = $mysqli->insert_id;
	if ( $idAuto > 0 )
	{
		$now = date("Y-m-d H:i:s", time());
		$q = 'insert into f_r_usuario_auto (id_usuario, id_auto, fecha_asignacion, status) values ('.getIdByVeepeId($veepeId).', '.$idAuto.', "'.$now.'", 1)';
		$mysqli->query($q);
		$res = true;
	}
	else
	{
		// no se pudo insertar el auto ->
		//$mysqli->rollback();
	}

	return $res;
}


function bajaAuto($idAuto)
{
	global $mysqli;
	$res = false;

	try {
		$q = "update f_r_usuario_auto set status=-1 where id_auto=".$idAuto;
		$rs = $mysqli->query($q);
		$res = true;
	} catch(Exception $e)
	{}

	return $res;
}

function cambioAuto($idAuto, $marca, $submarca, $modelo, $placa, $color)
{
	global $mysqli;
	$res = false;

	try {
		$q = 'update f_dc_automoviles set marca="'.$marca.'", submarca="'.$submarca.'", modelo="'.$modelo.'", placa="'.$placa.'", color="'.$color.'" where id_automovil='.$idAuto;
		$rs = $mysqli->query($q);
		$res = true;
	} catch(Exception $e)
	{}

	return $res;
}

// ___________________________________________________________________ Clientes
// !Clientes
/**
 * Da de alta los datos de un cliente
 *
 * @param String $nombre_empresa
 * @param String $contacto
 * @param String $email
 * @param String $puesto
 * @param String $celular
 * @param String $telefono
 * @param String $direccion
 * @return boolean $res 			Operación exitosa
 */
function altaCliente($nombre_empresa, $contacto, $email, $puesto, $celular, $telefono, $direccion )
{
	global $mysqli;
	$res = false;
	$sql = 'insert INTO f_dc_clientes (nombre_empresa, contacto, puesto, celular, telefono, direccion, email) VALUES ("'.$nombre_empresa.'", "'.$contacto.'", "'.$puesto.'", "'.$celular.'", "'.$telefono.'", "'.$direccion.'", "'.$email.'")';

	if ($mysqli->query($sql) != false){
		$res = true;
	}

	return $res;
}

/**
 * Modificación de los datos de un cliente
 *
 * @param String $nombre_empresa
 * @param String $contacto
 * @param String $email
 * @param String $puesto
 * @param String $celular
 * @param String $telefono
 * @param String $direccion
 * @return boolean $res 			Operación exitosa
 */
function cambioCliente($id_cliente, $nombre_empresa, $contacto, $email, $puesto, $celular, $telefono, $direccion )
{
	global $mysqli;
	$res = false;
	$sql = 'update f_dc_clientes SET nombre_empresa="'.$nombre_empresa.'", contacto="'.$contacto.'", email="'.$email.'", puesto="'.$puesto.'", celular="'.$celular.'", telefono="'.$telefono.'", direccion="'.$direccion.'" WHERE id_cliente=' . $id_cliente;

	if ($mysqli->query($sql) != false){
		$res = true;
	}

	return $res;
}

/**
 * Da de baja a un cliente
 *
 * @param String $id_cliente
 * @return boolean $res 			Operación exitosa
 */
function bajaCliente($id_cliente)
{
	global $mysqli;
	$res = false;
	$sql = 'delete FROM f_dc_clientes WHERE id_cliente=' . $id_cliente;

	if ($mysqli->query($sql) != false){
		$res = true;
	}

	return $res;
}

/**
 * Obtener los datos del cliente para mostrar
 *
 * @param String $id_cliente
 * @return Object $cliente 		Arreglo con los datos de todos los clientes
 */
function detalleCliente($id_cliente)
{
	global $mysqli;
	$clientes = array();
	$sql = 'select nombre_empresa AS nombreEmpresa, contacto, puesto, celular, telefono, email, direccion FROM f_dc_clientes WHERE id_cliente="'.$id_cliente.'"';

	$rs = $mysqli->query($sql);
	while($row = $rs->fetch_assoc())
	{
		$clientes = array(
			"nombreEmpresa" 	=> $row["nombreEmpresa"],
			"Contacto" 		=> $row["contacto"],
			"Puesto"		=> $row["puesto"],
			"Celular" 	=> $row["celular"],
			"Telefono" 	=> $row["telefono"],
			"Correo" 		=> $row["email"],
			"Direccion" 		=> $row["direccion"]
		);
	}

	return (object)array(
		"clientes"	=> $clientes
	);
}

/**
 * Obtener los datos de todos los clientes
 *
 * @return Object $clientes 		Arreglo con los datos de todos los clientes
 */
function getClientes()
{
	global $mysqli;
	$clientes = array();
	$sql = 'select id_cliente AS idCliente, nombre_empresa AS nombreEmpresa, contacto, puesto, telefono, email FROM f_dc_clientes';

	$rs = $mysqli->query($sql);
	while($row = $rs->fetch_assoc())
	{
		$clientes[] = array(
			"idCliente" 	=> $row["idCliente"],
			"nombreEmpresa" 	=> $row["nombreEmpresa"],
			"Contacto" 		=> $row["contacto"],
			"Puesto"		=> $row["puesto"],
			"Registro" 	=> $row["telefono"],
			"Correo" 		=> $row["email"]
		);
	}

	return (object)array(
		"clientes"	=> $clientes
	);
}

// ___________________________________________________________________ Estacionamientos
// !Estacionamientos

/**
 * Da de alta los datos de un Estacionamiento
 *
 * @param String $direccion
 * @param String $zona
 * @param String $capacidad
 * @param String $pensiones_contratadas
 * @param String $pensiones_compartidas
 * @param String $horario_servicio
 * @param String $terminos
 * @return boolean $res 			Operación exitosa
 */
function altaEstacionamiento($direccion, $zona, $capacidad, $pensiones_contratadas, $pensiones_compartidas, $horario_servicio, $terminos)
{
	global $mysqli;
	$res = false;
	$sql = 'insert into f_dc_estacionamientos (direccion, zona, capacidad, pensiones_contratadas, pensiones_compartidas, horario_servicio, terminos) VALUES ("'.$direccion.'", "'.$zona.'", "'.$capacidad.'", "'.$pensiones_contratadas.'", "'.$pensiones_compartidas.'", "'.$horario_servicio.'", "'.$terminos.'")';

	if ($mysqli->query($sql) === TRUE){
		$res = true;
	}

	return $res;
}


/**
 * Dar de baja un Estacionamiento
 *
 * @param String $id_estacionamiento
 * @return boolean $res 			Operación exitosa
 */
function bajaEstacionamiento($id_estacionamiento)
{
	global $mysqli;
	$res = false;
	$sql = 'update f_dc_estacionamientos set status=-1 WHERE id_estacionamiento=' . $id_estacionamiento;

	if ($mysqli->query($sql) != false){
		$res = true;
	}

	return $res;
}


/**
 * Modificación de los datos de un Estacionamiento
 *
 * @param String $id_estacionamiento
 * @param String $direccion
 * @param String $zona
 * @param String $capacidad
 * @param String $pensiones_contratadas
 * @param String $pensiones_compartidas
 * @param String $horario_servicio
 * @param String $terminos
 * @return boolean $res 			Operación exitosa
 */
function cambioEstacionamiento($id_estacionamiento, $direccion, $zona, $capacidad, $pensiones_contratadas, $pensiones_compartidas, $horario_servicio, $terminos)
{
	global $mysqli;
	$res = false;
	$sql = 'update f_dc_estacionamientos SET direccion="'.$direccion.'", zona="'.$zona.'", capacidad="'.$capacidad.'", pensiones_contratadas="'.$pensiones_contratadas.'", pensiones_compartidas="'.$pensiones_compartidas.'", horario_servicio="'.$horario_servicio.'", terminos="'.$terminos.'" WHERE id_estacionamiento=' . $id_estacionamiento;

	if ($mysqli->query($sql) != false){
		$res = true;
	}

	return $res;
}


/**
 * Obtener los datos de todos los Estacionamientos
 *
 * @return Array $estacionamientos 		Arreglo con los datos de todos los Estacionamientos
 */
function getEstacionamientos($idEmpresa=null)
{
	global $mysqli;
	//loadDrupalBootstrap();
	
	$estacionamientos = array();
	
	if($idEmpresa == null)
	{
		$sql = "SELECT est.id_estacionamiento AS idEstacionamiento, est.nombre, est.direccion, est.zona, est.capacidad, est.id_empresa AS idEmpresa, 
				pensiones_contratadas AS pensionesContratadas, ".
				"pensiones_compartidas AS pensionesCompartidas, horario_servicio AS horarioServicio, terminos, latitud, longitud, tarifa_hora, ".
				"tarifa_fraccion, tarifa_boleto_perdido, tarifa_pernocta, tiene_pension, empresa.nombre_empresa ".
				"FROM f_dc_estacionamientos est inner join f_dc_empresas empresa on empresa.id_empresa=est.id_empresa ".
				"where est.status=1 and empresa.status=1";
	}
	else
	{
		$sql = "SELECT est.id_estacionamiento AS idEstacionamiento, est.nombre, est.direccion, est.zona, est.capacidad, est.id_empresa AS idEmpresa, 
				pensiones_contratadas AS pensionesContratadas, ".
				"pensiones_compartidas AS pensionesCompartidas, horario_servicio AS horarioServicio, terminos, latitud, longitud, tarifa_hora, ".
				"tarifa_fraccion, tarifa_boleto_perdido, tarifa_pernocta, tiene_pension, empresa.nombre_empresa ".
				"FROM f_dc_estacionamientos est inner join f_dc_empresas empresa on empresa.id_empresa=est.id_empresa ".
				"where est.status=1 and empresa.status=1 and est.id_empresa =".$idEmpresa;
	}

	$rs = $mysqli->query($sql);
	while($row = $rs->fetch_assoc())
	{
		// obetenemos la lista de servicios
		$servicios = array();
		$rs2 = $mysqli->query("select s.id_servicio as idServicio, nombre, costo as monto from f_c_servicios s inner join f_r_estacionamientos_servicios es on s.id_servicio=es.id_servicio where s.status=1 and es.id_estacionamiento=".$row["idEstacionamiento"]);
		while($rServ = $rs2->fetch_assoc())
		{
			$servicios[] = $rServ;
		}
		
		// obetenemos la lista de pensiones(periodos, horarios y costo)
		$pensiones = array();
		$rs3 = $mysqli->query("select 
			p.periodo,
			p.id_pension as idPension,
			p.dia as costoDia,
			p.noche as costoNoche,
			p.todo_dia as costoTodoDia, 
			p.dia_compartida as costoDiaCompartida, 
			p.noche_compartida as costoNocheCompartida, 
			p.todo_dia_compartida as costoTodoDiaCompartida 
			from f_dc_pensiones p where p.status=1 and p.id_estacionamiento=".$row["idEstacionamiento"]." order by p.periodo asc");
		while($rPen = $rs3->fetch_assoc())
		{
			$pensiones[] = $rPen;
		}
		
		$estacionamientos[] = array(
			"idEstacionamiento" 	=> $row["idEstacionamiento"],
			"Nombre" 				=> $row["nombre"],
			"Direccion" 			=> $row["direccion"],
			"Zona"					=> $row["zona"],
			"Capacidad" 			=> $row["capacidad"],
			"PensionesContratadas" 	=> $row["pensionesContratadas"],
			"PensionesCompartidas"	=> $row["pensionesCompartidas"],
			"horarioServicio" 		=> $row["horarioServicio"],
			"Terminos" 				=> $row["terminos"],
			"Latitud" 				=> $row["latitud"],
			"Longitud" 				=> $row["longitud"],
			"TarifaHora" 			=> $row["tarifa_hora"],
			"TarifaFraccion" 		=> $row["tarifa_fraccion"],
			"TarifaBoletoPerdido" 	=> $row["tarifa_boleto_perdido"],
			"TarifaPernocta" 		=> $row["tarifa_pernocta"],
			"TienePension" 			=> $row["tiene_pension"]==1?true:false,
			"Operadora" 			=> $row["nombre_empresa"],
			"idEmpresa" 			=> $row["idEmpresa"],
			"Servicios"				=> $servicios,
			"Pensiones"				=> $pensiones
		);
	}

	return $estacionamientos;
}


/**
 * Obtener los datos de un estacionamiento
 *
 * @return Array $estacionamientos 		Arreglo con los datos de todos los Estacionamientos
 */
function getEstacionamiento($idEstacionamiento)
{
	$res = null;
	global $mysqli;
	
	$sql = "SELECT est.id_estacionamiento AS idEstacionamiento, est.nombre, est.direccion, est.zona, est.capacidad, pensiones_contratadas AS pensionesContratadas, ".
			"pensiones_compartidas AS pensionesCompartidas, horario_servicio AS horarioServicio, terminos, latitud, longitud, tarifa_hora, ".
			"tarifa_fraccion, tarifa_boleto_perdido, tarifa_pernocta, tiene_pension, empresa.nombre_empresa ".
			"FROM f_dc_estacionamientos est inner join f_dc_empresas empresa on empresa.id_empresa=est.id_empresa ".
			"where est.id_estacionamiento=".$idEstacionamiento." and est.status=1 and empresa.status=1";
	$rs = $mysqli->query($sql);
	
	if ($rs!=false && $rs->num_rows > 0)
	{
		$row = $rs->fetch_assoc();
		
		// obetenemos la lista de servicios
		$servicios = array();
		$rs2 = $mysqli->query("select s.id_servicio as idServicio, nombre, costo as monto from f_c_servicios s inner join f_r_estacionamientos_servicios es on s.id_servicio=es.id_servicio where s.status=1 and es.id_estacionamiento=".$row["idEstacionamiento"]);
		while($rServ = $rs2->fetch_assoc())
		{
			$servicios[] = $rServ;
		}
		
		// obetenemos la lista de pensiones(periodos, horarios y costo)
		$pensiones = array();
		$rs3 = $mysqli->query("select 
			p.periodo,
			p.id_pension as idPension,
			p.dia,
			p.noche,
			p.todo_dia as todoDia,
			p.dia_compartida as diaCompartida,
			p.noche_compartida as nocheCompartida,
			p.todo_dia_compartida as todoDiaCompartida
			from f_dc_pensiones p where p.status=1 and p.id_estacionamiento=".$row["idEstacionamiento"]);
		while($rPen = $rs3->fetch_assoc())
		{
			$pensiones[] = $rPen;
		}
		
		$res = array(
			"idEstacionamiento" 	=> $row["idEstacionamiento"],
			"Nombre" 				=> $row["nombre"],
			"Direccion" 			=> $row["direccion"],
			"Zona"					=> $row["zona"],
			"Capacidad" 			=> $row["capacidad"],
			"PensionesContratadas" 	=> $row["pensionesContratadas"],
			"PensionesCompartidas"	=> $row["pensionesCompartidas"],
			"horarioServicio" 		=> $row["horarioServicio"],
			"Terminos" 				=> $row["terminos"],
			"Latitud" 				=> $row["latitud"],
			"Longitud" 				=> $row["longitud"],
			"TarifaHora" 			=> $row["tarifa_hora"],
			"TarifaFraccion" 		=> $row["tarifa_fraccion"],
			"TarifaBoletoPerdido" 	=> $row["tarifa_boleto_perdido"],
			"TarifaPernocta" 		=> $row["tarifa_pernocta"],
			"TienePension" 			=> $row["tiene_pension"]==1?true:false,
			"Operadora" 			=> $row["nombre_empresa"],
			"Servicios"				=> $servicios,
			"Pensiones"				=> $pensiones
		);
	}

	return $res;
}


/**
 * Obtener los datos de estacionamientos que corresponden a una pensión compartida
 *
 * @return Array $estacionamientos 		Arreglo con los datos de todos los Estacionamientos
 */
function getEstPensionCompartida($idCompartida){
	
	$res = null;
	global $mysqli;
		
	$sql = "SELECT est.id_estacionamiento AS idEstacionamiento, est.nombre, est.direccion, est.zona, est.capacidad,  pensiones_contratadas AS pensionesContratadas, ".
			"pensiones_compartidas AS pensionesCompartidas, horario_servicio AS horarioServicio, terminos, latitud, longitud, tarifa_hora, ".
			"tarifa_fraccion, tarifa_boleto_perdido, tarifa_pernocta, tiene_pension, empresa.nombre_empresa ".
			"FROM f_dc_estacionamientos est inner join f_dc_pensiones pe on pe.id_estacionamiento=est.id_estacionamiento ".
			"inner join f_dc_empresas empresa on empresa.id_empresa=est.id_empresa ".
			"inner join f_dc_pensionados p on p.id_pension = pe.id_pension ".
			"where est.status=1 and pe.status=1 and p.id_compartida = '".$idCompartida."'";

	$rs = $mysqli->query($sql);
	
	$rs = $mysqli->query($sql);
	while($row = $rs->fetch_assoc())
	{
		// obetenemos la lista de servicios
		$servicios = array();
		$rs2 = $mysqli->query("select s.id_servicio as idServicio, nombre, costo as monto from f_c_servicios s inner join f_r_estacionamientos_servicios es on s.id_servicio=es.id_servicio where s.status=1 and es.id_estacionamiento=".$row["idEstacionamiento"]);
		while($rServ = $rs2->fetch_assoc())
		{
			$servicios[] = $rServ;
		}
		
		// obetenemos la lista de pensiones(periodos, horarios y costo)
		$pensiones = array();
		$rs3 = $mysqli->query("select 
			p.periodo,
			p.id_pension as idPension,
			p.dia,
			p.noche,
			p.todo_dia as todoDia,
			p.dia_compartida as diaCompartida,
			p.noche_compartida as nocheCompartida,
			p.todo_dia_compartida as todoDiaCompartida
			from f_dc_pensiones p where p.status=1 and p.id_estacionamiento=".$row["idEstacionamiento"]);
		while($rPen = $rs3->fetch_assoc())
		{
			$pensiones[] = $rPen;
		}
		
		
		
		$estacionamientos[] = array(
			"idEstacionamiento" 	=> $row["idEstacionamiento"],
			"Nombre" 				=> $row["nombre"],
			"Direccion" 			=> $row["direccion"],
			"Zona"					=> $row["zona"],
			"Capacidad" 			=> $row["capacidad"],
			"PensionesContratadas" 	=> $row["pensionesContratadas"],
			"PensionesCompartidas"	=> $row["pensionesCompartidas"],
			"horarioServicio" 		=> $row["horarioServicio"],
			"Terminos" 				=> $row["terminos"],
			"Latitud" 				=> $row["latitud"],
			"Longitud" 				=> $row["longitud"],
			"TarifaHora" 			=> $row["tarifa_hora"],
			"TarifaFraccion" 		=> $row["tarifa_fraccion"],
			"TarifaBoletoPerdido" 	=> $row["tarifa_boleto_perdido"],
			"TarifaPernocta" 		=> $row["tarifa_pernocta"],
			"TienePension" 			=> $row["tiene_pension"]==1?true:false,
			"Operadora" 			=> $row["nombre_empresa"],
			"Servicios"				=> $servicios,
			"Pensiones"				=> $pensiones
		);
	}


	return $estacionamientos;
}


/**
 * Obtener una lista de los operadores de un estacionamiento dado
 *
 * @return StdClass[] $operadores. Arreglo de operadores con la sig esturctura: idUsuario, nombre, turno, faltas
 */
function getOperadores($idEstacionamiento)
{
	global $mysqli;
	$res = Array();

	$fechaActual = date("Y-m-d");
	$q = 'select p.uid as idUsuario, concat(nombre, " ", apellido_paterno, " ", apellido_materno) as nombre, a.turno, a.observaciones as observaciones '.
		'from f_dc_perfilusuario p inner join users_roles ur on p.uid=ur.uid '.
		'left join f_r_asistencia a on a.id_usuario=p.uid '.
		//'where a.fecha = "'.$fechaActual.'" and rid='.ROL_OPERADOR.' and p.status=1 and p.id_estacionamiento = '.$idEstacionamiento;
		'where rid='.ROL_OPERADOR.' and p.status=1 and p.id_estacionamiento = '.$idEstacionamiento;
	$rs = $mysqli->query($q);

	if ( $rs!=false )
	{
		// para cada usuario, obtenemos sus faltas
		while ($row = $rs->fetch_assoc())
		{
			$row["faltas"] = getFaltas($row["idUsuario"]);
			$row["tieneAsistencia"] = tieneAsistencia($row["idUsuario"], date("Y-m-d"));

			$res[] = $row;
		}
	}
	else
	{
		global $mensaje;
		$mensaje = "Sucedió un error al obtener la lista de operadores. ".$q;
	}

	return $res;
}


function getFaltas($idOperador)
{
	global $mysqli;
	$res = 0;

	$d1 = date("Y-m-1", time());
	$d2 = date("Y-m-d", time());
	$q = 'select count(*) as c from f_r_asistencia where fecha between "'.$d1.'" and "'.$d2.'" and asistio = 1 and id_usuario = '.$idOperador;
	$rs = $mysqli->query($q);

	if ( $rs!=false )
	{
		$row = $rs->fetch_assoc();
		$res = intval(date('d')) - intval($row["c"]);
	}

	return $res;
}


function setAsistencia($idOperador, $idEstacionamiento, $asistencia, $observaciones)
{
	global $mysqli;
	$res = false;

	$fechaActual = date("Y-m-d");
	// existe el registro?
	$q = 'select count(*)as cuenta from f_r_asistencia where fecha="'.$fechaActual.'" and id_usuario='.$idOperador;
	$rs = $mysqli->query($q);

	if ( $rs!=false )
	{
		$now = date("Y-m-d H:i:s", time());
		$row = $rs->fetch_assoc();
		if ( $row["cuenta"] > 0 )
		{
			// ya existe -> actualizar
			$q2 = 'update f_r_asistencia set asistio='.intval($asistencia).', observaciones="'.$observaciones.'", hora_registro="'.$now.'" where fecha="'.$fechaActual.'" and id_usuario='.$idOperador;
		}
		else
		{
			// no existe -> agregar
			$q2 = 'insert into f_r_asistencia (id_usuario, id_estacionamiento, fecha, hora_registro, asistio, observaciones) values '.
				'('.$idOperador.', '.$idEstacionamiento.', "'.$fechaActual.'", "'.$now.'", '.$asistencia.', "'.$observaciones.'")';
		}

		$rs2 = $mysqli->query($q2);
		if ( $rs2!=false )
		{
			$res = true;
		}
		else
		{
			global $mensaje;
			$mensaje = $q2;
		}
	}

	return $res;
}


function setAsistencias($idEstacionamiento, $datos)
{
	global $mysqli;
	$res = false;
	$fechaActual = date("Y-m-d");

	//print_r($datos);

	for($i=0; $i<count($datos); $i++)
	{
		$registro = $datos[$i];

		// existe el registro?
		$q = 'select count(*)as cuenta from f_r_asistencia where fecha="'.$fechaActual.'" and id_usuario='.$registro["idUsuario"];
		$rs = $mysqli->query($q);

		if ( $rs!=false )
		{
			$now = date("Y-m-d H:i:s", time());
			$row = $rs->fetch_assoc();
			if ( $row["cuenta"] > 0 )
			{
				// ya existe -> actualizar
				$q2 = 'update f_r_asistencia set asistio='.intval($registro["tieneAsistencia"]).', observaciones="'.$registro["observaciones"].'", hora_registro="'.$now.'", turno="'.$registro["turno"].'" where fecha="'.$fechaActual.'" and id_usuario='.$registro["idUsuario"];
			}
			else
			{
				// no existe -> agregar
				$q2 = 'insert into f_r_asistencia (id_usuario, id_estacionamiento, fecha, hora_registro, asistio, observaciones) values '.
					'('.$registro["idUsuario"].', '.$idEstacionamiento.', "'.$fechaActual.'", "'.$now.'", '.$registro["intAsistencia"].', "'.$registro["observaciones"].'")';
			}

			$rs2 = $mysqli->query($q2);
			if ( $rs2!=false )
			{
				$res = true;
			}
			else
			{
				global $mensaje;
				$mensaje = $q2;
			}
		}
	}

	return $res;
}


function tieneAsistencia($idOperador, $fecha=null)
{
	global $mysqli;
	$res = false;

	$fecha = $fecha==null ? date("Y-m-d") : $fecha;
	$q = 'select asistio from f_r_asistencia where id_usuario = '.$idOperador.' and fecha = "'.$fecha.'"';
	$rs = $mysqli->query($q);

	if ($rs!=false)
	{
		if ( $rs->num_rows > 0 )
		{
			$row = $rs->fetch_assoc();
			$res = $row["asistio"] == 1 ? true : false;
		}
		else
			$res = false;	// si no tiene registro asumimos que aún no se ha registrado, ergo falta
	}

	return $res;
}


// ___________________________________________________________________ Métodos de pago
// !Métodos de pago

/**
  * altaMetodoPago
  *
  * @return int -1=error, 0=bien, pero no tiene método de pago, 1=bien y tiene método de pago
  */
function altaMetodoPago($veepeId, $nombreCuenta, $numeroTarjeta, $mesExp, $anhoExp, $cvv, $nombreTitular, $direccionTitular, $cpTitular, $emailTitular)
{
	global $mensaje;
	global $mysqli;

	$res = false;
	$token = "";
	$tipo = "";
	$uid = getIdByVeepeId($veepeId);
	$intratoken = md5($cvv.$anhoExp.$mesExp.$numeroTarjeta);
	$now = date("Y-m-d H:i:s", time());

	loadDrupalBootstrap();

	// verificamos si esa tarjeta ya está dada de alta
	$idMetodoPago = db_query("select id_metodospago from f_dc_metodospago where intratoken = :t", array(":t"=>$intratoken))->fetchField();

	if ( $idMetodoPago!=null )	// ya está dada de alta
	{
		// si no está asociada, la asociamos
		$n = db_query('select count(*) from f_r_usuario_metodopago where id_usuario='.$uid.' and id_metodopago='.$idMetodoPago)->fetchField();
		if ($n < 1)
		{
			db_insert('f_r_usuario_metodopago')->fields(array(
				"id_usuario"			=> $uid,
				"id_metodopago"			=> $idMetodoPago,
				"nombre_cuenta"			=> $nombreCuenta,
				"fecha_asignacion"		=> $now,
				"status"				=> 1,
			))->execute();
		}
		else
		{
			// si ya estaba asociada, sólo nos aseguramos de que esté activa
			db_update("f_r_usuario_metodopago")
				->fields(array(
					"nombre_cuenta"			=> $nombreCuenta,
					"status"				=> 1
				))
				->condition('id_usuario', $uid, '=')
				->condition('id_metodopago', $idMetodoPago, '=')
				->execute();
		}
		
		$res = true;
	}
	else	// no está dada de alta
	{
		// lo registramos con banwire
		// -------------------------------------------------------------------------
		// solicitud a banwire para agregar la tarjeta
		$fields = array('method'		=> 'add', 
						'user'			=> BANWIRE_USER, 
						'number'		=> $numeroTarjeta, 
						'exp_month'		=> $mesExp, 
						'exp_year'		=> $anhoExp, 
						'cvv'			=> $cvv, 
						'name'			=> $nombreTitular, 
						'address'		=> $direccionTitular, 
						'postal_code'	=> $cpTitular, 
						'email'			=> $emailTitular);
		//$mensaje .= print_r($fields, true);
	
		// process data
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, URL_PAGO."?action=card");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_ENCODING, "");
		curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			"cache-control: no-cache",
			"content-type: application/x-www-form-urlencoded",
		));
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	
		// exceute!
		$result = curl_exec($ch);
		$error = curl_error($ch);
		curl_close($ch);
		
		if ($error)
		{
			$res = false;
			$mensaje = "Lo sentimos, no se pudo conectar con la plataforma de pago.";
			@notificarDev("Banwire no contestó", "No se pudo acceder a Banwire!: ".$error);
		}
		else
		{
			try {
				$response = json_decode($result);
				
				if ( isset($response->error) )
				{
					// banwire contestó con un error
					$res = false;
					$mensaje = "La plataforma de pago indica el siguiente error: ".$response->error;
					@notificarDev("Banwire contestó con un error", "Banwire contestó con un error: ".$response->error);
				}
				else
				{
					if ( $response->result == true )
					{
						// se agregó!
						$token = $response->token;
						$tipo = $response->card->type;
					}
				}
			} catch (Exception $e) {
				// sucedió un error -> notificar porque es un error importante porque la respuesta no era json
				notificarDev("Banwire contestó con algo que no es json", "Banwire contestó con algo que no es json: ".$r);
				$mensaje = "La plataforma de pago indica el siguiente error: ".$e;
				//$mensaje = "algo tronó: ".$e;
			}
			// -------------------------------------------------------------------------
			
			if ( $token != "" )
			{
				$pseudo = "**** **** **** ".substr($numeroTarjeta, -4);

				$q = 'insert into f_dc_metodospago (nombre_tarjeta, tipo, pseudonumero, token, intratoken) values ("'.$nombreTitular.'", "'.$tipo.'", "'.$pseudo.'", "'.$token.'", "'.$intratoken.'")';
				$rs = $mysqli->query($q);
			
				if ( $rs!=false )
				{
					$nuevoId = $mysqli->insert_id;
					$q = 'insert into f_r_usuario_metodopago (id_usuario, id_metodopago, nombre_cuenta, fecha_asignacion, status) values ('.$uid.', '.$nuevoId.', "'.$nombreCuenta.'", "'.$now.'", 1)';
					$rs = $mysqli->query($q);
			
					if ( $rs!=false )
						$res = true;
				}
			}
			else
			{
				notificarDev("Banwire no regresó error, pero no hay token", "Banwire no regresó error, pero no hay token:");
			}
		}

	} // fin del else si es local o con banwire
	
	
	
	//-----------Checamos si tiene pago pendientes--------------------
	
	if($res){
	
		$pagosPendientes = db_query("select * from f_dc_pagos_programados where id_usuario =".$uid. " and status = 1 and DATE(fecha_vencimiento) <= CURDATE()")->fetchAll();

		if(count($pagosPendientes) > 0)
		{

			if( $token == null ){
				$token = getTokenMetodoPago( $uid );
			}
			
			if ($token!=null)
			{
				
				foreach($pagosPendientes as $pagos){

					if( $pagos->tipo == 1)
					{
						$operacion = db_query("select * from f_dc_operaciones where id_operacion=".$pagos->id_operacion)->fetchAssoc();
						$referencia = "".$operacion["hora_entrada"];
						$referencia = str_ireplace(" ", "", $referencia);
						$referencia = str_ireplace("-", "", $referencia);
						$referencia = str_ireplace(":", "", $referencia);
						$referencia .= $pagos->id_operacion;
					}
					else if( $pagos->tipo == 3)//si es pensión
					{
						$fechaContratacion = db_query("select fecha_contratacion from f_dc_pensionados where id_pensionado=".$pagos->id_operacion)->fetchField();
						$referencia = "".$fechaContratacion;
						$referencia = str_ireplace(" ", "", $referencia);
						$referencia = str_ireplace("-", "", $referencia);
						$referencia = str_ireplace(":", "", $referencia);
						$referencia .= $pagos->id_operacion;
					}

					$costo = $pagos->monto;
					$idOperacion = $pagos->id_operacion;
					$tipo = $pagos->tipo;

					$r = pagar($referencia, $token, $costo, $tipo, $idOperacion); // $referencia, $token, $monto, $tipo, $idMovimiento=-1	

					if ( $r )
					{
						$mysqli->query("update f_dc_pagos_programados set status=3 where id_pago_programado=".$pagos->id_pago_programado);
					}

				}

			}
			else
			{
				// TODO: error crítico! llegamos aquí y no debimos llegar sin tener un token de pago
				$mensaje = "No pudimos hacer el pago porque no existe un método de pago asociado.";
			}
		}
	}
	
	//--------------------------------------------------------------

	return $res;
}


function bajaMetodoPago($idMetodo, $idUsuario)
{
	global $mysqli;
	$res = false;

	try {
		$q = "update f_r_usuario_metodopago set status=-1 where id_metodopago=".$idMetodo." and id_usuario=".$idUsuario;
		$rs = $mysqli->query($q);
		$res = true;
	} catch (Exception $e) {}

	return $res;
}

/* **********************************
	*
	*	ESTE MÉTODO NO SE UTILIZARÁ POR CUESTIONES DE SEGURIDAD
	*
	*********************************** */
/*function cambioMetodoPago($idMetodo, $nombreCuenta, $numeroTarjeta, $mesExp, $anhoExp, $cvv, $nombreTitular, $direccionTitular, $cpTitular, $emailTitular)
{
	global $mysqli;
	global $mensaje;
	$res = false;
	
	// primero checamos si ya la tenemos tokenizada
	$q = 'select count(*) from f_dc_metodospago where pseudonumero = "'.$numeroTarjeta.'"';
	$rs = $mysqli->query($q);
	
	if ( $rs!=false )
	{
		if ( $rs->num_rows > 0 )
		{
			// ya existe
			$row = $rs->fetch_assoc();
			// actualizamos el método de pago con la info del ya existente
		}
		else
		{
			// es nueva
		}
	}

	// 2do: actualizar con banwire
	// ______________________________________________________________________________________________
	$fields = array('method'		=> 'add', 
					'user'			=> 'veepe', 
					'number'		=> $numeroTarjeta, 
					'exp_month'		=> $mesExp, 
					'exp_year'		=> $anhoExp, 
					'cvv'			=> $cvv, 
					'name'			=> $nombreTitular, 
					'address'		=> $direccionTitular, 
					'postal_code'	=> $cpTitular, 
					'email'			=> $emailTitular);
	// process data
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://cr.banwire.com/?action=card");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	// exceute!
	$result = curl_exec($ch);
	curl_close($ch);
	
	// did it work?
	try {
		$mensaje .= $result;
		$response = json_decode($result);
		
		if ( isset($response->error) )
		{
			// banwire contestó con un error
		}
		else
		{
			if ( $response->result == true )
			{
				// se agregó!
				$token = $response->token;
				$tipo = $response->card->type;

				$pseudo = "**** **** **** ".substr($numeroTarjeta, -4);
				try {
					$q = 'update f_dc_metodospago set nombre_cuenta="'.$nombreCuenta.'", pseudonumero="'.$pseudo.'", tipo="'.$tipo.'", token="'.$token.'" where id_metodospago='.$idMetodo;
					$rs = $mysqli->query($q);
					$res = true;
				} catch (Exception $e) {}
			}
		}
	} catch (Exception $e) {
		// 2do: sucedió un error -> notificar porque es un error importante porque la respuesta no era json
		$mensaje = "algo tronó: ".$e;
	}
	// ______________________________________________________________________________________________


	return $res;
}*/


function getTokenMetodopago($idu)
{
	global $mysqli;
	$res = null;

	$q = 'select token from f_dc_metodospago mp inner join f_r_usuario_metodopago ump on mp.id_metodospago = ump.id_metodopago where ump.status=1 and id_usuario = '.$idu.' limit 1';
	$rs = $mysqli->query($q);
	if ( $rs!=false )
	{
		$row = $rs->fetch_assoc();
		$res = $row["token"];
	}
	
	return $res;
}




// ___________________________________________________________________ Operacion
// !Operación

function aceptarSolicitudSalida($idOperacion, $tiempo)
{
	$res = false;
	loadDrupalBootstrap();

	//$transaction = db_transaction();
	try {
		// ahora hacemos el update (esto sólo funciona cuando te pasas una hora, pero debe ser más que suficiente)
		$hora = intval(date("H"));
		$min = intval(date("i")+$tiempo);
		if ($min > 60)
		{
			$hora++;
			$min -= 60;
		}
		
		$now = date("Y-m-d H:i:s", mktime($hora, $min, date("s"), date("m"), date("d"), date("Y")) );
		
		db_update("f_dc_operaciones")
			->fields(array(
				"status"				=> ESTADO_ENSALIDA,
				"hora_estimada_entrega"	=> $now,
				"tiempo_entrega"		=> intval($tiempo)
			))
			->condition('id_operacion', $idOperacion, '=')
			->execute();
		
		// notificamos al usuario
		$uid = db_query("select id_usuario from f_dc_operaciones where id_operacion=".$idOperacion)->fetchField();
		enviarNotificacion($uid, "Tu auto será entregado en ".$tiempo." minutos.");

		// temporizamos, ie agregamos un registro temporal para revisar el envío de la notificación a los 2 minutos y del reingreso
		exec('echo "php /var/www/html/ws/checarTiempo.php '.$idOperacion.'" | at now + '.($tiempo-2).' minutes');
		
		$res = true;
	}
	catch (Exception $e)
	{
		//$transaction->rollback();
		watchdog_exception('Funk', $e);
		notificarDev("Excepción al aceptar la solicitud", "Excepción: ".$e);
		$res = false;
	}

	return $res;
}


// todo: este método puede queda rmuuuuucho mejor si se usa el id de la operación en vez de las placas
function aceptarSolicitudSalidaPlaca($placa, $tiempo)
{
	global $mysqli;

	$res = false;

	try {
		// obtenemos el id del auto con esas placas
		$q = "select id_automovil from f_dc_automoviles where placa = \"".$placa."\"";
		$rs = $mysqli->query($q);
		if ( $rs!=false )
		{
			if ( $rs->num_rows > 0 )
			{
				$row = $rs->fetch_assoc();
				$idAuto = $row["id_automovil"];

				// ahora hacemos el update (esto sólo funciona cuando te pasas una hora, pero debe ser más que suficiente)
				$hora = intval(date("H"));
				$min = intval(date("i")+$tiempo);
				if ($min > 60)
				{
					$hora++;
					$min -= 60;
				}
				
				$now = date("Y-m-d H:i:s", mktime($hora, $min, date("s"), date("m"), date("d"), date("Y")) );
				$q = 'update f_dc_operaciones set status='.ESTADO_ENSALIDA.', hora_estimada_entrega="'.$now.'", tiempo_entrega='.intval($tiempo).' where status='.ESTADO_SOLICITADO.' and id_auto='.$idAuto.' order by id_operacion desc limit 1';
				$mysqli->query($q);
				
				// notificamos al usuario
				$q = "select id_usuario from f_r_usuario_auto where id_auto=".$idAuto;
				$rs = $mysqli->query($q);
				if ( $rs->num_rows > 0 )
				{
					$row = $rs->fetch_assoc();
					enviarNotificacion($row["id_usuario"], "Tu auto será entregado en ".$tiempo." minutos.");
					
					// temporizamos, ie agregamos un registro temporal para revisar el envío de la notificación a los 2 minutos y del reingreso
					$q = 'select id_operacion from f_dc_operaciones where id_auto='.$idAuto.' order by id_operacion desc limit 1';
					$rs = $mysqli->query($q);
					if ( $rs!=false )
					{
						$row = $rs->fetch_assoc();
						exec('echo "php /var/www/html/ws/checarTiempo.php '.$row['id_operacion'].'" | at now + '.($tiempo-2).' minutes');
					}
				}

				$res = true;
			}
			else
				notificarDev("a", "bb ".$q);
		}
		else
			notificarDev("ccc", "d ".$q);

	} catch (Exception $e) {
		notificarDev("Excepción al aceptar la solicitud", "Excepción: ".$e);
	}

	return $res;
}


/**
  * aceptarTicket - Acepta un ticket y da un recordatorio de agregar el método de pago, si no existe.
  *
  * @param $idOperacion El id de la operación que se está acepando
  *
  * @return int -1=error, 0=bien, pero no tiene método de pago, 1=bien y tiene método de pago
  */
function aceptarTicket($idOperacion)
{
	global $mysqli;
	$tieneMPago = false;
	$res = -1;

	$q = "select id_usuario from f_dc_operaciones where id_operacion=".$idOperacion;
	$rs = $mysqli->query($q);
	if ( $rs!=false )
	{
		$row = $rs->fetch_assoc();
		$tieneMetodoPago = getTieneMetodoPago($row["id_usuario"]);
	}
	
	$q = "update f_dc_operaciones set status=".ESTADO_INGRESADO." where id_operacion=".$idOperacion;
	$rs = $mysqli->query($q);
	
	if ( $rs != false )
	{
		if ( $tieneMetodoPago )
			$res = 1;
		else
			$res = 0;
	}
	
	return $res;
}


/**
  * altaOperacion - Da de alta una operación (incluidas fotos)
  *
  * @param $_POST Contiene los parámetros de la operación
  * @param $_FILES Contiene las fotos
  *
  * @return int El id de la operación creada, o -1 si sucedió un error
  */
function altaOperacion()
{
	global $mysqli;
	global $mensaje;
	$res = -1;

	$idEstacionamiento = $_POST['idEstacionamiento'];
	$veepeId = $_POST['veepeId'];
	$idAuto = $_POST['idAuto'];
	$recibio = $_POST['recibio'];
	$idUsuario = getIdByVeepeId($veepeId);
	$dejaLentes = strtolower($_POST["dejaLentes"]) == "true" ? 1 : 0;
	$dejaLaptop = strtolower($_POST["dejaLaptop"]) == "true" ? 1 : 0;
	$dejaTablet = strtolower($_POST["dejaTablet"]) == "true" ? 1 : 0;
	$dejaCelular = strtolower($_POST["dejaCelular"]) == "true" ? 1 : 0;
	$otrosObjetos = $_POST["otrosObjetos"];
	$comentarios = $_POST["comentarios"];
	$esFinal = isset($_POST["esFinal"])?$_POST["esFinal"]:"false";		// true=no se esperan fotos

	// existe ya una operación "abierta" para este veepeid
	$q = 'select count(*) as c from f_dc_operaciones where id_usuario='.$idUsuario.' and status<10 and status>=3';
	$rs = $mysqli->query($q);
	if ( $rs!=false )
	{
		$row = $rs->fetch_assoc();
		if ($row["c"] > 0)
		{
			$mensaje = "Ya existe una operación para este VeepeId";
		}
		else
		{
			$now = date("Y-m-d H:i:s", time());
			$idOperacion = -1;

			// checamos si ya existe reserva o es un registro nuevo
			$q = 'select id_operacion from f_dc_operaciones where status=1 and id_usuario='.$idUsuario.' and id_estacionamiento='.$idEstacionamiento.' order by id_operacion desc limit 1';
			$rs = $mysqli->query($q);
			if ( $rs!=false && $rs->num_rows > 0 )
			{
				// ya existe y está en este estacionamiento -> usar este mismo
				$row = $rs->fetch_assoc();
				$idOperacion = $row["id_operacion"];
				
				// y le ponemos los datos necesarios
				$mysqli->query('update f_dc_operaciones set id_auto='.$idAuto.', hora_entrada="'.$now.'", recibio='.$recibio.', deja_lentes='.$dejaLentes.
					', deja_laptop='.$dejaLaptop.', deja_tablet='.$dejaTablet.', deja_celular='.$dejaCelular.', otros_objetos="'.$otrosObjetos.
					'", comentarios="'.$comentarios.'", status='.($esFinal=="true"?ESTADO_PORAPROBAR:-2).' where id_operacion='.$idOperacion);
			}
			else
			{
				// no existe -> crear registro
				
					// primero limpiamos las otras operaciones que pudieran estar pendientes, como reservas en otros estacionamientos
				$mysqli->query('update f_dc_operaciones set status=-1 where id_usuario='.$idUsuario.' and status='.ESTADO_RESERVADO);
				
				$q = "insert into f_dc_operaciones (id_estacionamiento, id_usuario, id_auto, hora_entrada, recibio, deja_lentes, deja_laptop, deja_tablet, deja_celular, ".
					"otros_objetos, comentarios, status) values (".$idEstacionamiento.", ".$idUsuario.", ".$idAuto.", \"".$now."\", ".$recibio.", ".$dejaLentes.", ".$dejaLaptop.", ".$dejaTablet.", ".$dejaCelular.", \"".$otrosObjetos."\", \"".$comentarios."\", ".($esFinal=="true"?ESTADO_PORAPROBAR:-2).")";
				$rs = $mysqli->query($q);
				
				if ($rs!=false)
					$idOperacion = $mysqli->insert_id;
			}
			
			if ($esFinal=="true")
			{
				enviarNotificacion($idUsuario, "Tu auto ha sido recibido.");
				@exec('echo "php /var/www/html/ws/checarAceptacion.php '.$idOperacion.'" | at now + 5 minutes');
			}
			$res = $idOperacion;
		}
	}
	else
	{
		$mensaje = "Sucedió un error al ejecutar el query ".$q;
	}

	return $res;
}


function altaFotoOperacion()
{
	global $mysqli;
	global $mensaje;
	$res = false;

	$idOperacion = $_POST['idOperacion'];
	$veepeId = $_POST['veepeId'];
	$idUsuario = getIdByVeepeId($veepeId);
	$lado = ucfirst($_POST["lado"]);
	
	if ( !file_exists(ROOT."/fotos/".$veepeId."/".$idOperacion."/") )
		mkdir(ROOT."/fotos/".$veepeId."/".$idOperacion."/", 0777, true);
	@move_uploaded_file($_FILES["foto".$lado]["tmp_name"], ROOT."/fotos/".$veepeId."/".$idOperacion."/foto".$lado.".jpg");
	try {
		make_thumb(ROOT."/fotos/".$veepeId."/".$idOperacion."/foto".$lado.".jpg", ROOT."/fotos/".$veepeId."/".$idOperacion."/foto".$lado."_thumb.jpg", 128);
	} catch (Exception $e)
	{
		if ( _DEBUG_ ) $mensaje = "Sucedió un error al crear los thumbs: ".$e;
	}

	if ( isset($_POST["esFinal"]) && $_POST["esFinal"] == "true" )
	{
		$q = 'update f_dc_operaciones set '.
			'foto'.$lado.'="/fotos/'.$veepeId.'/'.$idOperacion.'/foto'.$lado.'.jpg", '.
			'status='.ESTADO_PORAPROBAR.' '.
			'where id_operacion='.$idOperacion;
		$rs = $mysqli->query($q);

		// notificamos por push al usuario
		enviarNotificacion($idUsuario, "Su auto ha sido recibido.");
		@exec('echo "php /var/www/html/ws/checarAceptacion.php '.$idOperacion.'" | at now + 5 minutes');
	}
	else
	{
		$q = 'update f_dc_operaciones set '.
			'foto'.$lado.'="/fotos/'.$veepeId.'/'.$idOperacion.'/foto'.$lado.'.jpg" '.
			'where id_operacion='.$idOperacion;
		$rs = $mysqli->query($q);
	}
	$res = true;

	return $res;
}


function completarAlta($idOperacion)
{
	global $mysqli;
	$res = false;
	
	$q = "update f_dc_operaciones set status=".ESTADO_PORAPROBAR." where id_operacion=".$idOperacion;
	$rs = $mysqli->query($q);
	
	if ( $rs!=false )
		$res = true;
	
	return $res;
}


function enviarNotificacion($uid, $texto="", $comando=null)
{
	global $mysqli;
	$res = false;

	// obtenemos el token para este uid
	$q = 'select token, plataforma from f_dc_pushtokens where id_usuario='.$uid;
	$rs = $mysqli->query($q);
	
	if ( $rs != false )
	{
		// enviamos la notificación a todos los dispositivos
		while($row = $rs->fetch_assoc())
		{
			$token = $row["token"];
			
			if ( strtolower($row["plataforma"]) == "android" )
			{
				try {
					$data = array("title"=>"Veepe", "text"=>$texto);
					$dataString = json_encode(array("to"=>$token, "data"=>$data));
					
					$ch = curl_init();
					@curl_setopt($ch, CURLOPT_URL, "https://fcm.googleapis.com/fcm/send");
					@curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
					@curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					
					@curl_setopt($ch, CURLOPT_HTTPHEADER, array(
						'Content-Type: application/json',
						'Authorization: key=AIzaSyAnAD50iFFEi5Nv3ggE24qC9MLdBk_j1ZQ',
						'Content-Length: '.strlen($dataString)));
	
					$res = @curl_exec($ch);
					@curl_close($ch);
					//$mensaje .= $res;
				} catch (Exception $e) {
					notificarDev("Error de FCM", "Sucedió un error al enviar un push al usuario ".$uid.", mensaje: ".$texto.", error: ".$e);
				}
			}
			else
			{
				try {
					require_once 'ApnsPHP/Autoload.php';
					// Instantiate a new ApnsPHP_Push object
					$push = new ApnsPHP_Push(
						ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION,
						'php/server_certificates_bundle_production.pem'
					);
					// Set the Root Certificate Autority to verify the Apple remote peer
					$push->setRootCertificationAuthority('php/entrust_root_certification_authority.pem');
					// Connect to the Apple Push Notification Service
					$push->connect();
					
					// Instantiate a new Message with a single recipient
					$message = new ApnsPHP_Message($token);
					
					$message->setCustomIdentifier($uid."-".time());
					// Set badge icon to "1"
					$message->setBadge(1);
					// Set a simple welcome text
					$message->setText($texto);
					// Play the default sound
					$message->setSound();
					// Set the expiry value to 60 seconds
					$message->setExpiry(60);
					// Add the message to the message queue
					$push->add($message);
					// Send all messages in the message queue
					$push->send();
					// Disconnect from the Apple Push Notification Service
					$push->disconnect();
					// Examine the error message container
					
					if (!empty($push->_aErrors))
						notificarDev("Error de APNs", "Sucedió un error al enviar un push al usuario ".$uid.", mensaje: ".$texto.", error:".var_dump($aErrorQueue));
				} catch(Exception $e) {
					notificarDev("Error de APNs", "Sucedió un error al enviar un push al usuario ".$uid.", mensaje: ".$texto.", error: ".$e);
				}
			}
		}
		
		$res = true;
	}
	
	return $res;
}


function enviarNotificacionEstacionamiento($idEstacionamiento, $texto)
{
	global $mysqli;
	global $mensaje;
	$res = false;
	
	$q = "select token from f_dc_pushtokens_estacionamientos where id_estacionamiento=".$idEstacionamiento;
	$rs = $mysqli->query($q);
	
	if ( $rs!=false )
	{
		try {
			require_once 'ApnsPHP/Autoload.php';
			$push = new ApnsPHP_Push(
				ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION,
				'php/server_certificates_bundle_production.pem'
			);
			// Set the Root Certificate Autority to verify the Apple remote peer
			$push->setRootCertificationAuthority('php/entrust_root_certification_authority.pem');
			// Connect to the Apple Push Notification Service
			$push->connect();
	
			while($row=$rs->fetch_assoc())
			{
				// Instantiate a new Message with a single recipient
				$message = new ApnsPHP_Message($row["token"]);
				$message->setCustomIdentifier("".time());
				// Set badge icon to "1"
				$message->setBadge(1);
				// Set a simple welcome text
				$message->setText($texto);
				// Play the default sound
				$message->setSound();
				// Set the expiry value to 60 seconds
				$message->setExpiry(60);
				// Add the message to the message queue
				$push->add($message);
			}
	
			// Send all messages in the message queue
			$push->send();
			// Disconnect from the Apple Push Notification Service
			$push->disconnect();
			
			// Examine the error message container
			if (!empty($push->_aErrors))
				notificarDev("Error de APNs", "Sucedió un error al enviar un push al estacionamiento ".$idEstacionamiento.", mensaje: ".$texto.", error:".var_dump($aErrorQueue));
		} catch(Exception $e) {
			notificarDev("Error de APNs", "Sucedió un error al enviar un push al estacionamiento: ".$idEstacionamiento.", mensaje: ".$texto);
		}
	}
	
	return $res;
}


function make_thumb($src, $dest, $desired_width) {

	/* read the source image */
	$source_image = imagecreatefromjpeg($src);
	$width = imagesx($source_image);
	$height = imagesy($source_image);
	
	/* find the "desired height" of this thumbnail, relative to the desired width  */
	$desired_height = floor($height * ($desired_width / $width));
	
	/* create a new, "virtual" image */
	$virtual_image = imagecreatetruecolor($desired_width, $desired_height);
	
	/* copy source image at a resized size */
	imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
	
	/* create the physical thumbnail image to its destination */
	imagejpeg($virtual_image, $dest);
}


function finalizarOperacion($idOperacion, $rating)
{
	global $mysqli;
	$res = false;
	
	$q = 'update f_dc_operaciones set rating = '.$rating.', status=11 where id_operacion='.$idOperacion;
	$rs = $mysqli->query($q);
	
	if ( $rs!=false )
	{
		$res = true;
	}
	
	return $res;
}


function getOperacionesPendientes($veepeId, $incluirEvaluacion=false)
{
	global $mysqli;

	$res = null;

	$idUsuario = getIdByVeepeId($veepeId);

	// información de la operación
	$q = "select o.id_operacion as idOperacion, o.id_estacionamiento as idEstacionamiento, id_usuario as idUsuario, ".
		"id_auto as idAuto, hora_entrada as FechaIngreso, hora_salida as HoraSalida, recibio as Recibio, Monto, ".
		"fotoIzquierda as _fotoIzquierda, fotoDelantera as _fotoDelantera, fotoDerecha as _fotoDerecha, fotoTrasera as _fotoTrasera, ".
		"deja_lentes as dejaLentes, deja_laptop as dejaLaptop, ".
		"deja_tablet as dejaTablet, deja_celular as dejaCelular, otros_objetos as otrosObjetos, comentarios, o.status as Status ".
		"from f_dc_operaciones o inner join f_dc_perfilusuario p on o.id_usuario=p.uid ".
		($incluirEvaluacion?"":"inner join f_r_operaciones_pendientes op on op.id_operacion=o.id_operacion ").
		"where o.id_usuario = ".$idUsuario." and o.status > 0 and o.status < ".($incluirEvaluacion?"11":"10")." limit 1";
	$rs = $mysqli->query($q);

	if ( $rs!=false )
	{
		$res = array();
		if ( $rs->num_rows > 0 )
		{
			$rowOp = $rs->fetch_assoc();
			$res["operacion"] = $rowOp;

			// obtenemos la info del carro
			$res["auto"] = $mysqli->query("select id_automovil as idAutomovil, marca, submarca, modelo, placa, color from f_dc_automoviles where id_automovil = ".$rowOp["idAuto"])->fetch_assoc();

			// obtenemos la info del estacionamiento
			$res["estacionamiento"] = $mysqli->query("select id_estacionamiento as idEstacionamiento, nombre, horario_servicio as horarioServicio, terminos, tarifa_hora as TarifaHora from f_dc_estacionamientos where id_estacionamiento=".$rowOp["idEstacionamiento"])->fetch_assoc();

			// lo eliminamos de la lista de "notificaciones"
			$mysqli->query("delete from f_r_operaciones_pendientes where id_operacion = ".$rowOp["idOperacion"]);
		}
	}

	return $res;
}


// TODO: si tiene, checar si aun es valida o ya expiró
function getReserva($idUsuario)
{
	global $mysqli;
	
	$res = null;
	
	$q = "select e.id_estacionamiento as IdEstacionamiento, e.nombre as NombreEstacionamiento, addtime(o.fecha_reserva, '00:30:00') as Hora ".
			"from f_dc_operaciones o inner join f_dc_estacionamientos e on e.id_estacionamiento=o.id_estacionamiento ".
			"where id_usuario=".$idUsuario." and o.status=1 limit 1";
	$rs = $mysqli->query($q);
	
	if ( $rs!=false )
	{
		if ( $rs->num_rows > 0 )
			$res = $rs->fetch_assoc();
		else
			notificarDev("Error al obtener la info de la reserva", "El query es ".$q);
	}
	else
		notificarDev("Error al obtener la info de la reserva", "El query es ".$q);
	
	return $res;
}


function getServicioActivo($veepeId)
{
	global $mysqli;
	global $mensaje;
	$res = null;
	
	// 2do: validar que no haya más de una operación activa para este veepeid

	$idUsuario = getIdByVeepeId($veepeId);

	// información de la operación
	$q = "select o.id_operacion as idOperacion, o.id_estacionamiento as idEstacionamiento, id_usuario as idUsuario, ".
		"id_auto as idAuto, hora_entrada as FechaIngreso, hora_salida as HoraSalida, hora_estimada_entrega as HoraEstimadaEntrega, recibio as Recibio, Monto as monto, ".
		"fotoIzquierda as _fotoIzquierda, fotoDelantera as _fotoDelantera, fotoDerecha as _fotoDerecha, fotoTrasera as _fotoTrasera, ".
		"deja_lentes as dejaLentes, deja_laptop as dejaLaptop, ".
		"deja_tablet as dejaTablet, deja_celular as dejaCelular, otros_objetos as otrosObjetos, comentarios, o.status as Status ".
		"from f_dc_operaciones o inner join f_dc_perfilusuario p on o.id_usuario=p.uid ".
		"where o.id_usuario = ".$idUsuario." and o.status >= ".ESTADO_PORAPROBAR." and o.status < ".ESTADO_ENTREGADO." limit 1";
	$rs = $mysqli->query($q);

	if ( $rs!=false )
	{
		if ( $rs->num_rows > 0 )
		{
			$res = array();

			$rowOp = $rs->fetch_assoc();
			$res["operacion"] = $rowOp;
			$res["operacion"]["tiempoAproximadoEntrega"] = getTiempoAproximadoEntrega( $rowOp["idEstacionamiento"] );
			if ( $res["operacion"]["HoraEstimadaEntrega"] == null )
				$res["operacion"]["HoraEstimadaEntrega"] = "1992-12-12 12:12:12";

			// obtenemos la info del carro
			$res["auto"] = $mysqli->query("select id_automovil as idAutomovil, marca, submarca, modelo, placa, color from f_dc_automoviles where id_automovil = ".$rowOp["idAuto"])->fetch_assoc();

			// obtenemos la info del estacionamiento
			$res["estacionamiento"] = $mysqli->query("select id_estacionamiento as idEstacionamiento, nombre, horario_servicio as horarioServicio, terminos, tarifa_hora as TarifaHora from f_dc_estacionamientos where id_estacionamiento=".$rowOp["idEstacionamiento"])->fetch_assoc();

			// lo eliminamos de la lista de "notificaciones"
			//$mysqli->query("delete from f_r_operaciones_pendientes where id_operacion = ".$rowOp["idOperacion"]);
		}
	}
	else
	{
		$mensaje = $q;
	}

	return $res;
}


function getServicioActivoById($idOperacion)
{
	global $mysqli;
	global $mensaje;
	$res = null;

	// información de la operación
	$q = "select o.id_operacion as idOperacion, o.id_estacionamiento as idEstacionamiento, id_usuario as idUsuario, ".
		"id_auto as idAuto, hora_entrada as FechaIngreso, hora_salida as HoraSalida, hora_estimada_entrega as HoraEstimadaEntrega, recibio as Recibio, Monto as monto, ".
		"fotoIzquierda as _fotoIzquierda, fotoDelantera as _fotoDelantera, fotoDerecha as _fotoDerecha, fotoTrasera as _fotoTrasera, ".
		"deja_lentes as dejaLentes, deja_laptop as dejaLaptop, ".
		"deja_tablet as dejaTablet, deja_celular as dejaCelular, otros_objetos as otrosObjetos, comentarios, o.status as Status ".
		"from f_dc_operaciones o inner join f_dc_perfilusuario p on o.id_usuario=p.uid ".
		//"where o.id_operacion = ".$idOperacion." and o.status >= ".ESTADO_PORAPROBAR." and o.status < ".ESTADO_ENTREGADO." limit 1";
		"where o.id_operacion = ".$idOperacion." limit 1";
	$mensaje = $q;
	$rs = $mysqli->query($q);

	if ( $rs!=false )
	{
		if ( $rs->num_rows > 0 )
		{
			$res = array();

			$rowOp = $rs->fetch_assoc();
			$res["operacion"] = $rowOp;
			$res["operacion"]["tiempoAproximadoEntrega"] = getTiempoAproximadoEntrega( $rowOp["idEstacionamiento"] );
			if ( $res["operacion"]["HoraEstimadaEntrega"] == null )
				$res["operacion"]["HoraEstimadaEntrega"] = "1992-12-12 12:12:12";

			// obtenemos la info del carro
			$res["auto"] = $mysqli->query("select id_automovil as idAutomovil, marca, submarca, modelo, placa, color from f_dc_automoviles where id_automovil = ".$rowOp["idAuto"])->fetch_assoc();

			// obtenemos la info del estacionamiento
			$res["estacionamiento"] = $mysqli->query("select id_estacionamiento as idEstacionamiento, nombre, horario_servicio as horarioServicio, terminos, tarifa_hora as TarifaHora from f_dc_estacionamientos where id_estacionamiento=".$rowOp["idEstacionamiento"])->fetch_assoc();

			// lo eliminamos de la lista de "notificaciones"
			$mysqli->query("delete from f_r_operaciones_pendientes where id_operacion = ".$rowOp["idOperacion"]);
		}
	}

	return $res;
}


function getDummieAutoEntregado($veepeId)
{
	global $mysqli;
	$res = array();

	$idUsuario = getIdByVeepeId($veepeId);
	$q = "select o.id_operacion as idOperacion, o.id_estacionamiento as idEstacionamiento, id_usuario as idUsuario, ".
		"id_auto as idAuto, hora_entrada as FechaIngreso, hora_salida as HoraSalida, hora_estimada_entrega as HoraEstimadaEntrega, recibio as Recibio, Monto as monto, ".
		"fotoIzquierda as _fotoIzquierda, fotoDelantera as _fotoDelantera, fotoDerecha as _fotoDerecha, fotoTrasera as _fotoTrasera, ".
		"deja_lentes as dejaLentes, deja_laptop as dejaLaptop, ".
		"deja_tablet as dejaTablet, deja_celular as dejaCelular, otros_objetos as otrosObjetos, comentarios, o.status as Status ".
		"from f_dc_operaciones o ".
		"where o.id_usuario = ".$idUsuario." and o.status = 10 limit 1";
	$rs = $mysqli->query($q);
	
	if ( $rs!=false )
	{
		if ( $rs->num_rows > 0 )
		{
			$rowOp = $rs->fetch_assoc();
			$res["operacion"] = $rowOp;
			$res["operacion"]["tiempoAproximadoEntrega"] = getTiempoAproximadoEntrega( $rowOp["idEstacionamiento"] );
			if ( $res["operacion"]["HoraEstimadaEntrega"] == null )
				$res["operacion"]["HoraEstimadaEntrega"] = "1992-12-12 12:12:12";

			// obtenemos la info del carro
			$res["auto"] = $mysqli->query("select id_automovil as idAutomovil, marca, submarca, modelo, placa, color from f_dc_automoviles where id_automovil = ".$rowOp["idAuto"])->fetch_assoc();

			// obtenemos la info del estacionamiento
			$res["estacionamiento"] = $mysqli->query("select id_estacionamiento as idEstacionamiento, nombre, horario_servicio as horarioServicio, terminos, tarifa_hora as TarifaHora from f_dc_estacionamientos where id_estacionamiento=".$rowOp["idEstacionamiento"])->fetch_assoc();
		}
	}
	
	return $res;
}


function getTiempoAproximadoEntrega($idEstacionamiento)
{
	global $mysqli;
	$res = 10;	// como default tomamos el promedio entre el máximo (15) y el mínimo (5)

	$q = 'select tiempo_entrega from f_dc_operaciones where id_estacionamiento='.$idEstacionamiento.' and tiempo_entrega > 0 order by id_operacion limit 1';
	$rs = $mysqli->query($q);

	if ( $rs!=false )
	{
		if ( $rs->num_rows > 0 )
		{
			$row = $rs->fetch_assoc();
			$res = $row["tiempo_entrega"];
		}
	}

	return $res;
}


function getOperacion($idOperacion)
{
	global $mysqli;

	$res = null;

	$q = "select o.id_operacion as idOperacion, o.id_estacionamiento as idEstacionamiento, id_usuario as idUsuario, ".
		"id_auto as idAuto, hora_entrada as FechaIngreso, hora_salida as HoraSalida, recibio as Recibio, Monto as monto, tiempo_entrega as tiempo, ".
		"fotoIzquierda as _fotoIzquierda, fotoDelantera as _fotoDelantera, fotoDerecha as _fotoDerecha, fotoTrasera as _fotoTrasera, ".
		"deja_lentes as dejaLentes, deja_laptop as dejaLaptop, ".
		"deja_tablet as dejaTablet, deja_celular as dejaCelular, otros_objetos as otrosObjetos, comentarios, o.status as Status ".
		"from f_dc_operaciones o inner join f_dc_perfilusuario p on o.id_usuario=p.uid ".
		"where o.id_operacion = ".$idOperacion." limit 1";
	$rs = $mysqli->query($q);

	if ( $rs!=false )
	{
		$res = array();
		$res["operacion"] = $rs->fetch_assoc();
	}

	return $res;
}


// 0=nip inválido, 1=todo bien, 3=no pasó el pago, 5=error general
function registrarSalida($idOperacion, $nip, $monto)
{
	global $mysqli;
	global $mensaje;
	$res = 5;

	// el nip es válido?
	$q = 'select id_usuario from f_dc_operaciones where id_operacion = '.$idOperacion;
	$rs = $mysqli->query($q);

	if ( $rs!=false )
	{
		if ( $rs->num_rows > 0 )
		{
			$row = $rs->fetch_assoc();
			$uid = $row["id_usuario"];

			if ( validarNip($uid, $nip) )
			{
				$now = date("Y-m-d H:i:s", time());
				$q = 'update f_dc_operaciones set status=10, hora_salida="'.$now.'", monto="'.$monto.'" where id_operacion='.$idOperacion;
				$rs = $mysqli->query($q);

				if ( $rs!=false )
				{
					// intentamos hacer el cobro
					if( realizarPago($idOperacion) == true )
					{
						enviarNotificacion($uid, "Su pago fue recibido. Por favor califique el servicio.");
						$res = 1;
					}
					else
					{
						enviarNotificacion($uid, "Su pago no pudo ser procesado. se intentará automáticamente más adelante. Por favor califique el servicio.");
						$res = 3;
					}
				}
			}
			else
			{
				$res = 0;
				$mensaje = "El NIP es inválido. Por favor revísalo e inténtalo nuevamente.";
			}
		}
		else
		{
			$mensaje = "Esa operación o el usuario, no existe.";
		}
	}

	return $res;
}


function reingresarAuto($idOperacion=null)
{
	$res = false;
	
	if ($idOperacion!=null)
	{
		loadDrupalBootstrap();
		
		// primero revisamos que la operación no hay asido completada previamente
		$status = db_query("select status from f_dc_operaciones where id_operacion=".$idOperacion)->fetchField();

		if ( $status < 10 )
		{
			// reingresamos
			$transaction = db_transaction();
			try {
				db_update("f_dc_operaciones")->fields(array(
					"hora_estimada_entrega"	=> NULL,
					"hora_salida"			=> NULL,
					"monto"					=> -1,
					"tiempo_entrega"		=> 0,
					"rating"				=> 5,
					"status"				=> ESTADO_INGRESADO
				))
				->condition('id_operacion', $idOperacion, '=')
				->execute();
				
				$res = true;
			}
			catch (Exception $e) {
				$transaction->rollback();
				watchdog_exception("Excepción en función reingresarAuto", $e);
			}
		}
		else
			$res = true;
	}
	
	return $res;
}


/**
  * reservarEstacionamiento - Hace una reserva en un estacionamiento
  *
  * @param $veepeId El veepeId
  * @param $idEstacionamiento El id del estacionamiento
  *
  * @return mixed[] un arreglo asociativo con los campos "IdEstacionamiento", "NombreEstacionamiento", "Hora"
  * Si no puedo generar la reserva, el mensaje de error va en $mensaje
  * -1=error general, 0=ya tenía otra reservacion, 1=todo bien, 2=estacionamiento lleno (estados deprecated)
  */
function reservarEstacionamiento($veepeId, $idEstacionamiento)
{
	global $mensaje;
	$res = null;
	
	loadDrupalBootstrap();
	
	// checamos si ya tiene un servicio
	$uid = getIdByVeepeId($veepeId);
	$yaTiene = db_query("select count(*) from f_dc_operaciones where id_usuario=".$uid." and status >= ".ESTADO_RESERVADO." and status<".ESTADO_ENTREGADO)->fetchField();
	if ( $yaTiene > 0 )
	{
		$mensaje = "No se registró la reserva porque tienes otra operación activa.";
	}
	else
	{
		// tiene disponibilidad el estacionamiento?
		$disponibles = db_query("select capacidad-(select count(*) from f_dc_operaciones where id_estacionamiento=".$idEstacionamiento." and status between 1 and 10) from f_dc_estacionamientos where id_estacionamiento = ".$idEstacionamiento)->fetchField();
		
		if ($disponibles > 0)
		{
			// registrar
			$idOperacion = db_insert('f_dc_operaciones')->fields(array(
				"id_estacionamiento"		=> $idEstacionamiento,
				"id_usuario"				=> $uid,
				"fecha_reserva"				=> date("Y-m-d H:i:s", time()),
				"status"					=> 1
			))->execute();
			
			// calendarizar el chequeo de expiración
			@exec('echo "php /var/www/html/ws/checarReserva.php '.$idOperacion.'" | at now + 30 minutes');
			
			// notificamos al estacionamiento
			enviarNotificacionEstacionamiento($idEstacionamiento, "¡Se ha recibido una nueva reserva!");
			
			$res = array(
				"IdEstacionamiento"=>$idEstacionamiento,
				"NombreEstacionamiento"=>db_query("select nombre from f_dc_estacionamientos where id_estacionamiento=".$idEstacionamiento)->fetchField(),
				"Hora"=>date("Y-m-d H:i:s", strtotime("+30 minutes")) );
		}
		else
		{
			$mensaje = "No se pudo hacer la reserva porque el estacionamiento ya está lleno.";
			//$res = 2;
		}
	}
	
	return $res;
}


/**
  * solicitarAuto - Solicita un auto que se encuentra en operación
  *
  * @param $idOperacion El id de la operación cuyo auto se quiere sacar
  *
  * @return int 0=no tiene método de pago(no se puede sacar el carro), 1=solicitado, -1=error (no se pudo solicitar)
  */
function solicitarAuto($idOperacion)
{
	$res = -1;
	
	loadDrupalBootstrap();
	
	$rs = db_query("select id_usuario, id_estacionamiento from f_dc_operaciones where id_operacion=".$idOperacion)->fetchAssoc();
	$tieneMetodoPago = getTieneMetodoPago($rs["id_usuario"]);
	
	if ($tieneMetodoPago)
	{
		$transaction = db_transaction();
		try {
			db_update("f_dc_operaciones")->fields(array(
				"status" => 5
			))
			->condition('id_operacion', $idOperacion, '=')
			->execute();
			$res = 1;

			// notificamos al estacionamiento
			enviarNotificacionEstacionamiento($rs["id_estacionamiento"], "Se ha solicitado un auto.");
		}
		catch (Exception $e) {
			$transaction->rollback();
			watchdog_exception("Excepción en función solicitarAuto", $e);
		}
	}
	else
		$res = 0;

	return $res;
}


function getStatusEstacionamiento($idEstacionamiento)
{
	global $mysqli;
	global $mensaje;

	// estadísticas
		// 2do: esto aun debe sacar bien los datos!!!!!!!!!!!!!!!
	$q = 'select capacidad as CapacidadTotal, pensiones_contratadas as Pensiones '.
		'from f_dc_estacionamientos where id_estacionamiento = '.$idEstacionamiento;
	$rs = $mysqli->query($q);
	if ( $rs!=false )
	{
		$estadisticas = $rs->fetch_assoc();
	}
	else
	{
		$mensaje = $q;
	}

	// operaciones actuales en ese estacionamiento
	$operaciones = array();
	$q = "select id_operacion as idOperacion, o.id_estacionamiento as idEstacionamiento, id_usuario as idUsuario, ".
		"concat(p.nombre, \" \", p.apellido_paterno) as Nombre, id_auto as idAuto, hora_entrada as HoraEntrada, hora_salida as HoraSalida, hora_estimada_entrega, ".
		"recibio as Recibio, tiempo_entrega as TiempoEntrega, o.status as Status, a.placa as Placa, a.marca as Marca, a.submarca as Submarca, ".
		"e.tarifa_hora as tarifaHora, o.fecha_reserva as FechaReserva ".
		"from f_dc_operaciones o inner join f_dc_perfilusuario p on o.id_usuario=p.uid ".
		"left join f_dc_automoviles a on a.id_automovil = o.id_auto ".
		"inner join f_dc_estacionamientos e on e.id_estacionamiento = o.id_estacionamiento ".
		"where o.id_estacionamiento = ".$idEstacionamiento." and ".
		"((o.status > 0 && o.status < ".ESTADO_EVALUACION.") or ".		// están activas
		"(o.status >= ".ESTADO_EVALUACION." && o.hora_salida >= subdate(curdate(),1)) ) ".		// son del dia de hoy, aunque ya hayan salido
		"order by o.status desc, o.hora_estimada_entrega asc";
	$rs = $mysqli->query($q);

	while($row = $rs->fetch_assoc())
	{
		$operaciones[] = array(
			"idOperacion"			=> $row["idOperacion"],
			"Usuario" 				=> $row["Nombre"],
			"Marca"					=> $row["Marca"],
			"Submarca"				=> $row["Submarca"],
			"Placa" 				=> $row["Placa"],
			"Entrada"				=> $row["HoraEntrada"],
			"Salida"				=> $row["HoraSalida"],
			"Recibio" 				=> getNombreById($row["Recibio"]),
			"Servicios" 			=> "",	// 2do: obtener la lista de servicios
			"TarifaHora"			=> $row["tarifaHora"],
			"Status" 				=> $row["Status"],
			"tiempoEspera" 			=> $row["TiempoEntrega"],
			"horaEstimadaEntrega"	=> $row["hora_estimada_entrega"],
			"fechaReserva"			=> $row["FechaReserva"],
			"pensiones"				=> getPensiones($row["idUsuario"], $idEstacionamiento)
		);
	}
	
	// contamos sólo las operaciones activas
	$opActivas = 0;
	$opReservas = 0;
	for($i=0; $i<count($operaciones); $i++)
	{
		if ($operaciones[$i]["Status"] >= ESTADO_PORAPROBAR and $operaciones[$i]["Status"] <= ESTADO_ENSALIDA )
			$opActivas++;

		if ($operaciones[$i]["Status"] == ESTADO_RESERVADO)
			$opReservas++;
	}

	$estadisticas["DisponibleGeneral"] = $estadisticas["CapacidadTotal"] - $opActivas;
	$estadisticas["Reserva"] = $opReservas;
	$estadisticas["PensionesDisponibles"] = $estadisticas["Pensiones"];	// 2do: las pensiones disponibles se tienen que calcular con base en las que ya entraron

	/*
	$estadisticas["General"] = 100;
	$estadisticas["IngresoGeneral"] = count($operaciones);
	$estadisticas["Pensiones"] = 10;
	$estadisticas["PensionesDisponibles"] = 10;
	*/

	return (object)array(
		"estadisticas"	=> $estadisticas,
		"operaciones"	=> $operaciones
	);
}



// ___________________________________________________________________ Pagos
// !Pagos

/**
  * debePagos - Valida si el usuario especificado tiene pagos que no se pudieron cobrar
  *
  * @param $uid El id del usuario
  *
  * @return bool *true* si el usuario tiene pagos pedientes, *false* en caso contrario
  */
function debePagos($uid)
{
	$res = false;
	global $mysqli;
	
	//$q = "select count(*) as c from f_dc_pagos_programados where status=1 and DATE(fecha_vencimiento) < CURDATE() and id_usuario=".$uid;
	$q = "select count(*) as c from f_dc_pagos_programados where status=1 and tipo=1 and id_usuario=".$uid;
	$rs = $mysqli->query($q);
	if ($rs!=false)
	{
		$row = $rs->fetch_assoc();
		$res = $row["c"] > 0;
	}
	else
		notificarDev("Error al ver si un usuario tiene pagos atrasados", "El query que tronó fue: ".$q);
	
	return $res;
}


/**
  * pagar - Realiza un pago a través de la plataforma de pago
  *
  * La respuesta del webservice tiene la forma (cuando es correcta)
  * {
  * 	"id": "trx.1Eq1y5wgaWtGmHwDhkeViRh8pUHq",
  * 	"auth_code": "100633",
  * 	"reference": "-10",
  * 	"description": "Payment on Demand",
  * 	"amount": 1160.5
  * }
  * o (cuando es incorrecta): 
  * {
  * 	"error": "un error",
  * }
  *
  * @param $referencia Cadena que representa la referencia del pago (una especie de folio único)
  * @param $token El token de la tarketa con la que se va a hacer el pago
  * @param $monto El monto (float) a cobrar
  * @param $tipo El tipo de pago; 1=pago operación, 3=pago pensión
  * @param $idMovimiento El id de la operación; puede ser el id de una operación o el id de una pensión
  *
  * @return bool *true* si se pudo ejecutar el pago; *false* en caso contrario
  */
function pagar($referencia, $token, $monto, $tipo, $idMovimiento=-1)
{
	$res = false;
	
	if ( floatval($monto) > 0 )	// solo enviamos el pago a banwire si el monto es mayor a cero
	{
		$fields = array('method'		=> 'payment', 
						'user'			=> BANWIRE_USER, 
						'reference'		=> $referencia,
						'token'			=> $token,
						'amount'		=> $monto);
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, URL_PAGO."?action=card");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	
		// ***************************
		// DESCOMENTAR EN PRODUCCIÓN
		$result = curl_exec($ch);
		// ***************************
		// QUITAR EN PRODUCCIÓN
		//$result = '{"id": "trx.1Eq1y5wgaWtGmHwDhkeViRh8pUHq","auth_code": "100633","reference": "-10","description": "Payment on Demand","amount": 1160.5}';
		// ***************************
		curl_close($ch);
	}
	
	try {
		$mensaje = $result;
		$response = json_decode($result);

		if ( !isset($response->error) || floatval($monto) == 0 )	// si el monto es cero registramos la operación, aunque no se haya hecho la operación con banwire
		{
			$now = date("Y-m-d H:i:s", time());
			$mensaje = "pago efectuado";
			db_insert('f_dc_pagos')->fields(array(
				"monto"				=> $monto,
				"id_operacion"		=> $idMovimiento,
				"fecha" 			=> $now,
				"folio"				=> $response->id." :: ".$response->auth_code,
				"tipo_pago"			=> $tipo
			))->execute();
			notificarDev("registro de pago", "ido: ".$idMovimiento.".Lo que regresa banwire: ".$result);
			$res = true;
		}
		else
		{
			$mensaje = $result;
			notificarDev("Error el ejecutar un pago", "Lo que regresa banwire: ".$result." para los datos referencia:".$referencia.", token:".$token.", monto:".$monto.", tipo:".$tipo.", idMovimiento:".$idMovimiento);
		}
	} catch (Exception $e) {
		// 2do: sucedió un error -> notificar porque es un error importante porque la respuesta no era json
		$mensaje = "algo tronó: ".$e;
	}
	
	return $res;
}


function realizarPago($idOperacion)
{
	global $mensaje;
	$res = false;

	loadDrupalBootstrap();
	$costoTotal = 0;
	
	// obtenemos la info de esta operación
	$operacion = db_query("select * from f_dc_operaciones where id_operacion=".$idOperacion)->fetchAssoc();
	
	// obtener las pensiones que tiene contratadas
	$pensiones = getPensiones($operacion["id_usuario"], $operacion["id_estacionamiento"]);
	
	// obtener costo por servicio de este estacionamiento
	$costosServicios = db_query("select id_servicio, costo from f_r_estacionamientos_servicios where id_estacionamiento=".$operacion["id_estacionamiento"])->fetchAll();
	$costoEstacionamiento = db_query("select tarifa_hora from f_dc_estacionamientos where id_estacionamiento=".$operacion["id_estacionamiento"])->fetchField();
	
	// calcular el costo total con base en los servicios solicitado
	$fechaIni = new DateTime( $operacion["hora_entrada"] );
	$fechaFin = new DateTime();
	$dif = $fechaIni->diff($fechaFin);
	
	$pensionesContratadas = isset($pensiones["normales"])?$pensiones["normales"]:array();
	$pensionesCompartidasContratadas = isset($pensiones["compartidas"])?$pensiones["compartidas"]:array();

	if ( count($pensionesContratadas) > 0 || count($pensionesCompartidasContratadas) > 0 )
	{
		// tiene alguna pensión para este estacionamiento!
		$suma = 0;
		// todo: este método tiene un error: si hay dos 3 al sumarse van a dar 6 pero eso no implica que sea de dia completo
		for ($i = 0; $i < count($pensionesContratadas); $i++)
		{
			$suma += $pensionesContratadas[$i]["horario"];
		}
		for ($i = 0; $i < count($pensionesCompartidasContratadas); $i++)
		{
			for ($j = 0; $j < count($pensionesCompartidasContratadas[$i]); $j++)
			{
				$suma += $pensionesCompartidasContratadas[$i][$j]["horario"];
			}
		}
		
		if ($suma >= 4)
		{
			// es de todo el dia -> no paga nada
			$costoTotal = 0;
		}
		else
		{
			$acumuladorHoras = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
			$contador = $fechaIni;
			$falta = true;
			$esElPrimero = true;
			$seguridad = 0;

			// paso 1: contabilizamos el total bruto
			while ($falta && $seguridad < 1000)
			{
				if ($contador == $fechaIni)	// es el primero -> tomamos sólo los minutos
				{
					$acumuladorHoras[intval($contador->format("H"))] += (60 - intval($contador->format("i")));
					if (intval($contador->format("Y"))==intval($fechaFin->format("Y")) && 
						intval($contador->format("m"))==intval($fechaFin->format("m")) && 
						intval($contador->format("d"))==intval($fechaFin->format("d")) && 
						intval($contador->format("H"))==intval($fechaFin->format("H")) )
					{
						$falta = false;
					}
				}
				else if (intval($contador->format("Y"))==intval($fechaFin->format("Y")) && 
						intval($contador->format("m"))==intval($fechaFin->format("m")) && 
						intval($contador->format("d"))==intval($fechaFin->format("d")) && 
						intval($contador->format("H"))==intval($fechaFin->format("H")) )	// es el último -> tomamos sólo los minutos
				{
					if (!$esElPrimero)
						$acumuladorHoras[intval($contador->format("H"))] += intval($fechaFin->format("i"));
					$falta = false;
				}
				else
				{
					$acumuladorHoras[intval($contador->format("H"))] += 60;	// es una hora intermedia -> contar completa
				}
				
				$esElPrimero = false;
				$seguridad++;
				$contador->add(new DateInterval("PT1H"));
			}
			// TODO: si $seguridad > 1000 es que algo está salió mal en el método
			
			// paso 2: ahora quitamos las horas cubiertas por las pensiones
			for ($i = 0; $i < count($pensionesContratadas); $i++)
			{
				if ( $pensionesContratadas[$i]["idEstacionamiento"] == $operacion["id_estacionamiento"] )
				{
					if ($pensionesContratadas[$i]["horario"] == 1)	// es de día
					{
						for ($m = 7; $m <= 18; $m++)
							$acumuladorHoras[$m] = 0;
					}
					else if ($pensionesContratadas[$i]["horario"] == 3)	// es de noche
					{
						for ($m = 19; $m <= 23; $m++) $acumuladorHoras[$m] = 0;
						for ($m = 0; $m <= 6; $m++) $acumuladorHoras[$m] = 0;
					}
				}
			}
			for ($i = 0; $i < count($pensionesCompartidasContratadas); $i++)
			{
				for ($j = 0; $j < count($pensionesCompartidasContratadas[$i]); $j++)
				{
					if ( $pensionesCompartidasContratadas[$i][$j]["idEstacionamiento"] == $operacion["id_estacionamiento"] )
					{
						if ($pensionesCompartidasContratadas[$i][$j]["horario"] == 1) // es de día
						{
							for ($m = 7; $m <= 18; $m++)
								$acumuladorHoras[$m] = 0;
						}
						else if ($pensionesCompartidasContratadas[$i][$j]["horario"] == 3)    // es de noche
						{
							for ($m = 19; $m <= 23; $m++) $acumuladorHoras[$m] = 0;
							for ($m = 0; $m <= 6; $m++) $acumuladorHoras[$m] = 0;
						}
					}
				}
			}

			// paso 3: hacemos la suma total
			$total = 0;
			for ($i = 0; $i < 24; $i++)
			{
				$total += $acumuladorHoras[$i];
			}
			$hrs = ($total / 60);
			$min = $total - ($hrs*60);

			if ($hrs == 0)
				$costoTotal = 0;
			else if ($hrs < 1)
				$costoTotal = 1 * $costoEstacionamiento;
			else
			{
				$fraccion = $min<15? .25 : ( $min<30? .5 : ( $min<45? .75 : 1 ) );
				$costoTotal = ($hrs + $fraccion) * $costoEstacionamiento;
			}

		}
	}
	else
	{
		// no tiene pensiones -> cobro normal
		if ( $dif->y < 1 && $dif->m < 1 && $dif->d < 1 && $dif->h < 1 )
		{
			// estuvimos menos de una hora -> cobrar hora completa
			$costoTotal += $costoEstacionamiento;
		}
		else
		{
			$costoTotal += ( ($dif->h * $costoEstacionamiento) + 			// horas
							($dif->d * $costoEstacionamiento * 24) +		// días 
							($dif->m * $costoEstacionamiento * 24 * 30) +	// meses
							($dif->y * $costoEstacionamiento * 24 * 30 * 12) // años
							);
			// y sumamos las fracciones
			if ( $dif->i > 45 )
				$costoTotal += $costoEstacionamiento*1;
			else if ( $dif->i > 30 )
				$costoTotal += $costoEstacionamiento*.75;
			else if ( $dif->i > 15 )
				$costoTotal += $costoEstacionamiento*.5;
			else if ( $dif->i > 0 )
				$costoTotal += $costoEstacionamiento*.25;
		}
	}

	
	// obtenemos el token de la tarjeta activa
	$token = getTokenMetodoPago( $operacion["id_usuario"] );
	
	// aplicar el cargo
	if ($token!=null)
	{
		// -------------------------------------------------------------------------
		// solicitud a banwire para ejecutar pago
		$referencia = "".$operacion["hora_entrada"];
		$referencia = str_ireplace(" ", "", $referencia);
		$referencia = str_ireplace("-", "", $referencia);
		$referencia = str_ireplace(":", "", $referencia);
		$referencia .= $idOperacion;
		
		$fechaFin->add(new DateInterval('P3D'));
		
		$res = pagar($referencia, $token, $costoTotal, 1, $idOperacion); // $referencia, $token, $monto, $tipo, $idMovimiento=-1
		if ( !$res )
		{
			$mensaje = "No se pudo procesar el pago. Se intentará automáticamente en tres días.";
			$idOperacion = db_insert('f_dc_pagos_programados')->fields(array(
				"id_usuario"			=> $operacion["id_usuario"],
				"id_operacion"			=> $idOperacion,
				"monto"					=> $costoTotal,
				"fecha_vencimiento"		=> $fechaFin->format('Y-m-d H:i:s'),
				"tipo"					=> 1,	// 1=operación, 3=pensión
				"status"				=> 1,	// 1=por cobrar
			))->execute();
		}
	}
	else
	{
		// 2do: error crítico! llegamos aquí y no debimos llegar sin tener un token de pago
		$mensaje = "No pudimos hacer el pago porque el token es nulo";
	}
	
	return $res;
}




// ___________________________________________________________________ Pensiones
// !Pensiones



/**
  * cancelarPension - Cancela una pensión normal o compartida
  *
  * @param $idPensionado El id del pensionado cuando es pensión normal, id_compartida cuando es pensión compartida
  * @param $tipoPension El tipo pensión a cancelar 1 = normal, 3 = compartida
  *
  * @return int Regresa 1 si todo bien, 0=error
  */
function cancelarPension($idPensionado, $tipoPension)	// no la persona, si no la instancia de la pensión para esta persona
{
	$res = 0;

	loadDrupalBootstrap();
	$fechaCancelacion = date('Y-m-d H:i:s', time());
	
	//$penalizacion = db_query("select ")->fetchField();

	$transaction = db_transaction();
	try
	{
		if($tipoPension == 1)
		{
			$id_usuario = db_query("select id_usuario from f_dc_pensionados where id_pensionado=".$idPensionado)->fetchField();
			$pagosProgramadosRestantes = db_query("select * from f_dc_pagos_programados where id_operacion=".$idPensionado. " and id_usuario=".$id_usuario. " and tipo=3 and fecha_vencimiento > NOW() order by fecha_vencimiento asc")->fetchAll();
			$primerPago = $pagosProgramadosRestantes[0];

			if(count($pagosProgramadosRestantes) > 0)
			{
				$idPagosProgramados = array();
				
				foreach ($pagosProgramadosRestantes as $pagos)
				{
					array_push($idPagosProgramados, $pagos->id_pago_programado);
				}

				db_update('f_dc_pagos_programados')->fields(array(
					'status'=>-1
				))
				->condition('id_usuario', $id_usuario, '=')
				->condition('tipo', 3, '=')
				->condition('id_pago_programado', array($idPagosProgramados), 'IN')
				->execute();
				
				db_update('f_dc_pensionados')->fields(array(
					'fecha_cancelacion'=>$fechaCancelacion,
					'status'=>7
				))
				->condition('id_pensionado', $idPensionado, '=')
				->execute();
			}
			else
			{
				db_update('f_dc_pagos_programados')->fields(array(
					'status'=>-1
				))
				->condition('id_operacion', $idPensionado, '=')
				->condition('id_usuario', $id_usuario, '=')
				->condition('tipo', 3, '=')
				->execute();

				//updateamos la tabla de pensionados y lo ponemos status 3
				db_update('f_dc_pensionados')->fields(array(
					'status'=>3
				))
				->condition('id_pensionado', $idPensionado, '=')
				->execute();
			}
		
		}
		else if($tipoPension == 3)
		{
			$id_usuario = db_query("select id_usuario from f_dc_pensionados where id_compartida='".$idPensionado."'")->fetchField();
			$idOperaciones = db_query("select id_pensionado from f_dc_pensionados where id_compartida='".$idPensionado."'")->fetchAll();
			
			$idPensiones = "(";
					
			$ultimoElemento = end($idOperaciones);
			
			$arregloPensiones = array();
			foreach ($idOperaciones as $pensionCompartida) {

				array_push($arregloPensiones, $pensionCompartida->id_pensionado);
				
				if($pensionCompartida == $ultimoElemento){
					$idPensiones .= "id_operacion= " . $pensionCompartida->id_pensionado;
				}else{
					$idPensiones .= "id_operacion= " . $pensionCompartida->id_pensionado . "  or  ";
				}
			}

			$idPensiones .=")";
			
			$pagosProgramadosRestantes = db_query("select * from f_dc_pagos_programados where ".$idPensiones. " and id_usuario=".$id_usuario. " and tipo=3 and fecha_vencimiento > NOW() order by fecha_vencimiento asc")->fetchAll();
			
			$primerPago = $pagosProgramadosRestantes[0];

			if(count($pagosProgramadosRestantes) > 0)
			{
				$idPagosProgramados = array();
				
				foreach ($pagosProgramadosRestantes as $pagos)
				{
					array_push($idPagosProgramados, $pagos->id_pago_programado);
				}

				db_update('f_dc_pagos_programados')->fields(array(
					'status'=>-1
				))
				->condition('id_usuario', $id_usuario, '=')
				->condition('tipo', 3, '=')
				->condition('id_pago_programado', array($idPagosProgramados), 'IN')
				->execute();
				
				db_update('f_dc_pensionados')->fields(array(
					'fecha_cancelacion'=>$fechaCancelacion,
					'status'=>7
				))
				->condition('id_pensionado', array($arregloPensiones), 'IN')
				->execute();
			}
			else
			{
				//updateamos la tabla de pensionados y lo ponemos status 3
				db_update('f_dc_pensionados')->fields(array(
					'status'=>3
				))
				->condition('id_compartida', $idPensionado, '=')
				->execute();
			}
		}
	
		// todo: cobrar la penalización, si existe
		
		$res = 1;
	}
	catch (Exception $e)
	{
		$transaction->rollback();
		watchdog_exception("Excepción altaUsuario", $e);
	}
	return $res;
}



/**
  * contratarPension - Contrata una pensión normal
  *
  * @param $idUsuario El id del usuario que contrata
  * @param $idPension El id de la pensión que se quiere contratar
  * @param $idMetodoPago El id del método de pago con que se va a pagar
  * @param $horario El horario en que se desea contratar (un idPension puede tener hasta 3 horarios, hence this)
  *
  * @return int Regresa 1 si todo bien, 3 si esa pensión ya está contratada, 5 si no hay disponibilidad, 0=error
  */
function contratarPension($idUsuario, $idPension, $idMetodoPago, $horario)
{
	global $mensaje;
	$res = 0;
	
	loadDrupalBootstrap();
	
	$idEstacionamiento = db_query("select id_estacionamiento from f_dc_pensiones where id_pension=".$idPension)->fetchField();
	$numeroPensionesConfiguradas = db_query("select pensiones_contratadas from f_dc_estacionamientos where id_estacionamiento = ".$idEstacionamiento)->fetchField();
	$numeroPensionesOcupadas = db_query("select count(pe.id_pensionado) from f_dc_pensionados pe
										inner join f_dc_pensiones pen on pen.id_pension = pe.id_pension
										where pen.id_estacionamiento = ".$idEstacionamiento. " and (pe.status = 1 or pe.status = 7)")->fetchField();
	$numeroPensionesDisponibles = $numeroPensionesConfiguradas - $numeroPensionesOcupadas;
	$estacionamiento = db_query("select nombre from f_dc_estacionamientos where id_estacionamiento=".$idEstacionamiento)->fetchField();
	
	if($numeroPensionesDisponibles > 0)
	{
		$yaEstaContratada = db_query("select count(*) from f_dc_pensionados where id_usuario =".$idUsuario." and id_pension =".$idPension." and horario=".$horario." and status in (1, 7)")->fetchField();
		if($yaEstaContratada == 0)
		{
			$fechaContratacion = date('Y-m-d H:i:s', time());
			$periodo = db_query("select periodo from f_dc_pensiones where id_pension=".$idPension)->fetchField();
			$costo = db_query("select ".($horario==1?"dia":($horario==3?"noche":"todo_dia"))." from f_dc_pensiones where id_pension=".$idPension)->fetchField();
			
			$transaction = db_transaction();
			try
			{
				// primero intentamos hacer el cobro
				$token = getTokenMetodoPago( $idUsuario );

				if ($token!=null)
				{
					// -------------------------------------------------------------------------
					// solicitud a banwire para ejecutar pago
					$referencia = "".$fechaContratacion;
					$referencia = str_ireplace(" ", "", $referencia);
					$referencia = str_ireplace("-", "", $referencia);
					$referencia = str_ireplace(":", "", $referencia);
					$referencia .= $idOperacion;
					
					$r = pagar($referencia, $token, $costo, 3, $idOperacion); // $referencia, $token, $monto, $tipo, $idMovimiento=-1
					
					if ( $r )
					{
						// registramos la pensión
						$idOperacion = db_insert('f_dc_pensionados')->fields(array(
							"id_usuario"			=> $idUsuario,
							"id_pension"			=> $idPension,
							"id_metodopago"			=> $idMetodoPago,
							"tipo_pension"			=> 1,
							"horario"				=> $horario,
							"costo"					=> $costo,
							"status"				=> 1,
							"fecha_contratacion"	=> $fechaContratacion,
							"fecha_cancelacion"		=> null
						))->execute();

						// se pudo hacer el primer cobro -> programamos los demás
						$dia = date('d', time());
						$mes = date('m', time());
						$anho = date('Y', time());
						$mes++;		// el segundo pago empieza el siguiente mes, a partir de hoy
						
						for ($i=2; $i<=$periodo; $i++)
						{
							db_insert('f_dc_pagos_programados')->fields(array(
								"id_usuario"		=> $idUsuario,
								"id_operacion"		=> $idOperacion,
								"monto"				=> $costo,
								"fecha_vencimiento"	=> $anho."-".$mes."-".$dia,
								"tipo"				=> 3,	//1=operacion, 3=pension
								"status"			=> 1,	// por cobrar
							))->execute();
							
							if ( $mes == 12 )
							{
								$mes = 1;
								$anho++;
							}
							else
								$mes++;
						}

						$res = 1;

						/*$mail = db_query("select nombre_usuario from f_dc_perfilusuario where uid=".$idUsuario)->fetchField();
						if (!enviarMailConFormato($mail, "Contratación de pensión", "Contratación de pensión", "Tu contratación de pensión para el estacionamiento ".$estacionamiento. " ha sido exitosa", ""));
						{
							watchdog_exception("contratacion pension normal::Error al enviar mail ".$mail, new Exception(""));
						}*/
					}
					else
					{
						$mensaje = "No se pudo hacer el cobro. Por favor actualiza tus métodos de pago.";
						notificarDev("programación de pagos", "no se pudo ejecutar el primer pago");
					}
				}
				else
				{
					// TODO: error crítico! llegamos aquí y no debimos llegar sin tener un token de pago
					$mensaje = "No pudimos hacer el pago porque no existe un método de pago asociado.";
				}
			}
			catch (Exception $e)
			{
				$transaction->rollback();
				watchdog_exception("Excepción contratarPensión", $e);
			}
			
			if (r)
			{
				$mail = db_query("select nombre_usuario from f_dc_perfilusuario where uid=".$idUsuario)->fetchField();
				if (!enviarMailConFormato($mail, "Contratación de pensión", "Contratación de pensión", "Tu contratación de pensión para el estacionamiento ".$estacionamiento." ha sido exitosa", ""));
				{
					watchdog_exception("contratacion pension normal::Error al enviar mail ".$mail, new Exception(""));
				}
			}
		}
		else
		{
			$res = 3;
		}
	}
	else
	{
		$res = 5;
	}

	return $res;
}


function contratarPensionCompartida($idUsuario, $veepeId, $listaEstacionamientos, $idMetodopago, $horarios, $idCompartida, $costos, $numeroPeriodos)
{
	$res = 0;
	$idOperacion = -1;

	loadDrupalBootstrap();
	$fechaContratacion = date('Y-m-d H:i:s', time());
	
	$arregloPensionados = array();
	
	$piezas = explode(",", $listaEstacionamientos);
	$_horarios = explode(",", $horarios);
	
	
	//-------checamos que no tenga configurada una pensión compartida con los mismos datos----------------------
	$yaEstaContratada = false;
	$pensionesCompartidasContratadas = db_query("select id_pension, horario, id_compartida from f_dc_pensionados where id_usuario =".$idUsuario." and tipo_pension = 3 and status in (1, 7) order by id_compartida desc")->fetchAll();
	
	$id_compartida = null;
	
	foreach($pensionesCompartidasContratadas as $p){
		
		if ($p->id_compartida != $id_compartida){
			$id_compartida = $p->id_compartida;
		}

		$contador = 0;
		for ($i = 0; $i < count($piezas); $i++)
		{
			$pension = db_query("select count(*) from f_dc_pensionados where id_usuario =".$idUsuario." and tipo_pension = 3 and status in (1, 7) and id_compartida ='" .$id_compartida. "' and id_pension=".$piezas[$i]." and horario=".$_horarios[$i])->fetchField();	
		
			if($pension > 0){
				$contador++;
			}
		}
	
		$numeroPensiones = db_query("select count(*) from f_dc_pensionados where id_usuario =".$idUsuario." and tipo_pension = 3 and status in (1, 7) and id_compartida ='".$id_compartida."'")->fetchField();	
		
		if ($numeroPensiones == $contador){
			$yaEstaContratada = true;
			break;
		}
	}
	
	
	//---------------------------------------------------------------------------------------------------------------
	
	
	if($yaEstaContratada == false)
	{

		$transaction = db_transaction();
		try
		{
			
			$_costos = explode(",", $costos);
			$periodo = db_query("select periodo from f_dc_pensiones where id_pension=".$piezas[0])->fetchField();
			$costoTotalMensual = 0;

			for ($i = 0; $i < count($piezas); $i++)
			{
				$id = db_insert('f_dc_pensionados')->fields(array(
						"id_usuario"			=> $idUsuario,
						"id_pension"			=> $piezas[$i],
						"id_metodopago"			=> $idMetodopago,
						"tipo_pension"			=> 3,
						"horario"				=> $_horarios[$i],
						"costo"					=> $_costos[$i],
						"status"				=> 1,
						"fecha_contratacion"	=> $fechaContratacion,
						"id_compartida"			=> $idCompartida
				))->execute();

				$costoTotalMensual += $_costos[$i];

				$arregloPensionados[$i] = $id;

				if ( $i==0 ) $idOperacion = $id;		// en este caso usamos el id de la primer "pensión" del grupo de copartidas para guardar en los pagos

			}

			// hacemos el pago
			$token = getTokenMetodoPago( $idUsuario );

			// aplicar el cargo
			if ($token!=null)
			{
				// -------------------------------------------------------------------------
				// solicitud a banwire para ejecutar pago
				$referencia = "".$fechaContratacion;
				$referencia = str_ireplace(" ", "", $referencia);
				$referencia = str_ireplace("-", "", $referencia);
				$referencia = str_ireplace(":", "", $referencia);
				$referencia .= $idCompartida;

				$r = pagar($referencia, $token, $costoTotalMensual, 3, $idOperacion);

				if ( $r )
				{
					// se pudo hacer el primer cobro -> programamos los demás
					$dia = date('d', time());
					$mes = date('m', time());
					$anho = date('Y', time());
					$mes++;		// el segundo pago empieza el siguiente mes, a partir de hoy

					for ($i=1; $i <= $numeroPeriodos-1; $i++)
					{

						for ($j=0; $j < count($arregloPensionados); $j++)
						{

							$idOperacion = db_insert('f_dc_pagos_programados')->fields(array(
								"id_usuario"		=> $idUsuario,
								"id_operacion"		=> $arregloPensionados[$j],
								"monto"				=> $_costos[$j],
								"fecha_vencimiento"	=> $anho."-".$mes."-".$dia,
								"tipo"				=> 3,	//1=operacion, 3=pension
								"status"			=> 1,	// por cobrar
							))->execute();

						}


						if ( $mes == 12 )
						{
							$mes = 1;
							$anho++;
						}
						else
							$mes++;

					}
				}
				else
					notificarDev("programación de pagos", "no se pudo ejecutrar el primer pago");
			}
			else
			{
				// 2do: error crítico! llegamos aquí y no debimos llegar sin tener un token de pago
				$mensaje = "No pudimos hacer el pago porque el token es nulo";
			}

			$res = 1;
		}
		catch (Exception $e)
		{
			$transaction->rollback();
			watchdog_exception("Excepción contratarPensiónCompartida", $e);
			$res = 0;
		}
	}
	else
	{
		$res = 3; // ya tiene una contratación igual
	}

	return $res;
}


 /**
  * getPensiones - Obtiene la lista de pensiones que un usuario tiene contratadas
  *
  * @param int $idUsuario El id del usuario
  * @param int $idEstacionamiento El id del estacionamiento al cual queremos limitar la búsqueda. Si es nulo, se busca en todos los estacionamientos

  *
  * @return mixed un arreglo asociativo con dos niveles: ["normales"] y ["compartidas"]. ["normales"] es un arreglo de objetos donde cada objeto es una pensión. ["compartidas"] es un arreglo donde cada casilla es una arreglo de ["normales"]
  */
function getPensiones($idUsuario, $idEstacionamiento=null)
{
	$res = array("normales"=>array(), "compartidas"=>array());
	
	global $mysqli;
	global $mensaje;
	
	$q = 'select pp.id_pensionado, pp.id_pension, pp.id_metodopago, pp.tipo_pension, pp.horario, pp.costo, pp.fecha_contratacion, pp.id_compartida, pp.status,
		e.id_estacionamiento, e.nombre as nombreEstacionamiento 
		from f_dc_pensionados pp inner join f_dc_pensiones p on pp.id_pension=p.id_pension 
				inner join f_dc_estacionamientos e on e.id_estacionamiento=p.id_estacionamiento 
		where id_usuario='.$idUsuario.' and (pp.status=1 or pp.status=7) and p.status=1 and e.status=1';
	
	if ( $idEstacionamiento!=null )
		$q .= " and e.id_estacionamiento=".intval($idEstacionamiento);
		
	//$mensaje .= $q;
	
	$rs = $mysqli->query($q);

	if ( $rs!=false )
	{
		$auxCompartidas = array();
		while ($row = $rs->fetch_assoc())
		{
			if ( $row["tipo_pension"] == 1 )
			{
				// es pensión normal, se va al arrego de normales
				$res["normales"][] = array(
					"idPension"				=> $row["id_pension"],
					"idPensionado"			=> $row["id_pensionado"],
					"idEstacionamiento"		=> $row["id_estacionamiento"],
					"nombreEstacionamiento"	=> $row["nombreEstacionamiento"],
					"horario"				=> $row["horario"],
					"tipo"					=> $row["tipo_pension"],
					"costo"					=> $row["costo"],
					"idMetodoPago"			=> $row["id_metodopago"],
					"fechaContratacion"		=> $row["fecha_contratacion"],
					"status"				=> $row["status"]
				);
			}
			else
			{
				// es compartida, lo agrupamos con sus otras
				if (!isset($auxCompartidas[$row["id_compartida"]]))
					$auxCompartidas[$row["id_compartida"]] = array();
					
				$auxCompartidas[$row["id_compartida"]][] = array(
					"idPension"				=> $row["id_pension"],
					"idPensionado"			=> $row["id_pensionado"],
					"idEstacionamiento"		=> $row["id_estacionamiento"],
					"nombreEstacionamiento"	=> $row["nombreEstacionamiento"],
					"horario"				=> $row["horario"],
					"tipo"					=> $row["tipo_pension"],
					"costo"					=> $row["costo"],
					"idMetodoPago"			=> $row["id_metodopago"],
					"fechaContratacion"		=> $row["fecha_contratacion"],
					"idCompartida"			=> $row["id_compartida"],
					"status"				=> $row["status"]
				);
			}
		}
		
		// ahora compactamos las compartidas
		foreach($auxCompartidas as $comp)
		{
			$res["compartidas"][] = $comp;
		}
		
		/*$res["compartidas"] = array();
		$res["simples"] = array();
		// ahora las organizamos por tipo
		foreach($aux as $a)
		{
			if ( count($a) > 1 )
			{
				// es compartida -> agregamos a la lista de compartidas
				$res["compartidas"][] = $a;
			}
			else if (count($a)==1)
			{
				// tiene una, pero podría ser una compartida con un solo estacionamiento
				if ( $a[0]["tipo"] == 1 )
					$res["simples"][] = $a;
				else
					$res["compartidas"][] = $a;
			}
		}*/
	}
	
	return $res;
}


// ___________________________________________________________________ Logs & Misc
// !Logs & Misc


 /**
  * Registra un error de la app en la bd
  *
  * Se recibe sólo de la app.
  *
  * @param string $veepeId El usuario que emite el error
  * @param string $error El texto del error (o excepción)
  * @param string $origen Identifica el origen del error, con el si formato: archivo::funcion/metodo
  * @param mixed $data Un arreglo asociativo con posibles variables e información extra
  *
  * @return void
  */
function logErrorApp($veepeId, $error, $origen, $data)
{
	global $mysqli;

	$q = 'insert into f_dc_errores (veepeid, error, origen) values ("'.$veepeId.'", "'.$error.'", "'.$origen.'")';
	$rs = $mysqli->query($q);
}


function reportException($exception="")
{
	$res = -1;
	
	// intentamos abrir el archivo
	try {
		$res = file_put_contents("logs/exceptions.log", "*********************************************\n".$exception."\n", FILE_APPEND);
		notificarDev("Excepción Veepe", $exception);
	} catch (Exception $e) {
		notificarDev("Error de reporteo Veepe", "No se pudo escribir al archivo de excepciones");
	}
	
	return $res;
}


function reportError($error="")
{
	$res = -1;
	
	// intentamos abrir el archivo
	try {
		$res = file_put_contents("logs/errors.log", "*********************************************\n".$error."\n", FILE_APPEND);
		//notificarDev("Error2 Veepe", $error);
	} catch (Exception $e) {
		notificarDev("Error de reporteo Veepe", "No se pudo escribir al archivo de errores");
	}
	
	return $res;
}








?>
