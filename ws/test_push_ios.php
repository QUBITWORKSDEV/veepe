<?php
	// el argumento es el id de la operación
	
	include_once "php/funciones.php";

	$token = $_GET["token"]; // $argv[1];
	$texto = "probando push ios";

	$dataString = json_encode(array("to"=>$token, "data"=>array("title"=>"Veepe", "text"=>$texto)));

	require_once 'php/ApnsPHP/Autoload.php';
	// Instantiate a new ApnsPHP_Push object
	$push = new ApnsPHP_Push(
		ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION,
		'php/server_certificates_bundle_production.pem'
	);
	// Set the Root Certificate Autority to verify the Apple remote peer
	$push->setRootCertificationAuthority('php/entrust_root_certification_authority.pem');
	// Connect to the Apple Push Notification Service
	$push->connect();
	// Instantiate a new Message with a single recipient
	$message = new ApnsPHP_Message($token);
	
	$message->setCustomIdentifier("161-".time());
	// Set badge icon to "1"
	$message->setBadge(1);
	// Set a simple welcome text
	$message->setText($texto);
	// Play the default sound
	$message->setSound();
	// Set the expiry value to 60 seconds
	$message->setExpiry(60);
	// Add the message to the message queue
	$push->add($message);
	// Send all messages in the message queue
	$push->send();
	// Disconnect from the Apple Push Notification Service
	$push->disconnect();
	// Examine the error message container
	$aErrorQueue = $push->getErrors();
	if (!empty($aErrorQueue)) {
		// todo: implementar una forma de reportar este error sin tener que enviarlo en el $message
		$mensaje .= var_dump($aErrorQueue);
		var_dump($aErrorQueue);
	}

