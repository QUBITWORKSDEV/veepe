<?php

$pdo = new PDO('mysql:host=localhost;dbname=veepe', 'toor', 'toor', [PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8']);


function determineType($Type)
{
    var_dump($Type);
    if (preg_match('/(decimal)|(double)|(float)|(float)|(int)|(tinyint)/', $Type)) {
        return 'numeric';
    } else {
        if (preg_match('/(tinytext)|(varchar)/', $Type)) {
            return 'string';
        } else {
            if (preg_match('/(datetime)/', $Type)) {
                return 'datetime';
            } else {
                if (preg_match('/(date)/', $Type)) {
                    return 'date';
                } else {
                    return 'text';
                }
            }
        }
    }
}

$query = $pdo->query('DESCRIBE  ' . $argv[1]);

$header        = [];
$table         = [];
$export_header = [];
$export        = [];
$formField     = [];
$update        = [];
$insert        = [];
$key           = null;
$order         = '->orderByHeader($header)';
if ('f_dc_operaciones' == $argv[1]) {
    $order = "->orderBy('hora_entrada', 'desc')";
}

if ($query->execute()) {
    $fields = $query->fetchAll(PDO::FETCH_ASSOC);

    foreach ($fields as $field) {

        extract($field);

        $fieldLabel = ucwords(str_replace('id ', '', str_replace('_', ' ', $Field)));

        $required = $Null === 'NO';
        $primary  = $Key === 'PRI';

        $dataType = determineType($Type);

        if ($primary) {
            $key               = $Field;
            $insert[$Field]    = null;
            $update[$Field]    = null;
            $formField[$Field] = '';
        }


        if (false !== strpos($Field, 'tarifa_') || 'monto' == $Field) {
            $table[$Field] = "array('data' => currency(\$res->$Field), 'align' => 'right'),";
        }

        if (false !== strpos($Field, 'tarifa_') || 'monto' == $Field) {
            $export[$Field] = "currency(\$record->$Field)";
        }

        if (0 === strpos($Field, 'id_')) {
            if (!$primary) {
                $table[$Field]  = "{$Field}_link(\$res->$Field),";
                $export[$Field] = "{$Field}_description(\$record->$Field)";
                $formField[$Field]
                                = "    \$form['$Field']                = array(
        '#type'          => 'select',
        '#title'         => t('" . $fieldLabel . "'),
        '#required'      => true,
        '#options'       => {$Field}_options_list(),
    );";
            } else {

                $table[$Field]  = "array('data' => \$res->$Field, 'align' => 'right'),";
                $export[$Field] = "\$record->$Field";
            }
        }

        if ('recibio' == $Field) {
            $table[$Field]  = "id_usuario_link(\$res->$Field),";
            $export[$Field] = "id_usuario_description(\$record->$Field)";
            $formField[$Field]
                            = "    \$form['$Field']                = array(
        '#type'          => 'select',
        '#title'         => t('" . $fieldLabel . "'),
        '#options'       => id_usuario_options_list(),
    );";
        }


        if (0 === strpos($Field, 'tiene_') || 0 === strpos($Field, 'deja_')) {
            $table[$Field]  = "empty(\$res->$Field)? 'No' : 'Si',";
            $export[$Field] = "empty(\$record->$Field)? 'No' : 'Si'";
            $formField[$Field]
                            = "    \$form['$Field']                = array(
        '#type'          => 'select',
        '#title'         => t('" . $fieldLabel . "'),
        '#options'       => array(0 => 'No', 1 => 'Si'),
    );";
        }

        if ('status' == $Field) {
            $table[$Field]  = "\$res->status > 0 ? 'Activo' : 'Inactivo',";
            $export[$Field] = "\$record->status > 0 ? 'Activo' : 'Inactivo'";
            $formField[$Field]
                            = "    \$form['status']                = array(
        '#type'          => 'select',
        '#title'         => t('Activo'),
        '#options'       => array(-1 => 'No', 1 => 'Si'),
    );";

        }

        if ('tipo_pension' == $Field) {
            $table[$Field]  = "\$res->tipo_pension == 1 ? 'No Compartida' : 'Compartida',";
            $export[$Field] = "\$record->tipo_pension == 1  ? 'No Compartida' : 'Compartida'";
            $formField[$Field]
                            = "    \$form['tipo_pension']                = array(
        '#type'          => 'select',
        '#title'         => t('Activo'),
        '#options'       => array(1 => 'No Compartida', 3 => 'Compartida'),
    );";

        }

        if ('horario' == $Field) {
            $table[$Field]  = "\$res->horario == 1? 'DIA' : (\$res->horario == 1? 'NOCHE' : 'TODO EL DÍA'),";
            $export[$Field]  = "\$res->horario == 1? 'DIA' : (\$res->horario == 1? 'NOCHE' : 'TODO EL DÍA')";
            $formField[$Field]
                            = "    \$form['horario']                = array(
        '#type'          => 'select',
        '#title'         => t('Activo'),
        '#options'       => array(1 => 'DIA', 3 => 'NOCHE', 5 => 'TODO EL DIA'),
    );";

        }

        if (!isset($insert[$Field]) && !$primary) {
            $insert[$Field] = "                        '$Field'                => \$form_state['input']['$Field'],";
        }

        if (!isset($update[$Field])) {
            $update[$Field] = "                        '$Field'                => \$form_state['input']['$Field'],";
        }

        if (!isset($export_header[$Field])) {
            $export_header[$Field] = $fieldLabel;
        }

        switch ($dataType) {
            case 'string':
                $max = preg_replace('/\D/', '', $Type);
                $max = empty($max) ? 1000 : $max;

                if (!isset($header[$Field])) {
                    $header[$Field] = "array('data' => '$fieldLabel', 'field' => '$Field'),";
                }
                if (!isset($formField[$Field])) {
                    $formField[$Field]
                        = "    \$form['$Field'] = array(
        '#type'      => 'textfield',
        '#title'     => t('$fieldLabel'),
        '#maxlength' => $max,
        '#required'         => " . ($required ? 'true' : 'false') . ",
    );";
                }
                if (!isset($table[$Field])) {
                    $table[$Field] = "\$res->$Field,";
                }
                if (!isset($export[$Field])) {
                    $export[$Field] = "\$record->$Field";
                }
                break;
            case 'numeric':
                if (!isset($header[$Field])) {
                    $header[$Field] = "array('data' => '$fieldLabel', 'field' => '$Field'),";
                }

                if (!isset($formField[$Field])) {
                    $formField[$Field]
                        = "    \$form['$Field'] = array( 
        '#type'             => 'textfield',
        '#title'            => t('$fieldLabel'),
        '#required'         => " . ($required ? 'true' : 'false') . ",
        '#element_validate' => array('element_validate_number'),
    );";
                }
                if (!isset($table[$Field])) {
                    $table[$Field] = "array('data' => \$res->$Field, 'align' => 'right'),";
                }
                if (!isset($export[$Field])) {
                    $export[$Field] = "\$record->$Field";
                }
                break;
            case 'text':
                if (!isset($header[$Field])) {
                    $header[$Field] = "//array('data' => '$fieldLabel', 'field' => '$Field'),";
                }
                if (!isset($formField[$Field])) {
                    $formField[$Field]
                        = "    \$form['$Field'] = array( 
        '#type'             => 'textarea',
        '#title'            => t('$fieldLabel'),
        '#required'         => " . ($required ? 'true' : 'false') . ",
    );";
                }
                if (!isset($table[$Field])) {
                    $table[$Field] = "//\$res->$Field,";
                }
                if (!isset($export[$Field])) {
                    $export[$Field] = "\$record->$Field";
                }
                if (!isset($header[$Field])) {
                    $header[$Field] = '//' . $header[$Field];
                }
                break;
            case 'datetime':
                if (!isset($formField[$Field])) {
                    $formField[$Field]
                        = "    \$form['$Field'] = array(
        '#type'      => 'datetime',
        '#title'     => t('$fieldLabel'),
        '#required'         => " . ($required ? 'true' : 'false') . ",
    );";
                }
                if (!isset($header[$Field])) {
                    $header[$Field] = "array('data' => '$fieldLabel', 'field' => '$Field'),";
                }
                if (!isset($table[$Field])) {
                    $table[$Field] = "\$res->$Field,";
                }
                if (!isset($export[$Field])) {
                    $export[$Field] = "\$record->$Field";
                }
                break;
            case 'date':
                if (!isset($formField[$Field])) {
                    $formField[$Field]
                        = "    \$form['$Field'] = array(
        '#type'      => 'date',
        '#title'     => t('$fieldLabel'),
        '#required'         => " . ($required ? 'true' : 'false') . ",
    );";
                }
                if (!isset($header[$Field])) {
                    $header[$Field] = "array('data' => '$fieldLabel', 'field' => '$Field'),";
                }
                if (!isset($table[$Field])) {
                    $table[$Field] = "\$res->$Field,";
                }
                if (!isset($export[$Field])) {
                    $export[$Field] = "\$record->$Field";
                }
                break;

        }

    }

}


$placeHolders = [
    '__table__'          => $argv[1],
    '__label_plural__'   => $argv[2],
    '__label_singular__' => $argv[3],
    '__name__'           => $argv[4],
    '__router__'         => $argv[5],
    '__header__fields__' => '            ' . implode("\n            ", $header),
    '__table_fields__'   => '            ' . implode("\n            ", $table),
    '__form__fields__'   => implode("\n", $formField),
    '__edit_fields__'    => implode("\n", $update),
    '__insert_fields__'  => implode("\n", $insert),
    '__key__'            => $key,
    '__order__'          => $order,
    '__export_header__'  => implode("',\n        '", $export_header),
    '__export__'         => implode(",\n        ", $export),
];

$module = file_get_contents(dirname($argv[0]) . '/template/name.module');
$info   = file_get_contents(dirname($argv[0]) . '/template/name.info');
foreach ($placeHolders as $placeHolder => $value) {
    $module = str_replace($placeHolder, $value, $module);
    $info   = str_replace($placeHolder, $value, $info);
}
$targetDir = './modules/veepe_' . $argv[4];

if (!is_dir($targetDir)) {
    umask(0);
    mkdir('./modules/veepe_' . $argv[4], 0777);
}

file_put_contents($targetDir . '/' . $argv[4] . '.module', $module);
file_put_contents($targetDir . '/' . $argv[4] . '.info', $info);