<?php

include_once("../sites/default/settings.php");

/* ----------------------------------------------------
  Variables globales usadas por Funktionell
  ---------------------------------------------------- */
global $base_path;

/* ----------------------------------------------------
		 Cargamos el bootstrap de Drupal
   ---------------------------------------------------- */
$currdir=getcwd();
chdir($base_path);
define('DRUPAL_ROOT', getcwd());
require_once("./includes/bootstrap.inc");
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
chdir($currdir);


if(esAgv()) $caso = 'admin_general_veepe';
if(esAva()) $caso = 'admin_veepe';
if(esSocio()) $caso = 'socio_veepe';
if(esSupervisor()) $caso = 'supervisor_veepe';

//Fechas de inicio y fin
$fechaIni = $_POST['fechaIni'];
$fechaFin = $_POST['fechaFin'];

//Regresar a formato 'mm/dd/yyyy'
list($anioI, $mesI, $diaI) = explode('-', $fechaIni);
list($anioF, $mesF, $diaF) = explode('-', $fechaFin);
$fInicio =  $mesI . '/' . $diaI . '/' . $anioI;
$fFin =  $mesF . '/' . $diaF . '/' . $anioF;

//Convertir a formato yyyy-mm-dd
$dateStart = strtotime($fInicio);
$dateEnd = strtotime($fFin);
$newInicio = date('Y-m-d', $dateStart);
$newFin = date('Y-m-d', $dateEnd);

//Variables del formulario
$empresa = $_POST['empresa'];
$zona = $_POST['zona'];
$estacionamiento = $_POST['estacionamiento'];
$siglas = $caso;

//Crear Excel
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setTitle("Reporte Veepe");
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->setActiveSheetIndex(0);

// Imprimimos los encabezados
//$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'id_operacion');
$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Empresa');
//$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'id_estacionamiento');
$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Estacionamiento');
$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Dirección');
$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Zona');
//$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'id_automovil');
//$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'marca');
//$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'submarca');
//$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'modelo');
$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Placas vehículo');
$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Usuario');
$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Email');
$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Entrada');
$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Salida');
$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Monto cobrado');
$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Monto por cobrar');
$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Calificación');
$objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Tiene pensión');

//Consulta
//Tablas
$query= db_select('f_dc_operaciones', 'op');
$query->leftJoin('f_dc_pagos', 'p', 'op.id_operacion = p.id_operacion');
$query->join('f_dc_estacionamientos', 'es', 'op.id_estacionamiento = es.id_estacionamiento');
$query->join('f_dc_empresas', 'em', 'em.id_empresa = es.id_empresa');
$query->join('f_dc_automoviles', 'au', 'op.id_auto = au.id_automovil');
$query->join('f_dc_perfilusuario', 'pu', 'pu.uid = op.id_usuario');

//Campos
//$query->fields('op',array('id_operacion'));
$query->fields('em',array('nombre_empresa'));
$query->fields('es',array('id_estacionamiento','nombre','direccion','zona'));
$query->fields('au',array('id_automovil','marca','submarca','modelo','placa'));
$query->fields('op',array('hora_entrada','hora_salida', 'rating'));
$query->fields('p',array('monto'));
$query->addField('op', 'monto', 'monto_teorico');
$query->addField('pu', 'nombre', 'nombre_usuario');
$query->fields('pu',array('apellido_paterno', 'email', 'uid'));

//Condiciones
if($empresa != '')
{
	$query->where("em.id_empresa = " . $empresa);
}
if($zona != '')
{
	$query->where("es.zona  = '" . $zona . "'");
}
if($estacionamiento != '')
{
	$query->where("es.id_estacionamiento  = '" . $estacionamiento . "'");
}
$query->where("hora_entrada BETWEEN '" . $newInicio . " 00:00:00' AND '" . $newFin . " 23:59:59'");
$query->where("hora_salida BETWEEN '" . $newInicio . " 00:00:00' AND '" . $newFin . " 23:59:59'");
$query->where("op.status >=10");
$query->where("p.tipo_pago = 1");

$query->where("p.tipo_pago = 1");

// Ordenamiento
$query->orderBy('hora_entrada', 'DESC');

//Crear consulta
$query->execute()->fetchObject();

//Ejectuar consulta
@$result = db_query($query)->fetchAll();

// Ponemos los registros
$i = 0; $c = 2;
foreach ($result as $row) {
	//$objPHPExcel->getActiveSheet()->SetCellValue('A' . $c, $row->id_operacion);
	$objPHPExcel->getActiveSheet()->SetCellValue('A' . $c, $row->nombre_empresa);
	//$objPHPExcel->getActiveSheet()->SetCellValue('B' . $c, $row->id_estacionamiento);
	$objPHPExcel->getActiveSheet()->SetCellValue('B' . $c, $row->nombre);
	$objPHPExcel->getActiveSheet()->SetCellValue('C' . $c, $row->direccion);
	$objPHPExcel->getActiveSheet()->SetCellValue('D' . $c, $row->zona);
	//$objPHPExcel->getActiveSheet()->SetCellValue('F' . $c, $row->id_automovil);
	//$objPHPExcel->getActiveSheet()->SetCellValue('G' . $c, $row->marca);
	//$objPHPExcel->getActiveSheet()->SetCellValue('H' . $c, $row->submarca);
	//$objPHPExcel->getActiveSheet()->SetCellValue('U' . $c, $row->modelo);
	$objPHPExcel->getActiveSheet()->SetCellValue('E' . $c, $row->placa);
	$objPHPExcel->getActiveSheet()->SetCellValue('F' . $c, $row->nombre_usuario." ".$row->apellido_paterno);
	$objPHPExcel->getActiveSheet()->SetCellValue('G' . $c, $row->email);
	$objPHPExcel->getActiveSheet()->SetCellValue('H' . $c, $row->hora_entrada);
	$objPHPExcel->getActiveSheet()->SetCellValue('I' . $c, $row->hora_salida);
	$objPHPExcel->getActiveSheet()->SetCellValue('J' . $c, $row->monto);
	if ($row->monto==null)
		$objPHPExcel->getActiveSheet()->SetCellValue('K' . $c, $row->monto_teorico);
	$objPHPExcel->getActiveSheet()->SetCellValue('L' . $c, $row->rating);
	
	//Consulta pensiones
	$pensiones= db_select('f_dc_pensionados', 'pe');
	$pensiones->fields('pe',array('id_pensionado'));
	$pensiones->where("pe.id_usuario = " . $row->uid);
	$pensiones->where("pe.status = " . "1");
	$pensiones->execute()->fetchObject();
	$pensiones = db_query($pensiones)->fetchAll();
		
	if(count($pensiones) > 0){
		$objPHPExcel->getActiveSheet()->SetCellValue('M' . $c, "Sí");
	}else{
		$objPHPExcel->getActiveSheet()->SetCellValue('M' . $c, "No");
	}
	
	
	$i++; $c++;
}

// Rename sheet
$fileName = date("Y") . date("m") . date("d") . "_reporte_" . $siglas . ".xlsx"; // str_replace('.php', '.xlsx', __FILE__);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

// configuramos los headers
header('Content-type: application/vnd.ms-excel');	// We'll be outputting an excel file
header('Content-Disposition: attachment; filename="'.$fileName.'"');	// setting name

$objWriter->save('php://output');	// Write file to the browser
//$objWriter->save($fileName);
//echo "<script>window.location='" . $fileName . "';</script>";
//echo "<script>this.close();</script>";


exit;

?>

