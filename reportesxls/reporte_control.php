<?php
include_once("../sites/default/settings.php");

/* ----------------------------------------------------
  Variables globales usadas por Funktionell
  ---------------------------------------------------- */
global $base_path;

/* ----------------------------------------------------
		 Cargamos el bootstrap de Drupal
   ---------------------------------------------------- */
$currdir=getcwd();
chdir($base_path);
define('DRUPAL_ROOT', getcwd());
require_once("./includes/bootstrap.inc");
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
chdir($currdir);

//Fechas de inicio y fin
$fechaIni = new DateTime($_POST["fechaIni"]);
$fechaFin = new DateTime($_POST['fechaFin']);

$diasSemana = array("D", "L", "M", "M", "J", "V", "S");

//Variables del formulario
//$empresa = $_POST['empresa'];
$zona = $_POST['zona'];
$estacionamiento = $_POST['estacionamiento'];

//Crear Excel
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setTitle("Reporte Control de personal");
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->setActiveSheetIndex(0);

// Cálculo de información
$diasPeriodo = $fechaFin->diff($fechaIni)->format("%a");

$listaUsuarios = db_query("select p.uid as uid, p.nombre as nombre, p.apellido_paterno as ap, p.apellido_materno as am, p.email as email, sum(a.asistio) as asistencias, a.turno 
							from f_dc_perfilusuario p inner join f_r_asistencia a on p.uid=a.id_usuario 
							where a.id_estacionamiento=".$estacionamiento." and a.fecha >= '".$_POST["fechaIni"]." 00:00:00' and a.fecha <= '".$_POST["fechaFin"]." 23:59:59' 
							group by a.id_usuario, p.nombre, p.apellido_paterno, p.apellido_materno, p.email")->fetchAll();

// Imprimimos los encabezados
$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Reporte de asistencia del '.$_POST["fechaIni"]." al ".$_POST["fechaFin"]);

$objPHPExcel->getActiveSheet()->SetCellValue('A2', 'Nombre');
$objPHPExcel->getActiveSheet()->SetCellValue('B2', 'Email');
$objPHPExcel->getActiveSheet()->SetCellValue('C2', 'Asistencias');
$objPHPExcel->getActiveSheet()->SetCellValue('D2', 'Turno');

// Imprimimos cada día
$fechaAux = new DateTime($_POST["fechaIni"]);
$aux = array();
$i = 1;
do {
	$aux[] = $diasSemana[$fechaAux->format("w")]." ".$fechaAux->format("d");
	$fechaAux->add(new DateInterval('P1D'));
	$i++;
} while($i <= $diasPeriodo);

$objPHPExcel->getActiveSheet()->fromArray($aux, NULL, "D2");

$row = 3;
foreach($listaUsuarios as $usuario)
{
	if ( $usuario->uid!=null )
	{
		$objPHPExcel->getActiveSheet()->SetCellValue("A".$row, $usuario->nombre." ".$usuario->ap." ".$usuario->am);
		$objPHPExcel->getActiveSheet()->SetCellValue("B".$row, $usuario->email);
		$objPHPExcel->getActiveSheet()->SetCellValue("C".$row, $usuario->asistencias);
		
		$turno = "";
		if($usuario->asistencias == "0"){
			$turno = "Matutino";
		}else if($usuario->asistencias == "1"){
			$turno = "Vespertino";
		}else if($usuario->asistencias == "2"){
			$turno = "Nocturno";
		}
		
		$objPHPExcel->getActiveSheet()->SetCellValue("D".$row, $turno);
		
		if ( $usuario->asistencias > 0 )
		{
			$listaAsistencias = db_query("select fecha, asistio from f_r_asistencia where id_usuario=".($usuario->uid)." and fecha >= '".$_POST["fechaIni"]." 00:00:00' and fecha <= '".$_POST["fechaFin"]." 23:59:59'")->fetchAll();
			foreach($listaAsistencias as $asistencia)
			{
				$auxDD = new DateTime($asistencia->fecha);
				$col = intval($fechaIni->diff($auxDD)->format("%a"));
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4+$col, $row, $fechaIni->diff($auxDD)->format("%a") /*$asistencia->asistio*/);
			}
		}
		
		$objPHPExcel->getActiveSheet()->SetCellValue("D".$row, $usuario->asistencias);
		$row++;
	}
}

// Rename sheet
$fileName = date("Y") . date("m") . date("d") . "_reporte_asistencias.xlsx"; // str_replace('.php', '.xlsx', __FILE__);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

// configuramos los headers
header('Content-type: application/vnd.ms-excel');	// We'll be outputting an excel file
header('Content-Disposition: attachment; filename="'.$fileName.'"');	// setting name

$objWriter->save('php://output');	// Write file to the browser
exit;
?>
