<?php
?>

<style>
  body footer {
    display: none;
  }
</style>

<div class="home">

  <div class="centerme">

    <!--Perfil _____________________________________________________________ -->
    <div class="row" style="text-align: center; padding: 8px 0px;">

      <!--Foto-->
      <div class="row">
        <div class="servicio_activo">
            <div class="img_reserva"></div>
        </div>
      </div><!-- end row-->

      <!--Nombre del usuario-->
      <div class="row">
        <h1 class="white" style="margin: 0px;">Activo</h1>
      </div><!-- end row-->

    </div><!-- end row-->

    <!--Datos del usuario __________________________________________________ -->
    <div class="row caja_perfil" style="margin: 16px 0px; padding-top: 24px; padding-bottom: 24px;" >
			<div class="col-md-6">
        <label>Nombre Servicio</label><span class="body3_B">Lavado</span>
      </div>
      <div class="col-md-6">
        <label>Tarifa</label><span class="body3_B">$80.00</span>
      </div>
			<div class="col-md-12" style="margin-bottom:16px;">
				<label style="padding-bottom: 0px;">Descripción</label><br>
				<span class="body3_B">
					Ex vis esse vitae appareat. Eu noluisse mediocrem philosophia mei,
					ad nisl melius doctus est. Explicari vulputate at vis, erant vocent
					assueverit mea te. Sea aperiri tractatos id. Cu qui unum ignota aperir
				</span>
			</div>
			<div class="col-md-12" style="margin-bottom:16px;">
				<label style="padding-bottom: 0px;">Términos y condiciones</label><br>
				<span class="body3_B">
					Saepe delicata scriptorem ea mei, in dictas consetetur definitionem
					eam. Nibh tempor ne duo. Nec delectus repudiandae cu, pro in abhorrean
					elaboraret, nisl percipitur no sit. Eum inimicus intellegam honestatis
					ea, sea et corpora convenire. Id vel luptatum explicari omittantur,
					cu ius etiam errem. Ut postea graeco repudiare eam, delicata disputati
					ne pri, ad sea ridens fabulas veritus.

					Ex vis esse vitae appareat. Eu noluisse mediocrem philosophia mei, ad
					nisl melius doctus est. Explicari vulputate at vis, erant vocent assue
					verit mea te. Sea aperiri tractatos id. Cu qui unum ignota aperiri.
				</span>
			</div>

    </div><!-- end row-->

		<!--Boton-->
		<div class="col-md-12" style="text-align: center; padding: 12px 0px 24px 0px;">
			<a href="" class="B_Regular_N">EDITAR</a>
		</div>

  </div><!-- end centerme-->
</div><!-- end #home-->
