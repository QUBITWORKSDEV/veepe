<?php
?>

<style>
  body footer {
    display: none;
  }
</style>

<div class="home">

  <div class="centerme">

    <!--Perfil _____________________________________________________________ -->
    <div class="row" style="text-align: center; padding: 8px 0px;">

      <!--Foto-->
      <div class="row">
        <div class="img_usuario_i"></div>
      </div><!-- end row-->

      <!--Nombre del usuario-->
      <div class="row">
        <h1 class="white">Nancy Potter</h1>
				<h2 class="white">Administrador General VEEPE</h2>
      </div><!-- end row-->

      <!--VeepeID-->
      <div class="row">
        <span><h4 class="white">VEEPE ID: </h4></span>
        <span><h4 class="blue">
          VEEPEID
        </h4></span>
      </div><!-- end row-->

			<!--Correo-->
      <div class="row">
          <span class="body3_A">dueno@veepe.com</span>
      </div><!-- end row-->

    </div><!-- end row-->

    <!--Datos del usuario __________________________________________________ -->
    <div class="row caja_perfil" style="margin: 16px 0px; padding-top: 24px; padding-bottom: 24px;" >
      <div class="col-md-6">
        <label>Lugar de Trabajo</label><span class="body3_B">VEEPE</span>
      </div>
      <div class="col-md-6">
        <label>Nombre</label><span class="body3_B">Nancy Potter Rubio</span>
      </div>
      <div class="col-md-6">
        <label>Correo</label><span class="body3_B">nancy_potter@veeepe.com</span>
      </div>
      <div class="col-md-6">
        <label>Puesto</label><span class="body3_B">Administrador General VEEEP</span>
      </div>
      <div class="col-md-6">
        <label>Celular</label><span class="body3_B">55 55 333  4444</span>
      </div>
      <div class="col-md-6">
        <label>Teléfono:</label><span class="body3_B">5 333 4444</span>
      </div>
      <div class="col-md-12">
        <label>Dirección</label><span class="body3_B">9676 Little Mount Apt. 463</span>
      </div>
    </div><!-- end row-->

		<!--Boton-->
		<div class="col-md-12" style="text-align: center; padding: 12px 0px 24px 0px;">
			<a href="" class="B_Regular_N">EDITAR</a>
		</div>

  </div><!-- end centerme-->
</div><!-- end #home-->
