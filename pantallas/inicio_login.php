<script src="pantallas/js/inicio_login.js"></script>

<div class="container">
	<div class="centerme">

		<!--Fomulario "formaDark" _____________________________________________________________________ -->
		<div id="formLogin" class="formaDark">

			<!--Logo Veepe-->
			<div class="row">
				<div class="logo_veepe_login" style="margin-bottom: 39px"></div>
			</div><!-- end row-->

			<!--Correo electrónico-->
			<div class="row row_validador">
				<div class="requerido">
					<input id="email" type="email" placeholder="Correo electrónico" style="text-align: center;" data-valida="requerido,email">
				</div>
			</div><!-- end row-->

			<!--Contraseña-->
			<div class="row row_validador">
				<div class="requerido">
					<input id="password" type="password" placeholder="Contraseña" style="text-align: center;" data-valida="requerido,password">
				</div>
			</div><!-- end row-->

			<!-- Button-->
			<div class="row" style="display: block; margin-top: 16px; margin-bottom: 24px;">
				<?php if (false) { ?>
				<!--Checkbox-->
				<div class="pull-left">
					<input id="input_checkbox1" type="checkbox"/><label><span class="checkbox" id="span_checkbox1" ></span>Recordarme</label>
				</div>
				<?php } ?>
				<!-- Button -->
				<div class="pull-right">
					<button id="ingresar" type="button" class="B_Regular_N">INICIAR SESIÓN</button>
				</div>
			</div><!-- end row-->

			<!--Olvidar Contraseña-->
			<div class="row" style="margin-bottom: 8px; text-align: center;">
				<a	class="hyperlink" href="inicio_recuperar">¿Olvidaste tu nombre de usuario o contraseña?</a>
			</div><!-- end row-->

			<!--Registro-->
			<div class="row" style="text-align: center;">
				<span id="loginTxt">¿No tienes cuenta? </span><span><a class="hyperlink" href="registrar">Regístrate</a></span>
			</div><!-- end row-->
		</div><!-- end .formDark-->

	</div><!-- end .centerme-->
</div><!-- end row-->
