<?php
global $user;

/*if(esSocio() || esSupervisor())
{
	
	$zonas = webGetZonasXCliente($clientes[0]->id_empresa);
	$estacionamientos = webGetEstacionamientosXCliente($clientes[0]->id_empresa);
}*/
$cliente = webGetEmpresaSocia($user->uid);
echo "<script>var ide=".$cliente[0]->id_empresa.";</script>";
?>

<style>
	body footer {
		display: none;
	}
</style>

<script src="pantallas/js/panel_control.js" type="text/javascript"></script>

<div class="home">
	<div class="centerme">
		<center><h2 style="color: white; font-weight: 100">Reporte de control de personal</h2></center>
		<br><br>
		<form id="frmGenerarReporte" class="formaDark" action="reportesxls/reporte_control.php" method="post" onsubmit="return validar_fechas()" target="_blank" style="width: 840px">

			<div class="row">
				<!-- Zona __________________________________________________________ -->
				<div id="div_zona" class="col-md-12 row_validador">
					<div class="requerido">
					<select id="select_zona" name="zona" onchange="dochange_zona();">

						<?php if (esAgv() || esAva()) { ?>
							<option value="">Zonas</option>
						<?php } ?>

						<?php if(esSocio() || esSupervisor()) { ?>
							<option value="-1">Todos las zonas</option>
							<?php foreach ($zonas as $zona) { ?>
								<option value="<?php echo $zona->zona; ?>"><?php echo $zona->zona; ?></option>
							<?php } ?>
						<?php } ?>

						</select>
					<div class="marca"></div>
					</div>
				</div>

				<!-- Estacionamiento _______________________________________________ -->
				<div id="div_estacionamiento" class="col-md-12 row_validador">
					<div class="requerido">
					<select id="select_estacionamiento" name="estacionamiento"	>

						<?php if (esAgv() || esAva()) { ?>
							<option value="">Estacionamientos</option>
						<?php } ?>

						<?php if(esSocio() || esSupervisor()) { ?>
							<!--<option value="-1">Todos los estacionamientos</option>-->
							<?php foreach ($estacionamientos as $estacionamiento) { ?>
								<option value="<?php echo $estacionamiento->id_estacionamiento; ?>"><?php echo $estacionamiento->nombre; ?></option>
							<?php } ?>
						<?php } ?>

					</select>
					<div class="marca"></div>
					</div>
				</div>

				<!-- Fecha inicio	_________________________________________________ -->
				<div class="col-md-6 row_validador">
					<div class="requerido">
						<input id="fechaIni" type="text" name="fechaIni" class="" placeholder="Fecha inicio" data-valida="requerido">
						<div class="marca"></div>
					</div>
				</div>
				<!-- Fecha fin	____________________________________________________ -->
				<div class="col-md-6 row_validador">
					<div class="requerido">
						<input id="fechaFin" type="text" name="fechaFin" class="" placeholder="Fecha final" data-valida="requerido">
						<div class="marca"></div>
					</div>
				</div>
			</div><!-- end row-->

			<!--Boton ____________________________________________________________ -->
			<div class="row">
				<div class="col-md-12" style="text-align: center; padding: 12px 0px 24px 0px;">
					<button id="btn_reporte" type="submit" class="B_Regular_N" >GENERAR REPORTE</button>
				</div>
			</div><!-- end row-->
		</form><!-- end formaDark-->

	</div><!-- end centerme-->
</div><!-- end #home-->
