<?php
	global $user;
	$datosPerfil = getPerfil($user->uid);
?>

<script src="pantallas/js/panel_perfil_editar.js" type="text/javascript"></script>

<script>
	btn_regresar("./perfil");
</script>

<style>
	body
	{
		background-image:none;
	}
</style>

<div>
	<div class="row" style="margin-bottom: 20px;">
		<center>
			<!--Foto-->
			<div class="img_usuario_n"></div>

			<!--Nombre del usuario-->
			<div class="row">
				<h1 class="white"><?php echo $datosPerfil[0]->nombre . " " . $datosPerfil[0]->apellido_paterno . " " . $datosPerfil[0]->apellido_materno; ?></h1>
				<h2 class="gray"><?php echo getNombreRol() ?></h2>
			</div><!-- end row-->

			<!--Correo-->
			<div class="row">
				<span class="body3_A"><?php echo $datosPerfil[0]->nombre_usuario; ?></span>
			</div><!-- end row-->
			
		</center>
	</div>


	<div id="frmPerfilEditar" class="formaDark" style="max-width: 848px; margin: 0 auto;">

		<div class="row">
			<div class="col-md-6">
				<div class="requerido">
					<input id="txtNombre" type="text" placeholder="Nombre" value="<?php echo $datosPerfil[0]->nombre; ?>" data-valida="requerido" />
					<div class="marca"></div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="requerido">
					<input id="txtApellidoPaterno" type="text" placeholder="Apellido paterno" value="<?php echo $datosPerfil[0]->apellido_paterno; ?>" data-valida="requerido"/>
					<div class="marca"></div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="">
					<input id="txtApellidoMaterno"  type="text" placeholder="Apellido materno" value="<?php echo $datosPerfil[0]->apellido_materno; ?>"/>
					<div class="marca"></div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="">
					<input id="txtEmail" disabled="disabled" type="text" placeholder="Correo Electrónico" value="<?php echo $datosPerfil[0]->email; ?>"  />
					<div class="marca"></div>
				</div>
			</div>
		</div>


		<div class="row">
			<div class="col-md-6">
				<div class="requerido">
					<input id="txtCelular" type="text" placeholder="Celular" value="<?php echo $datosPerfil[0]->celular; ?>" data-valida="requerido" />
					<div class="marca"></div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="">
					<input id="txtContrasenaActual" type="text" placeholder="Contraseña Actual" />
					<div class="marca"></div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="">
					<input id="txtContrasenaNueva" type="text" placeholder="Contraseña Nueva" />
					<div class="marca"></div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="">
					<input id="txtContrasenaNueva2" type="text" placeholder="Reingresar Contraseña" />
					<div class="marca"></div>
				</div>
			</div>
		</div>


		<div class="row" style="margin: 12px 0px;">
			<div class="col-md-6">
				<div id="cancelar" class="pull-right">
					<a href="javascript:cancelarEditarPerfil();" class="BB_Fantasma_N">CANCELAR</a>
				</div>
			</div>

			<div class="col-md-6">
				<button id="cmdModificar" type="button" class="B_Regular_N pull-left">MODIFICAR</button>
			</div>
		</div>

	</div>
</div>
