<?php
if (isset($_GET['id_pensionado'])) {
	$id_pensionado = $_GET['id_pensionado'];
	$id_usuario = db_query("select id_usuario from f_dc_pensionados where id_pensionado =" . $id_pensionado)->fetchField();
	$datosPerfil = getPerfil($id_usuario);

	$pensionCompartida = db_query("select tipo_pension from f_dc_pensionados where id_usuario=" . $id_usuario . " and tipo_pension = 3")->fetchField();
	$pensionNormal = db_query("select tipo_pension from f_dc_pensionados where id_usuario=" . $id_usuario . " and tipo_pension = 1")->fetchField();

	$estacionamientosPensionCompartida = db_query("select 
	e.nombre,
	case pe.horario
	when 1 then 'DIA'
	when 3 then 'NOCHE'
	when 5 then 'TODO EL DIA'
	end as horario,
	pe.costo,
	um.nombre_cuenta,
	pe.fecha_contratacion,
	pe.id_pensionado,
	pe.id_compartida,
	pe.status

	from f_dc_pensionados pe 
	inner join f_dc_pensiones p on p.id_pension = pe.id_pension
	inner join f_dc_estacionamientos e on e.id_estacionamiento = p.id_estacionamiento
	inner join f_dc_metodospago mp on mp.id_metodospago = pe.id_metodopago
	inner join f_r_usuario_metodopago um on um.id_metodopago = mp.id_metodospago
	where pe.id_usuario =" . $id_usuario . " and pe.tipo_pension = 3 and (pe.status = 1 or pe.status = 7) group by pe.id_compartida order by pe.fecha_contratacion desc")->fetchAll();

	$estacionamientosPensionNormal = db_query("select 
	e.nombre,
	case pe.horario
	when 1 then 'DIA'
	when 3 then 'NOCHE'
	when 5 then 'TODO EL DIA'
	end as horario,
	pe.costo,
	um.nombre_cuenta,
	pe.fecha_contratacion,
	p.periodo as mesesContratados,
	pe.id_pensionado,
	pe.status

	from f_dc_pensionados pe 
	inner join f_dc_pensiones p on p.id_pension = pe.id_pension
	inner join f_dc_estacionamientos e on e.id_estacionamiento = p.id_estacionamiento
	inner join f_dc_metodospago mp on mp.id_metodospago = pe.id_metodopago
	inner join f_r_usuario_metodopago um on um.id_metodopago = mp.id_metodospago
	where pe.id_usuario =" . $id_usuario . " and pe.tipo_pension = 1 and (pe.status = 1 or pe.status = 7) and p.status = 1 and e.status = 1 order by pe.fecha_contratacion desc")->fetchAll();
}
?>


<script>
	jQuery("#menuLateral").hide();
	btn_regresar("./pensionados");

</script>

<script src="pantallas/js/panel_pensionados_detalle.js"></script>

<style>
	.pensionCompartida > thead > tr > th{
		font-family: 'Open Sans', sans-serif;
		font-size: 12px;
		color: #A3A6A4;
		letter-spacing: 0px;
		line-height: 16px;
		text-align: center;
	}


	.pensionCompartida > tbody > tr > td{
		height: 40px;
	}


	.pensionNormal > thead > tr > th{
		font-family: 'Open Sans', sans-serif;
		font-size: 12px;
		color: #A3A6A4;
		letter-spacing: 0px;
		line-height: 16px;
		text-align: center;
	}

	.pensionNormal > tbody > tr > td{
		height: 40px;
	}

	.textoBlanco{
		font-family: 'Open Sans', sans-serif;
		font-size: 14px;
		color: #FFFFFF;
		letter-spacing: 0px;
		line-height: 16px;
	}

	.textoAzul{
		font-family: 'Open Sans', sans-serif;
		font-size: 14px;
		color: #00BFD6;
		letter-spacing: 0px;
		line-height: 16px;
		padding: 0px;
	}

	.tablaTitulo > tr > td{
		height: 56px;
	}

</style>


<div id="divContenedorPerfil" style="padding:0px; position: relative; text-align: center; margin: 0 0 0 43px;">



	<div class="text-center">
<!--		<img src="http://foxyhare.com/assets/images/test-user.png" alt="profile picture" class="img-circle text-center" style="width:104px; height: 104px; margin-top: 64px;">
			<h2><?php
		if ($datosPerfil != null) {
			echo $datosPerfil[0]->nombre . " " . $datosPerfil[0]->apellido_paterno . " " . $datosPerfil[0]->apellido_materno;
		}
		?></h2>

		<!--Foto-->
		<div class="row" style="margin-top: 64px;">
			<div class="img_usuario_i"></div>
		</div><!-- end row-->

		<!--Nombre del usuario-->
		<div class="row">
			<h1 class="white"><?php echo $datosPerfil[0]->nombre . " " . $datosPerfil[0]->apellido_paterno . " " . $datosPerfil[0]->apellido_materno; ?></h1>
			<!--<h2 class="white"><?php /* echo getNombreRol($user-roles[0]->rid); */ ?></h2>-->
		</div><!-- end row-->

		<!--Correo-->
		<div class="row">
			<span class="body3_A"><?php echo $datosPerfil[0]->nombre_usuario; ?></span>
		</div><!-- end row-->
	</div>


	<div>

		<div style="background-color:#00BFD6;  height: 32px;" class="row caja_perfil">
			<label>PERFIL</label>
		</div>

		<div id="perfilSinEditar"  class="text-center" style=" text-align: left; ">

			<div class="row caja_perfil" style="margin: auto; padding-top: 24px; padding-bottom: 24px;" >
				<div class="col-md-6">
					<label>Nombre</label><span class="body3_B"><?php echo $datosPerfil[0]->nombre . " " . $datosPerfil[0]->apellido_paterno . " " . $datosPerfil[0]->apellido_materno ?></span>
				</div>
				<div class="col-md-6">
					<label>Correo</label><span class="body3_B"><?php echo $datosPerfil[0]->email; ?></span>
				</div>
				<div class="col-md-6">
					<label>Celular</label><span class="body3_B"><?php echo $datosPerfil[0]->celular; ?></span>
				</div>
			</div>

		</div>

		<div style="background-color:#00BFD6;  height: 32px;" class="row caja_perfil">
			<label>SERVICIOS CONTRATADOS</label>
		</div>


		<?php
		if ($pensionCompartida != null || $pensionNormal != null) {


			if ($pensionCompartida != null) {

				foreach ($estacionamientosPensionCompartida as $pensionCompartida) {

					$estacionamientosCompartida = db_query("select 
					e.nombre,
					case pe.horario
					when 1 then 'DIA'
					when 3 then 'NOCHE'
					when 5 then 'TODO EL DIA'
					end as horario,
					pe.costo,
					um.nombre_cuenta,
					pe.fecha_contratacion,
					pe.id_pensionado,
					pe.id_compartida,
					p.periodo as mesesContratados,
					pe.status

					from f_dc_pensionados pe 
					inner join f_dc_pensiones p on p.id_pension = pe.id_pension
					inner join f_dc_estacionamientos e on e.id_estacionamiento = p.id_estacionamiento
					inner join f_dc_metodospago mp on mp.id_metodospago = pe.id_metodopago
					inner join f_r_usuario_metodopago um on um.id_metodopago = mp.id_metodospago
					where pe.id_usuario =" . $id_usuario . " and pe.id_compartida ='" . $pensionCompartida->id_compartida . "'")->fetchAll();


					$idPensiones = "(";

					$ultimoElemento = end($estacionamientosCompartida);
					foreach ($estacionamientosCompartida as $pensionCompartida) {

						if ($pensionCompartida == $ultimoElemento) {
							$idPensiones .= "id_operacion= " . $pensionCompartida->id_pensionado;
						} else {
							$idPensiones .= "id_operacion= " . $pensionCompartida->id_pensionado . "  or  ";
						}
					}

					$idPensiones .=")";

					$pagosCompartida = db_query("select * from f_dc_pagos_programados where " . $idPensiones . "  and id_usuario=" . $id_usuario . " and tipo = 3 and status != -1")->fetchAll();
					$pagosPagadosCompartida = db_query("select count(*) from f_dc_pagos_programados where  " . $idPensiones . " and id_usuario=" . $id_usuario . " and tipo = 3 and status = 3")->fetchField();

					if ((count($pagosCompartida) / 2) < 1) {
						$mesesPagadosCompartida = 1;
						$mesesPorCobrarCompartida = 0;
					} else {
						$mesesPagadosCompartida = 1 + $pagosPagadosCompartida;
						$mesesPorCobrarCompartida = (count($pagosCompartida) / 2) - $pagosPagadosCompartida;
					}
					?>
					<div class="row caja_perfil" style="margin: auto; padding-top: 24px; padding-bottom: 24px;" >

						<div style="background-color:#00BFD6;  height: 32px;" class="">

							<?php
							if ($pensionCompartida->status == 1) {
								?>

								<label style=" font-size: 12px; color: #FFFFFF; letter-spacing: 0px; line-height: 12px;">PENSIÓN COMPARTIDA</label>

								<?php
							} else if ($pensionCompartida->status == 7) {
								?>

								<label style=" font-size: 12px; color: #FFFFFF; letter-spacing: 0px; line-height: 12px;">PENSIÓN COMPARTIDA  (parcialmente cancelada)</label>
								<?php
							}
							?>



						</div>

						<div style="background-color: #090A19;">

							<table class="tablaTitulo" style="width: 100%;">
								<tr>
									<td><label class="textoBlanco">Tipo de pension</label><br><label class="textoAzul">Pensión compartida</label></td>
									<td><label class="textoBlanco">Forma de pago</label><br><label class="textoAzul"><?php echo $pensionCompartida->nombre_cuenta ?></label></td>
									<td><label class="textoBlanco">Fecha de contratación</label><br><label class="textoAzul"><?php echo $pensionCompartida->fecha_contratacion ?></label></td>
								</tr>

								<tr>
									<td><label class="textoBlanco">Meses contratados</label><br><label class="textoAzul"><?php echo $pensionCompartida->mesesContratados ?></label></td>
			<!--									<td><label class="textoBlanco">Horario</label><br><label class="textoAzul"><?php echo $pensionCompartida->horario ?></label></td>-->
									<td><label class="textoBlanco">Meses pagados</label><br><label class="textoAzul"><?php echo $mesesPagadosCompartida ?></label></td>
									<td><label class="textoBlanco">Meses por cobrar</label><br><label class="textoAzul"><?php echo $mesesPorCobrarCompartida ?></label></td>
								</tr>

								<tr>


									<td>
										<label class="textoBlanco">Total</label><br><label class="textoAzul">
											<?php
											$aux = 0.0;
											foreach ($estacionamientosCompartida as $pensionCompartida) {
												$aux = $aux + $pensionCompartida->costo * $pensionCompartida->mesesContratados;
											}
											echo $aux;
											?>
										</label>
									</td>
								</tr>

							</table>

						</div>

						<div>
							<table style="width: 100%;" class="pensionCompartida">
								<thead style="background-color: #2C2A2D;">
									<tr style="height: 48px;">
										<th>ESTACIONAMIENTO</th>
										<th>HORARIO</th>
										<th>COSTO HORARIO</th>
			<!--										<th>TOTAL</th>-->
									</tr>
								</thead>

								<tbody style="background-color: #E8EAE9;">

									<?php
									foreach ($estacionamientosCompartida as $pensionCompartida) {
										echo "<tr>";
										echo "<td>" . $pensionCompartida->nombre . "</td>";
										echo "<td>" . $pensionCompartida->horario . "</td>";
										echo "<td>" . $pensionCompartida->costo . "</td>";
//										echo "<td>" . $pensionCompartida->costo * $pensionCompartida->mesesContratados . "</td>";
										echo "</tr>";
									}
									?>

								</tbody>
							</table>
						</div>

						<div style="background-color:#090A19; height: 64px; text-align: center; padding: 12px 0px 24px 0px;">
							<?php
							if ($pensionCompartida->status == 1) {
								?>
								<button  title="CANCELAR CONTRATO" onclick="cancelarContrato(<?php echo ' \'' . $pensionCompartida->id_compartida . '\',\'' . 3 . '\' ' ?>);" class="BB_Fantasma_N">CANCELAR CONTRATO</button>
								<?php
							}
							?>
						</div>

					</div>
					<?php
				}
			}
			?>



			<?php
			if ($pensionNormal != null) {

				foreach ($estacionamientosPensionNormal as $pensionNormal) {

					$pagos = db_query("select * from f_dc_pagos_programados where id_operacion =" . $pensionNormal->id_pensionado . " and id_usuario=" . $id_usuario . " and tipo = 3 and status != -1")->fetchAll();
					$pagosPagados = db_query("select count(*) from f_dc_pagos_programados where id_operacion =" . $pensionNormal->id_pensionado . " and id_usuario=" . $id_usuario . " and tipo = 3 and status = 3")->fetchField();

					if (count($pagos) < 1) {
						$mesesPagados = 1;
						$mesesPorCobrar = 0;
					} else {
						$mesesPagados = 1 + $pagosPagados;
						$mesesPorCobrar = count($pagos) - $pagosPagados;
					}
					?>
					<div class="row caja_perfil" style="margin: auto; padding-top: 24px; padding-bottom: 24px;" >

						<div style="background-color:#00BFD6;  height: 32px;" class="">

							<?php
							if ($pensionNormal->status == 1) {
								?>

								<label style=" font-size: 12px; color: #FFFFFF; letter-spacing: 0px; line-height: 12px;">PENSIÓN NORMAL</label>

								<?php
							} else {
								?>
								<label style=" font-size: 12px; color: #FFFFFF; letter-spacing: 0px; line-height: 12px;">PENSIÓN NORMAL  (parcialmente cancelada)</label>
								<?php
							}
							?>
						</div>

						<div style="background-color: #090A19;">
							<table class="tablaTitulo" style="width: 100%;">
								<tr>
									<td><label class="textoBlanco">Tipo de pension</label><br><label class="textoAzul">Pensión Normal</label></td>
									<td><label class="textoBlanco">Forma de pago</label><br><label class="textoAzul"><?php echo $pensionNormal->nombre_cuenta ?></label></td>
									<td><label class="textoBlanco">Fecha de contratación</label><br><label class="textoAzul"><?php echo $pensionNormal->fecha_contratacion ?></label></td>
								</tr>

								<tr>
									<td><label class="textoBlanco">Meses contratados</label><br><label class="textoAzul"><?php echo $pensionNormal->mesesContratados ?></label></td>
									<td><label class="textoBlanco">Horario</label><br><label class="textoAzul"><?php echo $pensionNormal->horario ?></label></td>
									<td><label class="textoBlanco">Costo mensual</label><br><label class="textoAzul"><?php echo $pensionNormal->costo ?></label></td>
								</tr>

								<tr>
									<td><label class="textoBlanco">Meses pagados</label><br><label class="textoAzul"><?php echo $mesesPagados ?></label></td>
									<td><label class="textoBlanco">Meses por cobrar</label><br><label class="textoAzul"><?php echo $mesesPorCobrar ?></label></td>
								</tr>
							</table>
						</div>

						<div>
							<table style="width: 100%;" class="pensionNormal">
								<thead style="background-color: #2C2A2D;">
									<tr style="height: 48px;">
										<th>ESTACIONAMIENTO</th>
										<th>HORARIO</th>
										<th>TOTAL</th>
									</tr>
								</thead>

								<tbody style="background-color: #E8EAE9;">

									<?php
									echo "<tr>";
									echo "<td>" . $pensionNormal->nombre . "</td>";
									echo "<td>" . $pensionNormal->horario . "</td>";
									echo "<td>" . $pensionNormal->costo * $pensionNormal->mesesContratados . "</td>";
									echo "</tr>";
									?>

								</tbody>
							</table>
						</div>

						<div style="background-color:#090A19; height: 64px; text-align: center; padding: 12px 0px 24px 0px;">
							<?php
							if ($pensionNormal->status == 1) {
								?>
								<button title="" onclick="cancelarContrato(<?php echo ' \'' . $pensionNormal->id_pensionado . '\',\'' . 1 . '\' ' ?>);" class="BB_Fantasma_N">CANCELAR CONTRATO</button>
								<?php
							}
							?>
						</div>

					</div>
					<?php
				}
			}
		} else {
			?>
			<div class="text-center">
				<h1>No existen pensiones contratadas activas</h1>
			</div>
			<?php
		}
		?>

	</div>

</div>