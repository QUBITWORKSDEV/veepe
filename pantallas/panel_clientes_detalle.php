<?php
if (isset($_GET['idcliente'])) {

	$id_cliente = $_GET['idcliente'];
	$cliente = db_query("select
					pu.*,
					e.nombre_empresa,
					e.contacto,
					e.puesto,
					e.email as emailEmpresa,
					e.direccion as direccionEmpresa,
					e.celular as celularEmpresa,
					e.telefono as telefonoEmpresa,
					date_format(FROM_UNIXTIME(us.created), '%Y-%m-%d') as fechaCreacion

					from f_dc_perfilusuario pu
					inner join users us on us.uid = pu.uid
					inner join users_roles ur on ur.uid = pu.uid
					inner join f_r_usuario_empresa ue on ue.uid = pu.uid
					inner join f_dc_empresas e on e.id_empresa = ue.id_empresa
					where ur.rid = 6 and pu.status=1 and pu.uid=" . $id_cliente)->fetchAll();

	$rol = getRolesUsuario($id_cliente);
} else {
	$cliente = null;
}
?>

<script>
	jQuery("#menuLateral").hide();

	function editarCliente(uid) {
		location.href = './alta_cliente?idcliente=' + uid;
	}
	btn_regresar("./clientes");
</script>

<style>
	body footer {
		display: none;
	}
</style>


<div class="home">

	<div class="row" style="margin-top: 24px; margin-bottom: 100px;">

		<!--Perfil _____________________________________________________________ -->
		<div class="row" style="text-align: center;">

			<!--Foto-->
			<div class="row" style="margin-top: 64px;">
				<div class="img_usuario_i"></div>
			</div><!-- end row-->

			<!--Nombre del usuario-->
			<div class="row">
				<h1 class="white"><?php echo $cliente[0]->nombre . " " . $cliente[0]->apellido_paterno . " " . $cliente[0]->apellido_materno; ?></h1>
			</div><!-- end row-->

			<!--Correo-->
			<div class="row">
				<span class="body3_A"><?php echo $cliente[0]->nombre_usuario; ?> </span>
			</div><!-- end row-->

		</div><!-- end row-->

		<!--Datos del usuario __________________________________________________ -->
		<div class="row caja_perfil" style="margin-top: 24px;">

			<div class="row" style="margin-top: 16px;">


				<div class="row" style="text-align:center">
					<h3 class="white">Datos del Cliente</h3>
				</div>

				<div class="col-md-6">
					<label>Nombre:</label><span class="body3_B"><?php echo $cliente[0]->nombre . " " . $cliente[0]->apellido_paterno . " " . $cliente[0]->apellido_materno ?></span>
				</div>

				<div class="col-md-6" style="margin-bottom:16px;">
					<label style="padding-bottom: 0px;">Email:</label><span class="body3_B"><?php echo $cliente[0]->email; ?></span><br>
				</div>

				<div class="col-md-12">
					<label>Celular:</label><span class="body3_B"><?php echo $cliente[0]->celular ?></span>
				</div>

				<div class="row" style="text-align:center">
					<h3 class="white">Datos de la empresa</h3>
				</div>

				<div class="col-md-12">
					<label>Nombre de la empresa:</label><span class="body3_B"><?php echo $cliente[0]->nombre_empresa ?></span>
				</div>

				<div class="col-md-12">
					<label>Nombre del contacto:</label><span class="body3_B"><?php echo $cliente[0]->contacto ?></span>
				</div>

				<div class="col-md-6">
					<label>Email:</label><span class="body3_B"><?php echo $cliente[0]->emailEmpresa ?></span>
				</div>

				<div class="col-md-6">
					<label>Puesto:</label><span class="body3_B"><?php echo $cliente[0]->puesto ?></span>
				</div>

				<div class="col-md-6">
					<label>Celular:</label><span class="body3_B"><?php echo $cliente[0]->celularEmpresa ?></span>
				</div>

				<div class="col-md-6">
					<label>Telefono:</label><span class="body3_B"><?php echo $cliente[0]->telefonoEmpresa ?></span>
				</div>

				<div class="col-md-12">
					<label>Dirección:</label><span class="body3_B"><?php echo $cliente[0]->direccionEmpresa ?></span>
				</div>


			</div><!-- end row-->

			<!--Boton  ___________________________________________________________ -->
			<div class="row">
				<!--Boton-->
				<div class="col-md-12" style="text-align: center; padding: 12px 0px 24px 0px;">
					<a href="javascript:editarCliente('<?php echo $cliente[0]->uid; ?>')" class="B_Regular_N" >EDITAR</a>
				</div>
			</div><!-- end row-->

		</div><!-- end row-->


	</div><!-- end centerme-->
</div><!-- end #home-->
