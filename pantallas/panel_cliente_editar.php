<?php
$idcliente = $_GET['idcliente'];
$array = detalleCliente($idcliente);
drupal_add_js('var id_cliente = '. $idcliente, 'inline');
?>

<style>
	body #navbar,
	section > h1,
	section .region .region-content ,
	body footer,
	body header {
		display: none;
	}
	body{
		background: #090A19;
	}
</style>

<script src="pantallas/js/panel_cliente_editar.js"></script>
<script>
		btn_regresar("./clientes");
</script>

<div class="row">
	<div class="caja_registro centerme">

		<div class="row" style="margin-bottom: 20px;">
			<center>
				<!--Foto-->
				<div class="row">
					<div class="logo_alta"></div>
				</div><!-- end row-->

				<!--Nombre del usuario-->
				<div class="row">
					<h1 class="white"><?php echo $array->clientes["nombreEmpresa"]; ?></h1>
				</div><!-- end row-->
			</center>
		</div><!-- end row-->

		<!--Fomulario "formaDark" _____________________________________________________________________ -->
		<div class="formaDark">

			<!--Registro ___________________________________________________________________________________ -->
			<div class="row">

				<!--Lugar ........................................................ -->
				<div class="row">
					<div class="row_validador">
						<div class="requerido">
							<input id="txt_empresa" class="input_width100" type="text" placeholder="Nombre de la empresa" value="<?php echo $array->clientes["nombreEmpresa"]; ?>" data-valida="requerido"/>
							<div class="marca"></div>
						</div>
					</div>
				</div><!-- end row-->

				<!--Nombre del contacto .......................................... -->
				<div class="row">
					<div class="row_validador">
						<div class="requerido">
							<input id="txt_nombre" class="input_width100" type="text" placeholder="Nombre del contacto" value="<?php echo $array->clientes["Contacto"]; ?>" data-valida="requerido"/>
							<div class="marca"></div>
						</div>
					</div>
				</div><!-- end row-->

				<!--Correo y puesto .............................................. -->
				<div class="row">
					<div class="col-md-6 row_validador">
						<div class="requerido">
						<input id="txt_email" type="email" placeholder="Correo electrónico" value="<?php echo $array->clientes["Correo"]; ?>" data-valida="requerido,email">
						<div class="marca"></div>
						</div>
					</div>
					<div class="col-md-6 row_validador">
						<div class="requerido">
							<input id="txt_puesto" type="text" placeholder="Puesto" value="<?php echo $array->clientes["Puesto"]; ?>" data-valida="requerido"/>
							<div class="marca"></div>
						</div>
					</div>
				</div><!-- end row-->

				<!--Celular y telefono ........................................... -->
				<div class="row">
					<div class="col-md-6 row_validador">
						<div class="requerido">
							<input id="txt_celular" type="text"	maxlength="10" placeholder="Celular" value="<?php echo $array->clientes["Celular"]; ?>" data-valida="requerido,numero"/>
							<div class="marca"></div>
						</div>
					</div>
					<div class="col-md-6 row_validador">
						<div class="requerido">
							<input id="txt_telefono" type="text"	maxlength="10" placeholder="Teléfono" value="<?php echo $array->clientes["Telefono"]; ?>" data-valida="numero"/>
						</div>
					</div>
				</div><!-- end row-->

				<!--Dirección .................................................... -->
				<div class="row" style="height: 116px;">
					<div class="row_validador">
						<div class="requerido">
							<textarea id="txt_direccion" class="input_width100" rows="6" cols="40" placeholder="Dirección" data-valida="requerido">
								<?php echo trim($array->clientes["Direccion"], ' \t\n\r\0\x0B'); ?>
							</textarea>
							<div class="marca"></div>
						</div>
					</div>
				</div><!-- end row-->

			</div><!-- end row-->

			<!--Botón Registrar .................................................. -->
			<div class="row" style="margin: 8px 0px;">
				<div class="col-md-6">
					<div class="pull-right cancelar">
						<button id="btn_cancelar" class="BB_Fantasma_N" type="button">CANCELAR</button>
					</div>
				</div>
				<div class="col-md-6">
					<div class="pull-left guardar">
						<button id="btn_cliente_alta" class="B_Regular_N" type="button">GUARDAR</button>
					</div>
				</div>
			</div><!-- end row-->

		</div><!-- end .formDark-->
	</div><!-- end .centerme-->
</div><!-- end row-->
