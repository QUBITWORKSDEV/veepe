"use strict";

function cancelarContrato(idPensionado, tipoPension) {

	notificarUsuario('', 'Confirmación', '¿De verdad deseas cancelar esta contratación?', 'SI', function () {

		CLA.encolar("ajax.php",
			{
				m: 'cancelarPension',
				id_pensionado: idPensionado,
				tipoPension: tipoPension
			},
			
			function (response) {
				response = jsoner.parse(response);

				if (response + '' == '1')
				{
					jQuery("#btn_overlay_si").html('REDIRIGIENDO...');
					location.reload();
				}
				else
				{
					jQuery('input, button, select, textarea').removeAttr('disabled');
					jQuery("#btn_overlay_si").html('ELIMINAR');
					notificarUsuario('error', 'Error', 'No se pudo realizar la acción.');
				}
			},
			
			function () {
				jQuery("#btn_overlay_si").html('ELIMINANDO...');
				jQuery('input, button, select, textarea').attr('disabled', 'disabled');
			},
			
			true,
			0.0);
	}, 'NO', function () {
		ocultarNotificacion();
	})
}

