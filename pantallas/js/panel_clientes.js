"use strict";

// _____________________________________________________________________________________________________________ VARIABLES GLOBALES

// _____________________________________________________________________________________________________________ FUNCIONES GLOBALES
function editarCliente(idcliente) {
	location.href = './alta_cliente?idcliente=' + idcliente;
}

function detalleCliente(uid) {
	location.href = './detalle_cliente?idcliente=' + uid;
}

function emailCliente(email) {
	window.location = 'mailto:' + email;
}

function eliminarCliente(uid) {
	notificarUsuario('', 'Confirmación', '¿De verdad deseas eliminar a este cliente?', 'ELIMINAR', function () {

		CLA.encolar("ajax.php",
			{
				m: 'eliminarCliente',
				uid: uid
			},
			
			function (response) {
				response = jsoner.parse(response);

				if (response+'' == '1') {
					jQuery("#btn_overlay_si").html('REDIRIGIENDO...');
					location.reload();
				} else if(response+'' == '2'){
					jQuery('button, input, textarea').removeAttr('disabled');
					jQuery("#btn_overlay_si").html('ELIMINAR');
					notificarUsuario("", "Error", "No se pudo eliminar al cliente porque en alguno de sus estacionamientos tiene operaciones pendientes.", null, null, null, null);
				} else if(response+'' == '0') {
					jQuery('button, input, textarea').removeAttr('disabled');
					jQuery("#btn_overlay_si").html('ELIMINAR');
					notificarUsuario("", "Error", "No se pudo eliminar al cliente.", null, null, null, null);
				}
			},
			
			function () {
				jQuery("#btn_overlay_si").html('ELIMINANDO...');
				jQuery('button, input, textarea').attr('disabled', 'disabled');
			},
			true,
			0.0);

	}, 'CANCELAR', function () {
		ocultarNotificacion();
	})


}

// _____________________________________________________________________________________________________________ JQUERY
jQuery(document).ready(function ()
{
	// _____________________________________________________________________ configuraciones

	// _____________________________________________________________________ entrypt
	jQuery('#tblClientes').DataTable({
		"dom": '<"toolbar"f>rtp',
		"columnDefs": [
			{"width": "170px", "targets": 6},
			{ 'bSortable': false, 'aTargets': [ 0, 6 ] }
		],
		"language": {
			"zeroRecords": "No hay información disponible",
			paginate: {
				previous: 'Anterior',
				next: 'Siguiente'
			},
			aria: {
				paginate: {
					previous: 'Anterior',
					next: 'Siguiente'
				}
			}
		},
		"pageLength": 10
	});
	jQuery('#tblClientes_wrapper .toolbar').prepend('<span class="encabezado">Clientes</span>');

	jQuery('#tblClientes_filter').prepend('<div class="lupa"></div>');
	jQuery('input[type=search]').attr("placeholder", "Buscar ...");
	jQuery('input[type=search]').css("font-weight", "600");
	jQuery('input[type=search]').css("padding-left", "24px");

});// fin del document.ready
