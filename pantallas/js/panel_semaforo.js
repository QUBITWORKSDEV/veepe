"use strict";

// _____________________________________________________________________________________________________________ VARIABLES GLOBALES
var m_Pinger = class m_Pinger {
	constructor()
	{
		this.targets = new Array();
	}
	
	addTarget(name, url)
	{
		this.targets.push({
			'name':name,
			'url':url
		});
	}
	
	checkConnectivity()
	{
		var i;
		var esto = this;
		
		for (i=0; i<this.targets.length; i++)
		{
			jQuery.ajax({
				url:		this.targets[i].url,
				type:		'GET',
				context:	this.targets[i],
				dataType:	'jsonp',
				cache:		false,
				statusCode: {
					200: function (){
						jQuery('.indicador.' + this.name).removeClass('unknown error').addClass('ok');
					},
					404: function (){
						jQuery('.indicador.' + this.name).removeClass('unknown ok').addClass('error');
					},
					500: function (){
						jQuery('.indicador.' + this.name).removeClass('unknown ok').addClass('error');
					},
				}
			});
		}
	}
	
	getNameByUrl(url)
	{
		var res = '';
		var i;
		for (i=0; i<this.targets.length; i++)
		{
			if ( this.targets[i].url == url )
				res = this.targets[i].name;
		}
		
		return res;
	}
	
}

// _____________________________________________________________________________________________________________ FUNCIONES GLOBALES

// _____________________________________________________________________________________________________________ JQUERY
jQuery(document).ready(function ()
{
	// _____________________________________________________________________ configuraciones

	// _____________________________________________________________________ entrypt
	var pinger = new m_Pinger();
	
	pinger.addTarget('banwire',	'https://cr.banwire.com');
	//pinger.addTarget('waze',	'http://world.waze.com/map_frame/');
	pinger.addTarget('facebook','http://graph.facebook.com');
	
	pinger.checkConnectivity();
	setInterval(function(){ pinger.checkConnectivity(); }, 60*1000*5);	// 5 min

});// fin del document.ready










