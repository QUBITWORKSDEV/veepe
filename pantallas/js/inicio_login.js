"use strict";

// _____________________________________________________________________________________________________________ VARIABLES GLOBALES

// _____________________________________________________________________________________________________________ FUNCIONES GLOBALES

// _____________________________________________________________________________________________________________ JQUERY
jQuery(document).ready(function ()
{
	// _____________________________________________________________________ configuraciones
	jQuery('#formLogin').formulario({
		botonEnviar:	jQuery('button#ingresar'),
		parametrosCLA:	{
			datos: {
				m:	'validarUsuarioWeb',
				u:	'jQuery("input#email").val()',
				c:	'jQuery("input#password").val()',
			},
			prefuncion: function(){
				jQuery('button#ingresar').html('VALIDANDO...');
			},
			postfuncion: function(response){
				console.log(response);
				if ( response == true )
				{
					jQuery('button#ingresar').html('REDIRIGIENDO...');
					window.location = './';
				}
				else
				{
					jQuery('button#ingresar').html('INICIAR SESIÓN');
					notificarUsuario('error', 'Error al ingresar al sistema', 'El usuario o contraseña no es válido.', 'CERRAR');
				}
			},
			delayInicial: 0.0
		},
	});


	// _____________________________________________________________________ entrypt
	jQuery('input#email').focus();

});// fin del document.ready
