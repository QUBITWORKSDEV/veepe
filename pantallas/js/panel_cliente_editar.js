"use strict";

// _____________________________________________________________________________________________________________ VARIABLES GLOBALES

// _____________________________________________________________________________________________________________ FUNCIONES GLOBALES

// _____________________________________________________________________________________________________________ JQUERY
jQuery(document).ready(function ()
{
	// _____________________________________________________________________ configuraciones
	jQuery('.formaDark').formulario({
		botonEnviar:	jQuery('button#btn_cliente_alta'),
		botonCancelar:	jQuery('button#btn_cancelar'),
		
		onCancelar:		function(){
			window.location = './clientes';
		},
		
		parametrosCLA:	{
			datos: 	{
				m:			'cambioCliente',
				idcliente:	idcliente,
				empresa: 	jQuery('#txt_empresa').val(),
				contacto: 	jQuery('#txt_nombre').val(),
				email: 		jQuery('#txt_email').val(),
				puesto: 	jQuery('#txt_puesto').val(),
				celular: 	jQuery('#txt_celular').val(),
				telefono: 	jQuery('#txt_telefono').val(),
				direccion: 	jQuery('#txt_direccion').val()
			},
			prefuncion: function(){
			},
			postfuncion: function(response){
				if ( response == true ) {
					notificarUsuario('exito', 'Modifiación exitosa', 'Los datos del cliente han sido modificados exitosamente.');
					window.location ="./detalle_cliente?idcliente="+idcliente;
				}
				else {
					notificarUsuario('exito', 'Error al modificar datos', 'No se modificaron los datos del cliente. Te agradecemos intentar la acción nuevamente.');
				}
			},
			delayInicial: 0.0
		}
	});

	// _____________________________________________________________________ entrypt
	jQuery('#txt_direccion').val(jQuery('#txt_direccion').val().trim());

});// fin del document.ready
