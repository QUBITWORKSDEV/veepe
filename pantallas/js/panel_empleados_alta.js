"use strict";
// _____________________________________________________________________________________________________________ VARIABLES GLOBALES

// _____________________________________________________________________________________________________________ FUNCIONES GLOBALES
function agregarEmpleado(op) {

	var esValido = true;
	jQuery('#frmAltaEmpleado *[data-valida]').each(function () {
		esValido &= libValida.validarCampo(jQuery(this));
	});
	if (!esValido)
	{
		return;
	}

	var param = new Object();
	param.nombre = jQuery('#frmAltaEmpleado #txtNombre').val();
	param.apellido_paterno = jQuery('#frmAltaEmpleado #txtApellidoPaterno').val();
	param.apellido_materno = jQuery('#frmAltaEmpleado #txtApellidoMaterno').val();
	param.email = jQuery('#frmAltaEmpleado #txtEmail').val();
	param.celular = jQuery('#frmAltaEmpleado #txtCelular').val();

	if (op == "E")
	{
		param.uid = uid;
		notificarUsuario('', 'Confirmación', '¿De verdad deseas modificar a este empleado?', 'MODIFICAR', function () {
			CLA.encolar("ajax.php",
				{
					m: 'altaEmpleado',
					p: param,
					op: op
				},
				
				function (response) {
					response = jsoner.parse(response);
					if (response == 1)
					{
						jQuery("#btn_overlay_si").html('REDIRIGIENDO...');
						location.href = './empleados';
					}
					else if (response == 0)
					{
						jQuery("#btn_overlay_si").html('MODIFICAR');
						jQuery('input, button').removeAttr('disabled');
						libValida.crearMensajeError(jQuery('#frmAltaEmpleado input#txtEmail'), ('Correo incorrecto'));
						notificarUsuario("", "Correo incorrecto", "Ya existe un usuario con este correo.");
					}
					else
					{
						jQuery("#btn_overlay_si").html('MODIFICAR');
						jQuery('input, button').removeAttr('disabled');
						notificarUsuario("", "Error", "No se pudo realizar la acción.");
					}
				},
				
				function () {
					jQuery("#btn_overlay_si").html('MODIFICANDO...');
					jQuery('input, button, select, textarea').attr('disabled', 'disabled');
				}, 
				true, 
				0.0);

		}, 'CANCELAR', function () {
			ocultarNotificacion();
		})
	}
	else
	{
		CLA.encolar("ajax.php",
			{
				m: 'altaEmpleado',
				p: param,
				op: op
			},
			
			function (response) {
				response = JSON.parse(response);
				if (response == 1)
				{
					jQuery("#btn_agregar_empleado").html('REDIRIGIENDO...');
					location.href = './empleados';
				}
				else if (response == 0)
				{
					jQuery("#btn_agregar_empleado").html('AGREGAR');
					jQuery('input, button').removeAttr('disabled');
					libValida.crearMensajeError(jQuery('#frmAltaEmpleado input#txtEmail'), ('Correo incorrecto'));
					notificarUsuario("", "Correo incorrecto", "Ya existe un usuario con este correo.");
				}
				else
				{
					jQuery("#btn_agregar_empleado").html('AGREGAR');
					jQuery('input, button').removeAttr('disabled');
					notificarUsuario("", "Error", "No se pudo realizar la acción.");
				}
			},
			
			function () {
				jQuery("#btn_agregar_empleado").html('AGREGANDO...');
				jQuery('input, button, select, textarea').attr('disabled', 'disabled');
			}, 
			true, 
			0.0);
	}

}

function agregarEmpleadoSocio(op) {
	/*jQuery('#frmAltaEmpleado input, #frmAltaEmpleado select[data-valida], #frmAltaEmpleado textarea[data-valida]').each(function () {
	 ocultarMensajeError(jQuery(this));
	 });*/

	var esValido = true;
	jQuery('#frmAltaEmpleado *[data-valida]').each(function () {
		esValido &= libValida.validarCampo(jQuery(this));
	});
	if (!esValido)
	{
		return;
	}

	var param = new Object();
	param.nombre = jQuery('#frmAltaEmpleado #txtNombre').val();
	param.apellido_paterno = jQuery('#frmAltaEmpleado #txtApellidoPaterno').val();
	param.apellido_materno = jQuery('#frmAltaEmpleado #txtApellidoMaterno').val();
	param.email = jQuery('#frmAltaEmpleado #txtEmail').val();
	param.celular = jQuery('#frmAltaEmpleado #txtCelular').val();
	param.puesto = jQuery("#frmAltaEmpleado #selPuesto").val();
	param.id_empresa = id_empresa;
	param.id_estacionamiento = jQuery("#frmAltaEmpleado #selEstacionamiento").val();
	if (op == "E") {
		param.uid = uid;
	}

	if (op == "E")
	{
		notificarUsuario('', 'Confirmación', '¿De verdad deseas modificar a este empleado?', 'MODIFICAR', function () {
			CLA.encolar("ajax.php",
				{
					m: 'altaEmpleadoSocio',
					p: param,
					op: op
				},
				
				function (response) {
					jQuery('input, button').removeAttr('disabled');
					response = jsoner.parse(response);
					if (response == 1) {
						jQuery("#btn_overlay_si").html('REDIRIGIENDO...');
						location.href = './empleados';
					} else if (response == 0) {
						jQuery("#btn_overlay_si").html('MODIFICAR');
						jQuery('input, button, select, textarea').removeAttr('disabled');
						libValida.crearMensajeError(jQuery('#frmAltaEmpleado input#txtEmail'), ('Correo incorrecto'));
						notificarUsuario("", "Correo incorrecto", "Ya existe un usuario con este correo.", null, null, null, null);
					} else {
						jQuery("#btn_overlay_si").html('MODIFICAR');
						jQuery('input, button, select, textarea').removeAttr('disabled');
						notificarUsuario("", "Error", "No se pudo realizar la acción.", null, null, null, null);
					}
				},
				
				function () {
					jQuery("#btn_overlay_si").html('MODIFICANDO...');
					jQuery('input, button, select, textarea').attr('disabled', 'disabled');
				},
				true,
				0.0);

		}, 'CANCELAR', function () {
			ocultarNotificacion();
		})
	} else {
		CLA.encolar("ajax.php",
			{
				m: 'altaEmpleadoSocio',
				p: param,
				op: op
			},
			
			function (response) {
				jQuery('input, button').removeAttr('disabled');
				response = JSON.parse(response);
				if (response == 1) {
					jQuery("#btn_agregar_empleado_socio").html('REDIRIGIENDO...');
					location.href = './empleados';
				} else if (response == 0) {
					jQuery("#btn_agregar_empleado_socio").html('AGREGAR');
					jQuery('input, button, select, textarea').removeAttr('disabled');
					libValida.crearMensajeError(jQuery('#frmAltaEmpleado input#txtEmail'), ('Correo incorrecto'));
					notificarUsuario("", "Correo incorrecto", "Ya existe un usuario con este correo.", null, null, null, null);
				} else {
					jQuery("#btn_agregar_empleado_socio").html('AGREGAR');
					jQuery('input, button, select, textarea').removeAttr('disabled');
					notificarUsuario("", "Error", "No se pudo realizar la acción.", null, null, null, null);
				}
			},
			
			function () {
				jQuery("#btn_agregar_empleado_socio").html('AGREGANDO...');
				jQuery('input, button, select, textarea').attr('disabled', 'disabled');
			},
			true,
			0.0);
	}
}

function cancelarAltaEmpleado() {
	location.href = './empleados';
}

function mostrarEstacionamiento() {
	if (jQuery("#selPuesto").val() == 7) {
		jQuery("#estacionamiento").hide();
		jQuery("#selEstacionamiento").removeAttr("data-valida");
	} else {
		jQuery("#selEstacionamiento").attr("data-valida", "requerido");
		jQuery("#estacionamiento").show();
	}
}


// _____________________________________________________________________________________________________________ JQUERY
jQuery(document).ready(function ()
{
	// _____________________________________________________________________ configuraciones

	// _____________________________________________________________________ entrypt
	jQuery("#menuLateral").hide();
}); // fin del document.ready


