"use strict";

jQuery(document).ready(function ()
{
	// _____________________________________________________________________ configuraciones

	// _____________________________________________________________________ entrypt
	jQuery('#tblPensionados').DataTable({
		"dom": '<"toolbar"f>rtp',
		"columnDefs": [
			{"width": "170px", "targets": 3},
			{'bSortable': false, 'aTargets': [4]}
		],
		"language": {
			"zeroRecords": "No hay información disponible",
			paginate: {
				previous: 'Anterior',
				next: 'Siguiente'
			},
			aria: {
				paginate: {
					previous: 'Anterior',
					next: 'Siguiente'
				}
			}
		},
		"pageLength": 10
	});
	jQuery('#tblPensionados_wrapper .toolbar').prepend('<span class="encabezado">Pensionados</span>');

	jQuery('#tblPensionados_filter').prepend('<div class="lupa"></div>');
	jQuery('input[type=search]').attr("placeholder", "Buscar ...");
	jQuery('input[type=search]').css("font-weight", "600");
	jQuery('input[type=search]').css("padding-left", "24px");

});// fin del document.ready


function mostrarPensionados() {

	var id_estacionamiento = jQuery("#slctEstacionamientoPensionados").val();

	CLA.encolar("ajax.php",
		{
			m: 'getPensionadosPorEstacionamiento',
			id_estacionamiento: id_estacionamiento
		},
		
		function (response) {
			response = JSON.parse(response);

			jQuery("#tblPensionados > tbody").empty();

			if (response !== false && response.length > 0) {

				var html = "";

				jQuery.each(response, function (key, value) {
					html += "<tr>";
					html += "<td style='padding-left: 24px;'>" + value.nombreUsuario + "</td>";
					html += "<td>" + value.veepeid + "</td>";
					html += "<td>" + value.email + "</td>";
					html += "<td>";

					jQuery.each(value.servicios, function (key, value) {
						if (value.tipo_pension == "1") {
							html += '<div style="display: inline-block;" class="s_pension" href="#"></div>';
						}

						if (value.tipo_pension == "2") {
							html += '<div style="display: inline-block;" class="s_pension_compartida" href="#"></div>';
						}
					});

					html += "</td>";

					html += "<td>";
					html += '<div class="boton detalle" onclick="detallePensionado(' + value.id_pensionado + ')"></div>';
					html += '<div class="boton borrar" onclick="eliminarPensionado(' + value.id_pensionado + ')"></div>';
					html += "</td>";
					html += "</tr>";
				});

				jQuery("#tblPensionados > tbody").append(html);

			} else {
				var html = "";
				html += '<tr class="odd"><td valign="top" colspan="7" class="dataTables_empty">No hay información disponible</td></tr>';
				jQuery("#tblPensionados > tbody").append(html);
			}


		},
		function () {
//			jQuery("#btn_overlay_si").html('ELIMINANDO...');
//			jQuery('input, button, select, textarea').attr('disabled', 'disabled');
		},
		true,
		0.0);
}

function detallePensionado(id_pensionado) {
	location.href = './detalle_pensionado?id_pensionado=' + id_pensionado;
}

function eliminarPensionado(idPensionado) {
	notificarUsuario('', 'Confirmación', '¿De verdad deseas eliminar esta contratación?', 'ELIMINAR', function () {

		CLA.encolar("ajax.php",
			{
				m: 'eliminarContratacionPension',
				id_pensionado: idPensionado
			},
			
			function (response) {
				response = jsoner.parse(response);

				if (response + '' == '1')
				{
					jQuery("#btn_overlay_si").html('REDIRIGIENDO...');
					location.reload();
				}
				else
				{
					jQuery('input, button, select, textarea').removeAttr('disabled');
					jQuery("#btn_overlay_si").html('ELIMINAR');
					notificarUsuario('error', 'Error', 'No se pudo realizar la acción.');
				}
			},

			function () {
				jQuery("#btn_overlay_si").html('ELIMINANDO...');
				jQuery('input, button, select, textarea').attr('disabled', 'disabled');
			},
			
			true,
			0.0);
		
	}, 'CANCELAR', function () {
		ocultarNotificacion();
	})
}