"use strict";

// _____________________________________________________________________________________________________________ VARIABLES GLOBALES
var servicioPensionesNormal = false;
var servicioPensionesCompartida = false;

var periodosPensionesRestantes = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];
var periodosPensionesSeleccionados = [];

// _____________________________________________________________________________________________________________ FUNCIONES GLOBALES

function agregarEstacionamiento(op) {
	jQuery('#frmAltaEstacionamiento input, #frmAltaEstacionamiento select[data-valida], #frmAltaEstacionamiento textarea[data-valida]').each(function () {
		libValida.ocultarMensajeError(jQuery(this));
	});

	var esValido = true;

	jQuery('#frmAltaEstacionamiento *[data-valida]').each(function () {
		esValido &= libValida.validarCampo(jQuery(this));
	});

	//----------------------------------------------------------------------------
	//------------PENSIONES-------------------------------------------------------

	if (jQuery("#input_checkbox1").is(':checked')) {
		if (jQuery("#input_checkbox7").is(':checked') || jQuery("#input_checkbox8").is(':checked')) {

			var pensiones = {data: []};

			jQuery("#tblConfigurarPensiones > tbody").children().each(function () {

				var id = jQuery(this).attr("id");
				var periodo = jQuery(this).find(".selPeriodo").val();

				if (periodo != "-1")
				{
					var diaPensionNormal = jQuery(this).find(".dia").val();
					var nochePensionNormal = jQuery(this).find(".noche").val();
					var todoDiaPensionNormal = jQuery(this).find(".todoDia").val();

					var diaPensionCompartidal = jQuery(this).find(".diac").val();
					var nochePensionCompartida = jQuery(this).find(".nochec").val();
					var todoDiaPensionCompartida = jQuery(this).find(".todoDiac").val();

					pensiones.data.push(new Array(periodo, diaPensionNormal, nochePensionNormal, todoDiaPensionNormal, diaPensionCompartidal, nochePensionCompartida, todoDiaPensionCompartida));
				}
				else
				{
					esValido &= libValida.validarCampo(jQuery(this));
					return;
				}
			});

		} else {
			jQuery("#input_checkbox7").attr("data-valida", "requerido");
			jQuery("#input_checkbox8").attr("data-valida", "requerido");
			pensiones = null;
		}

	} else {
		pensiones = null;
	}

	//--------------------------------

	if (!esValido)
	{
		return;
	}

	var param = new Object();
	param.nombre = jQuery('#frmAltaEstacionamiento #txtNombre').val();
	param.direccion = jQuery('#frmAltaEstacionamiento #txtDireccion').val();
	param.latitud = jQuery('#frmAltaEstacionamiento #txtLatitud').val();
	param.longitud = jQuery('#frmAltaEstacionamiento #txtLongitud').val();
	param.zona = jQuery('#frmAltaEstacionamiento #txtZona').val();
	param.capacidad = jQuery('#frmAltaEstacionamiento #txtCapacidad').val();
	param.terminos = jQuery('#frmAltaEstacionamiento #txtCondiciones').val();
	param.horario_apertura = jQuery('#frmAltaEstacionamiento #txtHorarioApertura').val();
	param.horario_cierre = jQuery('#frmAltaEstacionamiento #txtHorarioCierre').val();
	param.pensiones_contratadas = jQuery('#frmAltaEstacionamiento #txtPensiones').val();
	//param.amonestacion_pension = jQuery('#frmAltaEstacionamiento #txtAmonestacion').val();
	param.amonestacion_pension = "0.0";

	var servicios = {data: []};

	var costoEstacionamiento = jQuery('#frmAltaEstacionamiento #txtCosto_estacionamiento').val();
	servicios.data.push(new Array("0", costoEstacionamiento));

//	if (jQuery('#frmAltaEstacionamiento #input_checkbox0').is(':checked')) {
		var costoTarifaEstacionamiento = jQuery('#frmAltaEstacionamiento #txtCosto_estacionamiento').val();
		servicios.data.push(new Array("5", costoTarifaEstacionamiento));
//	}

//	if (jQuery('#frmAltaEstacionamiento #input_checkbox2').is(':checked')) {
//		var costoPensionCompartida = jQuery('#frmAltaEstacionamiento #txtCosto_pension_compartida').val();
//		servicios.data.push(new Array("2", costoPensionCompartida));
//	}

	if (jQuery('#frmAltaEstacionamiento #input_checkbox3').is(':checked')) {
		var costoReserva = jQuery('#frmAltaEstacionamiento #txtCosto_apartado').val();
		servicios.data.push(new Array("3", costoReserva));
	}

	if (jQuery('#frmAltaEstacionamiento #input_checkbox4').is(':checked')) {
		var costoPensionValet = jQuery('#frmAltaEstacionamiento #txtCosto_valet_parking').val();
		servicios.data.push(new Array("4", costoPensionValet));
	}


	if (jQuery('#frmAltaEstacionamiento #input_checkbox1').is(':checked')) {

		if (jQuery('#frmAltaEstacionamiento #input_checkbox7').is(':checked')) {
			servicios.data.push(new Array("1", "0"));
		}

		if (jQuery('#frmAltaEstacionamiento #input_checkbox8').is(':checked')) {
			servicios.data.push(new Array("2", "0"));
		}
	}

//	if (jQuery('#frmAltaEstacionamiento #input_checkbox1').is(':checked')) {
//		var costoPension = jQuery('#frmAltaEstacionamiento #txtCosto_pension').val();
//		servicios.data.push(new Array("1", costoPension));
//	}


	if (servicios.data.length == 0) {
		servicios = null;
	}

	param.uid = uid;

	//---------------------------------------------------------------------------
	if (op == "E") {
		param.id_estacionamiento = id_estacionamiento;
	}

	if (op == "E") {

		notificarUsuario('', 'Confirmación', '¿De verdad deseas modificar a este estacionamiento?', 'MODIFICAR', function () {

			CLA.encolar("ajax.php",
				{
					m: 'altaEstacionamientos',
					p: param,
					servicios: servicios,
					pensiones: pensiones,
					op: op
				},
				
				function (response) {
					response = jsoner.parse(response);

					if (response == true) {
						jQuery("#btn_overlay_si").html('REDIRIGIENDO...');
						location.href = './estacionamientos';
					} else {
						jQuery("#btn_overlay_si").html('MODIFICAR');
						jQuery('input, button, select, textarea').removeAttr('disabled');
						notificarUsuario("", "Error", "No se pudo realizar la acción.");
					}
				},
				
				function () {
					jQuery("#btn_overlay_si").html('MODIFICANDO...');
					jQuery('input, button, select, textarea').attr('disabled', 'disabled');
				},
				true,
				0.0);

		}, 'CANCELAR', function () {
			ocultarNotificacion();
		})

	} else {

		CLA.encolar("ajax.php",
			{
				m: 'altaEstacionamientos',
				p: param,
				servicios: servicios,
				pensiones: pensiones,
				op: op
			},
			
			function (response) {
				response = jsoner.parse(response);
				if (response === true) {
					jQuery("#btn_agregar_estacionamiento").html('REDIRIGIENDO...');
					location.href = './estacionamientos';
				} else {
					jQuery("#btn_agregar_estacionamiento").html('AGREGAR');
					jQuery('input, button, select, textarea').removeAttr('disabled');
					notificarUsuario("", "Error", "No se pudo realizar la acción.");
				}
			},
			
			function () {
				jQuery("#btn_agregar_estacionamiento").html('AGREGANDO...');
				jQuery('input, button, select, textarea').attr('disabled', 'disabled');
			},
			true,
			0.0);
	}
}

function cancelarAltaEstacionamiento() {
	location.href = './estacionamientos';
}

function agregarPensiones() {
	if (jQuery('#frmAltaEstacionamiento #input_checkbox1').is(':checked')) {
		jQuery("#divPensiones").hide();
		jQuery(".selPeriodo").removeAttr("data-valida");
		jQuery("#txtAmonestacion").removeAttr("data-valida");
	} else {
		jQuery("#divPensiones").show();
		jQuery(".selPeriodo").attr("data-valida", "requerido");
		jQuery("#txtAmonestacion").attr("data-valida", "requerido");
	}
}


function agregarPeriodoPension() {
	var numeroPeriodo = jQuery("#tblConfigurarPensiones > tbody").children("tr").length;

	if (numeroPeriodo < 12) {

		numeroPeriodo += 1;

		var html = "";
		html += '<tr style="height: 40px; ">';
		html += '<td style="margin-right: 8px; border-right-width: 8px; border-right-style: solid;border-right-color: #090a19;">';
		html += '<select class="selPeriodo" data-valida="requerido">';
		html += '<option value="-1">Periodo</option>';

		jQuery.each(periodosPensionesRestantes, function (key, value) {
			html += '<option value="' + value + '">' + value + '</option>';
		});

		html += '</select></td>';

		if (jQuery("#input_checkbox7").is(':checked')) {
			html += '<td><input placeholder="MXN" type="text" class="pensionNormal dia"/></td>';
			html += '<td><input placeholder="MXN"  type="text" class="pensionNormal noche"/></td>';
			html += '<td style="margin-right: 8px; border-right-width: 8px; border-right-style: solid;border-right-color: #090a19;"><input placeholder="MXN"  type="text" class="pensionNormal todoDia" /></td>';
		} else {
			html += '<td><input placeholder="MXN" type="text" class="pensionNormal inputPensionDesactivado dia" disabled="disabled"/></td>';
			html += '<td><input placeholder="MXN"  type="text" class="pensionNormal inputPensionDesactivado noche" disabled="disabled"/></td>';
			html += '<td style="margin-right: 8px; border-right-width: 8px; border-right-style: solid;border-right-color: #090a19;"><input placeholder="MXN"  type="text" class="pensionNormal inputPensionDesactivado todoDia" disabled="disabled" /></td>';
		}

		if (jQuery("#input_checkbox8").is(':checked')) {
			html += '<td><input placeholder="MXN"  type="text" class="pensionCompartida inputPensionActivado diac"/></td>';
			html += '<td><input placeholder="MXN"  type="text" class="pensionCompartida inputPensionActivado nochec"/></td>';
			html += '<td><input placeholder="MXN"  type="text" class="pensionCompartida inputPensionActivado todoDiac"/></td>';
		} else {
			html += '<td><input placeholder="MXN"  type="text" class="pensionCompartida inputPensionDesactivado diac" disabled="disabled"/></td>';
			html += '<td><input placeholder="MXN"  type="text" class="pensionCompartida inputPensionDesactivado nochec" disabled="disabled"/></td>';
			html += '<td><input placeholder="MXN"  type="text" class="pensionCompartida inputPensionDesactivado todoDiac" disabled="disabled"/></td>';
		}

		html += '<td style="text-align: center; background-color: rgba(65,63,65,0.60);"><div id="btnQuitarPension' + numeroPeriodo + '" class="boton borrar" style="margin: 0px; "></div></td>';
		html += '</tr>';

		jQuery("#tblConfigurarPensiones > tbody").append(html);

		jQuery("#btnQuitarPension" + numeroPeriodo).click(function () {
			jQuery(this).parent().parent().remove();

			periodosPensionesRestantes = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];
			periodosPensionesSeleccionados = [];
			var periodoSelecionado = jQuery(this).val();
			jQuery(this).attr("id", periodoSelecionado);

			jQuery(".selPeriodo").each(function () {
				if (jQuery(this).val() != "-1") {
					periodosPensionesSeleccionados.push(jQuery(this).attr("id"));
				}
			});

			if (periodoSelecionado != "-1") {
				jQuery.each(periodosPensionesSeleccionados, function (key, value) {
					var index = periodosPensionesRestantes.indexOf(value);
					periodosPensionesRestantes.splice(index, 1);
				});
			}
			setearListaPeriodos();
		});

		jQuery(".selPeriodo").change(function () {
			periodosPensionesRestantes = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];
			periodosPensionesSeleccionados = [];
			var periodoSelecionado = jQuery(this).val();
			jQuery(this).attr("id", periodoSelecionado);
			jQuery(this).parent().parent().attr("id", "tr" + periodoSelecionado);

			jQuery(".selPeriodo").each(function () {

				if (jQuery(this).val() != "-1") {

					periodosPensionesSeleccionados.push(jQuery(this).attr("id"));
				}
			});

			if (periodoSelecionado != "-1") {
				jQuery.each(periodosPensionesSeleccionados, function (key, value) {
					var index = periodosPensionesRestantes.indexOf(value);
					periodosPensionesRestantes.splice(index, 1);
				});
			}

			setearListaPeriodos();

		});
		setearListaPeriodos();
	}
}


function setearListaPeriodos() {
	var periodosPensionesRestantesSinSeleccionar = periodosPensionesRestantes;
	jQuery(".selPeriodo").each(function () {

		var valorSeleccionado = jQuery(this).val();

		jQuery(this).empty();

		var options = "";
		options += '<option value="-1">Periodo</option>';

		if (valorSeleccionado != "-1") {

			periodosPensionesRestantes.push(valorSeleccionado);

			jQuery.each(periodosPensionesRestantes, function (key, value) {
				if (valorSeleccionado == value) {
					options += '<option value="' + value + '" selected>' + value + '</option>';
				} else {
					options += '<option value="' + value + '">' + value + '</option>';
				}
			});
			jQuery(this).append(options);

			var index = periodosPensionesRestantes.indexOf(valorSeleccionado);
			periodosPensionesRestantes.splice(index, 1);

		} else {
			jQuery.each(periodosPensionesRestantesSinSeleccionar, function (key, value) {
				options += '<option value="' + value + '">' + value + '</option>';
			});
			jQuery(this).append(options);
		}

	});
}


function activarPensionNormal() {
	if (jQuery("#input_checkbox7").is(':checked')) {
		jQuery("#input_checkbox7").prop('checked', false);
		jQuery(".pensionNormal").attr("disabled", "disabled");
		jQuery(".pensionNormal").val("");
		jQuery(".pensionNormal").addClass("inputPensionDesactivado");
		jQuery(".pensionNormal").removeClass("inputPensionActivado");
	} else {
		jQuery("#input_checkbox7").prop('checked', true);
		jQuery(".pensionNormal").removeAttr("disabled");
		jQuery(".pensionNormal").addClass("inputPensionActivado");
		jQuery(".pensionNormal").removeClass("inputPensionDesactivado");

		jQuery("#input_checkbox7").removeAttr("data-valida");
		jQuery("#input_checkbox8").removeAttr("data-valida");
	}
}

function pensionCompartida() {
	if (jQuery("#input_checkbox8").is(':checked')) {
		jQuery("#input_checkbox8").prop('checked', false);
		jQuery(".pensionCompartida").attr("disabled", "disabled");
		jQuery(".pensionCompartida").val("");
		jQuery(".pensionCompartida").addClass("inputPensionDesactivado");
		jQuery(".pensionCompartida").removeClass("inputPensionActivado");
	} else {
		jQuery("#input_checkbox8").prop('checked', true);
		jQuery(".pensionCompartida").removeAttr("disabled");
		jQuery(".pensionCompartida").addClass("inputPensionActivado");
		jQuery(".pensionCompartida").removeClass("inputPensionDesactivado");

		jQuery("#input_checkbox7").removeAttr("data-valida");
		jQuery("#input_checkbox8").removeAttr("data-valida");

	}
}

function mostrarPensiones() {
	jQuery("#divPensiones").show();
}

// _____________________________________________________________________________________________________________ JQUERY
jQuery(document).ready(function ()
{
	// _____________________________________________________________________ configuraciones

	jQuery(".selPeriodo").each(function () {
		if (jQuery(this).val() != "-1") {
			periodosPensionesSeleccionados.push(jQuery(this).attr("id"));
		}
	});
	
	jQuery.each(periodosPensionesSeleccionados, function (key, value) {
		var index = periodosPensionesRestantes.indexOf(value);
		periodosPensionesRestantes.splice(index, 1);
	});
			
	jQuery('.chkServicios').change(function () {
		var name = jQuery(this).prop("name");
		if (jQuery(this).is(':checked')) {
			jQuery(this).prop('checked', false);
			if (name != "input_checkbox3"){
				jQuery(this).parent().next().children().attr("disabled", "disabled");
				jQuery(this).parent().next().children().removeAttr("data-valida");
				jQuery(this).parent().next().children().val("");
			}
		} else {
			jQuery(this).prop('checked', true);
			if (name != "input_checkbox3"){
				jQuery(this).parent().next().children().removeAttr("disabled");
				jQuery(this).parent().next().children().attr("data-valida", "requerido");
			}
		}
	});

	jQuery(".selPeriodo").change(function () {
		periodosPensionesRestantes = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];
		periodosPensionesSeleccionados = [];
		var periodoSelecionado = jQuery(this).val();
		jQuery(this).attr("id", periodoSelecionado);
		jQuery(this).parent().parent().attr("id", "tr" + periodoSelecionado);

		jQuery(".selPeriodo").each(function () {
			if (jQuery(this).val() != "-1") {
				periodosPensionesSeleccionados.push(jQuery(this).attr("id"));
			}
		});

		if (periodoSelecionado != "-1") {
			jQuery.each(periodosPensionesSeleccionados, function (key, value) {
				var index = periodosPensionesRestantes.indexOf(value);
				periodosPensionesRestantes.splice(index, 1);
			});
		}

		setearListaPeriodos();
	});

	// _____________________________________________________________________ entrypt
	jQuery("#divPensiones").hide();

	if (servicioPensionesNormal == true) {
		jQuery("#input_checkbox7").attr("checked", "checked");
		jQuery(".pensionNormal").addClass("inputPensionActivado");
		jQuery(".pensionNormal").removeClass("inputPensionDesactivado");
		jQuery(".pensionNormal").removeAttr("disabled");
	} else {
		jQuery("#input_checkbox7").removeAttr("checked");
		jQuery(".pensionNormal").removeClass("inputPensionActivado");
		jQuery(".pensionNormal").addClass("inputPensionDesactivado");
		jQuery(".pensionNormal").attr("disabled", "disabled");
	}

	if (servicioPensionesCompartida == true) {
		jQuery(".pensionCompartida").addClass("inputPensionActivado");
		jQuery(".pensionCompartida").removeClass("inputPensionDesactivado");
		jQuery(".pensionCompartida").removeAttr("disabled");
		jQuery("#divPensiones :input").attr("disabled", "disabled");
	} else {
		jQuery("#input_checkbox8").removeAttr("checked");
		jQuery(".pensionCompartida").removeClass("inputPensionActivado");
		jQuery(".pensionCompartida").addClass("inputPensionDesactivado");
		jQuery(".pensionCompartida").attr("disabled", "disabled");
	}

	if (servicioPensiones == true) {
		jQuery("#divPensiones").show();
		jQuery("#divPensiones :input").attr("disabled", "disabled");
		jQuery("#input_checkbox1").attr("disabled", "disabled");
	} else {
		jQuery("#input_checkbox7").prop('checked', true);
		jQuery(".pensionNormal").removeAttr("disabled");
		jQuery(".pensionNormal").addClass("inputPensionActivado");
		jQuery(".pensionNormal").removeClass("inputPensionDesactivado");
	}

	jQuery('#datetimepicker1, #datetimepicker2').datetimepicker({
		format: 'HH:mm',
		allowInputToggle: true,
		//debug: true
	});

});// fin del document.ready

