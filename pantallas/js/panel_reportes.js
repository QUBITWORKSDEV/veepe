"use strict";

// _____________________________________________________________________________________________________________ VARIABLES GLOBALES

// _____________________________________________________________________________________________________________ FUNCIONES GLOBALES

function limpiar_select(select)
{
	for (var i = 0; i < select.length; i++)
	{
		select.remove(i);
	}
}

function validar_fechas(){
	var input_inicio = document.getElementById("fechaIni");
	var input_fin = document.getElementById("fechaFin");
	var inicio = input_inicio.value;
	var fin = input_fin.value;
	var tmp, fini, ffin;

	if (!inicio.match(/^\d\d\d\d?\-\d\d?\-\d\d$/))
	{
		notificarUsuario("error", "Formato de fecha incorrecto", "Verifique el formato de la fecha inicial (aaaa-mm-dd).");
		libValida.crearMensajeError(jQuery('#frmGenerarReporte input#fechaIni'), ('Formato de fecha incorrecto'));
		return false;
		
		if (!fin.match(/^\d\d\d\d?\-\d\d?\-\d\d$/))
		{
			//alert('Verifique el formato de la fecha final (dd/mm/aaaa)');
			notificarUsuario("error", "Formato de fecha incorrecto", "Verifique el formato de la fecha final (aaaa-mm-dd).");
			input_fin.focus();
			return false;
		}
	}
}

function dochange_cliente()
{
	var div_zona = document.getElementById("div_zona");
	var div_estacionamiento = document.getElementById("div_estacionamiento");
	var select_zona = document.getElementById("select_zona");
	var select_empresa = document.getElementById("select_empresa");
	var select_estacionamiento = document.getElementById("select_estacionamiento");
	var id_empresa = select_empresa.options[select_empresa.selectedIndex].value;
	var cadzona = select_estacionamiento.options[select_estacionamiento.selectedIndex].value;
	limpiar_select(select_zona);
	limpiar_select(select_estacionamiento);

	if( id_empresa != '')
	{
		CLA.encolar("ajax.php",
			{
				m: 'webGetZonasXCliente',
				uid: id_empresa
			},
			function (response)
			{
				jQuery('input, button, select').removeAttr('disabled');
				response = jsoner.parse(response);
	
				if (response != null)
				{
					if(response.length > 0)
					{
						select_zona.options[0] = new Option("Todos las zonas","");
						select_estacionamiento.options[0] = new Option("Todos los estacionamientos","");
						for (var i = 0; i < response.length; i++)
						{
							select_zona.options[i+1] = new Option(response[i]["zona"], response[i]["zona"]);
						}
					}
					else
					{
						select_zona.options[0] = new Option("Zonas","");
						select_estacionamiento.options[0] = new Option("Estacionamientos","");
					}
				}
				else
				{
					//console.log("No hay clientes registrados");
				}
			},
			function () {
				jQuery('input, button, select').attr('disabled', 'disabled');
			},
			true,
			0.0);

		estacionamientosXCliente(id_empresa);
	}
	else
	{
		select_zona.options[0] = new Option("Zonas","");
		select_estacionamiento.options[0] = new Option("Estacionamientos","");
	}
}

function dochange_zona()
{
	var div_estacionamiento = document.getElementById("div_estacionamiento");
	var select_estacionamiento = document.getElementById("select_estacionamiento");
	var id_empresa = select_empresa.options[select_empresa.selectedIndex].value;
	var zona = select_zona.options[select_zona.selectedIndex].value;
	limpiar_select(select_estacionamiento);

	if( zona != '')
	{
		CLA.encolar("ajax.php",
			{
				m: 'webGetEstacionamientoXZona',
				idEmpresa : id_empresa,
				zona: zona
				
			},
			function (response)
			{
				jQuery('input, button, select').removeAttr('disabled');
				response = jsoner.parse(response);
				if (response != null)
				{
					select_estacionamiento.options[0] = new Option("Todos los estacionamientos","");
					for (var i = 0; i < response.length; i++)
					{
						select_estacionamiento.options[i+1] = new Option(response[i]["nombre"], response[i]["id_estacionamiento"]);
					}
				}
				else
				{
					//console.log("No hay estacionamientos registrados");
				}
			},
			function () {
				jQuery('input, button, select').attr('disabled', 'disabled');
			},
			true,
			0.0);
	}
	else
	{
		estacionamientosXCliente(id_empresa);
	}
}

function estacionamientosXCliente(id_empresa)
{
	CLA.encolar("ajax.php",
		{
			m: 'webGetEstacionamientosXCliente',
			uid: id_empresa
		},
		function (response)
		{
			jQuery('input, button, select').removeAttr('disabled');
			response = jsoner.parse(response);
			if (response != null)
			{
				if(response.length > 0)
				{
					select_estacionamiento.options[0] = new Option("Todos los estacionamientos","");
					for (var i = 0; i < response.length; i++)
					{
						select_estacionamiento.options[i+1] = new Option(response[i]["nombre"], response[i]["nombre"]);
					}
				}
				else
				{
					select_zona.options[0] = new Option("Zonas","");
					select_estacionamiento.options[0] = new Option("Estacionamientos","");
				}
			}
			else
			{
				//console.log("No hay clientes registrados");
			}
		},
		function () {
			jQuery('input, button, select').attr('disabled', 'disabled');
		},
		true,
		0.0);
}

// _____________________________________________________________________________________________________________ JQUERY
jQuery(document).ready(function ()
{
	// _____________________________________________________________________ configuraciones

	// _____________________________________________________________________ entrypt
	jQuery('#fechaIni, #fechaFin').datetimepicker({
		format: 'YYYY-MM-DD',
		allowInputToggle: true,
		locale: 'es'
	});

	jQuery("#fechaIni").on("dp.change", function (e) {
		jQuery('#fechaFin').data("DateTimePicker").minDate(e.date);
	});

});// fin del document.ready
