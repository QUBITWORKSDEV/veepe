"use strict";

// _____________________________________________________________________________________________________________ VARIABLES GLOBALES

// _____________________________________________________________________________________________________________ FUNCIONES GLOBALES

function editarEstacionamiento(idestacionamiento)
{
	location.href = './alta_estacionamiento?idestacionamiento=' + idestacionamiento;
}

function altaEstacionamiento()
{
	location.href = './alta_estacionamiento';
}

function detalleEstacionamiento(idEstacionamiento)
{
	location.href = './detalle_estacionamiento?idestacionamiento=' + idEstacionamiento;
}

function eliminarEstacionamiento(id_estacionamiento)
{
	notificarUsuario('', 'Confirmación', '¿De verdad deseas eliminar este estacionamiento?', 'ELIMINAR', function () {

		CLA.encolar("ajax.php",
			{
				m:					'eliminarEstacionamiento',
				id_estacionamiento:	id_estacionamiento
			},
			
			function (response) {
				response = jsoner.parse(response);

				if (response+'' == '1')
				{
					jQuery("#btn_overlay_si").html('REDIRIGIENDO...');
					location.reload();
				}
				else if (response+'' == '2') 
				{
					jQuery('input, button, select, textarea').removeAttr('disabled');
					jQuery("#btn_overlay_si").html('ELIMINAR');
					notificarUsuario('error', 'Error', 'No se puede eliminar este estacionamiento porque tiene operaciones pendientes.');
				}
				else if(response+'' == '3')
				{
					jQuery('input, button, select, textarea').removeAttr('disabled');
					jQuery("#btn_overlay_si").html('ELIMINAR');
					notificarUsuario('error', 'Error', 'No se puede eliminar este estacionamiento porque tiene pensiones contratadas que todavia no terminan su periodo.');
				}
				else
				{
					jQuery('input, button, select, textarea').removeAttr('disabled');
					jQuery("#btn_overlay_si").html('ELIMINAR');
					notificarUsuario('error', 'Error', 'No se pudo realizar la acción.');
				}
			},
			function () {
				jQuery("#btn_overlay_si").html('ELIMINANDO...');
				jQuery('input, button, select, textarea').attr('disabled', 'disabled');
			},
			true,
			0.0);
	}, 'CANCELAR', function () {
		ocultarNotificacion();
	})
}


// _____________________________________________________________________________________________________________ JQUERY
jQuery(document).ready(function ()
{
	// _____________________________________________________________________ configuraciones

	// _____________________________________________________________________ entrypt
	jQuery('#tblEstacionamientos').DataTable({
		"dom": '<"toolbar"f>rtp',
		"columnDefs": [
			{"width": "170px", "targets": 4},
			{ 'bSortable': false, 'aTargets': [ 0, 4 ] }
		],
		"language": {
			"zeroRecords": "No hay información disponible",
			paginate: {
				previous: 'Anterior',
				next: 'Siguiente'
			},
			aria: {
				paginate: {
					previous: 'Anterior',
					next: 'Siguiente'
				}
			}
		},
		"pageLength": 10
	});
	jQuery('#tblEstacionamientos_wrapper .toolbar').prepend('<span class="encabezado">Estacionamientos</span>');

	jQuery('#tblEstacionamientos_filter').prepend('<div class="lupa"></div>');
	jQuery('input[type=search]').attr("placeholder", "Buscar ...");
	jQuery('input[type=search]').css("font-weight", "600");
	jQuery('input[type=search]').css("padding-left", "24px");

});// fin del document.ready
