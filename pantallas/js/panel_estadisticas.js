"use strict";

// _____________________________________________________________________________________________________________ VARIABLES GLOBALES

// _____________________________________________________________________________________________________________ FUNCIONES GLOBALES
function actualizarInfoOperaciones()
{
	var params = {
			m:					'getEstadisticas',
			empresa:			(typeof jQuery('#selEmpresa').val()!=='undefined' && jQuery('#selEmpresa').val()!='')?jQuery('#selEmpresa').val():null,
			estacionamiento:	jQuery('#selEstacionamiento').val()!=''?jQuery('#selEstacionamiento').val():null,
			zona:				jQuery('#selZona').val()!=''?jQuery('#selZona').val():null,
			fechaIni:			jQuery('#txtFechaInicio').val(),
			fechaFin:			jQuery('#txtFechaTermino').val()
		};
	console.log(params);
	CLA.encolar("ajax.php",
		params,
		function (response) {
			response = jsoner.parse(response);
			
			// ponemos los resumenes
			jQuery('#datosEmpresa #numeroOperacionesEmpresa').html( response.resumen.numOperaciones );
			jQuery('#datosEmpresa #numeroZonas').html( response.resumen.numZonas );
			jQuery('#datosEmpresa #numeroEstacionamientos').html( response.resumen.numEstacionamientos );
			jQuery('#datosEmpresa #ingresos').html( "MXN " + formatNumber(response.resumen.ingresos) );
			
			// limpiamos la tabla
			jQuery('#tblOperaciones tbody').empty();
			
			// creamos las filas
			var i;
			var ingresosTotales = 0.0;
			for (i=0; i<response.datos.length; i++)
			{
				jQuery('#tblOperaciones tbody').append('<tr>'+
					'<td></td>'+											// icono
					'<td>'+response.datos[i].nombre+'</td>'+
					'<td>'+response.datos[i].zona+'</td>'+
					'<td>'+response.datos[i].numOperaciones+'</td>'+
					(typeof response.datos[i].ingresos!=='undefined'?
						'<td>MXN ' + formatNumber(parseFloat(response.datos[i].ingresos)) + '</td>':'')+
					'</tr>');
				ingresosTotales += parseFloat(response.datos[i].ingresos);
			}
			jQuery('#ingresoTotal').html('MXN ' + formatNumber(ingresosTotales));
		},
		function () {
		},
		true,
		0.0);
}

function actualizarInfoEmpresas()
{
	CLA.encolar("ajax.php",
		{
			m:					'getEstadisticas',
			empresa:			(typeof jQuery('#selEmpresa').val()!=='undefined' && jQuery('#selEmpresa').val()!='')?jQuery('#selEmpresa').val():null,
			estacionamiento:	jQuery('#selEstacionamiento').val()!=''?jQuery('#selEstacionamiento').val():null,
			zona:				jQuery('#selZona').val()!=''?jQuery('#selZona').val():null,
			fechaIni:			null,
			fechaFin:			null,
			esResumen:			true
		},
		function (response) {
			response = jsoner.parse(response);
			console.log(response);
			
			// limpiamos la tabla
			jQuery('#tblEstadisticas tbody').empty();

			// ponemos los datos
			var i;
			for (i=0; i<response.datos.length; i++)
			{
				jQuery('#tblEstadisticas tbody').append('<tr>'+
					'<td></td>'+											// icono
					'<td>'+response.datos[i].nombre_empresa+'</td>'+
					'<td>'+response.datos[i].numZonas+'</td>'+
					'<td>'+response.datos[i].numEstacionamientos+'</td>'+
					'<td>'+response.datos[i].numOperaciones+'</td>'+
					(typeof response.datos[i].ingresos!=='undefined'?
						'<td>MXN ' + formatNumber(parseFloat(response.datos[i].ingresos)) + '</td>':'')+
					'</tr>');
			}
		},
		function () {
		},
		true,
		0.0);
}

function refreshZonas()
{
	CLA.encolar("ajax.php",
		{
			m:					'getZonasXCliente',
			empresa:			jQuery('#selEmpresa').val()!=''?jQuery('#selEmpresa').val():null,
		},
		function (response) {
			jQuery('select').removeAttr('disabled');
			response = jsoner.parse(response);

			jQuery('#selZona').empty().append('<option value="-1">Todas las zonas</option>');
			
			var i;
			for (i=0; i<response.length; i++)
			{
				jQuery('#selZona').append('<option value="'+response[i].zona+'">'+response[i].zona+'</option>');
			}
		},
		function () {
			jQuery('select').attr('disabled', 'disabled');
		},
		true,
		0.0);
}

function refreshEstacionamientos(debeActualizarTabla)
{
	CLA.encolar("ajax.php",
		{
			m:					'getEstacionamientoXZona',
			empresa:			(typeof jQuery('#selEmpresa').val()!=='undefined' && jQuery('#selEmpresa').val()!='')?jQuery('#selEmpresa').val():null,
			zona:				jQuery('#selZona').val()!=''?jQuery('#selZona').val():null,
		},
		function (response) {
			jQuery('select').removeAttr('disabled');
			response = jsoner.parse(response);

			jQuery('#selEstacionamiento').empty().append('<option value="-1">Todos los estacionamientos</option>');
			
			var i;
			for (i=0; i<response.length; i++)
			{
				jQuery('#selEstacionamiento').append('<option value="'+response[i].id_estacionamiento+'">'+response[i].nombre+'</option>');
			}
			
			if ( debeActualizarTabla )
				actualizarInfoOperaciones();
		},
		function () {
			jQuery('select').attr('disabled', 'disabled');
		},
		true,
		0.0);
}


// _____________________________________________________________________________________________________________ JQUERY
jQuery(document).ready(function ()
{
	// _____________________________________________________________________ configuraciones
	jQuery('#selEmpresa').change(function(){
		// actualizamos zonas
		refreshZonas();
		
		// actualizamos estacionamientos
		refreshEstacionamientos(true);
	});

	jQuery('#selZona').change(function(){
		// actualizamos estacionamientos
		refreshEstacionamientos(true);
	});

	jQuery('#selEstacionamiento').change(function(){
		actualizarInfoOperaciones();
	});

	jQuery("#fechaInicio").on("dp.change", function (e) {
		jQuery('#fechaTermino').data("DateTimePicker").minDate(e.date);
	});

	jQuery("#fechaTermino,#fechaInicio").on("dp.change", function(){
		actualizarInfoOperaciones();
	});

	// _____________________________________________________________________ entrypt
	jQuery('#fechaInicio, #fechaTermino').datetimepicker({
		format: 'YYYY-MM-DD',
		allowInputToggle: true,
		locale: 'es'
	});

	actualizarInfoOperaciones();
	if ( typeof esAgv !== 'undefined' && esAgv == true )
		actualizarInfoEmpresas();

});// fin del document.ready

