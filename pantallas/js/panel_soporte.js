
function getDatosUsuario()
{
	var veepeid = jQuery("#txtVeepeid").val();
	jQuery("#myTab").hide();
	
	CLA.encolar("ajax.php",
		{
			m:					'getDatosSoporteUsuario',
			veepeid:			veepeid
		},
		function (response) {
			
			response = jsoner.parse(response);
			
			if(response[0] != false){
			
				var perfil = response[0];
				var pensiones = response[0].PensionesContratadas.normales;
				var pensionesCompartidas = response[0].PensionesContratadas.compartidas;
				var operacionActual = response[1];
				var historialOperaciones = response[2];
				var metodos = response[0].MetodosPago;

				jQuery("#perfil").empty();
				jQuery("#operaciones").empty();
				jQuery("#pensiones").empty();

				//----------------PERFIL-------------------------
				var datosPerfil = "";

				datosPerfil += '<div class="row caja_perfil" style="margin: auto; padding-top: 24px; padding-bottom: 24px;" >';
				datosPerfil += '<div class="col-md-12">';
				datosPerfil += '<label>Nombre</label><span class="body3_B">'+perfil.nombre+ ' '+perfil.apellidoPaterno+ ' ' + perfil.apellidoMaterno+  '</span>';
				datosPerfil += '</div>';
				datosPerfil += '<div class="col-md-12">';	
				datosPerfil += '<label>Correo</label><span class="body3_B">' + perfil.correo + '</span>';	
				datosPerfil += '</div>';	
				datosPerfil += '<div class="col-md-12">';
				datosPerfil += '<label>Celular</label><span class="body3_B">'+perfil.celular+'</span>';	
				datosPerfil += '</div>';	
				datosPerfil += '<div class="col-md-12">';
				datosPerfil += '<label>VEEPEID</label><span class="body3_B">'+perfil.veepeId+'</span>';	
				datosPerfil += '</div>';
				datosPerfil += '</div>';
				jQuery("#perfil").append(datosPerfil);


				//-----------------OPERACIONES--------------------------
				var datosOperaciones = "";
				
				datosOperaciones += '<br>';
				datosOperaciones += '<br>';
				datosOperaciones += '<div class="row caja_perfil" style="margin: auto; padding-top: 24px; padding-bottom: 24px; width: 60%;" >';
				datosOperaciones += '<div class="col-md-12" style="text-align:center;">';
				datosOperaciones += '<label>Operaciones activas:</label><span class="body3_B">'+operacionActual.length +'</span>';
				datosOperaciones += '<label>Operaciones en el historial:</label><span class="body3_B">'+historialOperaciones.length+'</span>';
				datosOperaciones += '</div>';
				datosOperaciones += '</div>';
				datosOperaciones += '<br>';
				datosOperaciones += '<br>';
				
				if(operacionActual.length > 0){
					
					datosOperaciones += '<div class="row caja_perfil" style="margin: auto; padding-top: 24px; padding-bottom: 24px;" >';
					
					datosOperaciones += '<div class="col-md-12" style="text-align:center;">';
					datosOperaciones += '<label>Operación activa</label>';
					datosOperaciones += '</div>';
					
					datosOperaciones += '<div class="col-md-6">';
					datosOperaciones += '<label>Estacionamiento</label><span class="body3_B">'+operacionActual[0].nombre+'</span>';
					datosOperaciones += '</div>';
					
					datosOperaciones += '<div class="col-md-6">';	
					datosOperaciones += '<label>Fecha ingreso</label><span class="body3_B">'+operacionActual[0].hora_entrada+'</span>';	
					datosOperaciones += '</div>';	
					datosOperaciones += '<div class="col-md-6">';
					datosOperaciones += '<label>Hora estimada de entrega</label><span class="body3_B">'+operacionActual[0].hora_estimada_entrega+'</span>';	
					datosOperaciones += '</div>';	
					datosOperaciones += '<div class="col-md-6">';
					datosOperaciones += '<label>Hora salida</label><span class="body3_B">'+operacionActual[0].hora_salida+'</span>';	
					datosOperaciones += '</div>';
					datosOperaciones += '<div class="col-md-6">';

					if(operacionActual[0].status == 1){
						datosOperaciones += '<label>Status</label><span class="body3_B">Reservado</span>';	
					}else if(operacionActual[0].status == 3){
						datosOperaciones += '<label>Status</label><span class="body3_B">Se registró la entrada del auto y se espera aceptación</span>';	
					}else if(operacionActual[0].status == 5){
						datosOperaciones += '<label>Status</label><span class="body3_B">Servicio activo</span>';	
					}else if(operacionActual[0].status == 7){
						datosOperaciones += '<label>Status</label><span class="body3_B">El cliente solicitó su auto</span>';	
					}else if(operacionActual[0].status == 11){
						datosOperaciones += '<label>Status</label><span class="body3_B">Solicitud aprobada por el operador</span>';	
					}else if(operacionActual[0].status == 13){
						datosOperaciones += '<label>Status</label><span class="body3_B">Calificación de servicio</span>';	
					}else if(operacionActual[0].status == -1){
						datosOperaciones += '<label>Status</label><span class="body3_B">Expirado</span>';
					}

					datosOperaciones += '</div>';

					datosOperaciones += '</div>';
					datosOperaciones += '<br>';
				}
				
				datosOperaciones += '<br>';
				
				if(historialOperaciones.length > 0)
				{
					
					jQuery.each(historialOperaciones, function( key, value ) {
						
						datosOperaciones += '<div class="row caja_perfil" style="margin: auto; padding-top: 24px; padding-bottom: 24px;" >';
						
//						datosOperaciones += '<div class="col-md-12" style="text-align:center;">';
//						if(value.status > 0 && value.status < 11){
//							datosOperaciones += '<label>OPERACIÓN ACTIVA</label>';
//						}else{
//							datosOperaciones += '<label>HISTORIAL</label>';
//						}
//						datosOperaciones += '</div>';
						
						datosOperaciones += '<div class="col-md-6">';
						datosOperaciones += '<label>Estacionamiento</label><span class="body3_B">'+value.estacionamiento.nombre+'</span>';
						datosOperaciones += '</div>';
						datosOperaciones += '<div class="col-md-6">';	
						datosOperaciones += '<label>Fecha ingreso</label><span class="body3_B">'+value.operacion.FechaIngreso+'</span>';	
						datosOperaciones += '</div>';	
						datosOperaciones += '<div class="col-md-6">';
						datosOperaciones += '<label>Hora estimada de entrega</label><span class="body3_B">'+value.operacion.HoraEstimadaEntrega+'</span>';	
						datosOperaciones += '</div>';	
						datosOperaciones += '<div class="col-md-6">';
						datosOperaciones += '<label>Hora salida</label><span class="body3_B">'+value.operacion.HoraSalida+'</span>';	
						datosOperaciones += '</div>';
						datosOperaciones += '<div class="col-md-6">';
					
						if(value.operacion.status == 1){
							datosOperaciones += '<label>Status</label><span class="body3_B">Reservado</span>';	
						}else if(value.operacion.status == 3){
							datosOperaciones += '<label>Status</label><span class="body3_B">Se registró la entrada del auto y se espera aceptación</span>';	
						}else if(value.operacion.status == 5){
							datosOperaciones += '<label>Status</label><span class="body3_B">Servicio activo</span>';	
						}else if(value.operacion.status == 7){
							datosOperaciones += '<label>Status</label><span class="body3_B">El cliente solicitó su auto</span>';	
						}else if(value.operacion.status == 11){
							datosOperaciones += '<label>Status</label><span class="body3_B">Solicitud aprobada por el operador</span>';	
						}else if(value.operacion.status == 13){
							datosOperaciones += '<label>Status</label><span class="body3_B">Calificación de servicio</span>';	
						}else if(value.operacion.status == -1){
							datosOperaciones += '<label>Status</label><span class="body3_B">Expirado</span>';
						}
							
						datosOperaciones += '</div>';
						
						datosOperaciones += '<div class="col-md-6">';
						datosOperaciones += '<label>Monto</label><span class="body3_B">'+value.operacion.monto+'</span>';	
						datosOperaciones += '</div>';
						datosOperaciones += '</div>';
						datosOperaciones += '<br>';
						
					});
				}
				else
				{
					datosOperaciones += '<div class="row caja_perfil" style="margin: auto; padding-top: 24px; padding-bottom: 24px;" >';
					datosOperaciones += '<div class="col-md-12" style="text-align:center;">';
					datosOperaciones += '<label>No existen operaciones por el momento</label>';
					datosOperaciones += '</div>';	
					datosOperaciones += '</div>';
				}
				jQuery("#operaciones").append(datosOperaciones);



				if(pensiones.length > 0 || pensionesCompartidas[0].length > 0){

					//----------------PENSIONES------------------------------

					if(pensiones.length > 0)
					{

						jQuery.each(pensiones, function( key, value ) {
							var datosPensiones = "";
							datosPensiones += '<div class="row caja_perfil" style="margin: auto; padding-top: 24px; padding-bottom: 24px;" >';
							datosPensiones += '<div class="col-md-12" style="text-align:center;">';
							datosPensiones += '<label>PENSIÓN NORMAL</label>';
							datosPensiones += '</div>';
							datosPensiones += '<div class="col-md-12">';
							datosPensiones += '<label>Estacionamiento</label><span class="body3_B">'+value.nombreEstacionamiento+'</span>';
							datosPensiones += '</div>';
							datosPensiones += '<div class="col-md-12">';	
							datosPensiones += '<label>Fecha contratación</label><span class="body3_B">'+value.fechaContratacion+'</span>';	
							datosPensiones += '</div>';	
//							datosPensiones += '<div class="col-md-12">';	
//							datosPensiones += '<label>Meses contratados:</label><span class="body3_B">'+value.mesesContratados+'</span>';	
//							datosPensiones += '</div>';	
							datosPensiones += '<div class="col-md-12">';
							
							if(value.horario == 1){
								datosPensiones += '<label>Horario:</label><span class="body3_B">Día</span>';	
							}else if(value.horario == 3){
								datosPensiones += '<label>Horario:</label><span class="body3_B">Noche</span>';	
							}else if(value.horario == 5){
								datosPensiones += '<label>Horario:</label><span class="body3_B">Todo el día</span>';	
							}
							
							datosPensiones += '</div>';
							datosPensiones += '<div class="col-md-12">';	
							datosPensiones += '<label>Costo:</label><span class="body3_B">$'+value.costo+'</span>';	
							datosPensiones += '</div>';
							datosPensiones += '</div>';
							datosPensiones += '<br>';
							jQuery("#pensiones").append(datosPensiones);
						 });

					}

					 //--------------PENSIONES COMPARTIDAS--------------------

					if(pensionesCompartidas[0].length > 0)
					{
						var idCompartida = null; 
						var estacionamientos = "";

						jQuery.each(pensionesCompartidas[0], function( key, value ) 
						{

							if(idCompartida != value.idCompartida)
							{
								estacionamientos = "";
								idCompartida = value.idCompartida;

								jQuery.each(pensionesCompartidas[0], function( key, value )
								{

									if(value.idCompartida == idCompartida)
									{
										var horario = "";
										if(value.horario == 1){
											horario = "Día";
										}else if(value.horario == 3){
											horario = "Noche";
										}else if(value.horario == 5){
											horario = "Todo el día";	
										}
										
										estacionamientos += value.nombreEstacionamiento + " " + horario + " $" + value.costo;
										
										if(key == pensionesCompartidas[0].length - 1){
											estacionamientos += ".";	
										}else{
											estacionamientos += ",  ";	
										}
										
									}

								});


								var datosPensionesCompartidas = "";
								datosPensionesCompartidas += '<div class="row caja_perfil" style="margin: auto; padding-top: 24px; padding-bottom: 24px;" >';
								datosPensionesCompartidas += '<div class="col-md-12" style="text-align:center;">';
								datosPensionesCompartidas += '<label>PENSIÓN COMPARTIDA</label>';
								datosPensionesCompartidas += '</div>';
								datosPensionesCompartidas += '<div class="col-md-12">';
								datosPensionesCompartidas += '<label>Estacionamientos</label><span class="body3_B">'+estacionamientos+'</span>';
								datosPensionesCompartidas += '</div>';
								datosPensionesCompartidas += '<div class="col-md-12">';	
								datosPensionesCompartidas += '<label>Fecha contratación</label><span class="body3_B">'+value.fechaContratacion+'</span>';	
								datosPensionesCompartidas += '</div>';
//								datosPensionesCompartidas += '<div class="col-md-12">';	
//								datosPensionesCompartidas += '<label>Meses contratados</label><span class="body3_B">'+value.mesesContratados+'</span>';	
//								datosPensionesCompartidas += '</div>';	
								datosPensionesCompartidas += '</div>';
								datosPensionesCompartidas += '<br>';

								jQuery("#pensiones").append(datosPensionesCompartidas);
							}

						 });

					}

				}
				else
				{
					var sinPensiones = "";
					sinPensiones += '<div class="row caja_perfil" style="margin: auto; padding-top: 24px; padding-bottom: 24px;" >';
					sinPensiones += '<div class="col-md-12" style="text-align:center;">';
					sinPensiones += '<label>No existen pensiones por el momento</label>';
					sinPensiones += '</div>';	
					sinPensiones += '</div>';
					jQuery("#pensiones").append(sinPensiones);
				}


				 //-------------------MÉTODOS DE PAGO------------------------------

				 if(metodos.length > 0)
				 {
					jQuery("#metodos").empty();
					var contador = 0;
					jQuery.each(metodos, function( key, value ) {
						contador++;
						var datosMetodos = "";
						datosMetodos += '<div class="row caja_perfil" style="margin: auto; padding-top: 24px; padding-bottom: 24px;" >';
						datosMetodos += '<div class="col-md-12" style="text-align:center;">';
						datosMetodos += '<label>MÉTODO DE PAGO '+contador+'</label>';
						datosMetodos += '</div>';
						datosMetodos += '<div class="col-md-12">';
						datosMetodos += '<label>Cuenta</label><span class="body3_B">'+value.nombreCuenta+'</span>';
						datosMetodos += '</div>';
						datosMetodos += '<div class="col-md-12">';	
						datosMetodos += '<label>Tarjeta</label><span class="body3_B">'+value.nombreTarjeta+'</span>';	
						datosMetodos += '</div>';	
						datosMetodos += '</div>';
						datosMetodos += '<br>';
						jQuery("#metodos").append(datosMetodos);
					});
				}
				else
				{
					var sinMetodos = "";
					sinMetodos += '<div class="row caja_perfil" style="margin: auto; padding-top: 24px; padding-bottom: 24px;" >';
					sinMetodos += '<div class="col-md-12" style="text-align:center;">';
					sinMetodos += '<label>No existen métodos de pago registrados</label>';
					sinMetodos += '</div>';	
					sinMetodos += '</div>';
					jQuery("#metodos").append(sinMetodos);
				}
				 //------------------------------------------------------------

				 jQuery("#myTab").show();
			 
			}
			else
			{
				jQuery("#perfil").empty();
				jQuery("#operaciones").empty();
				jQuery("#pensiones").empty();
				jQuery("#metodos").empty();
				notificarUsuario('error', 'Error', 'No se encontró al usuario con el VEEPEID ingresado.');
			}
			 
		},
		function () {
		
		},
		true,
		0.0);
}



// _____________________________________________________________________________________________________________ JQUERY
jQuery(document).ready(function ()
{
	// _____________________________________________________________________ configuraciones
	jQuery("#myTab").hide();
	// _____________________________________________________________________ entrypt

});// fin del document.ready
