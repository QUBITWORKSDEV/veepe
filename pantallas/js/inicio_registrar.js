"use strict";

// _____________________________________________________________________________________________________________ VARIABLES GLOBALES

// _____________________________________________________________________________________________________________ FUNCIONES GLOBALES

// _____________________________________________________________________________________________________________ JQUERY
jQuery(document).ready(function ()
{
	// _____________________________________________________________________ configuraciones
	jQuery('#lnkTerminos').click(function(){
		jQuery('#overlayTerminos').show();
	});
	
	jQuery('#cmdCerrarTerminos').click(function(){
		jQuery('#overlayTerminos').hide();
	});

	jQuery('#formLogin').formulario({
		botonEnviar:	jQuery('button#registrar'),
		parametrosCLA: {		// lista de parámetros a usarse en la llamada CLA. Contiene:
			datos: {
				m:	'altaUsuario',
				u:	'jQuery("input#email").val()',
				p:	'jQuery("input#password").val()'
			},
			prefuncion: function(){
				var esValido = true;

				// validamos que los dos correos sean iguales
				if ( jQuery('input#email').val() != jQuery('input#confirm_email').val() )
				{
					jQuery('#alerta_email').fadeIn(100);
					//limpiar_input('input#email');
					jQuery('input#confirm_email').val('');
					jQuery('input#email').focus();
		
					esValido = false;
				}
		
				// validamos que las dos nuevas contraseñas sean iguales
				if ( jQuery('input#password').val() != jQuery('input#confirm_password').val() )
				{
					jQuery('#alerta_password').fadeIn(100);
					//limpiar_input('input#password');
					jQuery('input#confirm_password').val('');
					jQuery('input#password').focus();
					esValido = false;
				}
				
				if ( esValido )
					jQuery('button#registrar').html('REGISTRANDO...');
					
				return esValido;
			},
			postfuncion: function(response){
				if ( response == 1 ) {
					notificarUsuario('exito', 'Registro exitoso', 'Recibirás un correo para confirmar tu cuenta; sigue las instrucciones y completa tu registro.', 'CERRAR', function(){
						window.location = './';
					});
				}
				else {
					jQuery('button#registrar').html('REGISTRARTE');

					if ( response == 0 ) {
						notificarUsuario('error', 'Por favor intenta de nuevo', 'Ya existe una cuenta asociada a ese correo. Por favor intenta usando otro o inicia sesión.');
						jQuery('input#email, input#confirm_email').val('');
						jQuery('input#email, input#confirm_email').removeClass('error cruz success paloma');
						jQuery('input#password, input#confirm_password').val('');
						jQuery('input#password, input#confirm_password').removeClass('error cruz success paloma');
						jQuery('input#email').focus();
					}
					else {
						notificarUsuario('generico', '¡Upps! No sabemos que acaba de suceder...', 'No sabemos qué es lo que acaba de suceder. Te agradecemos intentar la acción nuevamente.');
					}
				}
			},
			delayInicial: 0.0
		}
	});

	/*jQuery('#formLogin input').keyup(function(e){
		if (e.which == 13)
			jQuery('button#registrar').click();
	});*/

	// _____________________________________________________________________ entrypt
	jQuery('input#email').focus();


});// fin del document.ready
