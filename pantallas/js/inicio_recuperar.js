'use strict';

// _____________________________________________________________________________________________________________ VARIABLES GLOBALES

// _____________________________________________________________________________________________________________ FUNCIONES GLOBALES


// _____________________________________________________________________________________________________________ JQUERY
jQuery(document).ready(function ()
{
	// _____________________________________________________________________ configuraciones
	jQuery('#formRecuperar').formulario({
		botonEnviar:	jQuery('button#cmdRecuperar'),
		parametrosCLA: {
			datos: {
				m:		'recuperarContrasenha',
				email:	'jQuery("input#email").val()'
			},
			prefuncion: function(){
				jQuery('input, button').attr('disabled', 'disabled');
			},
			postfuncion: function(response){
				jQuery('input, button').removeAttr('disabled');
				switch (response) {
					case 1:
						notificarUsuario('exito', '', 'Se ha enviado un correo con tu contraseña.', 'CERRAR', function(){
							window.location = './ingresar';
						});
						break;
					case 0:
						notificarUsuario('error', 'Error', 'No existe una cuenta asociada a ese correo. Por favor intenta de nuevo.', 'CERRAR', function () {
							window.location = './inicio_recuperar';
						});
						break;
					case 3:
					default:
						notificarUsuario('generico', '¡Upps! No sabemos que acaba de suceder...', 'No sabemos qué es lo que acaba de suceder. Te agradecemos intentar la acción nuevamente.', 'CERRAR', function () {
							window.location = './inicio_recuperar';
						});
						break;
				}
			},
			delayInicial: 0.0
		}
	});

	// _____________________________________________________________________ entrypt
	jQuery('#txtNombre').focus();

});// fin del document.ready
