"use strict";

// _____________________________________________________________________________________________________________ VARIABLES GLOBALES

// _____________________________________________________________________________________________________________ FUNCIONES GLOBALES
function agregarCliente(op) {
	var esValido = libValida.validarFormulario(jQuery('#frmAltaCliente'));

	if (!esValido)
		return;

	var param = new Object();
	param.nombre = jQuery('#frmAltaCliente #txtNombreCliente').val();
	param.apellido_paterno = jQuery('#frmAltaCliente #txtApellidoPaterno').val();
	param.apellido_materno = jQuery('#frmAltaCliente #txtApellidoMaterno').val();
	param.email = jQuery('#frmAltaCliente #txtEmailCliente').val();
	param.celular = jQuery('#frmAltaCliente #txtCelularCliente').val();

	param.nombre_empresa = jQuery('#frmAltaCliente #txtEmpresa').val();
	param.contacto = jQuery('#frmAltaCliente #txtContacto').val();
	param.emailEmpresa = jQuery('#frmAltaCliente #txtEmailEmpresa').val();
	param.celularEmpresa = jQuery('#frmAltaCliente #txtCelularEmpresa').val();
	param.telefonoEmpresa = jQuery('#frmAltaCliente #txtTelefonoEmpresa').val();
	param.direccionEmpresa = jQuery('#frmAltaCliente #txtDireccionEmpresa').val();
	param.puesto = jQuery('#frmAltaCliente #txtPuesto').val();

	if (op == "E") {
		param.uid = uid;
	}

	CLA.encolar("ajax.php",
		{
			m: 'altaClienteEmpresa',
			p: param,
			op: op
		},
		
		function (response) {
			response = jsoner.parse(response);

			if (response == true) {
				jQuery("#btn_cliente_alta").html('REDIRIGIENDO...');
				location.href = './clientes';
			} else if (response == 0) {
				jQuery("#btn_cliente_alta").html('GUARDAR');
				jQuery('button, input, textarea').removeAttr('disabled');
				libValida.crearMensajeError(jQuery('#frmAltaCliente input#txtEmailCliente'), ('Correo incorrecto'));
				notificarUsuario("error", "Correo incorrecto", "Ya existe un usuario con este correo.");
			} else {
				jQuery("#btn_cliente_alta").html('GUARDAR');
				jQuery('button, input, textarea').removeAttr('disabled');
				notificarUsuario("error", "Error", "No se pudo realizar la acción.");
			}
		},
		
		function () {
			jQuery("#btn_cliente_alta").html('GUARDANDO...');
			jQuery('button, input, textarea').attr('disabled', 'disabled');
		},
		true,
		0.0);
}

function cancelarAltaCliente() {
	location.href = './clientes';
}

// _____________________________________________________________________________________________________________ JQUERY
jQuery(document).ready(function ()
{
	// _____________________________________________________________________ configuraciones

	// _____________________________________________________________________ entrypt

});// fin del document.ready

