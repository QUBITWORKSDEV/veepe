"use strict";

// _____________________________________________________________________________________________________________ VARIABLES GLOBALES

// _____________________________________________________________________________________________________________ FUNCIONES GLOBALES
function cancelarEditarPerfil() {
	location.href = './perfil';
}

// _____________________________________________________________________________________________________________ JQUERY

jQuery(document).ready(function () {
	// _____________________________________________________________________ configuraciones
	jQuery('#frmPerfilEditar').formulario({
		botonEnviar:		jQuery('#cmdModificar'),
		esLlamadaCLA:		false,
		onEnviar:			function(){
			var cambiarContrasena = false;
			var param = {
				nombre:				jQuery('#frmPerfilEditar #txtNombre').val(),
				apellido_paterno:	jQuery('#frmPerfilEditar #txtApellidoPaterno').val(),
				apellido_materno:	jQuery('#frmPerfilEditar input#txtApellidoMaterno').val(),
				email:				jQuery('#frmPerfilEditar input#txtEmail').val(),
				celular:			jQuery('#frmPerfilEditar input#txtCelular').val(),
				contrasenaActual:	jQuery('#frmPerfilEditar input#txtContrasenaActual').val(),
				contrasenaNueva:	jQuery('#frmPerfilEditar input#txtContrasenaNueva').val(),
				contrasenaNueva2:	jQuery('#frmPerfilEditar input#txtContrasenaNueva2').val()
			};

			if (param.contrasenaActual != '' || param.contrasenaNueva != '' || param.contrasenaNueva2 != '') {
		
				if (param.contrasenaActual != '') {
		
					if (param.contrasenaActual != '' && param.contrasenaNueva != '' && param.contrasenaNueva2 != '') {
		
						if (param.contrasenaNueva != param.contrasenaNueva2) {
							notificarUsuario('error', 'Error', 'Las contraseñas en los campos no coinciden.');
		
							libValida.crearMensajeError(jQuery('#frmPerfilEditar input#txtContrasenaNueva'), ('Este campo no puede estar vacío'));
							libValida.crearMensajeError(jQuery('#frmPerfilEditar input#txtContrasenaNueva2'), ('Este campo no puede estar vacío'));
		
							cambiarContrasena = false;
							return;
						} else {
							cambiarContrasena = true;
						}
		
					} else {
						libValida.crearMensajeError(jQuery('#frmPerfilEditar input#txtContrasenaNueva'), ('Este campo no puede estar vacío'));
						libValida.crearMensajeError(jQuery('#frmPerfilEditar input#txtContrasenaNueva2'), ('Este campo no puede estar vacío'));
						return;
					}
		
				} else {
					libValida.crearMensajeError(jQuery('#frmPerfilEditar input#txtContrasenaActual'), ('Este campo no puede estar vacío'));
					return;
				}
			} else {
				cambiarContrasena = false;
			}
		
			notificarUsuario('', 'Confirmación', '¿De verdad deseas modificar tu perfil?', 'MODIFICAR', function () {
				CLA.encolar("ajax.php",
					{
						m:					'modificarPerfil',
						p:					param,
						uid:				null,	// para que tome el del usuario actualmente loggeado
						cambiarContrasena: cambiarContrasena
					},
					
					function (response) {
						response = jsoner.parse(response);
		
						if (response+'' == '1')
						{
							jQuery('#btn_overlay_si').html('REDIRIGIENDO...');
							location.href = './perfil';
						}
						else if (response+'' == '2')
						{
							jQuery("#btn_overlay_si").html('MODIFICAR');
							jQuery('input, button, select, textarea').removeAttr('disabled');
							
							libValida.crearMensajeError(jQuery('#frmPerfilEditar input#txtContrasenaActual'), ('Contraseña incorrecta'));
							notificarUsuario('error', 'Contraseña incorrecta', 'Tu contraseña es incorrecta.');
						}
						else if (response+'' == '0')
						{
							jQuery("#btn_overlay_si").html('MODIFICAR');
							jQuery('input, button, select, textarea').removeAttr('disabled');
							notificarUsuario('error', 'Error', 'No se pudo realizar la acción.');
						}
					},
					
					function () {
						jQuery("#btn_overlay_si").html('MODIFICANDO...');
						jQuery('input, button, select, textarea').attr('disabled', 'disabled');
					},
					true,
					0.0);
			}, 'CANCELAR', function () {
				ocultarNotificacion();
			})

		},
	});

	// _____________________________________________________________________ entrypt
	jQuery("#menuLateral").hide();
});












