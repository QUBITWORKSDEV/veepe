"use strict";

// _____________________________________________________________________________________________________________ VARIABLES GLOBALES

// _____________________________________________________________________________________________________________ FUNCIONES GLOBALES

// _____________________________________________________________________________________________________________ JQUERY
jQuery(document).ready(function ()
{
	// _____________________________________________________________________ configuraciones
	jQuery('#formNip').formulario({
		botonEnviar:	jQuery('button#cmdResetear'),
		parametrosCLA: {
			datos: {
				m:		'resetearNip',
				nip:	'jQuery("#nip1").val()',
				token:	token
			},
			prefuncion: function(){
				var esValido = true;
				
				if ( jQuery('input#nip1').val() != jQuery('input#nip2').val() )
				{
					libValida.crearMensajeError(jQuery('#nip1'), 'Los NIP deben coincidir');
					libValida.crearMensajeError(jQuery('#nip2'), 'Los NIP deben coincidir');
					jQuery('input#nip1').focus();
					esValido = false;
				}

				if ( esValido )
					jQuery('button#registrar').html('ACTUALIZANDO...');
					
				console.log(jQuery('#nip1').val());
				return esValido;
			},
			postfuncion: function(response) {
				if ( response == true )
				{
					notificarUsuario('exito', '', 'Se actualizó correctamente el NIP.', 'CERRAR', function(){
						window.location = '/';
					});
				}
				else
				{
					console.log(response);
					notificarUsuario('error', 'Error', 'Sucedió un error al actualizar el NIP. Inténtalo nuevamente.', 'Cerrar');
				}
			},
			delayInicial: 0.0
		},
	});


	// _____________________________________________________________________ entrypt
	jQuery('input#nip1').focus();

});// fin del document.ready
