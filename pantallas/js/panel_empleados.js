"use strict";

// _____________________________________________________________________________________________________________ VARIABLES GLOBALES

// _____________________________________________________________________________________________________________ FUNCIONES GLOBALES

function editarEmpleado(uid) {
	location.href = './alta_empleado?idempleado=' + uid;
}

function altaEmpleado() {
	location.href = './alta_empleado';
}

function detalleEmpleado(uid) {
	location.href = './detalle_empleado?idempleado=' + uid;
}

function emailEmpleado(email) {
	window.location = 'mailto:' + email;
}

function eliminarEmpleado(uid) {
	notificarUsuario('', 'Confirmación', '¿De verdad deseas eliminar a este empleado?', 'ELIMINAR', function () {
		CLA.encolar("ajax.php",
			{
				m: 'eliminarEmpleado',
				uid: uid
			},
			
			function (response) {
				response = jsoner.parse(response);

				if (response == true) {
					jQuery("#btn_overlay_si").html('REDIRIGIENDO...');
					location.reload();
				} else {
					jQuery("#btn_overlay_si").html('ELIMINAR');
					jQuery('input, button, select, textarea').removeAttr('disabled');
					notificarUsuario('error', 'Error', 'No se pudo realizar la acción.');
				}
			},
			
			function () {
				jQuery("#btn_overlay_si").html('ELIMINANDO...');
				jQuery('input, button, select, textarea').attr('disabled', 'disabled');
			},
			true,
			0.0);
	}, 'CANCELAR', function () {
		ocultarNotificacion();
	})
}

// _____________________________________________________________________________________________________________ JQUERY
jQuery(document).ready(function ()
{
	// _____________________________________________________________________ configuraciones

	// _____________________________________________________________________ entrypt

	jQuery('#tblEmpleados').DataTable({
		"dom": '<"toolbar"f>rtp',
		"columnDefs": [
			//{"width": "170px", "targets": 3},
			{'bSortable': false, 'aTargets': esAdmin ? [0, 5] : [0, 6]}
		],
		"language": {
			"zeroRecords": "No hay información disponible",
			paginate: {
				previous: 'Anterior',
				next: 'Siguiente'
			},
			aria: {
				paginate: {
					previous: 'Anterior',
					next: 'Siguiente'
				}
			}
		},
		"pageLength": 10
	});

	jQuery('#tblEmpleados_wrapper .toolbar').prepend('<span class="encabezado">Empleados</span>');

	jQuery('#tblEmpleados_filter').prepend('<div class="lupa"></div>');
	jQuery('input[type=search]').attr("placeholder", "Buscar ...");
	jQuery('input[type=search]').css("font-weight", "600");
	jQuery('input[type=search]').css("padding-left", "24px");

});// fin del document.ready
