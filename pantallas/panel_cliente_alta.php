<?php
if (isset($_GET['idcliente'])) {
	$id_cliente = $_GET['idcliente'];
	$cliente = db_query("select
					pu.*,
					e.nombre_empresa,
					e.contacto,
					e.puesto,
					e.email as emailEmpresa,
					e.direccion as direccionEmpresa,
					e.celular as celularEmpresa,
					e.telefono as telefonoEmpresa,
					date_format(FROM_UNIXTIME(us.created), '%Y-%m-%d') as fechaCreacion

					from f_dc_perfilusuario pu
					inner join users us on us.uid = pu.uid
					inner join users_roles ur on ur.uid = pu.uid
					inner join f_r_usuario_empresa ue on ue.uid = pu.uid
					inner join f_dc_empresas e on e.id_empresa = ue.id_empresa
					where ur.rid = 6 and pu.status=1 and pu.uid=" . $id_cliente)->fetchAll();

	$rol = getRolesUsuario($id_cliente);
} else {
	$cliente = null;
	$id_cliente = null;
}
?>

<style>
	body{
		background-image:none;
	}
</style>

<script src="pantallas/js/panel_cliente_alta.js"></script>

<script>
	jQuery("#menuLateral").hide();
	//var uid = "<?php /* echo $id_cliente; */ ?>";
	btn_regresar("./clientes");
	var uid = "<?php echo $id_cliente ?>";
</script>

<div>
	<div class="row" style="margin-bottom: 20px;">
		<center>
			<!--Foto-->
			<div class="img_usuario_n"></div>

			<!--Nombre del usuario-->
			<div>
				<?php if (isset($_GET['idcliente'])) { ?>
					<h1 class="white"><?php echo $cliente[0]->nombre . " " . $cliente[0]->apellido_paterno . " " . $cliente[0]->apellido_materno; ?></h1>
					<h2 class="white"><?php echo getNombreRol($rol[0]->rid) ?></h2>
				<?php } else { ?>
					<h1 class="white">Alta de Cliente</h1>
				<?php } ?>
			</div><!-- end row-->
		</center>
	</div><!-- end row-->

	<!--Fomulario "formaDark" _____________________________________________________________________ -->
	<div id="frmAltaCliente" class="formaDark" style="max-width: 848px; margin: 0 auto;">

		<!--Registro ___________________________________________________________________________________ -->
		<div>
			<div style="text-align:center">
				<h3 class="white">Datos del Cliente</h3>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="requerido">
						<input id="txtNombreCliente" type="text" placeholder="Nombre" value="<?php echo ($cliente == null ? "" : $cliente[0]->nombre); ?>" data-valida="requerido" />
						<div class="marca"></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="requerido">
						<input id="txtApellidoPaterno" type="text" placeholder="Apellido paterno" value="<?php echo ($cliente == null ? "" : $cliente[0]->apellido_paterno); ?>" data-valida="requerido"/>
						<div class="marca"></div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<input id="txtApellidoMaterno" type="text" placeholder="Apellido materno" value="<?php echo ($cliente == null ? "" : $cliente[0]->apellido_materno); ?>"/>
				</div>
				<?php if (!isset($_GET['idcliente'])) { ?>
					<div class="col-md-6">
						<div class="requerido">
							<input id="txtEmailCliente" type="text" placeholder="Correo Electrónico" data-valida="requerido,email" />
							<div class="marca"></div>
						</div>
					</div>
				<?php } else { ?>
					<div class="col-md-6">
						<input id="txtEmailCliente" disabled="disabled" type="text" value="<?php echo ($cliente == null ? "" : $cliente[0]->email); ?>" data-valida="requerido,email" />
						<div class="marca"></div>
					</div>
				<?php } ?>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="requerido">
						<input id="txtCelularCliente" type="text" placeholder="Celular" value="<?php echo ($cliente == null ? "" : $cliente[0]->celular); ?>" data-valida="requerido" />
						<div class="marca"></div>
					</div>
				</div>
			</div>

			<div style="text-align:center">
				<h3 class="white">Datos de la empresa</h3>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="requerido">
						<input id="txtEmpresa" value="<?php echo ($cliente == null ? "" : $cliente[0]->nombre_empresa); ?>" type="text" placeholder="Nombre de la empresa" data-valida="requerido"/>
						<div class="marca"></div>
					</div>
				</div>
			</div><!-- end row-->

			<div class="row">
				<div class="col-md-12">
					<div class="requerido">
						<input id="txtContacto" value="<?php echo ($cliente == null ? "" : $cliente[0]->contacto); ?>" type="text" placeholder="Nombre del contacto" data-valida="requerido"/>
						<div class="marca"></div>
					</div>
				</div>
			</div><!-- end row-->

			<div class="row">
				<div class="col-md-6">
					<div class="requerido">
						<input id="txtEmailEmpresa" value="<?php echo ($cliente == null ? "" : $cliente[0]->emailEmpresa); ?>" type="email" placeholder="Correo electrónico" data-valida="requerido,email">
						<div class="marca"></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="requerido">
						<input id="txtPuesto" value="<?php echo ($cliente == null ? "" : $cliente[0]->puesto); ?>" type="text" placeholder="Puesto" data-valida="requerido"/>
						<div class="marca"></div>
					</div>
				</div>
			</div><!-- end row-->

			<div class="row">
				<div class="col-md-6">
					<div class="requerido">
						<input id="txtCelularEmpresa" value="<?php echo ($cliente == null ? "" : $cliente[0]->celularEmpresa); ?>" type="text"   placeholder="Celular" data-valida="requerido"/>
						<div class="marca"></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="requerido">
						<input id="txtTelefonoEmpresa" value="<?php echo ($cliente == null ? "" : $cliente[0]->telefonoEmpresa); ?>" type="text"   placeholder="Teléfono" data-valida=""/>
					</div>
				</div>
			</div><!-- end row-->

			<div class="row">
				<div class="col-md-12">
					<div class="requerido">
						<textarea id="txtDireccionEmpresa" rows="6" cols="40" placeholder="Dirección" data-valida="requerido"><?php echo ($cliente == null ? "" : $cliente[0]->direccionEmpresa); ?></textarea>
						<div class="marca"></div>
					</div>
				</div>
			</div><!-- end row-->

		</div><!-- end row-->

		<!--Botón Registrar .................................................. -->
		<div class="row">
			<div class="col-md-6">
				<div class="pull-right cancelar">
					<button type="button" class="BB_Fantasma_N" onclick="cancelarAltaCliente()">CANCELAR</button>
				</div>
			</div>
			<div class="col-md-6">
				<div class="pull-left guardar">
					<?php if (!isset($_GET['idcliente'])) { ?>
						<button id="btn_cliente_alta" class="B_Regular_N" type="button" onclick="agregarCliente('A')">GUARDAR</button>
					<?php } else { ?>
						<button id="btn_cliente_alta" class="B_Regular_N" type="button" onclick="agregarCliente('E')">MODIFICAR</button>
					<?php } ?>
				</div>
			</div>
		</div><!-- end row-->

		<div style="text-align: center; margin: 24px 0px;">Se le enviará una notificación por correo, con su información.</div>

	</div><!-- end .formDark-->
</div><!-- end .centerme-->
