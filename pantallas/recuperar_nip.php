<?php
	$token = isset($_GET["t"]) ? $_GET["t"] : null;
	
	if ( $token==null )
	{
		// todo: escribir mensaje de error
		echo '<center style="margin-top:42px;"><h3>La url es inválida</h3></center>';
	}
	else
	{
		// validamos el token
		$idUsuario = db_query("select uid from f_dc_perfilusuario where status=1 and token_nip=:nu", array(":nu" => $token))->fetchField();
		
		if ( $idUsuario!=null )
		{
			echo "<script>var token='".$token."';</script>";
		?>
<script src="pantallas/js/recuperar_nip.js"></script>

<div class="container">
	<div class="centerme">

		<!--Fomulario "formaDark" _____________________________________________________________________ -->
		<div id="formNip" class="formaDark" style="">
			<!--Logo Veepe-->
			<div class="row">
				<div class="logo_veepe_login" style="margin-bottom: 39px"></div>
			</div><!-- end row-->

			<center><h3 style="color:white">Actualización de NIP</h3></center><br><br>

			<!-- nip -->
			<div class="row row_validador">
				<div class="requerido">
					<input id="nip1" type="text" maxlength="4" placeholder="Nuevo NIP" style="text-align: center;" data-valida="requerido,numero,{longitud|4|=}">
				</div>
			</div><!-- end row-->

			<!-- nip2 -->
			<div class="row row_validador">
				<div class="requerido">
					<input id="nip2" type="text" maxlength="4" placeholder="Confirmar NIP" style="text-align: center;" data-valida="requerido,numero,{longitud|4|=}">
				</div>
			</div><!-- end row-->

			<div class="row" style="display: block; margin-top: 16px; margin-bottom: 24px;">
				<center>
					<button id="cmdResetear" type="button" class="B_Regular_N">Actualizar NIP</button>
				</center>
			</div><!-- end row-->
		</div><!-- end .formDark-->

	</div><!-- end .centerme-->
</div><!-- end row-->
		<?php
		}
		else
			echo '<center style="margin-top:42px;"><h3>El token es inválido</h3></center>';
	}
?>

