<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
<script src="pantallas/js/panel_semaforo.js"></script>

<div class="centerme">
	<div style="margin-right: 24px" class="semaforo">
		<div class="encabezado">Sistema</div>
		<table cellpadding="0" cellspacing="0">
			<thead>
				<tr>
					<th width="40">&nbsp;</th>
					<th width="160">Funcionalidad</th>
					<th width="80" style="text-align: center">Estado</th>
					<th width="40">&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				<tr class="sistema_general" style="background-color: white">
					<td><img class="ico_semaforo" src="<?php echo drupal_get_path('theme', 'veepe'); ?>/img/iconos_menu/semaforo_i.svg" width="32" /></td>
					<td>Sistema general</td>
					<td style="text-align: center"><div class="indicador ok"></div></td>
					<td><button class="contactar"></button></td>
				</tr>
				<tr class="sitio_general" style="background-color: white">
					<td><img class="ico_semaforo" src="<?php echo drupal_get_path('theme', 'veepe'); ?>/img/iconos_menu/semaforo_i.svg" width="32" /></td>
					<td>Sitio general</td>
					<td style="text-align: center"><div class="indicador ok"></div></td>
					<td><button class="contactar"></button></td>
				</tr>
			</tbody>
			<tfoot>
				<tr><td colspan="4"></td></tr>
			</tfoot>
		</table>
	</div>
	
	<div class="semaforo">
		<div class="encabezado">Conexiones</div>
		<table cellpadding="0" cellspacing="0" border="0">
			<thead>
				<tr>
					<th width="40">&nbsp;</th>
					<th width="160">Funcionalidad</th>
					<th width="80" style="text-align: center">Estado</th>
					<th width="40">&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				<tr class="" style="background-color: white">
					<td><img class="ico_semaforo" src="<?php echo drupal_get_path('theme', 'veepe'); ?>/img/iconos_menu/semaforo_i.svg" width="32" /></td>
					<td>Plataforma de pago</td>
					<td style="text-align: center"><div class="indicador banwire ok"></div></td>
					<td><button class="contactar"></button></td>
				</tr>
				<!--<tr class="">
					<td><img class="ico_semaforo" src="<?php echo drupal_get_path('theme', 'veepe'); ?>/img/iconos_menu/semaforo_i.svg" width="32" /></td>
					<td>Waze</td>
					<td style="text-align: center"><div class="indicador waze ok"></div></td>
					<td><button class="contactar"></button></td>
				</tr>-->
				<!--<tr class="">
					<td><img class="ico_semaforo" src="<?php echo drupal_get_path('theme', 'veepe'); ?>/img/iconos_menu/semaforo_i.svg" width="32" /></td>
					<td>Google</td>
					<td style="text-align: center"><div class="indicador google ok"></div></td>
					<td><button class="contactar"></button></td>
				</tr>-->
				<tr class="" style="background-color: white">
					<td><img class="ico_semaforo" src="<?php echo drupal_get_path('theme', 'veepe'); ?>/img/iconos_menu/semaforo_i.svg" width="32" /></td>
					<td>Facebook</td>
					<td style="text-align: center"><div class="indicador facebook ok"></div></td>
					<td><button class="contactar"></button></td>
				</tr>
				<!--<tr class="">
					<td><img class="ico_semaforo" src="<?php echo drupal_get_path('theme', 'veepe'); ?>/img/iconos_menu/semaforo_i.svg" width="32" /></td>
					<td>Generador de reportes</td>
					<td style="text-align: center"><div class="indicador ok"></div></td>
					<td><button class="contactar"></button></td>
				</tr>-->
			</tbody>
			<tfoot>
				<tr><td colspan="4"></td></tr>
			</tfoot>
		</table>
	</div>
</div>


