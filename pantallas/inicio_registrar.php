<script src="pantallas/js/inicio_registrar.js"></script>

<div id="overlayTerminos" class="overlay" style="color: white; overflow: auto">
	<button id="cmdCerrarTerminos" style="position:fixed; right: 40px; top: 30px; background-image: url('sites/all/themes/veepe/img/botones/b_close_n.svg'); width: 30px; height: 30px; border: none; background-color: transparent; cursor: pointer"></button>
	<div style="max-width:70%; margin: 0 auto; padding: 40px 0">
		Términos y Condiciones de los Servicios de Veepe Parking<br>
		<br>
		Los siguientes términos y condiciones aplican para el uso de la plataforma y aplicación móvil (en conjunto la “Aplicación”) proporcionados por [VEEPE PARKING S.A.P.I. de C.V.] (“Veepe Parking”). Usted es el único responsable de su conducta en la Aplicación y del cumplimiento de estos términos y condiciones. <br>
		<br>
		Al usar o explorar la Aplicación, usted expresamente reconoce que leyó, comprendió y aceptó estar obligado por estos términos y condiciones.<br>
		<br>
		Veepe Parking provee el servicio de localización y reserva de estacionamientos, contratación de servicios de pensión y lavado de vehículos, así como el pago en línea de los servicios referidos (los “Servicios”).<br>
		<br>
		1. Términos Generales.<br>
		<br>
		Al usar la Aplicación, usted está de acuerdo en que la información que proporcione a través de la Aplicación podrá ser usada como mejor determine Veepe Parking, en los términos aquí señalados.<br>
		<br>
		La descarga de la Aplicación es gratuita <br>
		<br>
		Los Servicios proporcionados por Veepe Parking podrán cambiar sin previo aviso o bien, Veepe Parking podrá dejar de proporcionar los Servicios tanto a usted como a la generalidad de los usuarios, sin previo aviso.<br>
		<br>
		Aún y cuando la intención de Veepe Parking es la prestar los Servicios, usted reconoce que existirán ocasiones en las cuales los Servicios puedan ser interrumpidos, incluyendo enunciativa más no limitativamente, por mantenimiento, actualizaciones, reparaciones o por fallas en los equipos, interrupciones de la señal o cualquier otra, por las cuales los usuarios no podrán emitir reclamación alguna.<br>
		<br>
		Usted reconoce que su relación con Veepe Parking no es confidencial, fiscal, ni ningún tipo de relación especial, y la aceptación del uso que Veepe Parking dé a la información que usted proporcione a través de la Aplicación, de conformidad con los presentes Términos y Condiciones, no lo hace diferente a la posición en la cual se encuentra cualquier otro usuario de los Servicios.<br>
		<br>
		Es probable que para proporcionar la información contenida en la Aplicación Veepe Parking hubiere contratado a terceros para realizar los estudios e investigaciones correspondientes, así como los dibujos, diseños, sonidos, videos, textos, o fotografías, que se muestren en la Aplicación. Veepe Parking , advierte que cualquier material o contenido que no sea de su titularidad, ni que haya sido desarrollado por Veepe Parking  , podrían no ser veraces o no estar actualizados, por lo que Veepe Parking no se hace responsable.<br>
		<br>
		Es importante señalar que los Servicios derivan en otros servicios prestados por terceros, es decir, los servicios particulares de estacionamiento, lavado y operación de vehículos es prestado por terceros ajenos a Veepe Parking (“Servicios de Terceros”) sobre los cuales Veepe Parking  no tiene control alguno. Por lo tanto, Veepe Parking no será responsable de forma alguna de los Servicios de Terceros.<br>
		<br>
		2. Derechos.<br>
		<br>
		Sujeto a lo establecido en los presentes Términos y Condiciones, Veepe Parking le otorga a usted el derecho intransferible y no exclusivo para acceder de forma remota y utilizar los Servicios que Veepe Parking ofrece mediante una cuenta de usuario.<br>
		<br>
		<br>
		3. Responsabilidades del usuario. <br>
		<br>
		Mediante los presentes Términos y Condiciones, usted acepta que:<br>
		<br>
		Usted no utilizará el servicio de Veepe Parking para proporcionar información a terceros, sin autorización escrita por parte de Veepe Parking.<br>
		<br>
		Usted deberá acatar y seguir todas las normas de buena conducta socialmente aceptadas, así como todas las disposiciones legales aplicables.<br>
		<br>
		Usted deberá mantener y salvaguardar toda información marcada como confidencial.<br>
		<br>
		Los Servicios se proporcionan “tal cual” por lo cual Veepe Parking no da garantía alguna, ya sea expresa o implícita, que no se encuentre contenida en los presentes Términos y Condiciones, incluidas las garantías implícitas de comerciabilidad, idoneidad para un fin particular y no violación de disposiciones legales.<br>
		<br>
		Veepe Parking no será responsable por cualquier daño material, pérdida o robo del vehículo del usuario, toda vez que Veepe Parking funge como intermediario entre el usuario y los Servicios de Terceros para los Servicios, por lo cual Veepe Parking no puede garantizar que los Servicios de Terceros sean del agrado o satisfacción absoluta del usuario, ya que Veepe Parking no tiene control directo sobre los Servicios de Terceros.<br>
		<br>
		4. Prohibiciones del usuario.<br>
		<br>
		Usted acepta que no publicará, enviará por correo electrónico, pondrá a disposición ningún contenido ni usará la Aplicación:<br>
		<br>
		-Publicar información falsa, difamatoria o ilegal.<br>
		<br>
		-De forma que infrinja, viole o se apropie indebidamente de cualquier derecho de propiedad intelectual u otros derechos contractuales o de propiedad de terceros.<br>
		<br>
		-De forma que sea injurioso o difamatorio o de forma que sea de otro modo amenazante, abusiva, violenta, acosadora, maliciosa o dañina para cualquier persona o entidad o invasiva de la privacidad de otra persona.<br>
		<br>
		-De forma que sea perjudicial para los demás usuarios de cualquier manera.<br>
		<br>
		-Para suplantar a cualquier otra persona, o afirmar falsamente o de otro hacer declaraciones falsas de su afiliación con otra persona o entidad o para obtener acceso a la Aplicación sin autorización.<br>
		<br>
		-Para interferir o intentar interferir con el funcionamiento correcto de la Aplicación o impedir que otros usen la Aplicación o de una manera que interrumpa el flujo normal o el diálogo con un número excesivo de mensajes (ataque de inundación) a la Aplicación o que de otro modo afecte la capacidad de otras personas de usar la Aplicación.<br>
		<br>
		-Para usar cualquier medio manual o automatizado, incluidos agentes, robots, secuencias de comandos o arañas para tener acceso o administrar la cuenta de cualquier usuario o para supervisar o copiar la Aplicación o el contenido que esta incluye; <br>
		<br>
		-Para facilitar la distribución ilegal de contenido de información sensible o confidencial.<br>
		<br>
		-En una manera que incluya información personal o de identificación sobre otra persona sin el consentimiento explícito de esa persona; <br>
		<br>
		Asimismo, usted acepta no:<br>
		<br>
		-Acechar o acosar a persona alguna a través de la Aplicación.<br>
		<br>
		-Usar medios automatizados, incluidos arañas, robots, crawlers, herramientas de minería de datos o similares para descargar datos desde la Aplicación.<br>
		<br>
		-Intentar obtener acceso no autorizado a nuestros sistemas informáticos o participar en cualquier actividad que interrumpa, disminuya la calidad o interfiera con el rendimiento o perjudique la funcionalidad de la Aplicación.<br>
		<br>
		-Desarrollar, invocar o utilizar código para interrumpir, disminuir la calidad, interferir con el rendimiento, o perjudicar la funcionalidad de la Aplicación.<br>
		<br>
		Usted acepta no autorizar ni recomendar a un tercero a usar la Aplicación para facilitar cualquiera de las anteriores conductas prohibidas. Además, usted acepta que estos términos de servicio de la Aplicación entrarán en vigor para el beneficio de los usuarios y de los Servicio y que Veepe Parking tomar las medidas que consideren adecuadas, incluyendo la eliminación de su contenido y la inhabilitación de su cuenta, con el fin de mantener el cumplimiento de estos términos de servicio de la Aplicación.<br>
		<br>
		5. Registro.<br>
		<br>
		Para poder hacer uso de los Servicios, es indispensable que usted se registre en la aplicación lo cual podrá realizar mediante ingreso a la cesión de la red social denominada “Facebook” o a través de un correo electrónico, del cual el usuario es titular, y el llenado de un formulario.<br>
		<br>
		Será obligación de cada usuario registrarse proporcionando a Veepe Parking determinada información personal (nombre, edad, número de teléfono móvil, método de pago, datos del vehículo, entre otros) la cual deberá ser verás y será tratada de conformidad con el Aviso de Privacidad de Veepe Parking, mismo que puede encontrar en [*].<br>
		<br>
		6. Menores de edad. <br>
		<br>
		Los menores de edad (personas de menos de 18 años) pueden usar la Aplicación, siempre que obtengan el consentimiento de sus padres o tutores para el uso del servicio. Los menores que no cuenten con el consentimiento de sus padres o tutores no deben utilizar los Servicios, hasta que alcancen la edad de 18 años.<br>
		<br>
		Al usar la Aplicación, usted declara que es mayor de 18 años y en caso de ser menor de 18 años, usted declara que cuenta con la autorización expresa de sus padres o de quien ejerce la patria potestad sobre usted.<br>
		<br>
		7. Pagos.<br>
		<br>
		Usted realizará el pago por los Servicios mediante la Aplicación, ingresando una tarjeta de crédito o débito. La Aplicación se apoya en el servicio Banwire o cualquier plataforma para el procesamiento de pagos (el “Procesador de Pagos”), por lo cual Veepe Parking no será responsable en caso de que dicha aplicación no se encuentre funcional o presente fallas técnicas.<br>
		<br>
		El Procesador de Pagos es una entidad ajena a Veepe Parking por lo cual Veepe Parking no podrá ser responsable en el caso de que el Procesador de Pagos realice algún cargo indebido a los usuarios.<br>
		<br>
		Los datos de la/s tarjeta/s que ingrese deberán ser verdaderos y el titular deberá ser el usuario, Veepe Parking no será responsable en caso de que los usuarios hagan uso de información de tarjetas que hayan sido robadas u obtenidas por cualquier otro medio considerado ilegal. <br>
		<br>
		Cuando la tarjeta que haya introducido para el pago de los Servicios no cuente con fondos suficientes para cubrir los mismos Veepe Parking suspenderá la cuenta hasta que usted pague los Servicios adeudados.<br>
		<br>
		8. Contenido de Terceros.<br>
		<br>
		El contenido de los Servicios anunciados en la Aplicación son propiedad de terceros que por el simple hecho de anunciarlos en la Aplicación autorizan a Veepe Parking el uso de los mismos, mediante una licencia gratuita, no exclusiva de uso y reproducción del contenido proporcionado.<br>
		<br>
		Asimismo, Veepe Parking podrá incluir anuncios en la Aplicación los cuales podrán dirigir a páginas propiedad de terceros ajenos a Veepe. Parking En ningún momento Veepe Parking será responsable por el contenido de dichas páginas o vínculos.<br>
		<br>
		9. Sanciones.<br>
		<br>
		En el caso de que usted incumpla con los presentes Términos y Condiciones se podrá hacer acreedor a la suspensión temporal o definitiva de su cuenta, dicha sanción será impuesta por Veepe Parking, quien definirá la gravedad de la falta incurrida, así como la sanción correspondiente.<br>
		<br>
		10. Indemnización.<br>
		<br>
		Usted y cualquier usuario de los Servicios, acepta defender, indemnizar y mantener a salvo a 
		Veepe Parking , sus filiales, subsidiarias, funcionarios, directores, empleados, agentes, socios, contratistas y licenciantes, frente a cualquier reclamación o demanda, incluidos los honorarios razonables de abogados, interpuesta por terceros, que se derive o surja de: (a) cualquier contenido de la Aplicación que envíe, publique, transmita o ponga a disposición de algún tercero; (b) el uso que haga de la Aplicación; (c) cualquier infracción por su parte del presente contrato; (d) cualquier medida tomada por Veepe Parking como parte de su investigación de un supuesto incumplimiento o como resultado de su detección o decisión de incumplimiento del presente contrato; o (e) su violación de los derechos de otros. <br>
		<br>
		De igual forma usted no puede interponer una acción legal contra Veepe Parking , sus filiales, subsidiarias, funcionarios, directores, empleados, agentes, socios, contratistas y licenciantes como resultado de una decisión por parte de Veepe Parking   de eliminar o rehusarse a procesar cualquier información o contenido, advertirle, suspender o terminar su acceso al Sitio, o tomar cualquier otra medida durante la investigación de un supuesto incumplimiento o como resultado de la conclusión por Veepe Parking  de la existencia de un incumplimiento del presente Contrato. <br>
		<br>
		La presente cláusula de renuncia e indemnización se aplican a todas las violaciones descritas o contempladas en el presente contrato. Dicha obligación subsistirá la terminación o expiración del presente contrato y/o el uso que haga de la Aplicación. Usted reconoce que es responsable de todo uso que haga de la Aplicación mediante su cuenta, y que el presente contrato aplica a cualquier uso que haga de su cuenta. Usted acuerda cumplir con lo previsto en el presente contrato y defender, indemnizar y mantener a salvo a Veepe Parking frente a cualquier reclamación o demanda que surja como consecuencia del uso de su cuenta, tanto si dicho uso está expresamente autorizado por el usuario como si no lo está.<br>
		<br>
		11. Resolución de Conflictos. <br>
		<br>
		El usuario al hacer uso de los Servicios, acepta de manera expresa, someterse en caso de cualquier controversia, a la jurisdicción de los tribunales federales de la Ciudad de México, así como las leyes federales aplicables para el caso concreto, renunciando expresamente a cualquier otra jurisdicción que por motivo de su nacionalidad o domicilio pudiera corresponder.<br><br>
	</div>
</div>

<div class="container">
	<div class="centerme">

		<!--Fomulario "formaDark" _____________________________________________________________________ -->
		<form id="formLogin" class="formaDark">

			<!--Logo Veepe-->
			<div class="row">
				<div class="logo_veepe" style="margin-bottom: 29px"></div>
			</div><!-- end row-->

			<!--Correo electrónico-->
			<div class="row row_validador">
				<div class="requerido">
					<input id="email" type="email" placeholder="Correo electrónico" style="text-align: center;" data-valida="requerido,email">
				</div>
			</div><!-- end row-->

			<!--Confirmar correo electrónico-->
			<div class="row row_validador">
				<div class="requerido">
					<input id="confirm_email" type="email" placeholder="Confimar correo electrónico" style="text-align: center;" data-valida="requerido,email">
				</div>
			</div><!-- end row-->

			<!--Contraseña-->
			<div class="row row_validador">
				<div class="requerido">
					<input id="password" type="password" placeholder="Contraseña" style="text-align: center;" data-valida="requerido,password">
				</div>
			</div><!-- end row-->

			<!--Confirmar contraseña-->
			<div class="row row_validador">
				<div class="requerido">
					<input id="confirm_password" type="password" placeholder="Confirmar contraseña" style="text-align: center;" data-valida="requerido,password">
				</div>
			</div><!-- end row-->

			<!--Botón Registrar-->
			<div class="row">
				<button id="registrar" class="B_Regular_N" style="width:100%; margin-bottom:16px;" type="button">REGISTRARTE</button>
			</div><!-- end row-->

			<!--Botón Facebook-->
			<div class="row">
				<button class="B_Regular_N" style="width:100%; margin-bottom:16px; background-color:#3A5898; display:none;" type="button" name="facebook">INICIAR CON FACEBOOK</button>
			</div><!-- end row-->

			<!--Términos y condiciones-->
			<div class="row" style="text-align: center; margin-bottom: 8px;">
				<a class="hyperlink" id="lnkTerminos">Términos y condiciones</a>
			</div><!-- end row-->

			<!--Iniciar Sesión-->
			<div class="row" style="text-align: center;">
				<span id="loginTxt">¿Ya tienes cuenta? </span><span><a class="hyperlink" href="ingresar">Iniciar Sesión</a></span>
			</div><!-- end row-->
		</form><!-- end .formDark-->

	</div><!-- end .centerme-->
<div><!-- end row-->
