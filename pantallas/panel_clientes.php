<?php
$clientes = getClientesAVG();
?>

<script src="pantallas/js/panel_clientes.js"></script>

<div style="margin-bottom:32px;">
	<div class="caja_reportes">
		<!--Boton ______________________________________________________________ -->
		<div class="row">
			<div class="col-md-12" style="text-align: center; padding: 16px 0px">
				<a href="alta_cliente">
					<button type="button" class="B_Regular_N">INGRESAR NUEVO CLIENTE</button>
				</a>
			</div>
		</div><!-- end row-->

		<!--Tabla ______________________________________________________________ -->
		<div class="row">

			<div class="col-md-12">
				<table id="tblClientes">
					<thead>
						<tr>
							<th></th>
							<th>Nombre de la empresa</th>
							<th>Contacto</th>
							<th>Puesto</th>
							<th>Registro</th>
							<th>Correo</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach ($clientes as $cliente) {
							echo '<tr>';
							echo '<td><div class="icono empresa"></div></td>';
							echo '<td>' . $cliente->nombre_empresa . '</td>';
							echo '<td>' . $cliente->contacto . '</td>';
							echo '<td>' . $cliente->puesto . '</td>';
							echo '<td>' . $cliente->fechaCreacion . '</td>';
							echo '<td>' . $cliente->emailEmpresa . '</td>';
							echo '<td style="text-align:right">';
							echo '	<div class="boton detalle" onclick="detalleCliente(' . $cliente->uid . ')"></div>
									<div class="boton email" onclick="emailCliente(\''.$cliente->emailEmpresa.'\');"></div>
									<div class="boton editar" onclick="editarCliente(' . $cliente->uid . ')" ></div>
									<div class="boton borrar" onclick="eliminarCliente(' . $cliente->uid . ')"></div>
								</td>';
							echo '</tr>';
						}
						?>
					</tbody>
				</table>
			</div>

		</div><!-- end row -->

	</div><!-- end centerme-->
</div><!-- end #home-->
