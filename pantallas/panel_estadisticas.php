<?php
global $user;
if (esSocio() || esSupervisor()) {
	$id_empresa = db_query("select id_empresa from f_r_usuario_empresa where uid=" . $user->uid)->fetchField();
	$nombreEmpresa = db_query("select nombre_empresa from f_dc_empresas where id_empresa =" . $id_empresa)->fetchField();
	echo '<script>var esSocio=true;</script>';
}
else if (esAgv() || esAva())
{
	$nombreEmpresa = "";
	echo '<script>var esAgv=true;</script>';
}
?>
<script src="pantallas/js/panel_estadisticas.js"></script>

<div style="padding: 15px 24px 15px 72px">

	<?php if (esAgv() || esAva()) { ?>
	<!-- ESTADISTICAS GENERALES PARA VEEPE -->
	<div class="estadisticas" style="background-color: #090A19; color: white; margin: 0px;">
		<div style="text-align: center; font-size: 21px; font-family:'OpenSans'; padding-top: 12px; color:#EFF1F3">Estadísticas Generales</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-3 tarjeta usuarios">
					<div class="encabezado">Usuarios Finales Activos</div>
					<div class="cuerpo">
						<div class="icono persona"></div>
						<div class="cantidad"><?php echo getNumeroUsuariosActivos(); ?></div>
					</div>
				</div>
				<div class="col-md-3 tarjeta empresas">
					<div class="encabezado">Empresas Socias</div>
					<div class="cuerpo">
						<div class="icono empresa"></div>
						<div class="cantidad"><?php echo getNumeroEmpresasSocias(); ?></div>
					</div>
				</div>
				<div class="col-md-3 tarjeta estacionamientos">
					<div class="encabezado">Estacionamientos Totales</div>
					<div class="cuerpo">
						<div class="icono estacionamiento"></div>
						<div class="cantidad"><?php echo getNumeroEstacionamientos(); ?></div>
					</div>
				</div>
				<div class="col-md-3 tarjeta operaciones">
					<div class="encabezado">Operaciones Totales</div>
					<div class="cuerpo">
						<div class="icono operacion"></div>
						<div class="cantidad"><?php echo getNumeroOperacionesTotales(); ?></div>
					</div>
				</div>
			</div>
		</div>
	</div> <!-- fin del panel de estadísticas generales veepe -->
	<?php } ?>


	<?php if (esAgv() || esAva()) { ?>
	<!-- EMPRESAS SOCIAS -->
	<div class="estadisticas" style="margin: 0px 0px 28px 0px;">
		<div id="divGeneral">
			<div style="height: 48px; background-color: #00BFD6; color: white; text-align: center; line-height: 48px; font-size: 18px;">Empresas socias</div>
			<table id="tblEstadisticas" width="100%">
				<thead>
					<tr>
						<td></td>
						<td>Empresa</td>
						<td>Zonas</td>
						<td>Estacionamientos</td>
						<td>No. Operaciones</td>
						<?php if (esAgv()) { ?>
						<td>Ingresos</td>
						<?php } ?>
					</tr>
				</thead>
				<tbody></tbody>
				<tfoot><tr><td colspan="6"></td></tr></tfoot>
			</table>
		</div>
	</div>
	<?php } ?>


	<!--<div class="estadisticas">-->
		<div id="divDetalle" style="margin:0px;">
			<div id="nombreEmpresa" style="height: 48px; background-color: #00BFD6; color: white; text-align: center; line-height: 48px; font-size: 21px;">
				Operaciones <?php echo $nombreEmpresa; ?>
			</div>

			<div class="estadisticas" style="border-radius: 0px; border-color: transparent; border-width: 0px; background-color: #090A19; color: white; margin: 0px;">

				<!-- FILTROS -->
				<div style="padding: 15px">
					<div class="row">
						<div class='col-md-6'>
							<div class='input-group date inputGeneral rectanguloRadios' id='fechaInicio'>
								<div class="obligatorios"></div>
								<input style="width:100%" type='text' class="form-control inputGeneral inputNormal" id="txtFechaInicio" data-format="yyyy-MM-dd" data-valida="requerido" value="" placeholder="Fecha de inicio"/>
								<span class="input-group-addon" >
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>
						<div class='col-md-6'>
							<div class='input-group date inputGeneral rectanguloRadios' id='fechaTermino'>
								<div class="obligatorios"></div>
								<input style="width:100%" type='text' class="form-control inputGeneral inputNormal" id="txtFechaTermino" data-format="yyyy-MM-dd" data-valida="requerido" value="" placeholder="Fecha de término"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>
					</div>
					<div class="row formaDark" style="margin-top: 12px">
						<?php if (esAgv() || esAva()) { ?>
						<div class="col-md-4">
							<select id="selEmpresa" style="width: 100%;">
								<option selected value="-1">Todas las empresas</option>
								<?php
								$empresasSocias = getEmpresasSocias();
								foreach ($empresasSocias as $empresa) {
									echo '<option value="' . $empresa->id_empresa . '">' . $empresa->nombre_empresa . '</option>';
								}
								?>
							</select>
						</div>
						<?php } ?>
						<div class="col-md-<?php echo (esSocio()||esSupervisor())?"6":"4"; ?>">
							<select id="selZona" style="width: 100%;">
								<option selected value="-1">Todas las zonas</option>
								<?php
								$zonasEstadistica = getZonas();
								foreach ($zonasEstadistica as $zona) {
									echo '<option value="' . $zona->zona . '">' . $zona->zona . '</option>';
								}
								?>
							</select>
						</div>
						<div class="col-md-<?php echo (esSocio()||esSupervisor())?"6":"4"; ?>">
							<select id="selEstacionamiento" style="width: 100%;">
								<option selected value="-1">Todos los estacionamientos</option>
								<?php
								//$estacionamientosEstadistica = getEstacionamientos();
								$estacionamientosEstadistica = getEstacionamientoXZona($id_empresa, null);
								foreach ($estacionamientosEstadistica as $est) {
									echo '<option value="' . $est->id_estacionamiento . '">' . $est->nombre . '</option>';
								}
								?>
							</select>
						</div>
					</div>
				</div> <!-- fin de los filtros -->

				<!-- RESUMEN OPERACIONES -->
				<div id="datosEmpresa" style="padding: 15px">
					<?php $anchoCol = (esAgv() || esSocio()) ? 3 : 4; ?>
					<div class="row">
						<div class="col-md-<?php echo $anchoCol; ?> tarjeta operaciones">
							<div class="encabezado">No. de Operaciones</div>
							<div class="cuerpo">
								<div class="icono operacion"></div>
								<div id="numeroOperacionesEmpresa" class="cantidad"></div>
							</div>
						</div>
						<div class="col-md-<?php echo $anchoCol; ?> tarjeta zonas">
							<div class="encabezado">Zonas</div>
							<div class="cuerpo">
								<div class="icono zona"></div>
								<div id="numeroZonas" class="cantidad"></div>
							</div>
						</div>
						<div class="col-md-<?php echo $anchoCol; ?> tarjeta estacionamientos">
							<div class="encabezado">Estacionamientos</div>
							<div class="cuerpo">
								<div class="icono estacionamiento"></div>
								<div id="numeroEstacionamientos" class="cantidad"></div>
							</div>
						</div>
						<?php if (esAgv() || esSocio()) { ?>
						<div class="col-md-3 tarjeta ingresos">
							<div class="encabezado">Ingresos</div>
							<div class="cuerpo">
								<div class="icono ingresos"></div>
								<div id="ingresos" class="cantidad"></div>
							</div>
						</div>
						<?php } ?>
					</div>
				</div>


				<table id="tblOperaciones" width="100%">
					<thead>
						<tr>
							<td></td>
							<td>Estacionamiento</td>
							<td>Zona</td>
							<td>No. Operaciones</td>
							<?php if (esAgv() || esSocio()) { ?>
								<td>Ingresos</td>
							<?php } ?>
						</tr>
					</thead>
					<tbody></tbody>
					<tfoot>
						<tr>
							<td colspan="6">
								<?php if (esAgv() || esSocio()) { ?>
								<div id="divIngreso" class="pull-right" style="margin:8px 16px; color: white; font-size: 18px">
									Ingresos totales:&nbsp;&nbsp;&nbsp;&nbsp;<span id="ingresoTotal"></span>
								</div>
								<?php } ?>
							</td>
						</tr>
					</tfoot>
				</table>
				
			</div>
		</div>
	<!--</div>-->
	
</div> <!-- fin de la pantalla -->



