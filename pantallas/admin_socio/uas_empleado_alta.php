<?php
?>

<style>
	body #navbar,
	section > h1,
	section .region .region-content ,
	body footer,
	body header {
	  display: none;
	}
</style>

<!--<script src="pantallas/admin_socio/js/uas_empleado_editar.js"></script>-->

<div class="row">
  <div class="caja_registro centerme">

		<div class="row" style="margin-bottom: 20px;">
			<center>
				<!--Foto-->
				<div class="row">
					<div class="img_usuario_n"></div>
				</div><!-- end row-->

				<!--Nombre del usuario-->
				<div class="row">
					<h1 class="white">Nombre del Empleado</h1>
				</div><!-- end row-->

			</center>
		</div><!-- end row-->

    <!--Fomulario "formaDark" _____________________________________________________________________ -->
    <form class="formaDark" action="">

			<!--Registro ___________________________________________________________________________________ -->
			<div class="row">

	        <!--Lugar ........................................................ -->
          <div class="row">
            <div class="row_validador">
							<div class="requerido">
                <select class="input_width100" data-valida="requerido">
                  <option value="" disabled selected style="display:none">Lugar</option>
                  <option value="Lugar_01">Lugar_01</option>
                  <option value="Lugar_02">Lugar_02</option>
                  <option value="Lugar_03">Lugar_03</option>
                </select>
								<div class="marca"></div>
			    		</div>
	          </div>
          </div><!-- end row-->

          <!--Nombre y apellido paterno .................................... -->
	        <div class="row">
	          <div class="col-md-6 row_validador">
							<div class="requerido">
			    			<input id="txtNombre" type="text" placeholder="Nombre" data-valida="requerido"/>
								<div class="marca"></div>
			    		</div>
	          </div>
	          <div class="col-md-6 row_validador">
							<div class="requerido">
			    			<input id="txtApellidoPaterno" type="text" placeholder="Apellido paterno" data-valida="requerido"/>
								<div class="marca"></div>
			    		</div>
	          </div>
	        </div><!-- end row-->

	        <!--Apellido materno y puesto -->
					<div class="row">
	          <div class="col-md-6 row_validador">
							<div class="requerido">
			    			<input id="txtApellidoMaterno" type="text" placeholder="Apellido materno" data-valida="requerido"/>
								<div class="marca"></div>
			    		</div>
	          </div>
	          <div class="col-md-6 row_validador">
							<div class="requerido">
                <select id="puesto" data-valida="requerido">
                  <option value="" disabled selected style="display:none">Puesto</option>
                  <option value="Puesto_01">Lugar_01</option>
                  <option value="Puesto_02">Lugar_02</option>
                  <option value="Puesto_03">Lugar_03</option>
                </select>
								<div class="marca"></div>
			    		</div>
	          </div>
	        </div><!-- end row-->

          <!--Celular y telefono ........................................... -->
					<div class="row">
	          <div class="col-md-6 row_validador">
							<div class="requerido">
                <input id="celular" type="text"  maxlength="10" placeholder="Celular" data-valida="requerido,numero"/>
								<div class="marca"></div>
			    		</div>
	          </div>
	          <div class="col-md-6 row_validador">
							<div class="requerido">
                <input id="telefono" type="text"  maxlength="10" placeholder="Teléfono" data-valida="requerido,numero"/>
								<div class="marca"></div>
			    		</div>
	          </div>
	        </div><!-- end row-->

					<!--Dirección .................................................... -->
          <div class="row" style="height: 116px;">
            <div class="row_validador">
      				<div class="requerido">
      					<textarea id="txtdireccion" class="input_width100" rows="6" cols="40" placeholder="Dirección" data-valida="requerido"></textarea>
      					<div class="marca"></div>
      				</div>
	          </div>
          </div><!-- end row-->

          <!--Correo electrónico ........................................... -->
          <div class="row">
	          <div class="col-md-6 row_validador">
							<div class="requerido">
                <input id="email" type="email" placeholder="Correo electrónico" data-valida="requerido,email">
								<div class="marca"></div>
			    		</div>
	          </div>
	        </div><!-- end row-->

	    </div><!-- end row-->

      <!--Botón Registrar .................................................. -->
      <div class="row" style="margin: 8px 0px;">
				<div class="col-md-6">
					<div class="pull-right cancelar">
							<a class="BB_Fantasma_N" href="uas_inicio">CANCELAR</a>
					</div>
				</div>
				<div class="col-md-6">
					<div class="pull-left guardar">
							<button id="editar" class="B_Regular_N" type="button">GUARDAR</button>
					</div>
				</div>
      </div><!-- end row-->

			<div class="row body3" style="text-align: center; margin: 24px 0px;">
				Notificación de envió de información por correo. (se le envía VEEPE ID Y CONTRASEÑA)
			</div>

    </form><!-- end .formDark-->
  </div><!-- end .centerme-->
</div><!-- end row-->
