"use strict";

// _____________________________________________________________________________________________________________ VARIABLES GLOBALES

// _____________________________________________________________________________________________________________ FUNCIONES GLOBALES

// _____________________________________________________________________________________________________________ JQUERY
jQuery(document).ready(function ()
{
	// _____________________________________________________________________ configuraciones
	initvalida();
	jQuery('button#editar').click(function(){

		// validamos el formulario
		var cadsql = 0;
		var esValidoNip = false;
		var esValidoPass = false;
		var v1 = validarCampo(jQuery('input#txtNombre'));
		var v2 = validarCampo(jQuery('input#txtApellidoPaterno'));
		var v3 = validarCampo(jQuery('input#txtApellidoMaterno'));
		var v4 = validarCampo(jQuery('input#celular'));
		var v5 = validarCampo(jQuery('input#old_password'));


		// checamos si se ingresó un nuevo password
		if(jQuery('input#password').val() != ''){
			// validamos que las dos nuevas contraseñas sean iguales
			if ( jQuery('input#password').val() != jQuery('input#confirm_password').val() )
			{
				jQuery('#alerta_password').fadeIn(100);
				limpiar_input('input#password');
				limpiar_input('input#confirm_password');
				jQuery('input#password').focus();
			}
			else{
				esValidoPass = true;
			}
		}

		if(v1 && v2 && v3 && v4 && v5 ){ cadsql = 1; }
		if(v1 && v2 && v3 && v4 && v5 && esValidoPass){ cadsql = 2;	}

		switch (cadsql) {
			case 1:
							//Actualizar el nombre y/o celular
							console.log('Caso 1');
							CLA.encolar("ajax.php",
								{
									m:		'webEditarPerfilNombre',
									nom:	jQuery('input#txtNombre').val()==null?'':jQuery('input#txtNombre').val(),
									app:	jQuery('input#txtApellidoPaterno').val()==null?'':jQuery('input#txtApellidoPaterno').val(),
									apm:	jQuery('input#txtApellidoMaterno').val()==null?'':jQuery('input#txtApellidoMaterno').val(),
									cel:	jQuery('input#celular').val()==null?'':jQuery('input#celular').val(),
									pass:	jQuery('input#old_password').val()==null?'':jQuery('input#old_password').val(),
									nip:	jQuery('input#old_nip').val()==null?'':jQuery('input#old_nip').val()
								},

								function(response){
									jQuery('input, button, select').removeAttr('disabled');
									console.debug( response );
									response = JSON.parse(response);

									if ( response == true )
									{
										window.location = './';
									}
									else
									{
										jQuery('#alerta_alta_error').fadeIn(500);
									}
								},

								function(){
									// prefunción
									jQuery('input, button, select').attr('disabled', 'disabled');
								},
								true,
								0.0);
				break;
			case 2:
							console.log('Caso 2');
							//Actualizar contraseña
							CLA.encolar("ajax.php",
								{
									m:		'webEditarSocioPass',
									nom:	jQuery('input#txtNombre').val()==null?'':jQuery('input#txtNombre').val(),
									app:	jQuery('input#txtApellidoPaterno').val()==null?'':jQuery('input#txtApellidoPaterno').val(),
									apm:	jQuery('input#txtApellidoMaterno').val()==null?'':jQuery('input#txtApellidoMaterno').val(),
									cel:	jQuery('input#celular').val()==null?'':jQuery('input#celular').val(),
									pass:	jQuery('input#old_password').val()==null?'':jQuery('input#old_password').val(),
									newpass:	jQuery('input#password').val()==null?'':jQuery('input#password').val(),
									nip:	jQuery('input#old_nip').val()==null?'':jQuery('input#old_nip').val()
								},

								function(response){
									jQuery('input, button, select').removeAttr('disabled');
									console.debug( response );
									console.log( response );
									response = JSON.parse(response);

									if ( response == true )
									{
										window.location = './';
									}
									else
									{
										jQuery('#alerta_alta_error').fadeIn(500);
									}
								},

								function(){
									// prefunción
									jQuery('input, button, select').attr('disabled', 'disabled');
								},
								true,
								0.0);
				break;
			default:
		}


	});

	// _____________________________________________________________________ entrypt

});// fin del document.ready
