"use strict";

// _____________________________________________________________________________________________________________ VARIABLES GLOBALES

// _____________________________________________________________________________________________________________ FUNCIONES GLOBALES
function limpiar_password(){
	limpiar_input('input#old_password');
	limpiar_input('input#password');
	limpiar_input('input#confirm_password');
	jQuery('input#password').focus();
}

// _____________________________________________________________________________________________________________ JQUERY
jQuery(document).ready(function ()
{
	// _____________________________________________________________________ configuraciones
	initvalida();
	jQuery('button#editar').click(function(){
		var php_id_user = id_user;

		// validamos el formulario
		var cadsql = -1;
		var v1 = validarCampo(jQuery('input#txtNombre'));
		var v2 = validarCampo(jQuery('input#txtApellidoPaterno'));
		var v3 = validarCampo(jQuery('input#txtApellidoMaterno'));
		var v4 = validarCampo(jQuery('input#celular'));

		//Si los campos de datos son válidos
		if(v1 && v2 && v3 && v4){
			cadsql = 2;	//Cambiar todos los datos

			//Si el campo de old_password esta lleno
			if(jQuery('input#old_password').val() != ''){

				//Los campos de nuevo password estan vacíos
				if ( jQuery('input#password').val() == '' && jQuery('input#confirm_password').val() == '' ){
					limpiar_password();
				}
				//Los campos de nuevo password al menos uno esta lleno
				else{
					if ( jQuery('input#password').val() == jQuery('input#confirm_password').val() )
					{
						cadsql = 1; //Cambiar contraseña
					}
					else{
						cadsql = 0; //las contraseñas nuevas no coinciden
					}
				}
			}
			//Si el campo de old_password esta vacío
			else{
				limpiar_password();
			}


			switch (cadsql) {
				case 0:
						console.log('Contraseñas no coinciden');
					break;
				case 1:
								//Actualizar password
								console.log('Caso 1');
								CLA.encolar("ajax.php",
									{
										m:		'webEditarPerfilPass',
										uid:  php_id_user,
										pass:	jQuery('input#old_password').val()==null?'':jQuery('input#old_password').val(),
										newpass:	jQuery('input#password').val()==null?'':jQuery('input#password').val(),
									},

									function(response){
										jQuery('input, button, select').removeAttr('disabled');
										response = JSON.parse(response);

										if ( response == true )
										{
											//window.location = './';3
											console.log("EXITO 1");
										}
										else
										{
											console.log("Verificar la contraseña actual. ");
										}
									},

									function(){
										// prefunción
										jQuery('input, button, select').attr('disabled', 'disabled');
									},
									true,
									0.0);
				case 2:
								//Actualizar datos
								console.log('Caso 2');
								CLA.encolar("ajax.php",
									{
										m:		'webEditarPerfil',
										uid:	php_id_user,
										nom:	jQuery('input#txtNombre').val()==null?'':jQuery('input#txtNombre').val(),
										app:	jQuery('input#txtApellidoPaterno').val()==null?'':jQuery('input#txtApellidoPaterno').val(),
										apm:	jQuery('input#txtApellidoMaterno').val()==null?'':jQuery('input#txtApellidoMaterno').val(),
										cel:	jQuery('input#celular').val()==null?'':jQuery('input#celular').val(),
									},

									function(response){
										jQuery('input, button, select').removeAttr('disabled');
										response = JSON.parse(response);

										if ( response == true )
										{
											//window.location = './';
											console.log("EXITO 2");
										}
										else
										{
											console.log("Error al modificar los datos.");
										}
									},

									function(){
										// prefunción
										jQuery('input, button, select').attr('disabled', 'disabled');
									},
									true,
									0.0);
					break;
				default:

			} //end switch

		} //end if(v1 && v2 && v3 && v4)


	});

	// _____________________________________________________________________ entrypt

});// fin del document.ready
