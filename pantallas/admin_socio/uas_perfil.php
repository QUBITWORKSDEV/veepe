<?php
global $user;
$uid = $user -> uid;
$array = getUsuario(getVeepeidById($uid));
?>

<style>
  body footer {
    display: none;
  }
</style>

<div class="home">

  <div class="centerme">

    <!--Perfil _____________________________________________________________ -->
    <div class="row" style="text-align: center; padding: 8px 0px;">

      <!--Foto-->
      <div class="row">
        <div class="img_usuario_i"></div>
      </div><!-- end row-->

      <!--Nombre del usuario-->
      <div class="row">
        <h1 class="white"><?php echo $array["nombre"] . " " . $array["apellidoPaterno"] . " " . $array["apellidoMaterno"]; ?></h1>
        <h2 class="white">Socio VEEPE</h2>
      </div><!-- end row-->

      <!--VeepeID-->
      <div class="row">
        <span><h4 class="white">VEEPE ID: </h4></span>
        <span><h4 class="blue">
          <?php  $str = $array["VeepeId"]; $cad = strtoupper($str); echo $cad; ?>
        </h4></span>
      </div><!-- end row-->

      <!--Correo-->
      <div class="row">
          <span class="body3_A"><?php echo $array["Correo"]; ?></span>
      </div><!-- end row-->

    </div><!-- end row-->

    <!--Boton ______________________________________________________________ -->
    <div class="row">
      <div class="col-md-12" style="text-align: center; padding: 12px 0px 24px 0px;">
        <a href="" class="B_Regular_N">EDITAR</a>
      </div>
    </div><!-- end row-->

  </div><!-- end centerme-->
</div><!-- end #home-->
