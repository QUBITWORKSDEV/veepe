<?php
?>

<style>
	body footer {
		display: none;
	}
</style>

<div class="home">
	<div class="centerme caja_reportes">
		<!--Boton ______________________________________________________________ -->
		<div class="row">
			<div class="col-md-12" style="text-align: center; padding: 16px 0px">
				<button type="button" class="B_Regular_N">INGRESAR NUEVO ESTACIONAMIENTO</button>
			</div>
		</div><!-- end row-->

		<!--Tabla ______________________________________________________________ -->
		<div class="row tabla_estacionamientos">

			<!-- Búsqueda ________________________________________________________ -->
			<div class="row">
				<div class="row_buscar">
					<h3 class="white">Estacionamientos</h3>
					<div class="pull-right">
						<input class="input_buscar" type="text" placeholder="Buscar...">
						<div class="lupa"></div>
					</div>
				</div>
			</div>

			<!-- Tabla Header ____________________________________________________ -->
			<div class="row">
				<div class="p_header_col1"> <span>Imagen</span> </div>
				<div class="p_header_col2">Nombre del Estacionamiento</div>
				<div class="p_header_col3">VEEPE ID</div>
				<div class="p_header_col4">Correo</div>
				<div class="p_header_col5">Servicios</div>
				<div class="p_header_col6"> <span>Iconos</span> </div>
			</div>

			<!-- Tabla Contenido _________________________________________________ -->
			<div class="row">
				<div class="estacionamientos">
					<table>
						<tr>
							<td class="p_col1"><div class="p_img"></div></td>
							<td class="p_col2">Milton Flower Thor</td>
							<td class="p_col3">ID0001</td>
							<td class="p_col4">Estacionamiento Funk</td>
							<td class="p_col5">
								<div class="s_estacionamiento" href="#"></div>
								<div class="s_valet" href="#"></div>
								<div class="s_reserva" href="#"></div>
								<div class="s_lavado" href="#"></div>
								<div class="s_pension" href="#"></div>
								<div class="s_pension_compartida" href="#"></div>
							</td>
							<td class="p_col6">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="p_col1"><div class="p_img"></div></td>
							<td class="p_col2">Milton Flower Thor</td>
							<td class="p_col3">ID0001</td>
							<td class="p_col4">Estacionamiento Funk</td>
							<td class="p_col5">
								<div class="s_estacionamiento" href="#"></div>
								<div class="s_valet" href="#"></div>
								<div class="s_reserva" href="#"></div>
								<div class="s_lavado" href="#"></div>
								<div class="s_pension" href="#"></div>
								<div class="s_pension_compartida" href="#"></div>
							</td>
							<td class="p_col6">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="p_col1"><div class="p_img"></div></td>
							<td class="p_col2">Milton Flower Thor</td>
							<td class="p_col3">ID0001</td>
							<td class="p_col4">Estacionamiento Funk</td>
							<td class="p_col5">
								<div class="s_estacionamiento" href="#"></div>
								<div class="s_valet" href="#"></div>
								<div class="s_reserva" href="#"></div>
								<div class="s_lavado" href="#"></div>
								<div class="s_pension" href="#"></div>
								<div class="s_pension_compartida" href="#"></div>
							</td>
							<td class="p_col6">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="p_col1"><div class="p_img"></div></td>
							<td class="p_col2">Milton Flower Thor</td>
							<td class="p_col3">ID0001</td>
							<td class="p_col4">Estacionamiento Funk</td>
							<td class="p_col5">
								<div class="s_estacionamiento" href="#"></div>
								<div class="s_valet" href="#"></div>
								<div class="s_reserva" href="#"></div>
								<div class="s_lavado" href="#"></div>
								<div class="s_pension" href="#"></div>
								<div class="s_pension_compartida" href="#"></div>
							</td>
							<td class="p_col6">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="p_col1"><div class="p_img"></div></td>
							<td class="p_col2">Milton Flower Thor</td>
							<td class="p_col3">ID0001</td>
							<td class="p_col4">Estacionamiento Funk</td>
							<td class="p_col5">
								<div class="s_estacionamiento" href="#"></div>
								<div class="s_valet" href="#"></div>
								<div class="s_reserva" href="#"></div>
								<div class="s_lavado" href="#"></div>
								<div class="s_pension" href="#"></div>
								<div class="s_pension_compartida" href="#"></div>
							</td>
							<td class="p_col6">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="p_col1"><div class="p_img"></div></td>
							<td class="p_col2">Milton Flower Thor</td>
							<td class="p_col3">ID0001</td>
							<td class="p_col4">Estacionamiento Funk</td>
							<td class="p_col5">
								<div class="s_estacionamiento" href="#"></div>
								<div class="s_valet" href="#"></div>
								<div class="s_reserva" href="#"></div>
								<div class="s_lavado" href="#"></div>
								<div class="s_pension" href="#"></div>
								<div class="s_pension_compartida" href="#"></div>
							</td>
							<td class="p_col6">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="p_col1"><div class="p_img"></div></td>
							<td class="p_col2">Milton Flower Thor</td>
							<td class="p_col3">ID0001</td>
							<td class="p_col4">Estacionamiento Funk</td>
							<td class="p_col5">
								<div class="s_estacionamiento" href="#"></div>
								<div class="s_valet" href="#"></div>
								<div class="s_reserva" href="#"></div>
								<div class="s_lavado" href="#"></div>
								<div class="s_pension" href="#"></div>
								<div class="s_pension_compartida" href="#"></div>
							</td>
							<td class="p_col6">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="p_col1"><div class="p_img"></div></td>
							<td class="p_col2">Milton Flower Thor</td>
							<td class="p_col3">ID0001</td>
							<td class="p_col4">Estacionamiento Funk</td>
							<td class="p_col5">
								<div class="s_estacionamiento" href="#"></div>
								<div class="s_valet" href="#"></div>
								<div class="s_reserva" href="#"></div>
								<div class="s_lavado" href="#"></div>
								<div class="s_pension" href="#"></div>
								<div class="s_pension_compartida" href="#"></div>
							</td>
							<td class="p_col6">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="p_col1"><div class="p_img"></div></td>
							<td class="p_col2">Milton Flower Thor</td>
							<td class="p_col3">ID0001</td>
							<td class="p_col4">Estacionamiento Funk</td>
							<td class="p_col5">
								<div class="s_estacionamiento" href="#"></div>
								<div class="s_valet" href="#"></div>
								<div class="s_reserva" href="#"></div>
								<div class="s_lavado" href="#"></div>
								<div class="s_pension" href="#"></div>
								<div class="s_pension_compartida" href="#"></div>
							</td>
							<td class="p_col6">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="p_col1"><div class="p_img"></div></td>
							<td class="p_col2">Milton Flower Thor</td>
							<td class="p_col3">ID0001</td>
							<td class="p_col4">Estacionamiento Funk</td>
							<td class="p_col5">
								<div class="s_estacionamiento" href="#"></div>
								<div class="s_valet" href="#"></div>
								<div class="s_reserva" href="#"></div>
								<div class="s_lavado" href="#"></div>
								<div class="s_pension" href="#"></div>
								<div class="s_pension_compartida" href="#"></div>
							</td>
							<td class="p_col6">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="p_col1"><div class="p_img"></div></td>
							<td class="p_col2">Milton Flower Thor</td>
							<td class="p_col3">ID0001</td>
							<td class="p_col4">Estacionamiento Funk</td>
							<td class="p_col5">
								<div class="s_estacionamiento" href="#"></div>
								<div class="s_valet" href="#"></div>
								<div class="s_reserva" href="#"></div>
								<div class="s_lavado" href="#"></div>
								<div class="s_pension" href="#"></div>
								<div class="s_pension_compartida" href="#"></div>
							</td>
							<td class="p_col6">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="p_col1"><div class="p_img"></div></td>
							<td class="p_col2">Milton Flower Thor</td>
							<td class="p_col3">ID0001</td>
							<td class="p_col4">Estacionamiento Funk</td>
							<td class="p_col5">
								<div class="s_estacionamiento" href="#"></div>
								<div class="s_valet" href="#"></div>
								<div class="s_reserva" href="#"></div>
								<div class="s_lavado" href="#"></div>
								<div class="s_pension" href="#"></div>
								<div class="s_pension_compartida" href="#"></div>
							</td>
							<td class="p_col6">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="p_col1"><div class="p_img"></div></td>
							<td class="p_col2">Milton Flower Thor</td>
							<td class="p_col3">ID0001</td>
							<td class="p_col4">Estacionamiento Funk</td>
							<td class="p_col5">
								<div class="s_estacionamiento" href="#"></div>
								<div class="s_valet" href="#"></div>
								<div class="s_reserva" href="#"></div>
								<div class="s_lavado" href="#"></div>
								<div class="s_pension" href="#"></div>
								<div class="s_pension_compartida" href="#"></div>
							</td>
							<td class="p_col6">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="p_col1"><div class="p_img"></div></td>
							<td class="p_col2">Milton Flower Thor</td>
							<td class="p_col3">ID0001</td>
							<td class="p_col4">Estacionamiento Funk</td>
							<td class="p_col5">
								<div class="s_estacionamiento" href="#"></div>
								<div class="s_valet" href="#"></div>
								<div class="s_reserva" href="#"></div>
								<div class="s_lavado" href="#"></div>
								<div class="s_pension" href="#"></div>
								<div class="s_pension_compartida" href="#"></div>
							</td>
							<td class="p_col6">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="p_col1"><div class="p_img"></div></td>
							<td class="p_col2">Milton Flower Thor</td>
							<td class="p_col3">ID0001</td>
							<td class="p_col4">Estacionamiento Funk</td>
							<td class="p_col5">
								<div class="s_estacionamiento" href="#"></div>
								<div class="s_valet" href="#"></div>
								<div class="s_reserva" href="#"></div>
								<div class="s_lavado" href="#"></div>
								<div class="s_pension" href="#"></div>
								<div class="s_pension_compartida" href="#"></div>
							</td>
							<td class="p_col6">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="p_col1"><div class="p_img"></div></td>
							<td class="p_col2">Milton Flower Thor</td>
							<td class="p_col3">ID0001</td>
							<td class="p_col4">Estacionamiento Funk</td>
							<td class="p_col5">
								<div class="s_estacionamiento" href="#"></div>
								<div class="s_valet" href="#"></div>
								<div class="s_reserva" href="#"></div>
								<div class="s_lavado" href="#"></div>
								<div class="s_pension" href="#"></div>
								<div class="s_pension_compartida" href="#"></div>
							</td>
							<td class="p_col6">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>

					</table>
				</div><!-- end empleados-->
			</div><!-- end row-->

		</div><!-- end Tabla-->

	</div><!-- end centerme-->
</div><!-- end #home-->
