<?php
?>

<style>
	body #navbar,
	section > h1,
	section .region .region-content ,
	body footer,
	body header {
		display: none;
	}
</style>

<!--<script src="pantallas/admin_socio/js/uas_empleado_editar.js"></script>-->

<div class="row">
	<div class="caja_registro centerme_estacionamiento">

		<div class="row" style="margin-bottom: 20px;">
			<!--Foto-->
			<div class="row">
				<div class="img_estacionamiento_h"></div>
			</div><!-- end row-->

			<!--Nombre del usuario-->
			<div class="row" style="text-align: center;">
				<h1 class="white">Alta Estacionamiento</h1>
			</div><!-- end row-->
		</div><!-- end row-->

		<!--Fomulario "formaDark" ______________________________________________ -->
		<form class="formaDark" action="">
	
			<!--Registro _________________________________________________________ -->
			<div class="row">
	
				<!--Nombre del estacionamieto .................................... -->
				<div class="row">
					<div class="row_validador">
						<div class="requerido">
							<input id="txtnombre" class="input_width100" type="text" placeholder="Nombre del Estacionamiento" data-valida="requerido"/>
							<div class="marca"></div>
						</div>
					</div>
				</div><!-- end row-->
	
				<!--Dirección .................................................... -->
				<div class="row" style="height:116px;">
					<div class="row_validador">
						<div class="requerido">
							<textarea id="txtdireccion" class="input_width100" rows="6" cols="40" placeholder="Dirección" data-valida="requerido"></textarea>
							<div class="marca"></div>
						</div>
					</div>
				</div><!-- end row-->
	
				<!--Latitud y longitud ........................................... -->
				<div class="row">
					<div class="col-md-6 row_validador">
						<div class="requerido">
							<input id="txtxlatitud" type="text" placeholder="Longitud" data-valida="requerido"/>
							<div class="marca"></div>
						</div>
					</div>
					<div class="col-md-6 row_validador">
						<div class="requerido">
							<input id="txtlongitud" type="text" placeholder="Latitud" data-valida="requerido"/>
							<div class="marca"></div>
						</div>
					</div>
				</div><!-- end row-->
	
				<!--Capacidad y pensiones contratadas ........................... -->
				<div class="row">
					<div class="col-md-6 row_validador">
						<div class="requerido">
							<input id="txtcapacidad" type="text" placeholder="Capacidad" data-valida="requerido"/>
							<div class="marca"></div>
						</div>
					</div>
					<div class="col-md-6 row_validador">
						<div class="requerido">
							<input id="txtpensiones" type="text" placeholder="Lugares para pensión" data-valida="requerido"/>
							<div class="marca"></div>
						</div>
					</div>
				</div><!-- end row-->
	
				<!--Horario de apertura y cierre ................................. -->
				<div class="row">
					<div class="col-md-6 row_validador">
						<div class="requerido">
							<select id="horario_apertura" data-valida="requerido">
								<option value="" disabled selected style="display:none">Horario apertura</option>
								<option value="Lugar_01">horario_apertura_01</option>
								<option value="Lugar_02">horario_apertura_02</option>
								<option value="Lugar_03">horario_apertura_03</option>
							</select>
							<div class="marca"></div>
						</div>
					</div>
					<div class="col-md-6 row_validador">
						<div class="requerido">
							<select id="horario_ciere" data-valida="requerido">
								<option value="" disabled selected style="display:none">Horario cierre</option>
								<option value="Lugar_01">horario_ciere_01</option>
								<option value="Lugar_02">horario_ciere_02</option>
								<option value="Lugar_03">horario_ciere_03</option>
							</select>
							<div class="marca"></div>
						</div>
					</div>
				</div><!-- end row-->
	
				<!--Correo electronico y veepeid ................................. -->
				<div class="row">
					<div class="col-md-6 row_validador">
						<div class="requerido">
							<input id="email" type="email" placeholder="Correo electrónico" data-valida="requerido,email"/>
							<div class="marca"></div>
						</div>
					</div>
					<div class="col-md-6 row_validador">
						<div class="requerido">
							<input id="veepeid" type="email" placeholder="VEEPE ID" data-valida="requerido"/>
							<div class="marca"></div>
						</div>
					</div>
				</div><!-- end row-->
	
				<!--Titulo alta de servicios ..................................... -->
				<div class="row" style="text-align:center">
					<h3 class="white">Alta Servicios</h3>
				</div>
	
				<!--Pension y pension compartida ................................. -->
				<div class="row ">
					<div class="col-md-6 row_validador ">
						<div class="requerido row_servicio" >
							<input id="input_checkbox1" type="checkbox"/><label><span class="checkbox" id="span_checkbox1"></span>Pensión</label>
							<input id="costo_pension" class="row_costos pull-right" type="text" placeholder="Costo del servicio" maxlength="4" data-valida="numero"/>
						</div>
					</div>
					<div class="col-md-6 row_validador">
						<div class="requerido row_servicio" >
							<input id="input_checkbox2" type="checkbox"/><label><span class="checkbox" id="span_checkbox2"></span>Pensión compartida</label>
							<input id="costo_pension_compartida" class="row_costos pull-right" type="text" placeholder="Costo del servicio" maxlength="4" data-valida="numero"/>
						</div>
					</div>
				</div><!-- end row-->
	
				<!--Apartado y Valet parking ..................................... -->
				<div class="row ">
					<div class="col-md-6 row_validador ">
						<div class="requerido row_servicio" >
							<input id="input_checkbox3" type="checkbox"/><label><span class="checkbox" id="span_checkbox3"></span>Apartado</label>
							<input id="costo_apartado" class="row_costos pull-right" type="text" placeholder="Costo del servicio" maxlength="4" data-valida="numero"/>
						</div>
					</div>
					<div class="col-md-6 row_validador">
						<div class="requerido row_servicio" >
							<input id="input_checkbox4" type="checkbox"/><label><span class="checkbox" id="span_checkbox4"></span>Valet Parking</label>
							<input id="costo_valet_parking" class="row_costos pull-right" type="text" placeholder="Costo del servicio" maxlength="4" data-valida="numero"/>
						</div>
					</div>
				</div><!-- end row-->
	
				<!--Promociones activas .......................................... -->
				<div class="row">
					<div class="col-md-6 row_validador ">
						<div class="requerido row_servicio" >
							<input id="input_checkbox5" type="checkbox"/><label><span class="checkbox" id="span_checkbox5"></span>Promociones activas</label>
							<input id="promociones" class="row_costos pull-right" type="text" placeholder="Costo del servicio" maxlength="4" data-valida="numero"/>
						</div>
					</div>
				</div><!-- end row-->
	
				<!--Terminos y condiciones ....................................... -->
				<div class="row" style="height:256px;">
					<div class="requerido">
						<textarea id="txtcondiciones" class="input_width100" rows="17" cols="40" placeholder="Términos y condiciones"></textarea>
					</div>
				</div><!-- end row-->
	
				<!--Titulo alta de servicios ..................................... -->
				<div class="row" style="text-align:center">
					<h3 class="white">Servicios Adicionales</h3>
				</div>
	
				<!--Lavado y lavado encerado ................................. -->
				<div class="row ">
					<div class="col-md-6 row_validador ">
						<div class="requerido row_servicio" >
							<input id="input_checkbox6" type="checkbox"/><label><span class="checkbox" id="span_checkbox6"></span>Lavado</label>
							<input id="costo_lavado" class="row_costos pull-right" type="text" placeholder="Costo del servicio" maxlength="4" data-valida="numero"/>
						</div>
					</div>
					<div class="col-md-6 row_validador">
						<div class="requerido row_servicio" >
							<input id="input_checkbox7" type="checkbox"/><label><span class="checkbox" id="span_checkbox7"></span>Lavado encerado</label>
							<input id="costo_encerado" class="row_costos pull-right" type="text" placeholder="Costo del servicio" maxlength="4" data-valida="numero"/>
						</div>
					</div>
				</div><!-- end row-->
	
				<!--Nombre de la empresa ......................................... -->
				<div class="row">
					<div class="row_validador">
						<div class="requerido">
							<input id="txtempresa" class="input_width100" type="text" placeholder="Nombre de la Empresa" data-valida="requerido"/>
							<div class="marca"></div>
						</div>
					</div>
				</div><!-- end row-->
	
				<!--RFC .................................-........................ -->
				<div class="row">
					<div class="row_validador">
						<div class="requerido">
							<input id="txtrfc" class="input_width100" type="text" placeholder="RFC" data-valida="requerido"/>
							<div class="marca"></div>
						</div>
					</div>
				</div><!-- end row-->
	
				<!--Dirección .................................................... -->
				<div class="row" style="height:116px;">
					<div class="row_validador">
						<div class="requerido">
							<textarea id="txtdireccion_contacto" class="input_width100" rows="6" cols="40" placeholder="Dirección" data-valida="requerido"></textarea>
						</div>
					</div>
				</div><!-- end row-->
	
				<!--Nombre del contacto .......................................... -->
				<div class="row">
					<div class="col-md-6 row_validador">
						<div class="requerido">
							<input id="txtnombre_contacto" type="text" placeholder="Nombre Contacto" data-valida="requerido"/>
							<div class="marca"></div>
						</div>
					</div>
					<div class="col-md-6 row_validador">
						<div class="requerido">
							<input id="txtapp_contacto" type="text" placeholder="Apellido Paterno" data-valida="requerido"/>
							<div class="marca"></div>
						</div>
					</div>
				</div><!-- end row-->
	
				<!--Apellido materno y correo electrónico ........................ -->
				<div class="row">
					<div class="col-md-6 row_validador">
						<div class="requerido">
							<input id="txtapm_contacto" type="text" placeholder="Apellido Materno" data-valida="requerido"/>
							<div class="marca"></div>
						</div>
					</div>
					<div class="col-md-6 row_validador">
						<div class="requerido">
							<input id="email" type="email" placeholder="Correo electrónico" data-valida="requerido,email"/>
							<div class="marca"></div>
						</div>
					</div>
				</div><!-- end row-->
	
				<!--Telefono ..................................................... -->
				<div class="row">
					<div class="col-md-6 row_validador">
						<div class="requerido">
							<input id="telefono" type="text"	maxlength="10" placeholder="Teléfono" data-valida="requerido,numero"/>
							<div class="marca"></div>
						</div>
					</div>
				</div><!-- end row-->
	
			</div><!-- end row-->
	
			<!--Botón Cancelar y aceptar ..........................................-->
			<div class="row" style="margin: 24px 0px 100px 0px;">
				<div class="col-md-6">
					<div class="pull-right cancelar">
						<a class="BB_Fantasma_N" href="uas_inicio">CANCELAR</a>
					</div>
				</div>
				<div class="col-md-6">
					<div class="pull-left guardar">
						<button id="editar" class="B_Regular_N" type="button">GUARDAR</button>
					</div>
				</div>
			</div><!-- end row-->
	
		</form><!-- end .formDark-->

	</div><!-- end .centerme-->
</div><!-- end row-->
