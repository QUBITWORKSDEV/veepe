<?php
  global $user;
  $uid = $user -> uid;
  $array = getUsuario(getVeepeidById($uid));
?>

<style>
  body footer {
    display: none;
  }
</style>

<div class="home" style="overflow-y: scroll;">

  <div class="row" style="margin-top: 24px; margin-bottom: 100px;">

    <!--Perfil _____________________________________________________________ -->
    <div class="row" style="text-align: center;">

      <!--Foto-->
      <div class="row">
        <div class="img_estacionamiento_h"></div>
      </div><!-- end row-->

      <!--Nombre del usuario-->
      <div class="row">
        <h1 class="white">Nombre del Estacionamiento</h1>
      </div><!-- end row-->

      <!--VeepeID-->
      <div class="row">
        <span><h4 class="white">VEEPE ID: </h4></span>
        <span><h4 class="blue">
          VEEPEID
        </h4></span>
      </div><!-- end row-->

      <!--Correo-->
      <div class="row">
          <span class="body3_A">estacionamiento@veepe.com</span>
      </div><!-- end row-->

    </div><!-- end row-->

    <!--Datos del usuario __________________________________________________ -->
    <div class="row caja_perfil" style="margin-top: 24px;">

      <div class="row" style="margin-top: 16px;">
        <!--Nombre del estacionamiento ..................................... -->
        <div class="col-md-12">
          <label>Nombre Estacionamiento</label><span class="body3_B">Estacionamiento Funk</span>
        </div>

        <!--Dirección del estacionamiento .................................. -->
        <div class="col-md-12" style="margin-bottom:16px;">
          <label style="padding-bottom: 0px;">Dirección</label><br>
          <span class="body3_B">
            Pestalozzi 231 Anexo 1 Col. Narvarte Delegación Benito Juárez 03020 Distrito Federal México
          </span>
        </div>

        <!--Latitud y longitud ............................................. -->
        <div class="col-md-6">
          <label>Latitud</label><span class="body3_B">19.435237</span>
        </div>
        <div class="col-md-6">
          <label>Longitud</label><span class="body3_B">-99.180345</span>
        </div>

        <!--Latitud y longitud ............................................. -->
        <div class="col-md-6">
          <label>Capacidad</label><span class="body3_B">30</span>
        </div>
        <div class="col-md-6">
          <label>Pensiones contratadas</label><span class="body3_B">10</span>
        </div>

        <!--Horario de apertura y cierre ................................... -->
        <div class="col-md-6">
          <label>Horario de apertura</label><span class="body3_B">10:00 am</span>
        </div>
        <div class="col-md-6">
          <label>Horario de cierre</label><span class="body3_B">11:00 pm</span>
        </div>

        <!--Correo electrónico ............................................. -->
        <div class="col-md-6">
          <label>Correo</label><span class="body3_B">estacionamiento@funktionell.com.mx</span>
        </div>

        <!--Servicios ______________________________________________________ -->
        <div class="col-md-12" style="text-align: center">
          <h3 class="white">Servicios</h3>
        </div>

        <!--Checkboxes de servicios ........................................ -->
        <div class="col-md-6">
          <input type="checkbox" checked /><label><span class="checkbox" ></span>Servicio 1</label>
        </div>
        <div class="col-md-6">
          <input type="checkbox" checked /><label><span class="checkbox" ></span>Servicio 2</label>
        </div>
        <div class="col-md-6">
          <input type="checkbox" checked /><label><span class="checkbox" ></span>Servicio 3</label>
        </div>
        <div class="col-md-6">
          <input type="checkbox" checked /><label><span class="checkbox" ></span>Servicio 4</label>
        </div>

        <!--Dirección del estacionamiento .................................. -->
        <div class="col-md-12" style="margin-bottom:16px;">
          <label style="padding-bottom: 0px;">Terminos y condiciones</label><br>
          <span class="body3_B">
            Saepe delicata scriptorem ea mei, in dictas consetetur definitionem eam. Nibh tempor ne duo. Nec delectus repudiandae cu, pro in abhorreant elaboraret, nisl percipitur no sit. Eum inimicus intellegam honestatis ea, sea et corpora convenire. Id vel luptatum explicari omittantur, cu ius etiam errem. Ut postea graeco repudiare eam, delicata disputationi ne pri, ad sea ridens fabulas veritus.
            Ex vis esse vitae appareat. Eu noluisse mediocrem philosophia mei, ad nisl melius doctus est. Explicari vulputate at vis, erant vocent assueverit mea te. Sea aperiri tractatos id. Cu qui unum ignota aperiri.
          </span>
        </div>

      </div><!-- end row-->

      <!--Boton  ___________________________________________________________ -->
      <div class="row">
        <!--Boton-->
        <div class="col-md-12" style="text-align: center; padding: 12px 0px 24px 0px;">
          <a href="uf_editar" class="B_Regular_N">EDITAR</a>
        </div>
      </div><!-- end row-->

    </div><!-- end row-->


  </div><!-- end centerme-->
</div><!-- end #home-->
