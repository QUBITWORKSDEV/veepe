<?php
?>

<style>
  body footer {
    display: none;
  }
</style>

<div class="home">
  <div class="centerme caja_reportes">
    <!--Boton ______________________________________________________________ -->
    <div class="row">
      <div class="col-md-12" style="text-align: center; padding: 16px 0px">
        <button type="button" class="B_Regular_N">INGRESAR NUEVO CLIENTE</button>
      </div>
    </div><!-- end row-->

    <!--Tabla ______________________________________________________________ -->
    <div class="row">

      <!-- Búsqueda ________________________________________________________ -->
      <div class="row">
        <div class="row_buscar">
          <h3 class="white">Clientes</h3>
          <div class="pull-right">
            <input class="input_buscar" type="text" placeholder="Buscar...">
            <div class="lupa"></div>
          </div>
        </div>
      </div>

      <!-- Tabla Header ____________________________________________________ -->
      <div class="row">
        <div class="c_header_col1"> <span>Imagen</span> </div>
        <div class="c_header_col2">Nombre de Empresa</div>
        <div class="c_header_col3">VEEPE ID</div>
        <div class="c_header_col4">Contacto</div>
        <div class="c_header_col5">Puesto</div>
        <div class="c_header_col6">Registro</div>
				<div class="c_header_col7">Correo</div>
				<div class="c_header_col8"> <span>Iconos</span> </div>
      </div>

      <!-- Tabla Contenido _________________________________________________ -->
      <div class="row">
        <div class="clientes">
          <table>
						<tr>
							<td class="c_col1"><div class="c_img"></div></td>
							<td class="c_col2">BNI Estacionamientos</td>
							<td class="c_col3">ID0001</td>
							<td class="c_col4">Rosetta Little</td>
							<td class="c_col5">Administrador</td>
							<td class="c_col6">01/08/2016</td>
							<td class="c_col7">gillian_stiedemann@metz.net</td>
							<td class="c_col8">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="c_col1"><div class="c_img"></div></td>
							<td class="c_col2">BNI Estacionamientos</td>
							<td class="c_col3">ID0001</td>
							<td class="c_col4">Rosetta Little</td>
							<td class="c_col5">Administrador</td>
							<td class="c_col6">01/08/2016</td>
							<td class="c_col7">gillian_stiedemann@metz.net</td>
							<td class="c_col8">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="c_col1"><div class="c_img"></div></td>
							<td class="c_col2">BNI Estacionamientos</td>
							<td class="c_col3">ID0001</td>
							<td class="c_col4">Rosetta Little</td>
							<td class="c_col5">Administrador</td>
							<td class="c_col6">01/08/2016</td>
							<td class="c_col7">gillian_stiedemann@metz.net</td>
							<td class="c_col8">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="c_col1"><div class="c_img"></div></td>
							<td class="c_col2">BNI Estacionamientos</td>
							<td class="c_col3">ID0001</td>
							<td class="c_col4">Rosetta Little</td>
							<td class="c_col5">Administrador</td>
							<td class="c_col6">01/08/2016</td>
							<td class="c_col7">gillian_stiedemann@metz.net</td>
							<td class="c_col8">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="c_col1"><div class="c_img"></div></td>
							<td class="c_col2">BNI Estacionamientos</td>
							<td class="c_col3">ID0001</td>
							<td class="c_col4">Rosetta Little</td>
							<td class="c_col5">Administrador</td>
							<td class="c_col6">01/08/2016</td>
							<td class="c_col7">gillian_stiedemann@metz.net</td>
							<td class="c_col8">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="c_col1"><div class="c_img"></div></td>
							<td class="c_col2">BNI Estacionamientos</td>
							<td class="c_col3">ID0001</td>
							<td class="c_col4">Rosetta Little</td>
							<td class="c_col5">Administrador</td>
							<td class="c_col6">01/08/2016</td>
							<td class="c_col7">gillian_stiedemann@metz.net</td>
							<td class="c_col8">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="c_col1"><div class="c_img"></div></td>
							<td class="c_col2">BNI Estacionamientos</td>
							<td class="c_col3">ID0001</td>
							<td class="c_col4">Rosetta Little</td>
							<td class="c_col5">Administrador</td>
							<td class="c_col6">01/08/2016</td>
							<td class="c_col7">gillian_stiedemann@metz.net</td>
							<td class="c_col8">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="c_col1"><div class="c_img"></div></td>
							<td class="c_col2">BNI Estacionamientos</td>
							<td class="c_col3">ID0001</td>
							<td class="c_col4">Rosetta Little</td>
							<td class="c_col5">Administrador</td>
							<td class="c_col6">01/08/2016</td>
							<td class="c_col7">gillian_stiedemann@metz.net</td>
							<td class="c_col8">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="c_col1"><div class="c_img"></div></td>
							<td class="c_col2">BNI Estacionamientos</td>
							<td class="c_col3">ID0001</td>
							<td class="c_col4">Rosetta Little</td>
							<td class="c_col5">Administrador</td>
							<td class="c_col6">01/08/2016</td>
							<td class="c_col7">gillian_stiedemann@metz.net</td>
							<td class="c_col8">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="c_col1"><div class="c_img"></div></td>
							<td class="c_col2">BNI Estacionamientos</td>
							<td class="c_col3">ID0001</td>
							<td class="c_col4">Rosetta Little</td>
							<td class="c_col5">Administrador</td>
							<td class="c_col6">01/08/2016</td>
							<td class="c_col7">gillian_stiedemann@metz.net</td>
							<td class="c_col8">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="c_col1"><div class="c_img"></div></td>
							<td class="c_col2">BNI Estacionamientos</td>
							<td class="c_col3">ID0001</td>
							<td class="c_col4">Rosetta Little</td>
							<td class="c_col5">Administrador</td>
							<td class="c_col6">01/08/2016</td>
							<td class="c_col7">gillian_stiedemann@metz.net</td>
							<td class="c_col8">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="c_col1"><div class="c_img"></div></td>
							<td class="c_col2">BNI Estacionamientos</td>
							<td class="c_col3">ID0001</td>
							<td class="c_col4">Rosetta Little</td>
							<td class="c_col5">Administrador</td>
							<td class="c_col6">01/08/2016</td>
							<td class="c_col7">gillian_stiedemann@metz.net</td>
							<td class="c_col8">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="c_col1"><div class="c_img"></div></td>
							<td class="c_col2">BNI Estacionamientos</td>
							<td class="c_col3">ID0001</td>
							<td class="c_col4">Rosetta Little</td>
							<td class="c_col5">Administrador</td>
							<td class="c_col6">01/08/2016</td>
							<td class="c_col7">gillian_stiedemann@metz.net</td>
							<td class="c_col8">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="c_col1"><div class="c_img"></div></td>
							<td class="c_col2">BNI Estacionamientos</td>
							<td class="c_col3">ID0001</td>
							<td class="c_col4">Rosetta Little</td>
							<td class="c_col5">Administrador</td>
							<td class="c_col6">01/08/2016</td>
							<td class="c_col7">gillian_stiedemann@metz.net</td>
							<td class="c_col8">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="c_col1"><div class="c_img"></div></td>
							<td class="c_col2">BNI Estacionamientos</td>
							<td class="c_col3">ID0001</td>
							<td class="c_col4">Rosetta Little</td>
							<td class="c_col5">Administrador</td>
							<td class="c_col6">01/08/2016</td>
							<td class="c_col7">gillian_stiedemann@metz.net</td>
							<td class="c_col8">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="c_col1"><div class="c_img"></div></td>
							<td class="c_col2">BNI Estacionamientos</td>
							<td class="c_col3">ID0001</td>
							<td class="c_col4">Rosetta Little</td>
							<td class="c_col5">Administrador</td>
							<td class="c_col6">01/08/2016</td>
							<td class="c_col7">gillian_stiedemann@metz.net</td>
							<td class="c_col8">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="c_col1"><div class="c_img"></div></td>
							<td class="c_col2">BNI Estacionamientos</td>
							<td class="c_col3">ID0001</td>
							<td class="c_col4">Rosetta Little</td>
							<td class="c_col5">Administrador</td>
							<td class="c_col6">01/08/2016</td>
							<td class="c_col7">gillian_stiedemann@metz.net</td>
							<td class="c_col8">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="c_col1"><div class="c_img"></div></td>
							<td class="c_col2">BNI Estacionamientos</td>
							<td class="c_col3">ID0001</td>
							<td class="c_col4">Rosetta Little</td>
							<td class="c_col5">Administrador</td>
							<td class="c_col6">01/08/2016</td>
							<td class="c_col7">gillian_stiedemann@metz.net</td>
							<td class="c_col8">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="c_col1"><div class="c_img"></div></td>
							<td class="c_col2">BNI Estacionamientos</td>
							<td class="c_col3">ID0001</td>
							<td class="c_col4">Rosetta Little</td>
							<td class="c_col5">Administrador</td>
							<td class="c_col6">01/08/2016</td>
							<td class="c_col7">gillian_stiedemann@metz.net</td>
							<td class="c_col8">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="c_col1"><div class="c_img"></div></td>
							<td class="c_col2">BNI Estacionamientos</td>
							<td class="c_col3">ID0001</td>
							<td class="c_col4">Rosetta Little</td>
							<td class="c_col5">Administrador</td>
							<td class="c_col6">01/08/2016</td>
							<td class="c_col7">gillian_stiedemann@metz.net</td>
							<td class="c_col8">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="c_col1"><div class="c_img"></div></td>
							<td class="c_col2">BNI Estacionamientos</td>
							<td class="c_col3">ID0001</td>
							<td class="c_col4">Rosetta Little</td>
							<td class="c_col5">Administrador</td>
							<td class="c_col6">01/08/2016</td>
							<td class="c_col7">gillian_stiedemann@metz.net</td>
							<td class="c_col8">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
          </table>
        </div><!-- end empleados-->
      </div><!-- end row-->

    </div><!-- end Tabla-->

  </div><!-- end centerme-->
</div><!-- end #home-->
