<style>
	body { background-color: white; }
</style>

<br><br>

<div class="guiaestilo" style="padding:0px; position: relative; text-align: center;">
	<div id="preview">
		<center>
			
			<!-- PALETA DE COLORES -->
			<h1>Paleta de colores</h1>
			<br>
			<div class="paleta" style="display: flex; max-width: 960px; flex-wrap: wrap">
				<div class="color interactable dark_primary"
					data-titulo="Color - Dark Primary"
					data-tipo="color" 
					data-propiedad="@Dark_Primary"
					data-valor="#414042">Dark Primary<br><span class="colorcode">#414042</span></div>
				
				<div class="color interactable dark_secondary"
					data-titulo="Color - Dark Secondary" 
					data-tipo="color" 
					data-propiedad="@Dark_Secondary" 
					data-valor="#2C2A2D">Dark Secondary<br><span class="colorcode">#2C2A2D</span></div>
				
				<div class="color interactable dark_tetrary"
					data-titulo="Color - Dark Tetrary" 
					data-tipo="color" 
					data-propiedad="@Dark_Tetrary" 
					data-valor="#090A19">Dark Tetrary<br><span class="colorcode">#090A19</span></div>
				
				<div class="color interactable medium"
					data-titulo="Color - Medium" 
					data-tipo="color" 
					data-propiedad="@Medium" 
					data-valor="#A3A6A4">Medium<br><span class="colorcode">#A3A6A4</span></div>
				
				<div class="color interactable divider"
					data-titulo="Color - Divider" 
					data-tipo="color" 
					data-propiedad="@Divider" 
					data-valor="#D6D5D6">Divider<br><span class="colorcode">#D6D5D6</span></div>
				
				<div class="color interactable header"
					data-titulo="Color - Header" 
					data-tipo="color" 
					data-propiedad="@Header" 
					data-valor="#E8EAE9">Header<br><span class="colorcode">#E8EAE9</span></div>
				
				<div class="color interactable light_primary"
					data-titulo="Color - Light Primary" 
					data-tipo="color" 
					data-propiedad="@Light_Primary" 
					data-valor="#EFF1F3">Light Primary<br><span class="colorcode">#EFF1F3</span></div>
				
				<div class="color interactable accent"
					data-titulo="Color - Accent" 
					data-tipo="color" 
					data-propiedad="@Accent" 
					data-valor="#00BFD6">Accent<br><span class="colorcode">#00BFD6</span></div>
				
				<div class="color interactable accent_hover"
					data-titulo="Color - Accent Hover" 
					data-tipo="color" 
					data-propiedad="@Accent_Hover" 
					data-valor="#00ABD3">Accent Hover<br><span class="colorcode">#00ABD3</span></div>
				
				<div class="color interactable true"
					data-titulo="Color - True" 
					data-tipo="color" 
					data-propiedad="@True" 
					data-valor="#7ED321">True<br><span class="colorcode">#7ED321</span></div>
				
				<div class="color interactable false_normal"
					data-titulo="Color - False Normal" 
					data-tipo="color" 
					data-propiedad="@False_Normal" 
					data-valor="#D0021B">False Normal<br><span class="colorcode">#D0021B</span></div>
				
				<div class="color interactable false_hover"
					data-titulo="Color - False Hover" 
					data-tipo="color" 
					data-propiedad="@False_Hover" 
					data-valor="#AF0D13">False Hover<br><span class="colorcode">#AF0D13</span></div>
			</div>
	
			<br><br><br>
	
			<!-- USO TIPOGRAFICO *************************************************** -->
			<h1>Uso tipográfico</h1>
			<br>
			<div class="uso_tipografico" style="display: flex; max-width: 864px; flex-wrap: wrap; flex-direction: row">
				<div class="interactable" data-titulo="Uso tipográfico - H1" data-tipo="tipografia" data-propiedad="h1">
					<h1>H1</h1>
				</div>
				<div class="input_activo">Input Activo</div>
				<div><h2>H2</h2></div>
				<div class="input_error">Input Error</div>
				<div><h3>H3</h3></div>
				<div class="switch_inactivo">Switch Inactivo</div>
				<div><h4>H4</h4></div>
				<div class="switch_activo">Switch Activo</div>
				<div><h5>H5</h5></div>
				<div class="hyperlink">Hyperlink Normal</div>
				<div class="body1">Body1</div>
				<div class="hyperlink_activo">Hyperlink Activo</div>
				<div class="body2">Body2</div>
				<div class="table_header">Header Table</div>
				<div class="quote">Quote</div>
				<div class="table_subheader">Subheader Table</div>
				<div class="placeholder">Placeholder</div>
				<div class="table_content">Content Table</div>
				<div></div>
				<div class="table_selected">Selected Table</div>
			</div>
	
			<br><br><br>
	
	
			<!-- BUTTONS *********************************************************++ -->
			<div style="background: #000000; padding: 20px;">
				<h1 style="color: #FFFFFF;">BUTTONS</h1>
				<table style="width:85%;">
					<tr>
						<td colspan="2" style="padding-top: 40px"><center>
							<a href="#" class="BR_Regular_N">BOTON REGULAR NORMAL</a>
						</center></td>
					</tr>
					<tr>
						<td colspan="2" style="padding-top: 40px"><center>
							<button type="button" href="#" class="BR_Fantasma_N">BOTON FANTASMA NORMAL</button>
						</center></td>
					</tr>
					<tr>
						<td style="width:50%; padding-top: 40px">
							<a href="#" class="B_Regular_N">REGULAR BOTON NORMAL</a>
						</td>
						<td style="width:50%; padding-top: 40px">
							<a class="B_Regular_I">REGULAR BOTON INACTIVO</a>
						</td>
					</tr>
	
					<tr>
						<td style="padding-top: 40px">
							<a href="#" class="B_Fantasma_N">GHOST BUTTON NORMAL</a>
						</td>
						<td style="padding-top: 40px">
							<a class="B_Fantasma_I">GHOST BUTTON INACTIVO</a>
						</td>
					</tr>
	
					<tr>
						<td style="padding-top: 40px">
							<a href="#" class="B_Delete_N">REGULAR BOTON NORMAL</a>
						</td>
						<td style="padding-top: 40px">
							<a class="B_Delete_I">REGULAR BOTON INACTIVO</a>
						</td>
					</tr>
	
					<tr>
						<td style="padding-top: 40px">
							<a href="#" class="BB_Fantasma_N">GHOST BOTON NORMAL</a>
						</td>
						<td style="padding-top: 40px">
						</td>
					</tr>
				</table>
			</div>
	
			<!-- INPUTS ************************************************************ -->
			<h1>INPUTS</h1>
	
			<!-- FORMA DARK ******************************************************** -->
			<div style="background: #000000; padding: 20px;">
				<div class="formaDark">
	
					<!-- ESTILO ******************************************************** -->
					<h3 style="color: #FFFFFF">Login Dark - Estilo</h3>
					<div class="row">
						<input type="text" class="" placeholder="Normal">
						<input type="text" class="active" placeholder="Active">
						<div class="requerido">
							<input type="text" class="success" value="Success">
							<div class="paloma"></div>
						</div>
						<div class="requerido">
							<input type="text" class="error" value="Error">
							<div class="cruz"></div>
						</div>
						<div class="requerido">
							<input type="text" placeholder="Campo Requerido" />
							<div class="marca"></div>
						</div>
						<div class="requerido">
							<select>
								<option value="volvo">Volvo</option>
								<option value="saab">Saab</option>
								<option value="mercedes">Mercedes</option>
								<option value="audi">Audi</option>
							</select>
							<div class="marca"></div>
						</div>
						<div class="requerido">
							<textarea rows="8" cols="40">Textarea</textarea>
							<div class="marca"></div>
						</div>
					</div><!--end row -->
	
					<!-- EJEMPLO ******************************************************* -->
					<h3 style="color: #FFFFFF">Copiar Código</h3>
	
					<div class="row">
						<div class="row_validador">
							<div class="requerido">
								<input type="email" placeholder="Correo electrónico" data-valida="requerido,email"/>
								<div class="marca"></div>
							</div>
						</div>
					</div>
	
					<div class="row">
	
					</div>
	
					<div class="row">
						<div class="row_validador">
							<div class="requerido">
								<input type="text" placeholder="Campo no requerido"/>
							</div>
						</div>
					</div>
	
					<div class="row">
						<div class="row_validador">
							<div class="requerido">
								<input type="text" placeholder="Campo requerido" data-valida="requerido"/>
								<div class="marca"></div>
							</div>
						</div>
					</div>
	
					<div class="row">
						<div class="row_validador">
							<div class="requerido">
								<select>
									<option value="" disabled selected style="display:none">Select no requerido</option>
									<option value="value_01">norequerido_01</option>
									<option value="value_02">norequerido_02</option>
									<option value="value_03">norequerido_03</option>
								</select>
							</div>
						</div>
					</div>
	
					<div class="row">
						<div class="row_validador">
							<div class="requerido">
								<select data-valida="requerido">
									<option value="" disabled selected style="display:none">Select requerido</option>
									<option value="value_01">requerido_01</option>
									<option value="value_02">requerido_02</option>
									<option value="value_03">requerido_03</option>
								</select>
								<div class="marca"></div>
							</div>
						</div>
					</div>
	
					<div class="row" style="height:116px;">
						<div class="row_validador">
							<div class="requerido">
								<textarea id="" rows="6" cols="40" placeholder="Textarea no requerido"></textarea>
							</div>
						</div>
					</div>
	
					<div class="row" style="height:116px;">
						<div class="row_validador">
							<div class="requerido">
								<textarea id="" rows="6" cols="40" placeholder="Textarea requerido" data-valida="requerido"></textarea>
								<div class="marca"></div>
							</div>
						</div>
					</div><!-- end row-->
	
	
				</div><!--end formaDark -->
			</div><!--end row -->
	
			<!-- FORMA REGULAR ***************************************************** -->
			<div style="background: #FFFFFF; padding: 20px;">
				<div class="formaRegular">
	
					<!-- ESTILO ******************************************************** -->
					<h3>Login Regular</h3>
					<input type="text" class="" placeholder="Normal">
					<input type="text" class="active" placeholder="Active">
					<div class="requerido">
						<input type="text" class="success" value="Success">
						<div class="paloma"></div>
					</div>
					<div class="requerido">
						<input type="text" class="error" value="Error">
						<div class="cruz"></div>
					</div>
					<div class="requerido">
						<input type="text" placeholder="Campo Requerido" />
						<div class="marca"></div>
					</div>
	
					<div class="requerido">
						<select>
							<option value="volvo">Volvo</option>
							<option value="saab">Saab</option>
							<option value="mercedes">Mercedes</option>
							<option value="audi">Audi</option>
						</select>
						<div class="marca"></div>
					</div>
	
					<div class="requerido">
						<textarea rows="8" cols="40">Textarea</textarea>
						<div class="marca"></div>
					</div>
	
					<!-- EJEMPLO ******************************************************* -->
					<h3 style="color: #000000">Copiar Código</h3>
	
					<div class="row">
						<div class="row_validador">
							<div class="requerido">
								<input type="email" placeholder="Correo electrónico" data-valida="requerido,email"/>
								<div class="marca"></div>
							</div>
						</div>
					</div>
	
					<div class="row">
	
					</div>
	
					<div class="row">
						<div class="row_validador">
							<div class="requerido">
								<input type="text" placeholder="Campo no requerido"/>
							</div>
						</div>
					</div>
	
					<div class="row">
						<div class="row_validador">
							<div class="requerido">
								<input type="text" placeholder="Campo requerido" data-valida="requerido"/>
								<div class="marca"></div>
							</div>
						</div>
					</div>
	
					<div class="row">
						<div class="row_validador">
							<div class="requerido">
								<select>
									<option value="" disabled selected style="display:none">Select no requerido</option>
									<option value="value_01">norequerido_01</option>
									<option value="value_02">norequerido_02</option>
									<option value="value_03">norequerido_03</option>
								</select>
							</div>
						</div>
					</div>
	
					<div class="row">
						<div class="row_validador">
							<div class="requerido">
								<select data-valida="requerido">
									<option value="" disabled selected style="display:none">Select requerido</option>
									<option value="value_01">requerido_01</option>
									<option value="value_02">requerido_02</option>
									<option value="value_03">requerido_03</option>
								</select>
								<div class="marca"></div>
							</div>
						</div>
					</div>
	
					<div class="row" style="height:116px;">
						<div class="row_validador">
							<div class="requerido">
								<textarea id="" rows="6" cols="40" placeholder="Textarea no requerido"></textarea>
							</div>
						</div>
					</div>
	
					<div class="row" style="height:116px;">
						<div class="row_validador">
							<div class="requerido">
								<textarea id="" rows="6" cols="40" placeholder="Textarea requerido" data-valida="requerido"></textarea>
								<div class="marca"></div>
							</div>
						</div>
					</div><!-- end row-->
	
	
				</div><!--end formaRegular -->
			</div><!--end row -->
	
			<!-- TOOLTIPS ******************************************************** -->
			<div style="background: #FFFFFF; padding: 20px;">
				<h1>TOOLTIPS</h1>
				<a href="#" data-toggle="tooltip" data-placement="left" title="Hooray!">Left</a>
				<a href="#" data-toggle="tooltip" data-placement="top" title="Hooray!">Top</a>
				<a href="#" data-toggle="tooltip" data-placement="bottom" title="Hooray!">Bottom</a>
				<a href="#" data-toggle="tooltip" data-placement="right" title="Hooray!">Right</a>
			</div>
	
			<!-- CHECKBOXES ******************************************************** -->
			<div style="background: #000000; padding: 20px;">
				<h1 style="color: #FFFFFF">CHECKBOX</h1>
				<input id="input_checkbox1" type="checkbox"/><label><span class="checkbox" id="span_checkbox1" ></span>Check Box 1</label>
				<input id="input_checkbox2" type="checkbox"/><label><span class="checkbox" id="span_checkbox2" ></span>Check Box 2</label>
				<input id="input_checkbox3" type="checkbox"/><label><span class="checkbox" id="span_checkbox3" ></span>Check Box 3</label>
				<input id="input_checkbox4" type="checkbox"/><label><span class="checkbox" id="span_checkbox4" ></span>Check Box 4</label>
			</div>
	
			<!-- RADIO BUTTON ****************************************************** -->
			<div style="background: #000000; padding: 20px;">
				<h1 style="color: #FFFFFF">RADIO BUTTON</h1>
				<input id="input_radio1" name="grupo" type="radio" checked/><label><span class="radio" id="span_radio1" ></span>Radio button 1</label>
				<input id="input_radio2" name="grupo" type="radio"/><label><span class="radio" id="span_radio2" ></span>Radio button 2</label>
				<input id="input_radio3" name="grupo" type="radio"/><label><span class="radio" id="span_radio3" ></span>Radio button 3</label>
				<input id="input_radio4" name="grupo" type="radio"/><label><span class="radio" id="span_radio4" ></span>Radio button 4</label>
			</div>
		</center>
	</div>
	
	
	<div id="panel">
		<div style="position: relative; width: 100%; height: 100vh">
			<h3 class="titulo" style="color: white">Propiedades</h3><br><br>
			
			<div class="input sel_color" style="display: none">
				<span style="text-align: left">Color:</span><br>
				<input class="color" type="color" style="width: 200px" />
			</div>

			<div class="input sel_tipografia" style="display: none">
				<span style="text-align: left">Color:</span><br>
				<input class="color" type="color" style="width: 200px" />
				<span style="text-align: left">Fuente:</span><br>
				<input class="font" type="text" style="width: 200px" />
			</div>

			<button style="position: absolute; bottom: 40px; right:50%; margin: 0 auto; background: transparent">Generar LESS</button>
		</div>
	</div>
</div>


<script>
var target;
var titulo;
var tipo;
var valor;
var propiedad;

jQuery(document).ready(function(){
	jQuery('*.interactable').click(function(){
		target = jQuery(this);
		titulo = jQuery(this).data('titulo');			// Color fdrer
		tipo = jQuery(this).data('tipo');				// color
		valor = jQuery(this).data('valor');				// #233456
		propiedad = jQuery(this).data('propiedad');		// @Dark_primary
		
		// ponemos el titulo
		jQuery('#panel .titulo').html( titulo );
		
		// mostramos el control
		jQuery('#panel .input').hide();
		console.log(tipo);
		jQuery('#panel .input.sel_'+tipo).show();
		
		// ponemos el valor
		jQuery('#panel input.'+tipo).val( valor );
	});
	
	jQuery('#panel input').change(function(){
		// leemos el nuevo valor
		var nuevoValor = jQuery(this).val();
		
		// actualizamos el preview
		if ( tipo == 'color' )
		{
			target.css('background-color', nuevoValor);
		}
	});
});
</script>




















