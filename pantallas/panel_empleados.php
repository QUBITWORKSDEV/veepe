<?php
if (esAgv() || esAva()) {
	$sql_empleados = getEmpleadosAVG();
	$esAdmin = true;
} else if (esSocio()) {
	$sql_empleados = getEmpleadosSocio();
	$esAdmin = false;
}else if (esSupervisor()) {
	$sql_empleados = getEmpleadosSupervisor();
	$esAdmin = false;
}

function readableNombreRol($nombre)
{
	$res = $nombre;
	
	switch(strtolower($nombre))
	{
		case "jefe_estacionamiento":
			$res = "Jefe de estacionamiento";
			break;

		case "operador":
			$res = "Operador";
			break;

		case "supervisor":
			$res = "Supervisor";
			break;
	}
	
	return $res;
}
?>

<script src="pantallas/js/panel_empleados.js"></script>
<script>
	var esAdmin = "<?php echo $esAdmin ?>";
</script>

<div>
	<div class="caja_reportes">
		<!--Boton ______________________________________________________________ -->
		<div class="row">
			<div class="col-md-12" style="text-align: center; padding: 16px 0px">
				<button type="button" class="B_Regular_N" onclick="altaEmpleado()">AGREGAR NUEVO EMPLEADO</button>
			</div>
		</div><!-- end row-->

		<!--Tabla ______________________________________________________________ -->
		<div class="row">
			<div class="col-md-12">
				<?php
				if (esAgv() || esAva()) {
					?>
					<table id="tblEmpleados">
						<thead>
							<tr>
								<th></th>
								<th>Nombre</th>
								<th>Correo</th>
								<th>Celular</th>
								<th>Registro</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach ($sql_empleados as $empleado) {
								echo '<tr>';
								echo '<td><div class="icono empresa"></div></td>';
								echo '<td>' . $empleado->nombre . " " . $empleado->apellido_paterno . " " . $empleado->apellido_materno . '</td>';
								echo '<td>' . $empleado->email . '</td>';
								echo '<td>' . $empleado->celular . '</td>';
								echo '<td>' . $empleado->fechaCreacion . '</td>';
								echo '<td style="text-align:right">
									<div class="boton detalle" onclick="detalleEmpleado(' . $empleado->uid . ')"></div>
									<div class="boton email" onclick="emailEmpleado(\'' . $empleado->email . '\')"></div>
									<div class="boton editar" onclick="editarEmpleado(' . $empleado->uid . ')" ></div>
									<div class="boton borrar" onclick="eliminarEmpleado(' . $empleado->uid . ')"></div>
								</td>';
								echo '</tr>';
							}
							?>
						</tbody>
					</table>

				<?php } else if (esSocio() || esSupervisor()) { ?>
					<table id="tblEmpleados">
						<thead>
							<tr>
								<th></th>
								<th>Nombre</th>
								<th>Correo</th>
								<th>Celular</th>
								<th>Estacionamiento</th>
								<th>Registro</th>
								<th>Tipo empleado</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<?php
							if ($sql_empleados != null)
							{
								foreach ($sql_empleados as $empleado)
								{
									echo '<tr>';
									echo '<td><div class="icono empresa"></div></td>';
									echo '<td>' . $empleado->nombre . ' ' . $empleado->apellido_paterno . ' ' . $empleado->apellido_materno . '</td>';
									echo '<td>' . $empleado->email . '</td>';
									echo '<td>' . $empleado->celular . '</td>';
									echo '<td>' . $empleado->nombre_estacionamiento . '</td>';
									echo '<td>' . $empleado->fechaCreacion . '</td>';
									echo '<td>' . readableNombreRol($empleado->rol) . '</td>';
									echo '<td style="text-align:right">
											<div class="boton detalle" onclick="detalleEmpleado(' . $empleado->uid . ')"></div>
											<div class="boton email" onclick="emailEmpleado(\'' . $empleado->email . '\')"></div>
											<div class="boton editar" onclick="editarEmpleado(' . $empleado->uid . ')" ></div>
											<div class="boton borrar" onclick="eliminarEmpleado(' . $empleado->uid . ')"></div>
										</td>';
									echo '</tr>';
								}
							}
							?>
						</tbody>
					</table>
				<?php } ?>

			</div>
		</div><!-- end row -->

	</div>
</div><!-- end #home-->
