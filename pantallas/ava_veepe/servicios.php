<?php
?>

<style>
  body footer {
    display: none;
  }
</style>

<div class="home" style="overflow-y: scroll;">
  <div class="caja_servicios">
    <!--Boton ______________________________________________________________ -->
    <div class="row">
      <div class="col-md-12" style="text-align: center; padding: 16px 0px">
        <button type="button" class="B_Regular_N">INGRESAR SERVICIO</button>
      </div>
    </div><!-- end row-->

    <!--Servicios Activos __________________________________________________ -->
    <div class="row">
      <div class="caja_activos">
        <h3 class="white">Servicios Activos</h3>

        <div class="caja_item_servicio">
          <div class="servicio_activo">
              <div class="img_reserva"></div>
          </div>
          <h3 class="white">Estacionamiento</h3>
          <div class="s_iconos">
            <div class="s_icon b_detail"></div>
            <div class="s_icon b_edit"></div>
            <div class="s_icon b_erase"></div>
          </div>
        </div>

        <div class="caja_item_servicio">
          <div class="servicio_activo">
              <div class="img_reserva"></div>
          </div>
          <h3 class="white">Estacionamiento</h3>
          <div class="s_iconos">
            <div class="s_icon b_detail"></div>
            <div class="s_icon b_edit"></div>
            <div class="s_icon b_erase"></div>
          </div>
        </div>

        <div class="caja_item_servicio">
          <div class="servicio_activo">
              <div class="img_reserva"></div>
          </div>
          <h3 class="white">Estacionamiento</h3>
          <div class="s_iconos">
            <div class="s_icon b_detail"></div>
            <div class="s_icon b_edit"></div>
            <div class="s_icon b_erase"></div>
          </div>
        </div>

      </div><!-- end Servicios Activos-->

    </div><!-- end row-->

    <!--Servicios Inactivos __________________________________________________ -->
    <div class="row" style="margin-top:24px; margin-bottom: 156px;">
      <div class="caja_inactivos">
        <h3 class="white">Servicios Inactivos</h3>

        <div class="caja_item_servicio">
          <div class="servicio_inactivo">
              <div class="img_reserva"></div>
          </div>
          <h3 class="white">Estacionamiento</h3>
          <div class="s_iconos">
            <div class="s_icon b_detail"></div>
            <div class="s_icon b_edit"></div>
            <div class="s_icon b_erase"></div>
          </div>
        </div>

        <div class="caja_item_servicio">
          <div class="servicio_inactivo">
              <div class="img_reserva"></div>
          </div>
          <h3 class="white">Estacionamiento</h3>
          <div class="s_iconos">
            <div class="s_icon b_detail"></div>
            <div class="s_icon b_edit"></div>
            <div class="s_icon b_erase"></div>
          </div>
        </div>

        <div class="caja_item_servicio">
          <div class="servicio_inactivo">
              <div class="img_reserva"></div>
          </div>
          <h3 class="white">Estacionamiento</h3>
          <div class="s_iconos">
            <div class="s_icon b_detail"></div>
            <div class="s_icon b_edit"></div>
            <div class="s_icon b_erase"></div>
          </div>
        </div>

      </div><!-- end Servicios Activos-->

    </div><!-- end row-->

  </div><!-- end centerme-->
</div><!-- end #home-->
