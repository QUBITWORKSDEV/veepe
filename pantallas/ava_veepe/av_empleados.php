<?php ?>

<style>
	body footer {
		display: none;
	}
</style>

<div class="home">
	<div class="centerme caja_reportes">
		<!--Boton ______________________________________________________________ -->
		<div class="row">
			<div class="col-md-12" style="text-align: center; padding: 16px 0px">
				<button type="button" class="B_Regular_N">AGREGAR NUEVO EMPLEADO</button>
			</div>
		</div><!-- end row-->

		<div class="formaDark">
			<div class="row">
				<!-- Zona __________________________________________________________ -->
				<div class="col-md-12 row_validador">
					<div class="requerido">
						<select>
							<option value="" disabled selected style="display:none">Zona</option>
							<option value="Zona_01">Zona_01</option>
							<option value="Zona_02">Zona_02</option>
							<option value="Zona_03">Zona_03</option>
						</select>
						<div class="marca"></div>
					</div>
				</div>
				<!-- Lugar de tranajo ______________________________________________ -->
				<div class="col-md-12 row_validador">
					<div class="requerido">
						<select>
							<option value="" disabled selected style="display:none">Lugar</option>
							<option value="Lugar_01">Lugar_01</option>
							<option value="Lugar_02">Lugar_02</option>
							<option value="Lugar_03">Lugar_03</option>
						</select>
						<div class="marca"></div>
					</div>
				</div>
			</div><!-- end row-->
		</div><!-- end formaDark-->

		<!--Tabla ______________________________________________________________ -->
		<div class="row">

			<!-- Búsqueda ________________________________________________________ -->
			<div class="row">
				<div class="row_buscar">
					<h3 class="white">Empleados</h3>
					<div class="pull-right">
						<input class="input_buscar" type="text" placeholder="Buscar...">
						<div class="lupa"></div>
					</div>
				</div>
			</div>

			<!-- Tabla Header ____________________________________________________ -->
			<div class="row">
				<div class="e_header_col1"> <span>Imagen</span> </div>
				<div class="e_header_col2">Nombre del empleado</div>
				<div class="e_header_col3">VEEPE ID</div>
				<div class="e_header_col4">Lugar de trabajo</div>
				<div class="e_header_col5">Tipo de empleado</div>
				<div class="e_header_col6"> <span>Iconos</span> </div>
			</div>

			<!-- Tabla Contenido _________________________________________________ -->
			<div class="row">
				<div class="empleados">
					<table>
						<tr>
							<td class="e_col1"><div class="e_img"></div></td>
							<td class="e_col2">Milton Flower Thor</td>
							<td class="e_col3">ID0001</td>
							<td class="e_col4">Estacionamiento Funk</td>
							<td class="e_col5">Administrador</td>
							<td class="e_col6">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="e_col1"><div class="e_img"></div></td>
							<td class="e_col2">Milton Flower Thor</td>
							<td class="e_col3">ID0001</td>
							<td class="e_col4">Estacionamiento Funk</td>
							<td class="e_col5">Administrador</td>
							<td class="e_col6">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="e_col1"><div class="e_img"></div></td>
							<td class="e_col2">Milton Flower Thor</td>
							<td class="e_col3">ID0001</td>
							<td class="e_col4">Estacionamiento Funk</td>
							<td class="e_col5">Administrador</td>
							<td class="e_col6">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="e_col1"><div class="e_img"></div></td>
							<td class="e_col2">Milton Flower Thor</td>
							<td class="e_col3">ID0001</td>
							<td class="e_col4">Estacionamiento Funk</td>
							<td class="e_col5">Administrador</td>
							<td class="e_col6">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="e_col1"><div class="e_img"></div></td>
							<td class="e_col2">Milton Flower Thor</td>
							<td class="e_col3">ID0001</td>
							<td class="e_col4">Estacionamiento Funk</td>
							<td class="e_col5">Administrador</td>
							<td class="e_col6">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="e_col1"><div class="e_img"></div></td>
							<td class="e_col2">Milton Flower Thor</td>
							<td class="e_col3">ID0001</td>
							<td class="e_col4">Estacionamiento Funk</td>
							<td class="e_col5">Administrador</td>
							<td class="e_col6">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="e_col1"><div class="e_img"></div></td>
							<td class="e_col2">Milton Flower Thor</td>
							<td class="e_col3">ID0001</td>
							<td class="e_col4">Estacionamiento Funk</td>
							<td class="e_col5">Administrador</td>
							<td class="e_col6">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="e_col1"><div class="e_img"></div></td>
							<td class="e_col2">Milton Flower Thor</td>
							<td class="e_col3">ID0001</td>
							<td class="e_col4">Estacionamiento Funk</td>
							<td class="e_col5">Administrador</td>
							<td class="e_col6">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="e_col1"><div class="e_img"></div></td>
							<td class="e_col2">Milton Flower Thor</td>
							<td class="e_col3">ID0001</td>
							<td class="e_col4">Estacionamiento Funk</td>
							<td class="e_col5">Administrador</td>
							<td class="e_col6">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="e_col1"><div class="e_img"></div></td>
							<td class="e_col2">Milton Flower Thor</td>
							<td class="e_col3">ID0001</td>
							<td class="e_col4">Estacionamiento Funk</td>
							<td class="e_col5">Administrador</td>
							<td class="e_col6">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="e_col1"><div class="e_img"></div></td>
							<td class="e_col2">Milton Flower Thor</td>
							<td class="e_col3">ID0001</td>
							<td class="e_col4">Estacionamiento Funk</td>
							<td class="e_col5">Administrador</td>
							<td class="e_col6">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="e_col1"><div class="e_img"></div></td>
							<td class="e_col2">Milton Flower Thor</td>
							<td class="e_col3">ID0001</td>
							<td class="e_col4">Estacionamiento Funk</td>
							<td class="e_col5">Administrador</td>
							<td class="e_col6">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="e_col1"><div class="e_img"></div></td>
							<td class="e_col2">Milton Flower Thor</td>
							<td class="e_col3">ID0001</td>
							<td class="e_col4">Estacionamiento Funk</td>
							<td class="e_col5">Administrador</td>
							<td class="e_col6">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="e_col1"><div class="e_img"></div></td>
							<td class="e_col2">Milton Flower Thor</td>
							<td class="e_col3">ID0001</td>
							<td class="e_col4">Estacionamiento Funk</td>
							<td class="e_col5">Administrador</td>
							<td class="e_col6">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="e_col1"><div class="e_img"></div></td>
							<td class="e_col2">Milton Flower Thor</td>
							<td class="e_col3">ID0001</td>
							<td class="e_col4">Estacionamiento Funk</td>
							<td class="e_col5">Administrador</td>
							<td class="e_col6">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="e_col1"><div class="e_img"></div></td>
							<td class="e_col2">Milton Flower Thor</td>
							<td class="e_col3">ID0001</td>
							<td class="e_col4">Estacionamiento Funk</td>
							<td class="e_col5">Administrador</td>
							<td class="e_col6">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="e_col1"><div class="e_img"></div></td>
							<td class="e_col2">Milton Flower Thor</td>
							<td class="e_col3">ID0001</td>
							<td class="e_col4">Estacionamiento Funk</td>
							<td class="e_col5">Administrador</td>
							<td class="e_col6">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
						<tr>
							<td class="e_col1"><div class="e_img"></div></td>
							<td class="e_col2">Milton Flower Thor</td>
							<td class="e_col3">ID0001</td>
							<td class="e_col4">Estacionamiento Funk</td>
							<td class="e_col5">Administrador</td>
							<td class="e_col6">
								<div class="b_detail" href="#"></div>
								<div class="b_email" href="#"></div>
								<div class="b_edit" href="#"></div>
								<div class="b_erase" href="#"></div>
							</td>
						</tr>
					</table>
				</div><!-- end empleados-->
			</div><!-- end row-->

		</div><!-- end Tabla-->

	</div><!-- end centerme-->
</div><!-- end #home-->
