<?php
?>

<style>
  body footer {
    display: none;
  }
</style>

<div class="home">

  <div class="centerme">

    <!--Perfil _____________________________________________________________ -->
    <div class="row" style="text-align: center; padding: 8px 0px;">

      <!--Foto-->
      <div class="row">
        <div class="img_usuario_n"></div>
      </div><!-- end row-->

      <!--Nombre del usuario-->
      <div class="row">
        <h1 class="white">Empleado</h1>
      </div><!-- end row-->

      <!--VeepeID-->
      <div class="row">
        <span><h4 class="white">VEEPE ID: </h4></span>
        <span><h4 class="blue">
          VEEPEID
        </h4></span>
      </div><!-- end row-->

    </div><!-- end row-->

    <!--Datos del usuario __________________________________________________ -->
    <div class="row caja_perfil" style="margin: 16px 0px; padding-top: 24px;" >
      <div class="col-md-6">
        <label>Lugar de Trabajo</label><span class="body3_B">Estacionamiento Funk</span>
      </div>
      <div class="col-md-6">
        <label>Nombre</label><span class="body3_B">Milton Flower Thor</span>
      </div>
      <div class="col-md-6">
        <label>Correo</label><span class="body3_B">milton@veeepe.com</span>
      </div>
      <div class="col-md-6">
        <label>Puesto</label><span class="body3_B">Jefe de estacionamiento</span>
      </div>
      <div class="col-md-6">
        <label>Celular</label><span class="body3_B">55 55 6666 7777</span>
      </div>
      <div class="col-md-6">
        <label>Teléfono:</label><span class="body3_B">8 888 8888</span>
      </div>
      <div class="col-md-12">
        <label>Dirección</label><span class="body3_B">Pestalozzi 231 Narvarte,  Benito Juárez C.P. 03020 CDMX, México </span>
      </div>

      <!--Boton-->
      <div class="col-md-12" style="text-align: center; padding: 12px 0px 24px 0px;">
        <a href="" class="B_Regular_N">EDITAR</a>
      </div>
    </div><!-- end row-->

    <!--Tabla ______________________________________________________________ -->
    <div class="row">

      <!-- Búsqueda ________________________________________________________ -->
      <div class="row">
        <div class="row_buscar">
          <h3 class="white">Historial</h3>
          <div class="pull-right">
            <input class="input_buscar" type="text" placeholder="Buscar...">
            <div class="lupa"></div>
          </div>
        </div>
      </div>

      <!-- Navegacion ______________________________________________________ -->
      <div class="row" style="background-color: #000000;">
        <div style="width: 336px; margin: 0 auto;">
          <ul class="nav nav-pills">
            <li><a href="#tab1" data-toggle="tab">AÑO</a></li>
            <li><a href="#tab2" data-toggle="tab">MES</a></li>
            <li><a href="#tab3" data-toggle="tab">SEMANA</a></li>
            <li class="active"><a href="#tab4" data-toggle="tab">DÍA</a></li>
          </ul>
        </div>
      </div>

      <!-- Tabla Header ____________________________________________________ -->
      <div class="row">
        <div class="h_header_col1">Nombre del Estacionamiento</div>
        <div class="h_header_col2">VEEPE ID</div>
        <div class="h_header_col3">Fecha</div>
        <div class="h_header_col4">Asistencia</div>
        <div class="h_header_col5"> Turno </div>
      </div>

      <!-- Tabla Contenido _________________________________________________ -->
      <div class="row">
        <div class="historial">
          <table>
            <tr>
              <td class="h_col1">Milton Flower Thor</td>
              <td class="h_col2">EF0001</td>
              <td class="h_col3">10 / 08 / 2016</td>
              <td class="h_col4">Laborando</td>
              <td class="h_col5">Matutino</td>
            </tr>
            <tr>
              <td class="h_col1">Milton Flower Thor</td>
              <td class="h_col2">EF0001</td>
              <td class="h_col3">10 / 08 / 2016</td>
              <td class="h_col4">Laborando</td>
              <td class="h_col5">Matutino</td>
            </tr>
            <tr>
              <td class="h_col1">Milton Flower Thor</td>
              <td class="h_col2">EF0001</td>
              <td class="h_col3">10 / 08 / 2016</td>
              <td class="h_col4">Laborando</td>
              <td class="h_col5">Matutino</td>
            </tr>
          </table>
        </div><!-- end tabla-->
      </div><!-- end row-->
    </div><!-- end tabla-->


  </div><!-- end centerme-->
</div><!-- end #home-->
