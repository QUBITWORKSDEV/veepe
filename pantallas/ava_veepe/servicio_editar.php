<?php
?>

<style>
	body #navbar,
	section > h1,
	section .region .region-content ,
	body footer,
	body header {
	  display: none;
	}
</style>

<div class="row">
  <div class="caja_registro centerme">

		<div class="row" style="margin-bottom: 20px;">
			<center>
        <!--Foto-->
        <div class="row ">
          <div class="servicio_inactivo">
              <div class="img_reserva"></div>
          </div>
        </div><!-- end row-->

        <!--Nombre del usuario-->
        <div class="row">
          <h1 class="white" style="margin: 0px;">Inactivo</h1>
        </div><!-- end row-->

			</center>
		</div><!-- end row-->

    <!--Fomulario "formaDark" _____________________________________________________________________ -->
    <form class="formaDark" action="">

			<!--Registro ___________________________________________________________________________________ -->
			<div class="row">

          <!--Activo ....................................................... -->
          <div class="col-md-12 row_validador">
            <div class="requerido row_servicio_activacion">
              <input id="input_checkbox1" type="checkbox"/><label><span class="checkbox" id="span_checkbox1"></span>Activar servicio</label>
            </div>
          </div>

	        <!--Nombre del Servicio .......................................... -->
	        <div class="row">
	          <div class="col-md-6 row_validador">
							<div class="requerido">
			    			<input id="txtNombre" class="input_width100" type="text" placeholder="Nombre del Servicio" value="" data-valida="requerido"/>
								<div class="marca"></div>
			    		</div>
	          </div>
	        </div><!-- end row-->

	        <!--Tarifa ....................................................... -->
          <div class="col-md-12 row_validador ">
            <div class="requerido row_servicio" >
              <input id="input_checkbox1" type="checkbox"/><label style="padding: 16px;"><span class="checkbox" id="span_checkbox1" style="display:none;"></span>Tarifa</label>
              <input id="tarifa" class="row_costos pull-right" type="text" placeholder="Costo del servicio" maxlength="4" data-valida="numero"/>
              <div class="marca"></div>
            </div>
          </div>

          <!--Dirección .................................................... -->
          <div class="row" style="height: 180px;">
            <div class="row_validador">
      				<div class="requerido">
                <label class="textarea_label">Descripción</label>
      					<textarea class="input_width100 servicios" rows="5" cols="40" placeholder="Descripcion">
Ex vis esse vitae appareat. Eu noluisse mediocrem philosophia mei, ad nisl melius doctus est. Explicari vulputate at vis, erant vocent assueverit mea te. Sea aperiri tractatos id. Cu qui unum ignota aperir
      					</textarea>
      				</div>
	          </div>
          </div><!-- end row-->

					<!--Términos y condiciones ....................................... -->
          <div class="row" style="height: 216px">
            <div class="row_validador">
      				<div class="requerido">
                <label class="textarea_label">Términos y condiciones</label>
      					<textarea class="input_width100 servicios" rows="12" cols="40" placeholder="Terminos y condiciones">
Saepe delicata scriptorem ea mei, in dictas consetetur definitionem eam. Nibh tempor ne duo. Nec delectus repudiandae cu, pro in abhorreant elaboraret, nisl percipitur no sit. Eum inimicus intellegam honestatis ea, sea et corpora convenire. Id vel luptatum explicari omittantur, cu ius etiam errem. Ut postea graeco repudiare eam, delicata disputationi ne pri, ad sea ridens fabulas veritus.

Ex vis esse vitae appareat. Eu noluisse mediocrem philosophia mei, ad nisl melius doctus est. Explicari vulputate at vis, erant vocent assueverit mea te. Sea aperiri tractatos id. Cu qui unum ignota aperiri.
      					</textarea>
      				</div>
	          </div>
          </div><!-- end row-->

	    </div><!-- end row-->

      <!--Botón Registrar .................................................. -->
      <div class="row" style="margin: 8px 0px;">
				<div class="col-md-6">
					<div class="pull-right cancelar">
							<a class="BB_Fantasma_N" href="uas_inicio">CANCELAR</a>
					</div>
				</div>
				<div class="col-md-6">
					<div class="pull-left guardar">
							<button id="editar" class="B_Regular_N" type="button">GUARDAR</button>
					</div>
				</div>
      </div><!-- end row-->

    </form><!-- end .formDark-->
  </div><!-- end .centerme-->
</div><!-- end row-->
