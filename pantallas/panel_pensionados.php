<?php
global $user;


if (esSocio() || esSupervisor()) {

	$empresa = db_query("select id_empresa from f_r_usuario_empresa where uid =" . $user->uid)->fetchField();
	$sql_estacionamientos = getEstacionamientosPorCliente($user->uid);
	$sql_pensionados = getPensionadosPorCliente($user->uid);
	
} else {
	
	$estacionamiento = db_query("select e.id_estacionamiento, e.nombre from f_dc_perfilusuario pu
									   inner join f_dc_estacionamientos e on e.id_estacionamiento = pu.id_estacionamiento
									   where pu.uid =" . $user->uid)->fetchAll();
	
	$sql_pensionados = getPensionadosPorEstacionamiento($estacionamiento[0]->id_estacionamiento);
	
}
?>

<script src="pantallas/js/panel_pensionados.js"></script>


<div class="" style="margin-bottom:32px;">
	<div class="caja_reportes">
		<!--Boton ______________________________________________________________ -->
		<div class="row formaDark" style="width: 100%;">
			<div class="col-md-12" style="text-align: center; padding: 16px 0px">
				<?php
				if (esSocio()) {
					?>
					<select id="slctEstacionamientoPensionados" onchange="mostrarPensionados()">
						<?php
						foreach ($sql_estacionamientos as $estacionamiento) {
							echo "<option value=" . $estacionamiento->id_estacionamiento . ">" . $estacionamiento->nombre . "</option>";
						}
						?>
					</select>

					<?php
				} else {
					?>
					<label><?php echo $estacionamiento[0]->nombre; ?></label>
					<?php
				}
				?>


			</div>
		</div><!-- end row-->

		<!--Tabla ______________________________________________________________ -->
		<div class="row">

			<div class="col-md-12">
				<table id="tblPensionados">
					<thead>
						<tr>
							<th style="padding-left: 24px;">Nombre de usuario</th>
							<th>VEEPE ID</th>
							<th>Correo</th>
							<th>Servicios</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach ($sql_pensionados as $pensionado) {
							echo '<tr>';

							echo '	<td style="padding-left: 24px;">' . $pensionado->nombreUsuario . '</td>';
							echo '	<td>' . $pensionado->veepeid . '</td>';
							echo '	<td>' . $pensionado->email . '</td>';


							echo '<td>';

							foreach ($pensionado->servicios as $servicio) {
								if ($servicio->tipo_pension == "1") {
									echo '<div style="display: inline-block;" class="s_pension" href="#"></div>';
								}

								if ($servicio->tipo_pension == "3") {
									echo '<div style="display: inline-block;" class="s_pension_compartida" href="#"></div>';
								}
							}

							echo '</td>';

							echo '<td>
									<div class="boton detalle" onclick="detallePensionado(' . $pensionado->id_pensionado . ')"></div>
									<div class="boton borrar" onclick="eliminarPensionado(' . $pensionado->id_pensionado . ')"></div>
								</td>';
							echo '</tr>';
						}
						?>
					</tbody>
				</table>
			</div><!-- end col -->

		</div><!-- end row -->

	</div><!-- end centerme-->
</div><!-- end #home-->
