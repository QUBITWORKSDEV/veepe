<?php
?>
<script src="pantallas/js/inicio_recuperar.js"></script>

<style>
	body #navbar,
	section h1,
	section .region .region-content ,
	body footer,
	body header {
		display: none;
	}
</style>
<div class="row">
	<div class="centerme">

		<!--Fomulario "formaDark" _____________________________________________________________________ -->
		<form id="formRecuperar" class="formaDark" action="ingresar">

			<!--Logo Veepe-->
			<div class="row">
				<div class="logo_veepe" </div>
			</div><!-- end row-->

			<div class="row">
				<div class="formato_titulo">
					Recuperar contraseña
				</div>
				<div class="formato_texto">
					Para recuperar tu contraseña indícanos con qué correo diste de alta tu cuenta VEEPE.
				</div>
			</div>

			<div style="text-align: center" >
				<!--Correo electrónico-->
				<div class="row row_validador" style="display: table; margin: 0 auto;">
					<div class="" style="width: 312px;">
						<input id="email" name="email" type="email" placeholder="Correo" style="text-align: center;" data-valida="requerido,email">
					</div>
				</div><!-- end row-->

				<!--Botón Registrar-->
				<div class="row" style="display: table; margin: 0 auto;">
					<button id="cmdRecuperar" class="B_Regular_N" type="button" style="width:312px;">ENVIAR</button>
				</div><!-- end row-->
			</div>
				

			</form><!-- end .formDark-->

	</div><!-- end .centerme-->
<div><!-- end row-->
