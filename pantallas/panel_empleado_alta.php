<?php
// todo: seguridad: validar que el idempleado pertenzca a la misma empresa que el usuario actual

global $user;
if (isset($_GET['idempleado'])) {
	$id_empleado = $_GET['idempleado'];

	echo '<script>var uid = ' . $id_empleado . ';</script>';

	$rol = getRolesUsuario($id_empleado);
	$datosPerfil = db_query("select * from f_dc_perfilusuario where uid=" . $id_empleado)->fetchAll();
} else {
	$datosPerfil = null;
}
?>

<style>
	body{
		background-image:none;
	}
</style>

<script src="pantallas/js/panel_empleados_alta.js"></script>
<script>
	btn_regresar("./empleados");
</script>

<div class="centerme">

	<div class="row" style="margin-bottom: 20px;">
		<center>
			<div class="img_usuario_n"></div>

			<div class="row">
				<?php if (isset($_GET['idempleado'])) { ?>
					<h1 class="white"><?php echo ($datosPerfil == null ? "" : $datosPerfil[0]->nombre . " " . $datosPerfil[0]->apellido_paterno . " " . $datosPerfil[0]->apellido_materno); ?></h1>
					<h2 class="white"><?php echo ($datosPerfil == null ? "" : getNombreRol($rol[0]->rid)) ?></h2>
				<?php } else { ?>
					<h1 class="white">Alta de Empleado</h1>
				<?php } ?>
			</div>

			<!--Correo-->
			<div class="row">
				<span class="body3_A"><?php echo ($datosPerfil == null ? "" : $datosPerfil[0]->nombre_usuario); ?></span>
			</div><!-- end row-->

		</center>
	</div>

	<?php if (esAgv() || esAva()) { ?>

		<div id="frmAltaEmpleado" class="formaDark" style="width: 840px;">

			<div class="row">
				<div class="col-md-6">
					<div class="requerido">
						<input id="txtNombre" type="text" placeholder="Nombre" value="<?php echo ($datosPerfil == null ? "" : $datosPerfil[0]->nombre); ?>" data-valida="requerido" />
						<div class="marca"></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="requerido">
						<input id="txtApellidoPaterno" type="text" placeholder="Apellido paterno" value="<?php echo ($datosPerfil == null ? "" : $datosPerfil[0]->apellido_paterno); ?>" data-valida="requerido"/>
						<div class="marca"></div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<input id="txtApellidoMaterno" type="text" placeholder="Apellido materno" value="<?php echo ($datosPerfil == null ? "" : $datosPerfil[0]->apellido_materno); ?>"/>
				</div>

				<?php if (!isset($_GET['idempleado'])) { ?>
					<div class="col-md-6">
						<div class="requerido">
							<input id="txtEmail" type="text" placeholder="Correo Electrónico" value="<?php echo ($datosPerfil == null ? "" : $datosPerfil[0]->email); ?>" data-valida="requerido,email" />
							<div class="marca"></div>
						</div>
					</div>
				<?php } else { ?>
					<div class="col-md-6">
						<div class="requerido">
							<input id="txtEmail" disabled="disabled" type="text" placeholder="Correo Electrónico" value="<?php echo ($datosPerfil == null ? "" : $datosPerfil[0]->email); ?>" data-valida="requerido,email" />
							<div class="marca"></div>
						</div>
					</div>
				<?php } ?>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="requerido">
						<input id="txtCelular" type="text" placeholder="Celular" value="<?php echo ($datosPerfil == null ? "" : $datosPerfil[0]->celular); ?>" data-valida="requerido" />
						<div class="marca"></div>
					</div>
				</div>
			</div>

			<div class="row" style="margin: 12px 0px;">
				<div class="col-md-6">
					<div id="cancelar" class="pull-right">
						<button id="btn_cancelar_empleado" type="button" onclick="cancelarAltaEmpleado();" class="BB_Fantasma_N">CANCELAR</button>
					</div>
				</div>

				<?php if (!isset($_GET['idempleado'])) { ?>
					<div class="col-md-6">
						<div id="guardar" class="pull-left">
							<button id="btn_agregar_empleado" type="button" onclick="agregarEmpleado('A');" class="B_Regular_N">AGREGAR</button>
						</div>
					</div>
				<?php } else { ?>
					<div class="col-md-6">
						<div id="guardar" class="pull-left">
							<button id="btn_modificar_empleado" type="button" onclick="agregarEmpleado('E');" class="B_Regular_N">MODIFICAR</button>
						</div>
					</div>
				<?php } ?>

			</div>
		</div> <!-- fin de frmAltaEmpleado -->

		<?php
	} else if (esSocio() || esSupervisor()) {
		$id_empresa = db_query("select id_empresa from f_r_usuario_empresa where uid=" . $user->uid)->fetchField();
		$estacionamientos = db_query("select * from f_dc_estacionamientos where id_empresa=" . $id_empresa . " and status = 1")->fetchAll();

		if (isset($_GET['idempleado'])) {
			$id_estacionamiento = db_query("select id_estacionamiento from f_dc_perfilusuario where uid=" . $id_empleado)->fetchField();
			$rol = db_query("select rid from users_roles where uid=" . $id_empleado)->fetchField();
		} else {
			$id_estacionamiento = null;
			$rol = null;
		}
		?>

		<script>
			var id_empresa = "<?php echo $id_empresa ?>";
		</script>

		<div id="frmAltaEmpleado" class="formaDark" style="width: 840px">

			<div class="row">
				<div class="col-md-6">
					<div class="requerido">
						<input id="txtNombre" type="text" placeholder="Nombre" value="<?php echo ($datosPerfil == null ? "" : $datosPerfil[0]->nombre); ?>" data-valida="requerido" />
						<div class="marca"></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="requerido">
						<input id="txtApellidoPaterno" type="text" placeholder="Apellido paterno" value="<?php echo ($datosPerfil == null ? "" : $datosPerfil[0]->apellido_paterno); ?>" data-valida="requerido"/>
						<div class="marca"></div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<input id="txtApellidoMaterno"  type="text" placeholder="Apellido materno" value="<?php echo ($datosPerfil == null ? "" : $datosPerfil[0]->apellido_materno); ?>"/>
				</div>
				<div class="col-md-6">
					<div class="requerido">
						<select id="selPuesto" data-valida="requerido" onchange="mostrarEstacionamiento()">
							<option value="" disabled selected style="display:none">Puesto</option>

							<?php if (esSocio()) { ?>
								<option value="7"  <?php echo ($rol == 7 ? "selected" : "") ?>>Supervisor</option>
							<?php } ?>
							<option  value="10" <?php echo ($rol == 10 ? "selected" : "") ?>>Operador</option>
							<option  value="9" <?php echo ($rol == 9 ? "selected" : "") ?>>Jefe de estacionamiento</option>
						</select>
						<div class="marca"></div>
					</div>
				</div>
			</div>

			<div id="estacionamiento" class="row" <?php echo ($id_estacionamiento != null ? "" : "hidden") ?> >
				<div class="col-md-12">
					<div class="requerido">
						<select id="selEstacionamiento" data-valida="requerido">
							<option value="" disabled selected style="display:none">Estacionamiento</option>

							<?php
							foreach ($estacionamientos as $estacionamiento) {
								if ($id_estacionamiento == $estacionamiento->id_estacionamiento) {
									echo "<option selected='selected' value='" . $estacionamiento->id_estacionamiento . "'>" . $estacionamiento->nombre . "</option>";
								} else {
									echo "<option value='" . $estacionamiento->id_estacionamiento . "'>" . $estacionamiento->nombre . "</option>";
								}
							}
							?>

						</select>
						<div class="marca"></div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="requerido">
						<input id="txtCelular" type="text" placeholder="Celular" value="<?php echo ($datosPerfil == null ? "" : $datosPerfil[0]->celular); ?>" data-valida="requerido,numero" />
						<div class="marca"></div>
					</div>
				</div>

				<!--<div class="col-md-6">
					<div class="requerido">
						<input id="txtCelular" type="text" placeholder="Teléfono" value="<?php echo ($datosPerfil == null ? "" : $datosPerfil[0]->celular); ?>" data-valida="requerido,numero" />
						<div class="marca"></div>
					</div>
				</div>
			</div>

			<div class="row">-->
				<div class="col-md-6">
					<div class="requerido">
						<input id="txtEmail" <?php echo isset($_GET['idempleado'])?'disabled="disabled"':''; ?> type="text" placeholder="Email" value="<?php echo ($datosPerfil == null ? "" : $datosPerfil[0]->email); ?>" <?php echo !isset($_GET['idempleado'])?'data-valida="requerido,email"':''; ?> />
						<?php echo isset($_GET['idempleado'])?'':'<div class="marca"></div>'; ?>
					</div>
				</div>
			</div>

			<div class="row" style="margin: 12px 0px;">
				<div class="col-md-6">
					<div id="cancelar" class="pull-right">
						<button id="btn_cancelar_empleado_socio" type="button" onclick="javascript:cancelarAltaEmpleado();" class="BB_Fantasma_N">CANCELAR</button>
					</div>
				</div>
				<div class="col-md-6">
					<div id="guardar" class="pull-left">
						<?php if (!isset($_GET['idempleado'])) { ?>
							<button id="btn_agregar_empleado_socio" type="button" onclick="javascript:agregarEmpleadoSocio('A');" class="B_Regular_N">AGREGAR</button>
						<?php } else { ?>
							<button id="btn_modificar_empleado_socio" type="button" onclick="javascript:agregarEmpleadoSocio('E');" class="B_Regular_N">MODIFICAR</button>
						<?php } ?>
					</div>
				</div>
			</div>

		</div>
	<?php } ?>

</div>
