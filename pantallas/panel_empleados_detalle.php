<?php
if (isset($_GET['idempleado'])) {
	db_query("SET lc_time_names = 'es_UY'");
	$idEmpleado = $_GET['idempleado'];
	$empleado = db_query("select pu.*, e.nombre as nombre_estacionamiento, 
					date_format(FROM_UNIXTIME(u.created), '%d de %M %Y')as fechaCreacion
					from f_dc_perfilusuario pu
					inner join users_roles ur on ur.uid = pu.uid
					inner join users u on u.uid = pu.uid
					left join f_dc_estacionamientos e on e.id_estacionamiento = pu.id_estacionamiento
					where pu.uid =" . $idEmpleado)->fetchAll();
}
?>
<script>
	jQuery("#menuLateral").hide();
	btn_regresar("./empleados");
	
	function editarEmpleado(uid) {
		location.href = './alta_empleado?idempleado=' + uid;
	}
</script>

<style>
	body footer {
		display: none;
	}
</style>

<div class="home">

	<div style="margin-top: 24px; margin-bottom: 100px;">

		<!--Perfil _____________________________________________________________ -->
		<div class="row" style="text-align: center;">
			<div class="row" style="margin-top: 64px;">
				<div class="img_usuario_i"></div>
			</div>
			<div class="row">
				<h1 class="white"><?php echo $empleado[0]->nombre . " " . $empleado[0]->apellido_paterno . " " . $empleado[0]->apellido_materno; ?></h1>
			</div>
			<div class="row">
				<span class="body3_A"><?php echo $empleado[0]->nombre_usuario; ?> </span>
			</div>
		</div>

		<!--Datos del usuario __________________________________________________ -->
		<div class="row caja_perfil" style="margin-top: 24px;">
			<div class="row" style="margin-top: 16px;">
				<div class="col-md-6">
					<label>Nombre:</label><span class="body3_B"><?php echo $empleado[0]->nombre . " " . $empleado[0]->apellido_paterno . " " . $empleado[0]->apellido_materno; ?></span>
				</div>
				<div class="col-md-6" style="margin-bottom:16px;">
					<label>Email:</label><span class="body3_B"><?php echo $empleado[0]->email; ?></span><br>
				</div>
				<div class="col-md-6">
					<label>Celular:</label><span class="body3_B"><?php echo $empleado[0]->celular; ?></span>
				</div>
				<div class="col-md-6" style="margin-bottom:16px;">
					<label>Estacionamiento:</label><span class="body3_B"><?php echo $empleado[0]->nombre_estacionamiento; ?></span>
				</div>
				<div class="col-md-6">
					<label>Registro:</label><span class="body3_B"><?php echo $empleado[0]->fechaCreacion; ?></span>
				</div>
			</div>

			<!--Boton  ___________________________________________________________ -->
			<div class="row">
				<div class="col-md-12" style="text-align: center; padding: 12px 0px 24px 0px;">
					<a href="javascript:editarEmpleado('<?php echo $empleado[0]->uid; ?>')" class="B_Regular_N" >EDITAR</a>
				</div>
			</div>
		</div><!-- end row-->

	</div><!-- end centerme-->
</div><!-- end #home-->
