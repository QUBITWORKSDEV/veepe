<script src="pantallas/js/panel_estacionamientos_alta.js" type="text/javascript"></script>

<?php
global $user;
$estacionamiento = null;

if (isset($_GET['idestacionamiento'])) {
	$idEstacionamiento = $_GET['idestacionamiento'];
	$estacionamiento = getEstacionamiento($idEstacionamiento);
	//$servicios = getServiciosEstacionamiento($idEstacionamiento);
	//$pensiones = getPensionesEstacionamiento($idEstacionamiento);
	$amonestacionPension = getAmonestacionPension($idEstacionamiento);


	echo '<script>var servicioPensiones = false;</script>';
	echo '<script>servicioPensionesNormal = false;</script>';
	echo '<script>servicioPensionesCompartida = false;</script>';
	$servicioPensiones = false;

	if ($estacionamiento["Servicios"] != null) {
		foreach ($estacionamiento["Servicios"] as $servicio) {
			if ($servicio["idServicio"] == 1 || $servicio["idServicio"] == 2) {
				echo '<script>var servicioPensiones = true;</script>';

				if ($servicio["idServicio"] == 1) {
					echo '<script>servicioPensionesNormal = true;</script>';
				}


				if ($servicio["idServicio"] == 2) {
					echo '<script>servicioPensionesCompartida = true;</script>';
				}

				$servicioPensiones = true;
			}
		}
	} else {
		echo '<script>var servicioPensiones = false;</script>';
		echo '<script>var servicioPensionesNormal = false;</script>';
		echo '<script>var servicioPensionesCompartida = false;</script>';
		$servicioPensiones = false;
		$amonestacionPension = null;
	}

	echo '<script>var id_estacionamiento = ' . $idEstacionamiento . ';</script>';
} else {
	$servicios = null;
	echo '<script>var servicioPensiones = false;</script>';
	echo '<script>var servicioPensionesNormal = false;</script>';
	echo '<script>var servicioPensionesCompartida = false;</script>';
	$servicioPensiones = false;
	$amonestacionPension = null;
}
?>

<style>
	body{
		background-image:none;
	}

	#tblConfigurarPensiones > thead {
		font-family: 'Open Sans', sans-serif;
		font-size: 12px;
		color: #FFFFFF;
		letter-spacing: 0px;
		line-height: 16px;
		font-weight: lighter;
	}

	#tblConfigurarPensiones > thead > tr > th > label {
		font-family: 'Open Sans', sans-serif;
		font-size: 12px;
		color: #FFFFFF;
		letter-spacing: 0px;
		line-height: 16px;
		font-weight: lighter;
	}

	#tblConfigurarPensiones > thead > tr > th {
		text-align: center;
		font-weight: lighter;
		height: 40px;
	}

	#tblConfigurarPensiones > tbody > tr > td > input{
		text-align: center;
	}

	.pensionNormal::-webkit-input-placeholder { color: #00BFD6; }
	.pensionNormal::-moz-placeholder {color: #00BFD6; }
	.pensionNormal:-ms-input-placeholder { color: #00BFD6; } 
	.pensionNormal:-o-input-placeholder { color: #00BFD6; } 

	#tblConfigurarPensiones > tbody > tr > td{
		//background-color: rgba(65,63,65,0.60);
	}

	.inputPensionDesactivado::-webkit-input-placeholder { color: #A3A6A4; }
	.inputPensionDesactivado::-moz-placeholder {color: #A3A6A4; }
	.inputPensionDesactivado:-ms-input-placeholder { color: #A3A6A4; } 
	.inputPensionDesactivado:-o-input-placeholder { color: #A3A6A4; } 

	.inputPensionActivado::-webkit-input-placeholder { color: #00BFD6; }
	.inputPensionActivado::-moz-placeholder {color: #00BFD6; }
	.inputPensionActivado:-ms-input-placeholder { color: #00BFD6; } 
	.inputPensionActivado:-o-input-placeholder { color:#00BFD6; } 

	#tblConfigurarPensiones > tbody > tr > td > input{
		font-family: 'Open Sans', sans-serif;
		font-weight: 400;
		letter-spacing: 0px;
		width: 100%;
		height: 40px;
		padding: 0px 0px;
		margin-bottom: 0px;
		border: none;
		font-size: 14px;
		background-color: rgba(65,63,65,0.60);
		color: #00BFD6;
		line-height: 16px;
	}

	#tblConfigurarPensiones > tbody > tr > td > select{
		font-family: 'Open Sans', sans-serif;
		font-weight: 400;
		letter-spacing: 0px;
		width: 100%;
		height: 40px;
		padding: 0px 0px;
		margin-bottom: 0px;
		border: none;
		font-size: 14px;
		line-height: 40px;
		background-color: rgba(65,63,65,0.60);
		color: #00BFD6;
		line-height: 16px;
		text-align: center;
	}
</style>

<script>
	jQuery("#menuLateral").hide();
	btn_regresar("./estacionamientos");

	var uid = "<?php echo $user->uid; ?>";
</script>

<div class="">
	<div class="row" style="margin-bottom: 20px;">
		<center>
			<div class="img_estacionamiento_h"></div>

			<?php if (!isset($_GET['idestacionamiento'])) { ?>
				<h1 class="white">Alta Estacionamiento</h1>
			<?php } else { ?>
				<h1 class="white"><?php echo $estacionamiento["Nombre"]; ?></h1>
			<?php } ?>
		</center>
	</div>

	<div id="frmAltaEstacionamiento" class="formaDark" style="max-width: 848px; margin: 0 auto;">
		<!--Nombre del estacionamieto .................................... -->
		<div class="row">
			<div class="col-md-12">
				<div class="requerido">
					<input id="txtNombre" value="<?php echo ($estacionamiento == null ? "" : $estacionamiento["Nombre"]); ?>" type="text" placeholder="Nombre del Estacionamiento" data-valida="requerido"/>
					<div class="marca"></div>
				</div>
			</div>
		</div>

		<!--Dirección .................................................... -->
		<div class="row"><!-- 116 -->
			<div class="col-md-12">
				<div class="requerido">
					<textarea id="txtDireccion" rows="6" cols="40" placeholder="Dirección" data-valida="requerido"><?php echo ($estacionamiento == null ? "" : $estacionamiento["Direccion"]); ?></textarea>
					<div class="marca"></div>
				</div>
			</div>
		</div><!-- end row-->

		<!--Zona .................................................... -->
		<div class="row">
			<div class="col-md-12">
				<div class="requerido">
					<input id="txtZona" value="<?php echo ($estacionamiento == null ? "" : $estacionamiento["Zona"]); ?>" type="text" placeholder="Zona" data-valida="requerido"/>
					<div class="marca"></div>
				</div>
			</div>
		</div><!-- end row-->

		<!--Latitud y longitud ........................................... -->
		<div class="row">
			<div class="col-md-6">
				<div class="requerido">
					<input id="txtLatitud" type="text" value="<?php echo ($estacionamiento == null ? "" : $estacionamiento["Latitud"]); ?>" placeholder="Latitud" data-valida="requerido,numero"/>
					<div class="marca"></div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="requerido">
					<input id="txtLongitud" type="text" value="<?php echo ($estacionamiento == null ? "" : $estacionamiento["Longitud"]); ?>" placeholder="Longitud" data-valida="requerido,numero"/>
					<div class="marca"></div>
				</div>
			</div>
		</div><!-- end row-->

		<!--Capacidad y pensiones ........................... -->
		<div class="row">
			<div class="col-md-6">
				<div class="requerido">
					<input id="txtCapacidad" value="<?php echo ($estacionamiento == null ? "" : $estacionamiento["Capacidad"]); ?>" type="text" placeholder="Capacidad total" data-valida="requerido,numero"/>
					<div class="marca"></div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="requerido">
					<input id="txtPensiones" value="<?php echo ($estacionamiento == null ? "" : $estacionamiento["PensionesContratadas"]); ?>" type="text" placeholder="Capacidad pensiones" data-valida="requerido"/>
					<div class="marca"></div>
				</div>
			</div>
		</div><!-- end row-->

		<!--Horario de apertura y cierre ................................. -->
		<div class="row">
			<div class="col-md-6">
				<div class="requerido">
					<div class="form-group">
						<div class='input-group date' id='datetimepicker1'>
							<input id="txtHorarioApertura" value="<?php echo ($estacionamiento == null ? "" : substr($estacionamiento["horarioServicio"], 0, strpos($estacionamiento["horarioServicio"], '-'))); ?>" class="form-control" type="text" placeholder="Horario apertura" data-valida="requerido"/>
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-time"></span>
							</span>
						</div>
					</div>
					<div class="marca" style="z-index: 2"></div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="requerido">
					<div class="form-group">
						<div class='input-group date' id='datetimepicker2'>
							<input id="txtHorarioCierre" value="<?php echo ($estacionamiento == null ? "" : substr($estacionamiento["horarioServicio"], 8, strpos($estacionamiento["horarioServicio"], '-'))); ?>" class="form-control" type="text" placeholder="Horario cierre" data-valida="requerido"/>
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-time"></span>
							</span>
						</div>
						<div class="marca" style="z-index: 2"></div>
					</div>
				</div>
			</div>
		</div><!-- end row-->

		<!--Titulo alta de servicios ..................................... -->
		<div style="text-align:center">
			<h3 class="white">Alta Servicios</h3>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="check_input">
					<div class="pull-left">
						<input id="input_checkbox0" class="chkServicios" name="input_checkbox0" type="checkbox" checked="true" disabled="disabled"/>
						<label for="input_checkbox0">
							<span class="checkbox" id="span_checkbox0"></span>Tarifa estacionamiento
						</label>
					</div>
					<div class="pull-right">
						<input id="txtCosto_estacionamiento" data-valida="requerido" type="text" placeholder="Costo del servicio" value="<?php echo ($estacionamiento["TarifaHora"] == null ? "" : $estacionamiento["TarifaHora"]); ?>"/>
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="check_input">
					<div class="pull-left">
						<input id="input_checkbox3" class="chkServicios" name="input_checkbox3" type="checkbox" checked="" disabled="disabled" 
						<?php
						if ($estacionamiento["Servicios"] != null) {
							foreach ($estacionamiento["Servicios"] as $servicio) {
								if ($servicio["idServicio"] == 3) {
									echo 'checked="true"';
									$costoReserva = $servicio["monto"];
								}
							}
						} else {
							$costoReserva = null;
						}
						?>
						/><label disabled="disabled" for=""><span disabled="disabled" class="checkbox" ></span>Reserva</label>
					</div>
					<div class="pull-right">
						<input id="txtCosto_apartado" disabled="disabled" <?php /*echo (isset($costoReserva) ? "" : "disabled"); por el momento siempre está deshabilitado */ ?> type="text" placeholder="Costo del servicio" value="<?php echo "0"; /* (isset($costoReserva) ? $costoReserva : ""); por el momento el monto está fijo en cero */ ?>"/>
					</div>
				</div>
			</div>

			<!--Pension y pension compartida ................................. -->
			<div class="col-md-6">
				<div class="check_input">
					<div class="pull-left">
						<input id="input_checkbox1" class="chkServicios" name="input_checkbox1" type="checkbox" onclick="agregarPensiones()"
						<?php
						if ($estacionamiento["Servicios"] != null) {
							foreach ($estacionamiento["Servicios"] as $servicio) {
								if ($servicio["idServicio"] == 1 || $servicio["idServicio"] == 2) {
									echo 'checked="true"';
								}
							}
						} else {
							$costoPension = null;
						}
						?>
						/>
						<label id="label_checkbox1" class="servicio" for="input_checkbox1"><span class="checkbox" id="<?php echo $estacionamiento == null ? "span_checkbox1" : "" ?>" ></span>Pensiones</label>
					</div>
				</div>
			</div>

		</div><!-- end row-->


		<!--Configuracion de pensiones ....................................... -->
		<div id="divPensiones" class="row">

			<div class="row" style="text-align: center; background-color: #414042; margin-bottom: 8px;  margin-left: 8px; margin-right: 8px;">
				<label>Configuración de pensiones</label>
			</div>

			<div class="col-md-12" style="margin-bottom: 25px;">
				<div class="dataTables_wrapper no-footer">
					<table id="tblConfigurarPensiones" >
						<thead style="background-color: #414042;"> 
							<tr>
								<th colspan="1" style="margin-right: 8px; border-right-width: 8px; border-right-style: solid;border-right-color: #090a19; width: 15%;">Periodo<br>en meses</th>
								<th colspan="3" style="text-align: center; margin-right: 8px; border-right-width: 8px; border-right-style: solid; border-right-color: #090a19; width: 40%;">
									<input id="input_checkbox7" checked="checked" type="checkbox" name="input_checkbox7" onchange="activarPensionNormal()" />
									<label for="input_checkbox7" >
										<span class="checkbox" id="<?php echo $estacionamiento == null ? "span_checkbox7" : "" ?>" ></span>Pensión
									</label>
								</th>
								<th colspan="3" style="text-align: center; width: 40%;">
									<input id="input_checkbox8" type="checkbox" name="input_checkbox8" onchange="pensionCompartida()"
									<?php
									if ($estacionamiento["Servicios"] != null) {
										foreach ($estacionamiento["Servicios"] as $servicio) {
											if ($servicio["idServicio"] == 2) {
												echo 'checked="true"';
											}
										}
									}
									?>
									/><label for="input_checkbox8"><span class="checkbox" id="<?php echo $estacionamiento == null ? "span_checkbox8" : "" ?>" ></span>Pensión compartida</label></th>
								<th colspan="1"></th>
							</tr>

							<tr>
								<th style="width: 72px; margin-right: 8px; border-right-width: 8px; border-right-style: solid;border-right-color: #090a19;"></th>
								<th>Día</th>
								<th>Noche</th>
								<th style="margin-right: 8px;border-right-width: 8px;border-right-style: solid;border-right-color: #090a19;">24 horas</th>
								<th>Día</th>
								<th>Noche</th>
								<th>24 horas</th>
								<th style="width: 72px;"></th>
							</tr>
						</thead>
						<tbody>
							<?php if ($servicioPensiones == false) { ?>
								<tr style="height: 40px; ">
									<td style="margin-right: 8px; border-right-width: 8px; border-right-style: solid;border-right-color: #090a19;">
										<select class="selPeriodo">
											<option value="-1">Periodo</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
											<option value="5">5</option>
											<option value="6">6</option>
											<option value="7">7</option>
											<option value="8">8</option>
											<option value="9">9</option>
											<option value="10">10</option>
											<option value="11">11</option>
											<option value="12">12</option>
										</select>
									</td>
									<td><input type="text" placeholder="MXN" class="pensionNormal dia" /></td>
									<td><input type="text" placeholder="MXN" class="pensionNormal noche"/></td>
									<td style="margin-right: 8px; border-right-width: 8px; border-right-style: solid; border-right-color: #090a19;"><input type="text" placeholder="MXN" class="pensionNormal todoDia"/></td>
									<td><input type="text" placeholder="MXN" class="pensionCompartida diac" disabled="disabled"/></td>
									<td><input type="text" placeholder="MXN" class="pensionCompartida nochec" disabled="disabled"/></td>
									<td><input type="text" placeholder="MXN" class="pensionCompartida todoDiac" disabled="disabled"/></td>
									<td style="background-color: rgba(65,63,65,0.60);"></td>
								</tr>
								<?php
							} else {
								if ($estacionamiento["Pensiones"] != null) {
									$numeroPeriodo = 0;
									foreach ($estacionamiento["Pensiones"] as $p) {
										$numeroPeriodo += 1;
										echo '<tr id="tr' . $p["periodo"] . '" style="height: 40px;" >
									<td style="margin-right: 8px;border-right-width: 8px;border-right-style: solid;border-right-color: #090a19;">
										<select id="' . $p["periodo"] . '" class="selPeriodo" data-valida="requerido">
											<option value="-1">Periodo</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
											<option value="5">5</option>
											<option value="6">6</option>
											<option value="7">7</option>
											<option value="8">8</option>
											<option value="9">9</option>
											<option value="10">10</option>
											<option value="11">11</option>
											<option value="12">12</option>
										</select>
									</td>
									<td><input type="text" placeholder="MXN" class="pensionNormal dia" value="' . $p["dia"] . '" /></td>
									<td><input type="text" placeholder="MXN" class="pensionNormal noche" value="' . $p["noche"] . '"/></td>
									<td style="margin-right: 8px;border-right-width: 8px;border-right-style: solid;border-right-color: #090a19;"><input type="text" placeholder="MXN" class="pensionNormal todoDia" value="' . $p["todoDia"] . '"/></td>
									<td><input type="text" placeholder="MXN" class="pensionCompartida diac" disabled="disabled" value="' . $p["diaCompartida"] . '"/></td>
									<td><input type="text" placeholder="MXN" class="pensionCompartida nochec" disabled="disabled" value="' . $p["nocheCompartida"] . '"/></td>
									<td><input type="text" placeholder="MXN" class="pensionCompartida todoDiac" disabled="disabled" value="' . $p["todoDiaCompartida"] . '"/></td>

									<td style="text-align: center; background-color: rgba(65,63,65,0.60);">';
										if ($numeroPeriodo != 1 && $estacionamiento == null) {
											echo '<div id="btnQuitarPension' . $numeroPeriodo . '" class="boton borrar" style="margin: 0px; "></div>';
										}
										echo'</td>
									</tr>';

										echo '<script>
											
												jQuery("#btnQuitarPension" + "' . $numeroPeriodo . '").click(function () {
														jQuery(this).parent().parent().remove();

														periodosPensionesRestantes = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];
														periodosPensionesSeleccionados = [];
														var periodoSelecionado = jQuery(this).val();
														jQuery(this).attr("id", periodoSelecionado);

														jQuery(".selPeriodo").each(function () {
															if (jQuery(this).val() != "-1") {
																periodosPensionesSeleccionados.push(jQuery(this).attr("id"));
															}
														});

														if (periodoSelecionado != "-1") {
															jQuery.each(periodosPensionesSeleccionados, function (key, value) {
																var index = periodosPensionesRestantes.indexOf(value);
																periodosPensionesRestantes.splice(index, 1);
															});
														}
														setearListaPeriodos();
													});
										</script>';

										echo '<script>jQuery("#"+' . $p["periodo"] . ').val(' . $p["periodo"] . ')</script>';
									}
								} else {
									?>
									<tr style="height: 40px; ">
										<td>
											<select class="selPeriodo">
												<option value="-1">Periodo</option>
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
												<option value="5">5</option>
												<option value="6">6</option>
												<option value="7">7</option>
												<option value="8">8</option>
												<option value="9">9</option>
												<option value="10">10</option>
												<option value="11">11</option>
												<option value="12">12</option>
											</select>
										</td>
										<td><input type="text" placeholder="MXN" class="pensionNormal dia"  /></td>
										<td><input type="text" placeholder="MXN" class="pensionNormal noche"/></td>
										<td style="margin-right: 8px;border-right-width: 8px;border-right-style: solid;border-right-color: #090a19;"><input type="text" placeholder="MXN" class="pensionNormal todoDia"/></td>
										<td><input type="text" placeholder="MXN" class="pensionCompartida diac" disabled="disabled"/></td>
										<td><input type="text" placeholder="MXN" class="pensionCompartida nochec" disabled="disabled"/></td>
										<td><input type="text" placeholder="MXN" class="pensionCompartida todoDiac" disabled="disabled"/></td>
										<td style="background-color: rgba(65,63,65,0.60);"></td>
									</tr>
									<?php
								}
							}
							?>
						</tbody>

						
						<tfoot>
						
							<tr>
								<td colspan="8" style="background-color:#00BFD6; height: 16px; text-align: center;">
									<?php 
									if((!isset($_GET['idestacionamiento']))){ 
										echo '<div class="B_Plus_N" style=" position: absolute; left: 50%; margin-top: -8px;" onclick="agregarPeriodoPension()" ></div>';
									}
									?>
								</td>
							</tr>
							
						</tfoot>
						
					</table>
				</div>
			</div>

<!--			<div class="col-md-12" style="display: block;  overflow: hidden; white-space: nowrap; height: 40px;  margin-bottom: 9px; margin-right: 8px; width: 100%;">
				<div style="position: relative;display: inline-block; width: 100%; background-color: rgba(65,63,65,0.60);" >
					<label style="padding-left: 16px; width: 50%; background-color: rgb(65,63,60);">Amonestación:</label><input class="inputPensionActivado" style="width: 50%;background-color: rgb(65,63,60);" id="txtAmonestacion" type="text" placeholder="MXN" value="<?php echo $amonestacionPension; ?>" />
				</div>
			</div>-->
			
			
		</div>

		<!--Terminos y condiciones ....................................... -->
		<div class="row" style="height:256px;">
			<div class="col-md-12">
				<div class="requerido">
					<textarea id="txtCondiciones" class="input_width100" rows="17" cols="40" data-valida="requerido" placeholder="Términos y condiciones"><?php echo ($estacionamiento == null ? "" : $estacionamiento["Terminos"]); ?></textarea>
					<div class="marca"></div>
				</div>
			</div>
		</div><!-- end row-->

		<div class="row" style="margin: 12px 0px;">
			<div class="col-md-6">
				<div id="cancelar" class="pull-right">
					<button id="btn_cancelar_estacionamiento" type="button" onclick="cancelarAltaEstacionamiento();" class="BB_Fantasma_N">CANCELAR</button>
				</div>
			</div>
			<div class="col-md-6">
				<div id="guardar" class="pull-left">
					<?php if (!isset($_GET['idestacionamiento'])) { ?>
						<button id="btn_agregar_estacionamiento" type="button" onclick="agregarEstacionamiento('A');" class="B_Regular_N">AGREGAR</button>
					<?php } else { ?>
						<button id="btn_modificar_estacionamiento" type="button" onclick="agregarEstacionamiento('E');" class="B_Regular_N">MODIFICAR</button>
					<?php } ?>
				</div>
			</div>
		</div>

	</div><!-- end .formDark-->

</div><!-- end .centerme-->

