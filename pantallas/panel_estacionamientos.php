<?php
global $user;
$sql_estacionamientos = getEstacionamientosPorCliente($user->uid);
?>

<script src="pantallas/js/panel_estacionamientos.js"></script>

<div class="" style="margin-bottom:32px;">
	<div class="caja_reportes">
		<!--Boton ______________________________________________________________ -->
		<div class="row">
			<div class="col-md-12" style="text-align: center; padding: 16px 0px">
				<button type="button" class="B_Regular_N" onclick="altaEstacionamiento()">NUEVO ESTACIONAMIENTO</button>
			</div>
		</div><!-- end row-->

		<!--Tabla ______________________________________________________________ -->
		<div class="row">

			<div class="col-md-12">
				<table id="tblEstacionamientos">
					<thead>
						<tr>
							<th></th>
							<th>Nombre de Estacionamiento</th>
							<th>Zona</th>
							<th>Servicios</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach ($sql_estacionamientos as $estacionamiento) {
							echo '<tr>';
							echo '	<td><div class="icono empresa"></div></td>';
							echo '	<td>' . $estacionamiento->nombre . '</td>';
							echo '	<td>' . $estacionamiento->zona . '</td>';
							echo '	<td>';
						
//							if ($estacionamiento->tarifa_hora != null) {
//								echo '<div style="display: inline-block;" class="s_pension" href="#"></div>';
//							}

							foreach ($estacionamiento->servicios as $servicio) {
								
								if ($servicio->id_servicio == 5) {
									echo '<div style="display: inline-block;" class="s_estacionamiento" href="#"></div>';
								}
								
								if ($servicio->id_servicio == 1) {
									echo '<div style="display: inline-block;" class="s_pension" href="#"></div>';
								}

								if ($servicio->id_servicio == 2) {
									echo '<div style="display: inline-block;" class="s_pension_compartida" href="#"></div>';
								}

								if ($servicio->id_servicio == 3) {
									echo '<div style="display: inline-block;" class="s_reserva" href="#"></div>';
								}

								if ($servicio->id_servicio == 4) {
									echo '<div style="display: inline-block;" class="s_valet" href="#"></div>';
								}
							}

							echo '</td>';
							echo '	<td><div class="boton detalle" onclick="detalleEstacionamiento(' . $estacionamiento->id_estacionamiento . ')"></div>
									<div class="boton editar" onclick="editarEstacionamiento(' . $estacionamiento->id_estacionamiento . ')" ></div>
									<div class="boton borrar" onclick="eliminarEstacionamiento(' . $estacionamiento->id_estacionamiento . ')"></div>
								</td>';
							echo '</tr>';
						}
						?>
					</tbody>
				</table>
			</div><!-- end col -->

		</div><!-- end row -->

	</div><!-- end centerme-->
</div><!-- end #home-->
