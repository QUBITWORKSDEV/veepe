<script src="pantallas/js/panel_soporte.js"></script>

<div class="" style="margin-bottom:32px;">
	<div class="caja_reportes">
		<!--Boton ______________________________________________________________ -->
		<div class="row">
			<div class="col-md-12" style="text-align: center; padding: 16px 0px">
				<input id="txtVeepeid" type="text" style="width: 30%;" placeholder="VEEPEID">
				<br>
				<button type="button" class="B_Regular_N" onclick="getDatosUsuario()">BUSCAR USUARIO</button>
			</div>
		</div><!-- end row-->

		<!--Tabla ______________________________________________________________ -->
		<div class="row">

			<ul class="nav nav-tabs" id="myTab" role="tablist">

				<li class="nav-item">
					<a class="nav-link active" id="perfil-tab" data-toggle="tab" href="#perfil" role="tab" aria-controls="perfil" aria-expanded="true">Perfil</a>
				</li>

				<li class="nav-item">
					<a class="nav-link" id="operaciones-tab" data-toggle="tab" href="#operaciones" role="tab" aria-controls="operaciones">Operaciones</a>
				</li>

				<li class="nav-item">
					<a class="nav-link" id="pensiones-tab" data-toggle="tab" href="#pensiones" role="tab" aria-controls="pensiones">Pensiones</a>
				</li>

				<li class="nav-item">
					<a class="nav-link" id="metodos-tab" data-toggle="tab" href="#metodos" role="tab" aria-controls="metodos">Métodos de pago</a>
				</li>

			</ul>

			<div class="tab-content" id="myTabContent">

				<div class="tab-pane fade" id="perfil" role="tabpanel" aria-labelledby="perfil-tab">

				</div>

				<div class="tab-pane fade" id="operaciones" role="tabpanel" aria-labelledby="operaciones-tab">

				</div>

				<div class="tab-pane fade" id="pensiones" role="tabpanel" aria-labelledby="pensiones-tab">

				</div>

				<div class="tab-pane fade" id="metodos" role="tabpanel" aria-labelledby="metodos-tab">

				</div>

			</div>

		</div><!-- end row -->

	</div><!-- end centerme-->
	
</div><!-- end #home-->
