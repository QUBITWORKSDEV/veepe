<?php
if (isset($_GET['idestacionamiento'])) {
	$idEstacionamiento = $_GET['idestacionamiento'];
	$estacionamiento = getEstacionamiento($idEstacionamiento);
	/*$servicios = db_query("select * from f_r_estacionamientos_servicios es
							inner join f_c_servicios s on s.id_servicio = es.id_servicio
							where es.id_estacionamiento=" . $idEstacionamiento)->fetchAll();*/
	//$pensiones = getPensionesEstacionamiento($idEstacionamiento);
	$amonestacionPension = getAmonestacionPension($idEstacionamiento);
}
?>

<script>
	jQuery(document).ready(function ()
	{
		jQuery(".checkbox").css("cursor", "auto");
	});

	jQuery("#menuLateral").hide();
	btn_regresar("./estacionamientos");

	function editar_estacionamiento(idestacionamiento) {
		location.href = './alta_estacionamiento?idestacionamiento=' + idestacionamiento;
	}
</script>

<style>
	#tblPensiones{
		border:none;
		width: 100%;
		text-align: center;
	}

	.espacio{
		border-right-style:  solid;
		border-right-width: 20px;
		border-left-width: 10px;
		border-right-color:transparent;
		border-spacing: 5px 0px;
	}

	#tblPensiones > thead > tr > th{
		background-color: #414042;
		text-align: center;
		font-family: 'Open Sans', sans-serif;
		font-size: 12px;
		color: #FFFFFF;
		letter-spacing: 0px;
		line-height: 16px;
		height: 40px;
	}

	#tblPensiones > tbody > tr > td {
		background-color: #414042;
		//border-collapse: inherit;
	}
</style>

<div class="home" >
	<div class="row" style="margin-top: 24px; margin-bottom: 100px;">

		<!--Perfil _____________________________________________________________ -->
		<div class="row" style="text-align: center;">
			<!--Foto-->
			<div class="row">
				<div class="img_estacionamiento_h"></div>
			</div><!-- end row-->

			<!--Nombre del usuario-->
			<div class="row">
				<h1 class="white"><?php echo $estacionamiento["Nombre"]; ?></h1>
			</div><!-- end row-->
		</div><!-- end row-->

		<!--Datos del usuario __________________________________________________ -->
		<div class="row caja_perfil" style="margin-top: 24px;">

			<div class="row" style="margin-top: 16px;">
				<!--Nombre del estacionamiento ..................................... -->
				<div class="col-md-12">
					<label>Nombre Estacionamiento</label><span class="body3_B"><?php echo $estacionamiento["Nombre"]; ?></span>
				</div>

				<!--Dirección del estacionamiento .................................. -->
				<div class="col-md-12" style="margin-bottom:16px;">
					<label style="padding-bottom: 0px;">Dirección</label>
					<span class="body3_B">
						<?php echo $estacionamiento["Direccion"]; ?>
					</span>
				</div>

				<div class="col-md-12" style="margin-bottom:16px;">
					<label style="padding-bottom: 0px;">Zona</label>
					<span class="body3_B">
						<?php echo $estacionamiento["Zona"]; ?>
					</span>
				</div>

				<!--Latitud y longitud ............................................. -->
				<div class="col-md-6">
					<label>Latitud</label><span class="body3_B"><?php echo $estacionamiento["Latitud"]; ?></span>
				</div>
				<div class="col-md-6">
					<label>Longitud</label><span class="body3_B"><?php echo $estacionamiento["Longitud"]; ?></span>
				</div>

				<!--Latitud y longitud ............................................. -->
				<div class="col-md-6">
					<label>Capacidad</label><span class="body3_B"><?php echo $estacionamiento["Capacidad"]; ?></span>
				</div>
				<div class="col-md-6">
					<label>Pensiones contratadas</label><span class="body3_B"><?php echo $estacionamiento["PensionesContratadas"]; ?></span>
				</div>

				<!--Horario de apertura y cierre ................................... -->
				<?php $horarios = explode(" - ", $estacionamiento["horarioServicio"]); ?>
				<div class="col-md-6">
					<label>Horario de apertura</label><span class="body3_B"><?php echo $horarios[0]; ?></span>
				</div>
				<div class="col-md-6">
					<label>Horario de cierre</label><span class="body3_B"><?php echo $horarios[1]; ?></span>
				</div>

				<!--Servicios ______________________________________________________ -->
				<div class="col-md-12" style="text-align: center">
					<h3 class="white">Servicios</h3>
				</div>

				<!--Checkboxes de servicios ........................................ -->
				<div class="col-md-6">
					<input type="checkbox" checked /><label><span class="checkbox"></span>Tarifa estacionamiento</label>&nbsp;&nbsp;&nbsp;
					<label><?php echo $estacionamiento["TarifaHora"]; ?></label>
				</div>

				<?php
				if ($estacionamiento["Servicios"] != null) {
					foreach ($estacionamiento["Servicios"] as $servicio) {
						if ($servicio["idServicio"] != "5") {
							if ($servicio["idServicio"] == "1" || $servicio["idServicio"] == "2") { 
								?>
								<div class="col-md-6">
									<input type="checkbox" checked /><label><span class="checkbox"></span><?php echo $servicio["nombre"]; ?></label>&nbsp;&nbsp;&nbsp;
									<label></label>
								</div>
								<?php
							} else {
								?>
								<div class="col-md-6">
									<input type="checkbox" checked /><label><span class="checkbox"></span><?php echo $servicio["nombre"]; ?></label>&nbsp;&nbsp;&nbsp;
									<label><?php echo $servicio["monto"]; ?></label>
								</div>
								<?php
							}
						}
					}
				}
				?>

				<!--Pensiones.................................. -->
				<?php if ($estacionamiento["Pensiones"] != null) { ?>
					<div id="divPensiones" class="col-md-12" style="margin-bottom:16px; margin-top: 16px;">
						<table id="tblPensiones">
							<thead>
								<tr>
									<th style="width: 72px;">Periodo<br>en meses</th>
									<th style="background-color: transparent; width: 10px;"></th>
									<th colspan="3" class="espacio">Pensión</th>
									<th style="background-color: transparent; width: 10px;"></th>
									<th colspan="3" >Pensión compartida</th>
								</tr>
								<tr>
									<th style="width: 72px;" class="espacio"></th>
									<th style="background-color: transparent; width: 10px;"></th>
									<th>Día</th>
									<th>Noche</th>
									<th class="espacio">24 horas</th>
									<th style="background-color: transparent; width: 10px;"></th>
									<th>Día</th>
									<th>Noche</th>
									<th>24 horas</th>
								</tr>
							</thead>
							<tbody>
								<?php
								foreach ($estacionamiento["Pensiones"] as $p) {
									echo '<tr id="tr' . $p["periodo"] . '" style="height: 40px;">
									<td class="espacio">
										<label>' . $p["periodo"] . '</label>
									</td>
									<td style="background-color: transparent; width: 10px;"></td>
									<td ><label>' . $p["dia"] . '</label></td>
									<td><label>' . $p["noche"] . '</label></td>
									<td class="espacio" ><label >' . $p["todoDia"] . '</label></td>
									<td style="background-color: transparent; width: 10px;"></td>
									<td ><label>' . $p["diaCompartida"] . '</label></td>
									<td><label>' . $p["nocheCompartida"] . '</label></td>
									<td><label>' . $p["todoDiaCompartida"] . '</label></td>
									</tr>';
								}
								?>
							</tbody>
							<tfoot></tfoot>
						</table>
					</div>


<!--					<div class="col-md-12" style="margin-bottom:16px;">
						<label style="padding-bottom: 0px;">Amonestación</label>
						<span class="body3_B"><?php echo $amonestacionPension; ?></span>
					</div>-->

				<?php } ?>

				<!--Términos y condiciones.................................. -->
				<div class="col-md-12" style="margin-bottom:16px;">
					<label style="padding-bottom: 0px;">Términos y condiciones</label>
					<span class="body3_B">
						<?php echo $estacionamiento["Terminos"]; ?>
					</span>
				</div>

			</div><!-- end row-->

			<!--Boton	___________________________________________________________ -->
			<div class="row">
				<!--Boton-->
				<div class="col-md-12" style="text-align: center; padding: 12px 0px 24px 0px;">
					<a href="javascript:editar_estacionamiento('<?php echo $estacionamiento["idEstacionamiento"]; ?>')" class="B_Regular_N" >EDITAR</a>
				</div>
			</div><!-- end row-->

		</div><!-- end row-->

	</div><!-- end centerme-->

</div><!-- end #home-->