<script src="pantallas/usuario_final/js/uf_inicio.js" type="text/javascript"></script>

<?php
global $user;
$uid = $user->uid;
$array = getUsuario(getVeepeidById($uid));
?>

<style>
	body footer {
		display: none;
	}

	.selectorTabs {
		width: 1024px;
		display: inline-block;
		background-color: #00BFD6;
		border-radius: 25px;
		padding: 2px;
		position: relative;
		height: 40px;
		border: 2px solid @diver-color;
	} 

	.selectorTabs .selector {
		position: absolute;
        width: 222px;
        left: 0%;
        height: 32px;
        background-color: #FFFFFF;
        border-radius: 15px;
        transition: all .5s;
	} 

	.selectorTabs .opciones {
		position: absolute;
		line-height: 30px;
	} 

	.selectorTabs .opciones .opcion{
		display: inline-block;
		width: 337px; //calc((@anchoSelectorLogin - (3 * @paddingSelectorLogin)) / 3);
		line-height: 24px;
		color: #FFFFFF;
		font-size: 10px;
		cursor: pointer;
		text-align: center;
	} // selectorLogin

	//-----------------------------


	#cslidehome-slides h2 {
        margin-bottom: 10px;
        font-weight: 700;
    }

    /*=Slides
    ----------------------------------------------- */
    .cslidehome-slides-master {
        overflow: hidden;
        margin-bottom: 60px;
        width :1024px;
    }

    .cslidehome-slides-master:last-child {
        margin-bottom: 0;
    }

    .cslidehome-slides-container {
        visibility: hidden; 
    }

    .cslidehome-slide {
        float: left;
        padding: 30px;
        visibility: hidden;
    }

    .cslidehome-slide h2,
    .cslidehome-slide p {
        color: #fff; 
    }





</style>

<div class="home">

	<!--	<div class="centerme">-->

	<div class="row" style="text-align: center; padding: 8px 0px;">


		<!--Foto-->
		<div class="row" style="margin-top: 64px;">
			<div class="img_usuario_i"></div>
		</div><!-- end row-->

		<!--Nombre del usuario-->
		<div class="row">
			<h1 class="white">Nancy Potter</h1>
		</div><!-- end row-->

		<!--VeepeID-->
		<div class="row">
			<span><h4 class="white">VEEPE ID: </h4></span>
			<span><h4 class="blue">
					VEEPEID
				</h4></span>
		</div><!-- end row-->

		<!--Correo-->
		<div class="row">
			<span class="body3_A">dueno@veepe.com</span>
		</div><!-- end row-->


		<br>

		<div class="row">

			<div style="background-color:transparent; padding: 0px 0px 4px 0px; height: 48px;">  
				<div class="selectorTabs" style="">
					<div class="selector" style="width: 33.3%;"></div>
					<div class="opciones">
						<div id="opcionTabPerfil" class="opcion perfil" onclick="cambiarTabsHomePu(0)">PERFIL</div>
						<div id="opcionTabListaAutomoviles"  class="opcion lista_automoviles" onclick="cambiarTabsHomePu(1)" style="color:#0AB1B0">LISTA DE AUTOMÓVILES</div>
						<div id="opcionTabFormasPago"  class="opcion formas_pago" onclick="cambiarTabsHomePu(2)" style="color:#0AB1B0">FORMAS DE PAGO</div>
					</div>
				</div>
			</div>


			<div id="main" style="width: 1024px; margin: 0 auto; padding: 0 0px;">
				<section id="cslidehome-slides" class="cslidehome-slides-master clearfix">

					<div class="cslidehome-slides-container clearfix">

						<div id="slide0" class="cslidehome-slide" style=" visibility: visible;">
							<div  id="perfil" style="margin: 0 auto; width: 920px">
								<?php include_once("uf_perfil.php"); ?>
							</div>
						</div>

						<div id="slide1" class="cslidehome-slide">
							<div id="lista_automoviles" style="margin: 0 auto; width: 920px">
								<?php include_once("uf_lista_automoviles.php"); ?>
							</div> 
						</div>

						<div id="slide2" class="cslidehome-slide">
							<div id="formas_pago" style="margin: 0 auto; width: 920px">
								<?php include_once("uf_formas_pago.php"); ?>
							</div> 
						</div>

					</div>
				</section>

			</div>

		</div>



	</div>

	<!--	</div>-->






	<!--  <div class="centerme">
	
		Perfil ____________________________________________________________________________________ 
		<div class="row" style="text-align: center; padding: 8px 0px;">
	
		  Foto
		  <div class="row">
			<div class="img_usuario_i"></div>
		  </div> end row
	
		  Nombre del usuario
		  <div class="row">
			<h1 class="white"><?php echo $array["nombre"] . " " . $array["apellidoPaterno"] . " " . $array["apellidoMaterno"]; ?></h1>
		  </div> end row
	
		  VeepeID
		  <div class="row">
			<span><h4 class="white">VEEPE ID: </h4></span>
			<span><h4 class="blue">
	<?php
	$str = $array["VeepeId"];
	$cad = strtoupper($str);
	echo $cad;
	?>
			</h4></span>
		  </div> end row
	
		  Correo
		  <div class="row">
			  <span class="body3_A"><?php echo $array["Correo"]; ?></span>
		  </div> end row
	
		</div> end row
	
		Datos del usuario _________________________________________________________________________ 
		<div class="row caja_perfil" style="margin: 52px 0px;" >
		  Nombre y correo .................................................. 
		  <div class="row">
			<div class="col-md-6">
			  <label>Nombre:</label><span class="body3_B"><?php echo $array["nombre"] . " " . $array["apellidoPaterno"] . " " . $array["apellidoMaterno"]; ?></span>
			</div>
			<div class="col-md-6">
			  <label>Correo:</label><span class="body3_B"><?php echo $array["Correo"]; ?></span>
			</div>
		  </div> end row
	
		  Celular .......................................................... 
		  <div class="row">
			<div class="col-md-6">
			  <label>Celular:</label><span class="body3_B"><?php echo $array["Celular"]; ?></span>
			</div>
		  </div> end row
	
		</div> end row
	
		Boton y Leyenda ___________________________________________________________________________ 
		<div class="row">
		  Boton
		  <div class="col-md-12" style="text-align: center; padding: 12px 0px 24px 0px;">
			<a href="uf_editar" class="B_Regular_N">EDITAR</a>
		  </div>
	
		  Leyenda
		  <div class="col-md-12" style="text-align: center;">
			<span class="body3">Para cambiar la contraseña u otro dato hacer click en el botón de editar</span>
		  </div>
		</div> end row
	
	  </div> end centerme-->
</div><!-- end #home-->
