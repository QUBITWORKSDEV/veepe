<?php
	global $user;
	$uid = $user -> uid;
	$array = getUsuario(getVeepeidById($uid));
?>

<style>
	body #navbar,
	section > h1,
	section .region .region-content ,
	body footer,
	body header {
	  display: none;
	}
</style>

<script src="pantallas/usuario_final/js/uf_editar.js"></script>

<div class="row">
  <div class="caja_registro centerme">

		<div class="row" style="margin-bottom: 20px;">
			<center>
				<!--Foto-->
				<div class="row">
					<div class="img_usuario_n"></div>
				</div><!-- end row-->

				<!--Nombre del usuario-->
				<div class="row">
					<h1 class="white"><?php echo $array["nombre"] . " " . $array["apellidoPaterno"] . " " . $array["apellidoMaterno"]; ?></h1>
				</div><!-- end row-->

				<!--VeepeID-->
				<div class="row">
					<span><h4 class="white" style="padding: 8px 0px; margin: 0; display: inline-block">VEEPE ID: </h4></span>
					<span><h4 class="blue" style="padding: 8px 0px; margin: 0; display: inline-block">
						<?php  $str = $array["VeepeId"]; $cad = strtoupper($str); echo $cad; ?>
					</h4></span>
				</div><!-- end row-->

				<!--Correo-->
	      <div class="row">
	          <span class="body3_A"><?php echo $array["Correo"]; ?></span>
	      </div><!-- end row-->
			</center>
		</div><!-- end row-->

    <!--Fomulario "formaDark" _____________________________________________________________________ -->
    <form class="formaDark" action="uf_inicio">

			<!--Registro ___________________________________________________________________________________ -->
			<div class="row">

	        <!--Nombre y apellido paterno .................................... -->
	        <div class="row">
	          <div class="col-md-6 row_validador">
							<div class="requerido">
			    			<input id="txtNombre" type="text" placeholder="Nombre" value="<?php echo $array["nombre"]; ?>" data-valida="requerido"/>
								<div class="marca"></div>
			    		</div>
	          </div>
	          <div class="col-md-6 row_validador">
							<div class="requerido">
			    			<input id="txtApellidoPaterno" type="text" placeholder="Apellido paterno" value="<?php echo $array["apellidoPaterno"]; ?>" data-valida="requerido"/>
								<div class="marca"></div>
			    		</div>
	          </div>
	        </div><!-- end row-->

	        <!--Apellido materno ............................................. -->
					<div class="row">
	          <div class="col-md-6 row_validador">
							<div class="requerido">
			    			<input id="txtApellidoMaterno" type="text" placeholder="Apellido materno" value="<?php echo $array["apellidoMaterno"]; ?>" data-valida="requerido"/>
								<div class="marca"></div>
			    		</div>
	          </div>
	          <div class="col-md-6 row_validador">
							<div class="requerido">
			    			<input id="celular" type="text"  maxlength="10" placeholder="Celular" value="<?php echo $array["Celular"]; ?>" data-valida="requerido,numero"/>
								<div class="marca"></div>
			    		</div>
	          </div>
	        </div><!-- end row-->

					<!--Antiguo password, nuevo password y confirmar password ........ -->
	        <div class="row">
						<div class="col-md-3 row_registro">
							<div class="requerido">
								<input id="old_password" type="password" placeholder="Contraseña" data-valida="requerido">
								<div class="marca"></div>
							</div>
						</div><!-- end col-md-3-->
						<div class="col-md-3 row_registro">
							<div class="requerido">
								<input id="password" type="password" placeholder="Nueva Contraseña">
							</div>
						</div><!-- end col-md-3-->
						<div class="col-md-3 row_registro">
							<div class="requerido">
								<input id="confirm_password" type="password" placeholder="Confirmar Contraseña">
							</div>
						</div><!-- end col-md-3-->
	        </div><!-- end row-->

					<!--Antiguo nip, nuevo nip y confirmar nip ....................... -->
	        <div class="row">
						<div class="col-md-3 row_registro">
							<div class="requerido">
								<input id="old_nip" type="text" maxlength="4" placeholder="NIP actual (4 dígitos)" data-valida="requerido,numero"/>
								<div class="marca"></div>
							</div>
						</div><!-- end col-md-3-->
						<div class="col-md-3 row_registro">
							<div class="requerido">
								<input id="nip" type="text" maxlength="4" placeholder="Nuevo NIP (4 dígitos)"/>
							</div>
						</div><!-- end col-md-3-->
						<div class="col-md-3 row_registro">
							<div class="requerido">
								<input id="confirm_nip" type="text" maxlength="4" placeholder="Confirmar NIP (4 dígitos)"/>
							</div>
						</div><!-- end col-md-3-->
	        </div><!-- end row-->
	    </div><!-- end row-->

      <!--Botón Registrar .................................................. -->
      <div class="row" style="margin: 24px 0px;">
				<div class="col-md-6">
					<div class="pull-right cancelar">
							<a class="BB_Fantasma_N" href="uf_inicio">CANCELAR</a>
					</div>
				</div>
				<div class="col-md-6">
					<div class="pull-left guardar">
							<button id="editar" class="B_Regular_N" type="button">GUARDAR</button>
					</div>
				</div>
      </div><!-- end row-->

    </form><!-- end .formDark-->
  </div><!-- end .centerme-->
</div><!-- end row-->
