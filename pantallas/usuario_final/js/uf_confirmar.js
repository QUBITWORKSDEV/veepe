"use strict";

// _____________________________________________________________________________________________________________ VARIABLES GLOBALES

// _____________________________________________________________________________________________________________ FUNCIONES GLOBALES

// _____________________________________________________________________________________________________________ JQUERY
jQuery(document).ready(function ()
{
	// _____________________________________________________________________ configuraciones
	jQuery('.formaDark.confirmar').formulario({
		botonEnviar:	jQuery('button#cmdCompletar'),
		parametrosCLA: {
			datos: {
				m:			'completarRegistro',
				token:		token,
				nombre:		'jQuery("#txtNombre").val()',
				apePaterno:	'jQuery("#txtApellidoPaterno").val()',
				apeMaterno:	'jQuery("#txtApellidoMaterno").val()',
				nip:		'jQuery("#txtNip").val()',
				celular:	'jQuery("#txtCelular").val()'
			},
			prefuncion: function(){
				jQuery('#cmdCompletar').html('COMPLETANDO...');
			},
			postfuncion: function(response){
				if ( response == true )
				{
					notificarUsuario('generico', '', 'Tu registro ha sido completado. Ya puedes iniciar sesión en la app Veepe usando tu correo y tu contraseña.', 'Aceptar', function(){
						// redirección a home
						window.location = '.';
					});
				}
				else
				{
					jQuery('#cmdCompletar').html('COMPLETAR REGISTRO');
					notificarUsuario('error', 'Error', 'Sucedió un error al completar tu registro. Por favor inténtalo más tarde.', 'Cerrar');
				}
			},
			delayInicial: 0.0
		}
	});

	// _____________________________________________________________________ entrypt
	jQuery('#txtNombre').focus();

});// fin del document.ready
