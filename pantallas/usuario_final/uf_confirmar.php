<?php
	global $user;
	if ( $user->uid )
	{
		// hay un usuario loggeado -> deslogear y redireccionar
		header("location:user/logout?destination=uf_confirmar?t=".$_GET["t"]);
	}
	else
	{
		// Lo primero es leer los parámetros para ver que sí existan
		$token = isset($_GET["t"]) ? $_GET["t"] : null;
	
		if ( $token==null )
		{
			// 2do: escribir mensaje de error
			echo "error";
			exit(0);
		}
		else
		{
			// obtenemos el registro para este token
			$rs = db_query("select * from f_dc_perfilusuario where token = '".$token."'");
			
			if ( $rs->rowCount() < 1 )
			{
				// 2do: mensaje de error -> no existe nada de ese token
				echo "No existe este perfil";
				exit(0);
			}
			else
			{
				$array = $rs->fetchAssoc();
			}
		}
	}
?>

<script src="pantallas/usuario_final/js/uf_confirmar.js"></script>

<?php
	if ( $token!=null ) {
		echo '<script>var token="'.$token.'";</script>';
?>

<div class="container">
	<div id="caja_registro" class="centerme" style="width: 800px; max-width: 100%">

		<div class="row" style="margin-bottom: 20px;">
			<center>
				<!--Foto-->
				<div class="row">
					<img id="imgUsuario" src="<?php echo drupal_get_path('theme', 'veepe'); ?>/img/botones_tabla/b_tabla_user_n.svg" width="104px">
				</div><!-- end row-->

				<!--Nombre del usuario-->
				<div class="row">
					<h1 class="white"><?php echo $array["nombre"] . " " . $array["apellido_paterno"] . " " . $array["apellido_materno"]; ?></h1>
				</div><!-- end row-->
			</center>
		</div><!-- end row-->

		<!--Fomulario "formaDark" _____________________________________________________________________ -->
		<form class="formaDark confirmar" novalidate="novalidate">

			<!--Registro ___________________________________________________________________________________ -->
			<!--Primera fila-->
			<div class="row">
				<div class="row_validador col-md-6">
					<div class="requerido">
						<input id="txtNombre" type="text" placeholder="Nombre" data-valida="requerido,texto" value="<?php echo $array["nombre"]; ?>" />
						<div class="marca"></div>
					</div>
				</div>
				
				<div class="row_validador col-md-6">
					<div class="requerido">
						<input id="txtApellidoPaterno" type="text" placeholder="Apellido paterno" data-valida="requerido,texto" value="<?php echo $array["apellido_paterno"]; ?>" />
						<div class="marca"></div>
					</div>
				</div>
			</div><!-- end row-->

			<!--Segunda fila-->
			<div class="row">
				<div class="row_validador col-md-6">
					<input id="txtApellidoMaterno" type="text" placeholder="Apellido materno" data-valida="texto" value="<?php echo $array["apellido_materno"]; ?>" />
				</div>
				<div class="row_validador col-md-6">
					<input type="text" disabled="disabled" value="<?php echo $array["nombre_usuario"]; ?>" >
				</div>
			</div><!-- end row-->

			<!--Tercera fila-->
			<div class="row">
				<div class="row_validador col-md-6">
					<input id="txtCelular" type="text" maxlength="10" placeholder="Celular" data-valida="" value="<?php echo $array["celular"]; ?>" />
				</div>
			</div><!-- end row-->

			<!--Quinta fila-->
			<div class="row">
				<!--<div id="rowRegistro" class="col-md-3">
					<div class="requerido">
						<input name="nip" type="text" maxlength="4" placeholder="NIP actual (4 dígitos)" data-valida="requerido,numero" />
						<div class="marca"></div>
					</div>
				</div><!-- end col-md-3-->
				<div class="row_validador col-md-6">
					<div class="requerido">
						<input id="txtNip" type="text" maxlength="4" placeholder="Nuevo NIP (4 dígitos)" data-valida="requerido,numero,{longitud|4|=}" />
						<div class="marca"></div>
					</div>
				</div><!-- end col-md-6-->
				<div class="row_validador col-md-6">
					<div class="requerido">
						<input id="txtNip2" type="text" maxlength="4" placeholder="Confirmar NIP (4 dígitos)" data-valida="requerido,numero,{longitud|4|=}" />
						<div class="marca"></div>
					</div>
				</div><!-- end col-md-6-->
			</div><!-- end row-->

			<!--Botón Registrar-->
			<div class="row" style="margin: 8px 0px 24px 0px;">
				<!--<div class="col-md-6">
					<div id="cancelar" class="pull-right">
						<button type="button" class="BB_Fantasma_N" name="cancelar">CANCELAR</button>
					</div>
				</div>-->
				<div class="col-md-12">
					<center><button class="B_Regular_N" type="button" id="cmdCompletar">COMPLETAR REGISTRO</button></center>
				</div>
			</div><!-- end row-->

			<div class="row body3" style="text-align: center;" >
				Se te enviará un correo con tu información.
			</div>

		</form><!-- end .formDark-->
	</div><!-- end .centerme-->
</div><!-- end row-->
<?php } ?>




