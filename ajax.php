<?php
	include_once("sites/default/settings.php");

	/* ----------------------------------------------------
			 Cargamos el bootstrap de Drupal
	   ---------------------------------------------------- */
	$currdir=getcwd();
	chdir($base_path);
	define('DRUPAL_ROOT', getcwd());
	require_once("./includes/bootstrap.inc");
	drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
	chdir($currdir);


	// Por seguridad, solo los usuarios registrados pueden hacer requests. Si no está loggeado, lo mandamos al home
	if ( !user_is_logged_in() && !drupal_is_front_page() )
	{
		header("location:./");
		exit(0);
	}

	/*
		Los parámetros deben ser:
		m - el nombre del método a ejecutar
		id - el parámetro a pasar a la función (por el momento sólo admite un parámetro)
	*/

	if ( $_SERVER['REQUEST_METHOD'] == "GET" )
		$data = $_GET;
	else if ( $_SERVER['REQUEST_METHOD'] == "POST" )
		$data = $_POST;
	else
	{
		// aquí deberíamos hacer una notifcación pesada
		return null;
	}

	if ( isset($data['m']) )
	{
		// cuando hay un parámetro 'id' estamos invocando una función de parámetro singular
		if ( isset($data['id']) )
		{
			$res = call_user_func($data['m'], $data['id']);
		}
		else
		{
			$m = $data['m'];
			$params = $data;
			unset($params['m']);	// le quitamos el nombre del parámetro
			if (isset($params['q']))
				unset($params['q']);	// le quitamos el nombre del parámetro

			if ( sizeof($params)==0 )
			{
				$res = call_user_func($m);
			}
			else
			{
				$res = call_user_func_array($m, $params);
			}
		}

		// en teoría ya obtuvimos los datos -> regresamos una respuesta
		echo json_encode($res);
	}
	else
	{
		// falta información necesaria -> no se puede procesar -> regresamos null
		echo 'error al ejecutar el método: '.print_r($data);
	}
