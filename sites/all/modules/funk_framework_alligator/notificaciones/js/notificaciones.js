
// Esta variable contiene el objeto jQuery donde se mostrarán las notificaciones
var _notificaciones_proto = jQuery('<div id="alerta" class="overlay modal" style="display:none;">'+
										'<div class="centerme">'+
											'<div class="row" style="margin-bottom: 24px">'+
												'<div class="icon"></div>'+
											'</div>'+
											'<div class="row">'+
												'<div class="title">'+
													'¡Upps! No sabemos que acaba de suceder...'+
												'</div>'+
												'<div class="body">'+
													'No sabemos qué es lo que acaba de suceder. Te agradecemos intentar la acción nuevamente.'+
												'</div>'+
											'</div>'+
											'<div class="row" style="text-align:center;">'+
												'<button type="button" id="cmdSi" class="B_Regular_N">CANCELAR</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+
												'<button type="button" id="cmdNo" class="B_Regular_N">ACEPTAR</button>'+
											'</div>'+
										'</div>'+
									'</div>');

/**
  * notificarUsuario - Envía una notificación al usuario especificado
  *
  * Las notificaciones pueden enviarse dentro del sistema o a través de correo. En ambos casos se
  * crea un registro del usuario que creó la notificación y todos sus contenidos.
  *
  * @param int $usuario El uid del usuario al que se le envía la notificación
  * @param int $tipoNotificacion El tipo de notificación que se envía: 1:correo, 3:sistema
  * @param string $texto El cuerpo (html) de la notificación
  * @param int $severidad La severidad de la notificación. 1:informacion, 2:validacion, 3:precaucion, 4:error. Es *opcional*
  * @param object opciones Un objeto asociativo con textos adicionales (titulo, textoSi, textoNo, callbackSi, callbackNo)
  *
  * @return boolean Regresa True si se pudo enviar la notificación; False en caso contrario.
  */
//function notificarUsuario(usuario, tipoNotificacion, texto, severidad, opciones)
function notificarUsuario(tipo, titulo, cuerpo, textosi, callbacksi, textono, callbackno)
{
	if ( typeof titulo === 'undefined' || titulo == null || typeof cuerpo === 'undefined' || cuerpo == null )
		return;
	
	_notificaciones_proto.addClass('overlay');
	
	// agregamos la clase de acuerdo al tipo
	if ( tipo+'' != '' )
		_notificaciones_proto.addClass(tipo);
	
	// confifguramos el contenido
	_notificaciones_proto.find('.title').html( titulo );
	_notificaciones_proto.find('.body').html( cuerpo );
	
	// configuramos el botón
		// texto si
	if ( typeof textosi !== 'undefined' && textosi != null )
		_notificaciones_proto.find('#cmdSi').off().html(textosi);
	else
		_notificaciones_proto.find('#cmdSi').off().html('CERRAR');

	if ( typeof callbacksi !== 'undefined' && callbacksi != null )
		_notificaciones_proto.find('#cmdSi').click( callbacksi );
	else
		_notificaciones_proto.find('#cmdSi').click( function(){ ocultarNotificacion(); } );

		// texto no
	if ( typeof textono !== 'undefined' && textono != null )
		_notificaciones_proto.find('#cmdNo').off().show().html(textono);
	else
		_notificaciones_proto.find('#cmdNo').off().hide();

	if ( typeof callbackno !== 'undefined' && callbackno != null )
		_notificaciones_proto.find('#cmdNo').click( callbackno );
	else
		_notificaciones_proto.find('#cmdNo').click( function(){ ocultarNotificacion(); } );
	
	// mostramos
	_notificaciones_proto.show(100);
}


function ocultarNotificacion()
{
	if (_notificaciones_proto!=null)
		_notificaciones_proto.hide(100);
}


/**
  * notificaciones_init - Inicializa el objeto jQuery de las notificaciones
  *
  * Verifica si existe el objeto prototipo que se usará para mostrar las notificaciones.
  * Si no existe, lo crea.
  */
function notificaciones_init()
{
	if ( _notificaciones_proto == null )
	{
		_notificaciones_proto = jQuery('<div class="modal notificacion" id="_notificaciones_proto" tabindex="-1" role="dialog" aria-labelledby="Notificacion">'+
											'<div class="modal-dialog modal-sm" role="document">'+
												'<div class="modal-content">'+
													'<div class="modal-header">'+
														'<span class="icono"></span>'+
														'<h4 class="modal-title" id="myModalLabel">#titulo#</h4>'+
													'</div>'+
													'<div class="modal-body">#cuerpo#</div>'+
													'<div class="modal-footer">'+
														'<button type="button" id="cmdNo" class="btn btn-default" data-dismiss="modal">#textono#</button>'+
														'<button type="button" id="cmdSi" class="btn btn-primary">#textosi#</button>'+
													'</div>'+
												'</div>'+
											'</div>'+
										'</div>');
	}

	console.log( _notificaciones_proto );
	jQuery('body').append(_notificaciones_proto);
	_notificaciones_proto.on('shown.bs.modal', function(e){
		jQuery('#documento').addClass('blurred');
	});
	_notificaciones_proto.on('hidden.bs.modal', function(e){
		jQuery('#documento').removeClass('blurred');
	});

	// limpiamos listeners
	_notificaciones_proto.find('#cmdSi').unbind();
	_notificaciones_proto.find('#cmdNo').unbind();
}

jQuery(document).ready(function(){
	notificaciones_init();
});
