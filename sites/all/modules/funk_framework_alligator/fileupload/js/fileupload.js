"option explicit";

jQuery.fn.recurso = function (params)
{
	if ( this.length < 1 ) return;
	
	var estado = 'liga';								// indica si se trata de una liga o un archivo
	var predata = this.attr('data-predata');			// si se desea que el control tenga un valor inicial
	var tipoPredata = (this.attr('data-predatatype').toUpperCase() === 'IMG') ? 'img' : 'url';
	var esto = this;										// referencia al objeto para poder referenciarlo externamente
	// ---------------------------------------
	// ********** settings ***********
	var settings = jQuery.extend({
		target: null, 									// el elemento DOM en el cual se desplegará el preview
		urlUpload: 'sites/default/fileuploads/', 		// la url dond se encuentra el script(php) que atiende la subida del archivo
		urlTarget: 'documentacion/', 					// la url a donde se subirán los archivos
		showFilename: true,								// muestra u oculta el nombre del archivo subido
		dropplet: null,									// el elemento sobre el cual se puede hacer drop de un archivo
		onDelete: null, 								// función que se ejecuta cuando se borra un archivo (o url)
		onChange: null, 								// función que se ejecuta cuando cambia el archivo(o url)
		operacion: null,								// el tipo de operación que se desea realizar (TIENEVALOR, PRESENTAR o nada)
		button: true,									// si aparece el botón de seleccionar documento o no
		acceptedTypes: 'img,pdf',						// lista (intrínseca) de extensiones aceptadas (img = gif|jpe?g|png), doc
		buttonText: 'Seleccionar'						// texto por default que aparece en los botones
	}, params);

	var acceptPattern = settings.acceptedTypes.replace(/\s/g, '');
	acceptPattern = acceptPattern.replace('img', 'gif|jpe?g|png');
	acceptPattern = acceptPattern.replace('doc', 'doc|docx');
	acceptPattern = acceptPattern.replace(',', '|');
	//var acceptPattern = new RegExp('(\.|\/)('+acceptPattern+')$', 'ig');
	var acceptPattern = new RegExp('(\\.)('+acceptPattern+')', 'ig');

	if (settings.operacion !== null && settings.operacion.toUpperCase() == 'GETVALOR')
	{
		var r = this.data('ultimoarchivo');
		return (typeof r === 'undefined') ? null : r;
		/*var n = jQuery(this).find('#contArchivo');
		if ( n.length > 0 )
			return jQuery(this).find('#contArchivo').html();
		else
			return null;*/
	}
	if (settings.operacion !== null && settings.operacion.toUpperCase() == 'TIENEVALOR')
	{
		var r = this.data('ultimoarchivo');
		return (typeof r === 'undefined') ? false : true;

		/*var fileActual = jQuery(this).find('#contArchivo').html();

		if (fileActual !== undefined && fileActual !== null && fileActual != '')
		{
			return true;
		}
		else
			return false;*/
	}
	// esta operación se usa para presentar los archivos que contiene
	else if (settings.operacion !== null && settings.operacion.toUpperCase() == 'PRESENTAR')
	{
		this.empty();

		// agregamos el contenedor de la lista de contenidos
		var contGenerico = jQuery('<div style="padding:5px">' + (predata == '' ? 'Aun no hay archivos o ligas adjuntos.' : '') + '</div>');
		this.append(contGenerico);

		var contArchivo = jQuery('<div style="padding:2px 0px"></div>');
		var contArchivoNombre = jQuery('<span id="contArchivo" style="cursor:pointer"></span>');
		contArchivo.append(contArchivoNombre);

		if (predata != '')
		{
			contGenerico.empty();

			var k = 0;
			if (predata != null)
			{
				var _pred = trim(predata).split('||');
				for (k = 0; k < _pred.length; k++)
				{
					var ele = _pred[k].split('|');

					var auxObj = new Object();
					auxObj.nombre = ele[0];
					auxObj.tipo = ele[1];
					esto.attr('data-ultimoArchivo', auxObj.nombre);

					var auxCont = contArchivo.clone();
					auxCont.find('#contArchivo').html(ele[0]);
					auxCont.find('#contArchivo').attr('tipo', ele[1]);

					auxCont.find('span#contArchivo, button#cmdPreviewArchivoEjemplo').click(function () {
						var d = auxCont.find('span#contArchivo');
						// desplegamos el recurso
						if (settings.target != null)
						{
							if (d.attr('tipo') != 'img')
								desplegarRecurso(d.html(), settings.target);
							else
								desplegarRecurso('sites/default/files/' + settings.urlTarget + d.html(), settings.target);
						}
					});

					contGenerico.append(auxCont);

				}
			}
		}
	}
	else
	{
		// ---------------------------------------
		// ********** inicializaciones ***********
		this.empty();

		// ----------------------------------------------
		// ********** construcción de la forma **********

		// agregamos el contenedor de la lista de contenidos
		var contGenerico = jQuery('<div class="contGenerico">' + (predata == '' ? 'Aun no hay archivos o ligas adjuntos.' : predata) + '</div>');
		if ( settings.showFilename == true )
		{
			this.append(contGenerico);
		}

		var contArchivo = jQuery('<div style="padding:0px 0px; height:36px"></div>');
		var contArchivoNombre = jQuery('<span id="contArchivo" style="cursor:pointer; line-height:36px"></span>');

		contArchivo.append(contArchivoNombre);

		// archivo
		var divArchivo = jQuery('<div ' + ((predata != '' && tipoPredata == 'url') ? 'style="display:none; text-align:right"' : 'style="text-align:right"') + '></div>');
		var progreso = jQuery('<div id="progresoEjemplo" class="progress" style="display:none"><div class="bar" style=""></div></div>');
		var boton = jQuery('<span class="btn filled fileinput-button" id="cmdSubirArchivo" style=""><i class="ico-upload"></i><span>'+settings.buttonText+'</span></span>');
		var fileInput = jQuery('<input id="fileArchivo" type="file" style="height:100%; opacity:0" name="files[]" data-id="' + this.data('id') + '">');
		if ( predata!='' )
			this.attr('data-ultimoarchivo', predata);

		boton.append(fileInput);
		if ( settings.button )
		{
			divArchivo.append(progreso);
			divArchivo.append(boton);
		}
		else
		{
			if ( settings.dropplet )
				settings.dropplet.append( fileInput );
		}

		this.append(divArchivo);

		if (settings.target != null && predata!='')
		{
			desplegarRecurso('sites/default/files/' + settings.urlTarget + predata, settings.target);
		}

		// configuración del file uploader
		fileInput.fileupload({
			url: settings.urlUpload,
			dataType: 'json',
			//acceptFileTypes: acceptPattern,
			dropZone: settings.dropplet,
			start: function (e) {
				// mostramos el progreso
				progreso.show();
				contArchivoNombre.html('');
			},
			add: function (e, data) {
				data.formData = {newPath: settings.urlTarget};
				var jqXHR = data.submit();

				//var acc = acceptPattern;

				console.debug( acceptPattern );
				console.debug( data.files[0].name );
				if (!acceptPattern.test(data.files[0].name))
				{
					// no es válido
					jqXHR.abort();

					alert("El tipo de archivo no es válido");
				}
			},
			done: function (e, data)
			{
				progreso.hide();

				if (data.result.files.length > 0)
				{
					var clon = contArchivo.clone();
					clon.find('#contArchivo').html(data.result.files[0].name);
					contGenerico.empty();
					contGenerico.append(clon);

					var auxObj = new Object();
					auxObj.nombre = data.result.files[0].name;
					auxObj.tipo = 'img';
					esto.attr('data-ultimoArchivo', auxObj.nombre);

					//clon.find('#cmdEliminarArchivoEjemplo').attr('indice', ultimoArchivo.length - 1);
					clon.find('#cmdEliminarArchivoEjemplo').click(function () {

						if (settings.onDelete != null)
							settings.onDelete(null);

						jQuery(this).parent().remove();
					});

					// lo desplegamos
					if (settings.target != null)
					{
						desplegarRecurso('sites/default/files/' + settings.urlTarget + data.result.files[0].name, settings.target);
					}

					// disparamos el eveneto de cambio, para actualizar con la bd
					if (settings.onChange != null)
						settings.onChange( esto.attr('data-ultimoArchivo').nombre );
				}
			},
			fail: function (e, data) {
				progreso.hide();
			},
			progressall: function (e, data) {
				var progress = parseInt(data.loaded / data.total * 100, 10);
				progreso.find('.bar').css('width', progress + '%');
			}
			//acceptFileTypes: /(\.|\/)(gif|jpe?g|png|pdf)$/ig
		});

	} // fin del if si hay operación especificada

	return this;

} // fin del plugin "recurso"


function desplegarRecurso(url, target)
{

	if (url != null && target != null)
	{
		target = (typeof target === 'string') ? jQuery('' + target) : jQuery(target);

		if (target.length > 0)
		{
			// sí hay un elemento target
			// ahora determinamos el tipo del recurso
			if (url.toUpperCase().endsWith('.JPG') || url.toUpperCase().endsWith('.JPEG') || url.toUpperCase().endsWith('.PNG') || url.toUpperCase().endsWith('.GIF'))
			{
				//if ( url.indexOf('/') == -1 /*!url.startsWith('sites/default/files/documentacion/')*/)
				//	url = 'sites/default/files/documentacion/' + url;
				target.attr('allowtransparency', 'true');
				target.css('background-size', 'contain');

				// el target es un iframe?
				if (target.prop("tagName") == 'IFRAME')
				{
					target.attr('src', '');
					target.css('background-image', 'url(\'' + encodeURI(url) + '\')');
				}
				else
				{
					target.css('background-image', 'url(\'' + url + '\')');
				}
			}
			else if (url.toUpperCase().endsWith('.PDF'))
			{
				// si es pdf tb lo desplegamos
				//if (!url.startsWith('sites/default/files/documentacion/'))
				//	url = 'sites/default/files/documentacion/' + url;

				if (target.prop("tagName") == 'IFRAME')
				{
					target.attr('src', encodeURI(url));
				}
			}
			else if (url.toLowerCase().endsWith('.docx') || url.toLowerCase().endsWith('.doc') || url.toLowerCase().endsWith('.xlsx') || url.toLowerCase().endsWith('.xls') || url.toLowerCase().endsWith('.pps') || url.toLowerCase().endsWith('.ppt') || url.toLowerCase().endsWith('.pptx') || url.toLowerCase().endsWith('.accdb') || url.toLowerCase().endsWith('.accde'))
			{
				// Lo desplegamos con el servicio de ms
				//if (!url.startsWith('sites/default/files/documentacion/'))
				//	url = 'sites/default/files/documentacion/' + url;
				url = 'http://view.officeapps.live.com/op/view.aspx?src=' + encodeURI('http://' + Drupal.settings.basePath + url);

				// es imagen
				if (target.prop("tagName") == 'IFRAME')
				{
					target.attr('src', encodeURI(url));
				}
			}
			else if (url.toUpperCase().endsWith('.MP4') || url.toLowerCase().endsWith('.docx') || url.toLowerCase().endsWith('.doc') || url.toLowerCase().endsWith('.zip') || url.toLowerCase().endsWith('.rar') || url.toLowerCase().endsWith('.xlsx') || url.toLowerCase().endsWith('.xls') || url.toLowerCase().endsWith('.avi') || url.toLowerCase().endsWith('.3gp') || url.toLowerCase().endsWith('.webm') || url.toLowerCase().endsWith('.ogv') || url.toLowerCase().endsWith('.pps') || url.toLowerCase().endsWith('.ppt') || url.toLowerCase().endsWith('.pptx') || url.toLowerCase().endsWith('.accdb') || url.toLowerCase().endsWith('.accde') || url.toLowerCase().endsWith('.accdt') || url.toLowerCase().endsWith('.accdr') || url.toLowerCase().endsWith('.wav') || url.toLowerCase().endsWith('.wma') || url.toLowerCase().endsWith('.wmv') || url.toLowerCase().endsWith('.mp3') || url.toLowerCase().endsWith('.flashpaper'))
			{
				// No lo desplegamos
				/*if ( !url.startsWith('sites/default/files/proyectos/') )
				 url = 'sites/default/files/proyectos/' + url;
				 
				 // es imagen
				 if ( target.prop("tagName") == 'IFRAME' )
				 {
				 target.attr('src', encodeURI(url));
				 }*/
			}
			else
			{
				// es url
				target.css('background-image', 'none');
				if (!url.startsWith('http://'))
					url = 'http://' + url;
				if (target.prop("tagName") == 'IFRAME')
				{
					target.attr('src', url);
				}
				else
				{
					target.load(url);
				}
			}
		}
	}
} // fin de la función despliegaRecurso






