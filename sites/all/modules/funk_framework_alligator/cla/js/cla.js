
var CLA = (function() {
	if ( arguments.callee._singletonInstance )
		return arguments.callee._singletonInstance;
	arguments.callee._singletonInstance = this;

	// ________________________________________________________________________ propiedades
	return {
		_cola:			[],
		procesando:		false,
		_maxIntentos:	3,
		_ultimoId:		0,

		/* par�metros:
			- url[string] la url (completa)
			- params[string] una cadena con formato GET para pasarle params al modulo
			- callback[function] opcional. una funci�n que se manda ejecutar al regreso y se le pasa como par�metro el data recibido
			- hacerSincrona[boolean] falso por default. si se hace asincrona, entonces al mismo tiempo que se ejecuta esta, se seguira procesando la cola. Lo normal es que, como es una cola, vayan de uno en uno. */
		encolar : function(pUrl, pParams, pCallback, pPrefuncion, hacerAsincrona, delay, pExtraData)
		{
			var self = this;
			var aux = new Object();
			aux.url = pUrl;
			aux.params = pParams;
			aux.callback = pCallback;
			aux.prefuncion = pPrefuncion;
			aux.asincrona = 1;	// 2do: asignar del param
			aux.intentos = 0;
			if ( pExtraData == undefined || pExtraData == null )
				aux.extraData = null;
			else
				aux.extraData = pExtraData;

			if(delay > 0)
				aux.delay = delay;
			else
				aux.delay = null;

			self._cola.push(aux);
			self._ultimoId++;

			if (!self.procesando)
				self.procesarCola.apply(self, []);	// invocaci�n din�mica con paso de contexto
		}, // fin de la funci�n encolar

		procesarCola : function()
		{
			var self = this;
			self.procesando = true;

			if ( !self._cola.length )
				self.procesando=false;
			else
			{
				if ( self._cola[0].prefuncion != null )
					self._cola[0].prefuncion(self._cola[0]);
			
				self._cola[0].intentos++;

				self.oriSuc = self._cola[0].callback;

				// preparamos la llamada
				jQuery.ajax({
					url: self._cola[0].url,
					type: 'post',
					data: self._cola[0].params,
					error: function(a, b, c) {
						if (self._cola[0].intentos >= self._maxIntentos)
						{
							// ya llegamos al m�ximo -> la quitamos
						}
					},
					complete: function(__data, textStatus) {
						self.procesando = false;
						var esto = self._cola[0];

						if ( self._cola.length )
						{
							self._cola.shift(); // sacamos al primer elemento

							if ( typeof(self.oriSuc) === 'function' ) {
								self.oriSuc.apply(self, [__data.responseText, textStatus, esto]);
							}
						}

						if ( self._cola.length )
						{
							if ( self._cola[0].delay != null )
								self.tid = setTimeout(function(){
									self.procesarCola.apply(self, []);
								},  self._cola[0].delay);
							else
								self.procesarCola.apply(self, []);
						}
					},
					success: function(__data, textStatus){
					}
				});	// fin de la llamada ajax
			} // fin del else
		} // fin de la funcion procesarCola


	} // fin del return m�gico
}());
	
