/****************************************************************

	libValida.js
	
	data.valida = "requerido,numero,{longitud,4,=},{iguala,#nip2}"

 ****************************************************************/


var libValida = {
	/**
	 * Referencia al prototipo del tooltip que se muestra cuando hay un error
	 */
 	protovalida:			null,

	/**
	 * Autoreferencia
	 */
	esto:					this,

	/**
	 * Inicializa los campos que tengan el atributo data-valida
	 */
	initValida:				function(){
		// TODO: Verificamos requerimientos
		if ( typeof jQuery === 'undefined' )
		{
			console.error('Debes cargar jQuery antes que el m�dulo libValida');
			return;
		}
	
		// verificamos que exista el tooltip
		this.protovalida = jQuery('<div class="validador"><div class="triangulito"></div><span class="mensaje"></span></div>');
	
		if (this.protovalida.length<1)
		{
			// no existe el proto -> no podemos crear tooltips
			return;
		}
	
		// buscamos todos los campos
		jQuery('*[data-valida]').each(function(){
	
			var elCampo = jQuery(this);
	
			if ( elCampo.attr('type') != undefined && elCampo.attr('type').toUpperCase() == 'FILE' )
			{
				// excepto cuando es archivo, que validamos al cambiar
				elCampo.change(function(){ libValida.validarCampo(this) });
				elCampo.blur(function(){ libValida.validarCampo(this) });
			}
			else
			{
				// este campo si tiene una regla de valida
				elCampo.focus(function(){ libValida.ocultarMensajeError(this) });
				// validamos cuando se pierde el foco
				elCampo.blur(function(){ libValida.validarCampo(this) });
			}
		});
	},

	/**
	 * Valida todos los campos contenidos en $ele
	 * @param {Object} el $contenedor que encierra a los elementos a validar
	 * @returns {bool} true si todos los campos son v�lidos, false en caso contrario
	 */
	validarFormulario:		function(ele){
		var res = true;
		
		ele = jQuery(ele);
		
		ele.find('*[data-valida]').each(function(){
			res &= libValida.validarCampo(jQuery(this));
		});
		
		return res;
	},

	/**
	 * Valida el campo especificado seg�n sus reglas
	 * @param {Object} el $elemento a validar
	 * @returns {bool} true si es v�lido, false en caso contrario
	 */
	validarCampo:			function(ele){
		ele = jQuery(ele);
		var res = true;

		if(typeof ele.data('valida') !== 'undefined')
		{
			// obtenemos las reglas de valida
			var reglas = ele.data('valida').split(',');
			res = this._validarCampo(ele, reglas);
		}
		
		return res;
	},

	/**
	 * M�todo interno que hace la validaci�n
	 * @param {Object} el $elemento a validar
	 * @param {string} la lista de categor�as bajo las cuales validar
	 * @returns {bool} true si es v�lido, false en caso contrario
	 */
	_validarCampo:			function(campo, condiciones){
		// limpiamos al control de validadores
		this.ocultarMensajeError(campo);
	
		var res = true;
	
		for (i=0; i<condiciones.length; i++)
		{
			var condicion = trim(condiciones[i]).toUpperCase();
			console.log(condicion);
			
			// __________________________________________________________________________________________________________ REQUERIDO
			if (condicion == "REQUERIDO" )
			{
				//Que no este vac�o o almenos haya un radio/checkbox seleccionado
				if ( campo.is('input:radio') || campo.is('input:checkbox') )
				{
					aux_name = campo.attr('name');
	
					res = jQuery('input[name="' + aux_name + '"]').is(':checked');
					if ( !res ){
						this.crearMensajeError(campo, 'Elija una opci�n.');
					}
				}
				else if ( campo.is('select') )
				{
					if ( campo.val()==null || trim(campo.val())=="" || campo.val()=="-1" )
					{
						this.crearMensajeError(campo, 'Este campo no puede estar vac�o');
						i=condiciones.length;  // cuando ni siquiera esta lleno, no checamos la	s demas condiciones
						res = false;
					}
				}
				else
				{
					if ( campo.val()!=null && trim(campo.val())!="" ) {}
					else
					{
						this.crearMensajeError(campo, 'Este campo no puede estar vac�o');
						i=condiciones.length;  // cuando ni siquiera esta lleno, no checamos las demas condiciones
						res = false;
					}
				}
			}
			// __________________________________________________________________________________________________________ TEXTO
			else if (condicion == "TEXTO" )
			{//Solo valores alfabeticos
				console.log(campo.val());
				if ( /^[(a-zA-Z)\!\$\&\(\)\.\,\;\:\-\_��������������\s]*$/.test(campo.val()) || campo.val().length==0 ) {}
				else
				{
					this.crearMensajeError(campo, 'Este campo s�lo puede contener caracteres alfab�ticos.');
					res = false;
				}
			}
			// __________________________________________________________________________________________________________ LOGIN
			else if ( condicion == "LOGIN" || condicion == "PASSWORD" || condicion == "CONTRASENHA" )
			{
				if ( (/^[0-9\w\!\$\&()\-_��������������]+$/.test(campo.val()) && campo.val().indexOf(' ')==-1) || campo.val().length==0 ) {}
				else
				{
					this.crearMensajeError(campo, 'Este campo s�lo puede contener caracteres alfanum�ricos\ny algunos s�mbolos de puntuaci�n.');
					res = false;
				}
			}
			// __________________________________________________________________________________________________________ ALFANUMERICO
			else if ( condicion == "ALFANUMERICO" )
			{
				if ( /^[0-9\w\!\$\&().,;:\-_��������������\s]+$/.test(campo.val()) || campo.val().length==0 ) {}
				else
				{
					this.crearMensajeError(campo, 'Este campo s�lo puede contener caracteres alfanum�ricos');
					res = false;
				}
			}
			// __________________________________________________________________________________________________________ CP
			else if ( condicion == "CP" )
			{
				if ( /^[0-9]{5}$/.test(campo.val()) || campo.val().length==0 ) {}
				else
				{
					this.crearMensajeError(campo, 'Debe proporcionar un CP v�lido.');
					res = false;
				}
			}
			// __________________________________________________________________________________________________________ TELEFONO
			else if ( condicion == "TELEFONO" )
			{
				if ( campo.attr('data-origen')!=undefined && campo.attr('data-origen').toUpperCase() == "MX" )
				{
					// para tel�fonos de m�xico la regla es que debe tener 10 digitos
					if ( /^[0-9(\+)\-#,\.\sa-zA-Z.]{10}$/.test(campo.val()) || campo.val().length==0 ) {}
					else
					{
						this.crearMensajeError(campo, 'Debe proporcionar un tel�fono v�lido (m�ximo 10 caracteres).');
						res = false;
					}
				}
				else
				{
					if ( /^[0-9(\+)\-#,\.\sa-zA-Z.]+$/.test(campo.val()) || campo.val().length==0 ) {}
					else
					{
						this.crearMensajeError(campo, 'Debe proporcionar un tel�fono v�lido.');
						res = false;
					}
				}
			}
			// __________________________________________________________________________________________________________ EMAIL
			else if ( condicion == "EMAIL" )
			{
				if ( /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test(campo.val()) || campo.val().length==0 ) {}
				else
				{
					this.crearMensajeError(campo, 'El correo electr�nico no es v�lido.');
					res = false;
				}
			}
			// __________________________________________________________________________________________________________ NUMERO
			else if ( condicion == "NUMERICO" || condicion == "NUMERO" )
			{
				if ( /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(campo.val()) || campo.val().length==0 ) {}
				else
				{
					this.crearMensajeError(campo, 'Este campo s�lo puede contener n�meros.');
					res = false;
				}
			}
			// __________________________________________________________________________________________________________ NOMBRE,APELLIDO
			else if ( condicion == "NOMBRE" || condicion == "APELLIDO" || condicion == "MUNICIPIO" || condicion == "LOCALIDAD" )
			{
				if ( /^[0-9(\+)��������������������'��\-#,\.\sa-zA-Z.]+$/.test(campo.val()) || campo.val().length==0 ) {}
				else
				{
					this.crearMensajeError(campo, 'Este campo s�lo puede contener n�meros.');
					res = false;
				}
			}
			// __________________________________________________________________________________________________________ URL
			else if ( condicion == "URL" )
			{
				if ( /https?:\/\/([-\w\'\.]+)+(:\d+)?(\/([\w\/_\.]*(\?\S+)?)?)?/.test(campo.val()) || campo.val()=='' ) {}
				else
				{
					this.crearMensajeError(campo, 'Este campo debe ser URL.');
					res = false;
				}
			}
			// __________________________________________________________________________________________________________ SEXO
			else if ( condicion == "SEXO" )
			{
				if ( /\w/.test(campo.val()) ) {}
				else
				{
					this.crearMensajeError(campo, 'Este campo s�lo puede contener n�meros.');
					res = false;
				}
			}
			// __________________________________________________________________________________________________________ TAMA�O
			else if ( condicion == 'TAMANHO' )
			{
				this.ocultarMensajeError(campo);
				if ( !jQuery.browser.msie )
				{
					// validamos el tama�o
					if ( campo.get(0).files.length > 0 )
					{
						var tam = campo.get(0).files[0].size;
	
						if ( tam > 30000000 )
						{
							this.crearMensajeError(campo, 'El archivo es demasiado grande.');
							res = false;
						}
					}
				}
			}
			// __________________________________________________________________________________________________________ LONGITUD
			else if ( /\{LONGITUD\|\d\|[=><!]+\}/.test(condicion) )
			{
				condicion = condicion.replace('{', '');
				condicion = condicion.replace('}', '');
				var partes = condicion.split('|');	// {longitud|4|=}

				if ( partes[2] == '=' ) 	  res = campo.val().length == parseInt(partes[1]);
				else if ( partes[1] == '!=' ) res = campo.val().length != parseInt(partes[1]);
				else if ( partes[1] == '<'  ) res = campo.val().length <  parseInt(partes[1]);
				else if ( partes[1] == '>'  ) res = campo.val().length >  parseInt(partes[1]);
				else if ( partes[1] == '<=' ) res = campo.val().length <= parseInt(partes[1]);
				else if ( partes[1] == '>=' ) res = campo.val().length >= parseInt(partes[1]);
				
				if (!res)
					this.crearMensajeError(campo, 'La longitud del campo es incorrecta.');
			}
			// __________________________________________________________________________________________________________ IGUALDAD
			else if ( /\{IGUALA\|[\s\S]+\}/.test(condicion) )
			{
				condicion = condicion.replace('{', '');
				condicion = condicion.replace('}', '');
				var partes = condicion.split('|');	// {iguala|#nip2}
				
				var otroValor = jQuery(partes[1]).val();
				
				if ( campo.val() != otroValor )
				{
					this.crearMensajeError(campo, 'Los campos deben tener el mismo valor.');
					res = false;
				}
			}
		}
	
		return res;
	},

	/**
	 * Esconde el tooltip y quita las clases de validaci�n de un elemento
	 * @param {Object} el $elemento a validar
	 */
	ocultarMensajeError:	function(ele){
		ele = jQuery(ele);

		ele.removeClass('error cruz').addClass('success paloma');
	
		if ( ele.next().hasClass('validador' ) )
			ele.next().remove();
	},
	
	/**
	 * Crea una instancia del mensaje de error y la adjunta al elemento, con un mensaje definido
	 * @param {Object} el $elemento a validar
	 * @param {string} el mensaje
	 */
	crearMensajeError:		function(campo, mensaje){
		campo.removeClass('success paloma').addClass('error cruz');
	
		if ( jQuery(campo).hasClass( "isdate" ) )
		{
		}
		else
		{
			if( campo.is(':radio') || campo.is(':checkbox') )
			{
			}
			else
			{
				// reseteamos, en caso de que ya tenga
				campo.find('.validator').remove();

				// TODO: creamos el nuevo validador y lo agregamos
				/*var validador = protovalida.clone();
				validador.find('.mensaje').html( mensaje );
				campo.after( validador );*/
			}
		}
	},
};

jQuery(document).ready(function(){
	libValida.initValida();
});




// DEPRECATED METHODS
/*
function addFuncionValida(campo)
{
	elCampo = jQuery(campo);
	if ( elCampo.attr('type') != undefined && elCampo.attr('type').toUpperCase() == 'FILE' )
	{
		// excepto cuando es archivo, que validamos al cambiar
		elCampo.change(function(){ validarCampo(this) });
		elCampo.blur(function(){ validarCampo(this) });
	}
	else
	{
		// este campo si tiene una regla de valida
		elCampo.focus(function(){ ocultarMensajeError(this) });
		// validamos cuando se pierde el foco
		elCampo.blur(function(){ validarCampo(this) });
	}
}

function removeFuncionValida(campo)
{
	elCampo = jQuery(campo);
	ocultarMensajeError(elCampo);
	if ( elCampo.attr('type') != undefined && elCampo.attr('type').toUpperCase() == 'FILE' )
	{
		// excepto cuando es archivo, que validamos al cambiar
		elCampo.off('change');
		elCampo.off('blur');
	}
	else
	{
		elCampo.off('focus');
		elCampo.off('blur');
	}
}
*/







