
jQuery.fn.formulario = function(params)
{
	var esto = jQuery(this);
	var puedeEnviar = true;


	//															********** settings ***********
	var settings = jQuery.extend({
		botonEnviar: null,		// el elemento a configurar como bot�n de enviar
		botonCancelar: null,	// el elemento a configurar como bot�n de cancelar
		botonLimpiar: null,		// el elemento a configurar como bot�n de limpiar
		
		onEnviar: null,			// callback a ejecutarse cuando se activa el bot�n enviar
		onCancelar: null,		// callback a ejecutarse cuando se activa el bot�n cancelar
		onLimpiar: null,		// callback a ejecutarse cuando se activa el bot�n limpiar
		
		esLlamadaCLA: true,		// si el formulario invoca una llamada de CLA
		parametrosCLA: {		// lista de par�metros a usarse en la llamada CLA. Contiene:
			url: 'ajax.php',	// url			-> la url. por default es ajax.php
			datos: [],			// datos		-> un Object con m: el nombre del m�todo, [...] los parametros a pasar. [v2: por default son los valores del formulario]
			prefuncion: null,	// prefuncion	-> funci�n que se ejecuta antes de ejecutar la llamada; default null
			postfuncion: null,	// postfuncion	-> funci�n que se ejecuta despu�s de ejecutar la llamada; default null
			delayInicial: 0.0	// delayInicial	-> delay en segundos antes de ejecutar la llamada
		},
	}, params );


	// 															********** inicializaciones ***********

	// ________________________________________________________ evento keyup en todos los campos
	esto.find('input').keyup(function(evt){
		if ( evt.keyCode == 13 )
		{
			// buscamos si hay elementos obligatorios no llenos
			var todoLleno = true;
			
			esto.find('input').each(function(){
				var vali = jQuery(this).attr('data-valida');
				if ( typeof vali !== 'undefined' && vali.indexOf('requerido')!=-1 && jQuery(this).val()=='' && todoLleno == true )
				{
					todoLleno = false;
					jQuery(this).focus();
				}
			});
			
			if ( todoLleno )
			{
				if ( settings.botonEnviar!=null )
					settings.botonEnviar.click();
			}
		}
	});
	
	// ________________________________________________________ configuraciones del bot�n enviar
	if ( settings.botonEnviar != null )
	{
		if ( settings.esLlamadaCLA )
		{
			settings.botonEnviar.off().click(function(){
				if (libValida.validarFormulario(esto))
				{
					if (settings.parametrosCLA.prefuncion!=null)
						puedeEnviar = settings.parametrosCLA.prefuncion();
		
					if ( puedeEnviar != false )
					{
						// recabamos datos
						var _datos = new Object();
						for (var property in settings.parametrosCLA.datos) {
							if (settings.parametrosCLA.datos.hasOwnProperty(property)) {
								if ( settings.parametrosCLA.datos[property].indexOf('jQuery')!=-1 )
									//settings.parametrosCLA.datos[property] = eval(settings.parametrosCLA.datos[property]);
									_datos[property] = eval(settings.parametrosCLA.datos[property]);
								else
									//settings.parametrosCLA.datos[property] = eval("'"+settings.parametrosCLA.datos[property]+"'");
									_datos[property] = eval("'"+settings.parametrosCLA.datos[property]+"'");
								console.log(property + '::' + _datos[property]);
							}
						}
						
						// configuramos la llamada CLA
						//console.log(settings.parametrosCLA.datos);
						CLA.encolar(
							(typeof settings.parametrosCLA.url === 'undefined' ? 'ajax.php' : settings.parametrosCLA.url),
							//(typeof settings.parametrosCLA.datos === 'undefined' ? {} : settings.parametrosCLA.datos),
							(typeof _datos === 'Object' ? {} : _datos),
							function(response){
								// rehabilitamos los controles
								jQuery('input, select, textarea').removeAttr('disabled');
								if (typeof settings.parametrosCLA.postfuncion!=='undefined' && settings.parametrosCLA.postfuncion!=null)
									settings.parametrosCLA.postfuncion(jsoner.parse(response));
							},
							function(){
								// deshabilitamos los controles
								jQuery('input, select, textarea').attr('disabled', 'disabled');
							},
							true, 
							(typeof settings.parametrosCLA.delayInicial === 'undefined' ? 0.0 : settings.parametrosCLA.delayInicial)
						);
					}
				}
			});
		}
		else
		{
			settings.botonEnviar.off().click(function(){
				// validar
				var r = libValida.validarFormulario(esto);
				
				if ( settings.onEnviar != null && r )
					settings.onEnviar();
			});
		}
	}


	
	// ________________________________________________________ configuraciones del bot�n cancelar
	if ( settings.botonCancelar != null )
	{
		settings.botonCancelar.off();
		
		if ( settings.onCancelar!=null )
			settings.botonCancelar.click(settings.onCancelar);
		else
		{
			settings.botonCancelar.click(function(){
				// limpiamos el formulario
				esto.find('input').val('');
				esto.find('select').val(-1);
				esto.find('textarea').html('');
			});
		}
	}

	// ________________________________________________________ configuraciones del bot�n limpiar
	if ( settings.botonLimpiar != null )
	{
		settings.botonLimpiar.off();

		if ( settins.onLimpiar!=null )
			settings.botonLimpiar.click(settings.onLimpiar);
		else
		{
			settings.botonCancelar.click(function(){
				// limpiamos el formulario
				esto.find('input').val('');
				esto.find('select').val(-1);
				esto.find('textarea').html('');
			});
			
		}
	}


} // fin del plugin "formulario"
