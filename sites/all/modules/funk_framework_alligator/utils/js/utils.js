function mysql2date(fecha)
{
	if ( typeof fecha !== 'undefined' && fecha != null )
	{
		var partes = fecha.split(' '); // partes[0]='2014-01-02', partes[1]='00:00:00'
		
		// TODO: convertir partes[0] al formato que se desee
		
		return partes[0];
	}
	else return null;
}

function trim(str)
{
	if (str && (typeof str) === 'string')
		return str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
	else
		return str;
}


function encodeStr(str)
{
	return str.replace(/[\u00A0-\u9999<>\&]/gim, function(i) {
		return '&#'+i.charCodeAt(0)+';';
	});
}

function getLocalData(d)
{
	if ( typeof(Storage) !== 'undefined' && typeof(d)!=='undefined' )
	{
		return localStorage.getItem(''+d);
	}
}

function setLocalData(k, v)
{
	if ( typeof(Storage) !== 'undefined' && typeof(k)!=='undefined' && typeof(v)!=='undefined' )
	{
		localStorage.setItem(''+k, v);
	}
}

function removeLocalData(k)
{
	if ( typeof(Storage) !== 'undefined' && typeof(k)!=='undefined' )
	{
		localStorage.removeItem(''+k);
	}
}

function truncar(cadena, longitud)
{
	if ( typeof longitud === 'undefined' ) longitud = 100;
	
	if ( cadena.length <= longitud )
		return cadena;		// no necesita
	else
		return cadena.substr(0, longitud)+'...'; // mb_substr($cadena, 0, $longitud)."...";
}

function formatNumber(n, c, d, t){
	c = isNaN(c = Math.abs(c)) ? 2 : c, 
	d = d == undefined ? "." : d, 
	t = t == undefined ? "," : t, 
	s = n < 0 ? "-" : "", 
	i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
	j = (j = i.length) > 3 ? j % 3 : 0;
	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};