'option explicit';

var grapher_canDraw = false;
var grapher_interval;

google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(function(){
	grapher_canDraw = true;
});

function grapher_doGraph(element, settings)
{
	var data = new google.visualization.DataTable();
	
	// agregamos las columnas
	var i;
	for (i=0; i<settings.seriesNames.length; i++)
	{
		data.addColumn((i==0?'string':'number'), settings.seriesNames[i]);
	}
	
	// agregamos los datos
	var tempData = new Array();
	for (i=0; i<settings.data.length; i++)
	{
		if ( settings.data[i] instanceof Array )
		{
			// asumimos que tiene la forma [etiqueta, valor]
			tempData.push( new Array(settings.data[i][0], settings.data[i][1]) );
		}
		else
		{
			// asumimos que es un dato atómico
			tempData.push( new Array('', eval(settings.data[i])) );
		}
	}
	data.addRows(tempData);
	
	var options = {
		'curveType':		settings.curveType,
		'animation':		{
			startup:	settings.animate
		},
		'backgroundColor':	settings.backgroundColor,
		'title':			settings.title,
		'titleTextStyle':{
			color:		settings.titleColor,
			fontName:	settings.titleFont,
			fontSize:	settings.titleSize,
			bold:		settings.titleBold,
			italic:		settings.titleItalic
		},
		'is3D':				settings.is3D,
		'pieSliceBorderColor':'transparent'
	};
	
	console.log(element);
	if ( settings.type == 'pie' )
		var chart = new google.visualization.PieChart(element[0]);
	else
		var chart = new google.visualization.LineChart(element[0]);
	chart.draw(data, options);
}


jQuery.fn.grapher = function (params)
{
	if ( this.length < 1 )
	{
		console.error('Debes pasar un parámetro -target- con un elemento jQuery válido (preferentemente un div)');
		return;		// si no se ha seleccionado un elemento, no proseguir
	}
	var esto = this;

	// __________________________________________________________ settings
	var settings = jQuery.extend({
		type:				'line',							// line, pie
		seriesNames:		['',''],						// un arreglo con la lista de nombres de columnas
		data:				[['No data',0]],				// arreglo con la lista de datos
		animate:			true,							// si debe animarse al iniciar
		curveType:			'function',						// si la línea es recta o polinomial
		backgroundColor:	'transparent',					// 2do: debe tomarlo del css
		title:				'',								// el título
		titleColor:			'black',						// el color del título
		titleFont:			'Arial',						// la fuente del título
		titleSize:			12,								// el tamaño del título
		titleBold:			false,							// si el titulo es bold
		titleItalic:		false,							// si el titulo es italic
		is3D:				false,							// si es 3d
	}, params);

	// todo: validaciones
	
	// todo: completar las validaciones

	// dibujamos
	if ( !grapher_canDraw )
	{
		// creamos un intervalo
		grapher_interval = setInterval(function(){
			// todo. checamos si ya está cargado el coso, para poder dobujar
			if ( grapher_canDraw )
			{
				clearInterval(grapher_interval);
				grapher_doGraph(esto, settings);
			}
		}, 200);
	}
	else
	{
		grapher_doGraph(esto, settings);
	}
} // fin del plugin





