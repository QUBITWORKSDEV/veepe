
/**
  * parseJson - Parsea una cadena JSON a uns instancia de tipo Object
  *
  * Es posible proporcionar callbacks
  *
  * @param string cadena La cadena JSON a parsear
  * @param function callbackError La función a llamar si sucede un error al parsear
  * @param function callbackExito La función a parsear si el parseo se ejecuta correctamente
  *
  * @return Object Una instancia de tipo Object con la información de la cadena JSON, null si no se pudo hacer la conversión
  */

var jsoner = {
	
	parse:	function(cadena, callbackError, callbackExito){
		var res = null;
	
		if ( typeof cadena!=='undefined' && cadena!=null )
		{
			try {
				res = JSON.parse(cadena);
	
				if ( typeof callbackExito!=='undefined' && callbackExito!=null )
					callbackExito();
			}
			catch (err) {
				if ( typeof callbackError!=='undefined' && callbackError!=null )
					callbackError();
				res = null;
			}
		}
	
		return res;
	},
};

