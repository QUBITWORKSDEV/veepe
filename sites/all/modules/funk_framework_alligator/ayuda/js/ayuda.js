
function mostrarAyuda()
{
	$('*[data-ayuda]').each(function(){
		$(this).tooltip({
			title: $(this).data('ayuda'),
			trigger: 'manual'
		}).tooltip('show');
	});
}