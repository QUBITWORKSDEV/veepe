"use strict";

function _scrollTo(ele)
{
	if ( typeof ele === 'string' )
		ele = jQuery(''+ele);
	else
		ele = jQuery(ele);

	jQuery('html, body').animate({
		scrollTop: ele.offset().top
	}, 1000);
}

