/****************************************************************

	libValida.js
	v 2.0

	Este fragmento de código se encarga de hacer validaes de
	acuerdo a ciertas expresiones regulares, creando mensajes
	de error cuando se necesita e impidiendo continuar a menos
	que todas las condiciones sean satisfechas.

	Requerimientos:
		- Un control con atributo id="protovalida"
		- jQuery 1.8 o superior invocado antes de esta función
		- El proto y cada control debe tener un campo
		(div, span o input) de id -valida-

	Ejemplo:
		<input type="text" data-valida="requerido,email" />

	Autor: Gabriel Hdez.
	5/dic/2011
	Modificado: Josil Adad
	18/sep/2013
	Modificado Gabriel Hdez
	14/Enero/2016

 ****************************************************************/


var protovalida;

function initvalida()
{
	// Verificamos requerimientos

	// verificamos que exista el tooltip
	protovalida = jQuery('<div class="validador"><div class="triangulito"></div><span class="mensaje"></span></div>');

	if (protovalida.length<1)
	{
		// no existe el proto -> no podemos crear tooltips
		return;
	}

	// buscamos todos los campos
	jQuery('*[data-valida]').each(function(){

		var elCampo = jQuery(this);

		if ( elCampo.attr('type') != undefined && elCampo.attr('type').toUpperCase() == 'FILE' )
		{
			// excepto cuando es archivo, que validamos al cambiar
			elCampo.change(function(){ validarCampo(this) });
			elCampo.blur(function(){ validarCampo(this) });
		}
		else
		{
			// este campo si tiene una regla de valida
			elCampo.focus(function(){ ocultarMensajeError(this) });
			// validamos cuando se pierde el foco
			elCampo.blur(function(){ validarCampo(this) });
		}
	});
} // initvalida

function addFuncionValida(campo)
{
	elCampo = jQuery(campo);
	if ( elCampo.attr('type') != undefined && elCampo.attr('type').toUpperCase() == 'FILE' )
	{
		// excepto cuando es archivo, que validamos al cambiar
		elCampo.change(function(){ validarCampo(this) });
		elCampo.blur(function(){ validarCampo(this) });
	}
	else
	{
		// este campo si tiene una regla de valida
		elCampo.focus(function(){ ocultarMensajeError(this) });
		// validamos cuando se pierde el foco
		elCampo.blur(function(){ validarCampo(this) });
	}
}

function removeFuncionValida(campo)
{
	elCampo = jQuery(campo);
	ocultarMensajeError(elCampo);
	if ( elCampo.attr('type') != undefined && elCampo.attr('type').toUpperCase() == 'FILE' )
	{
		// excepto cuando es archivo, que validamos al cambiar
		elCampo.off('change');
		elCampo.off('blur');
	}
	else
	{
		elCampo.off('focus');
		elCampo.off('blur');
	}
}

function validarCampo(ele)
{
	ele = jQuery(ele);
	var res = true;
	//if($.inArray('valida', ele.data()) != -1)
	if(ele.data('valida') != undefined)
	{
	// obtenemos las reglas de valida
		var reglas = ele.data('valida').split(',');
		res = _validarCampo(ele, reglas);
	}
	else
	{
		res = true
	}
	return res;
}

function _validarCampo(campo, condiciones)
{
	// limpiamos al control de validadores
	ocultarMensajeError(campo);

	var res = true;

	for (i=0; i<condiciones.length; i++)
	{
		if (trim(condiciones[i]).toUpperCase() == "REQUERIDO" )
		{//Que no este vacío o almenos haya un radio/checkbox seleccionado
			if ( campo.is('input:radio') || campo.is('input:checkbox') )
			{
				aux_name = campo.attr('name');

				res = jQuery('input[name="' + aux_name + '"]').is(':checked');
				if ( !res ){
					crearMensajeError(campo, 'Elija una opción.');
				}
			}
			else
			{
				if ( campo.is('select') )
				{
					if ( campo.val()==null || trim(campo.val())=="" || campo.val()=="-1" )
					{
						crearMensajeError(campo, 'Este campo no puede estar vacío');
						i=condiciones.length;  // cuando ni siquiera esta lleno, no checamos la	s demas condiciones
						res = false;
					}
				}
				else
				{
					if ( campo.val()!=null && trim(campo.val())!="" ) {}
					else
					{
						crearMensajeError(campo, 'Este campo no puede estar vacío');
						i=condiciones.length;  // cuando ni siquiera esta lleno, no checamos las demas condiciones
						res = false;
					}
				}
			}
		}
		else if ( trim(condiciones[i]).toUpperCase() == "TEXTO" )
		{//Solo valores alfabeticos
			if ( /[(a-zA-Z)\!\$\&\(\)\.\,\;\:\-\_áéíóúñÑÁÉÍÓÚüÜ\s]*/.test(campo.val()) || campo.val().length==0 ) {}
			else
			{
				crearMensajeError(campo, 'Este campo sólo puede contener caracteres alfabéticos.');
				res = false;
			}
		}
		/*else if ( trim(condiciones[i]).toUpperCase() == "TEXTO" )
		{
			if ( /[^0-9]/.test(campo.val()) || campo.val().length==0 ) {}
			else
			{
				crearMensajeError(campo, "Este campo sólo puede contener caracteres alfabéticos.");
				res = false;
			}
		}*/
		else if ( trim(condiciones[i]).toUpperCase() == "LOGIN" || trim(condiciones[i]).toUpperCase() == "PASSWORD" || trim(condiciones[i]).toUpperCase() == "CONTRASENHA" )
		{
			if ( (/[0-9\w\!\$\&()\-_áéíóúñÑÁÉÍÓÚüÜ]+/.test(campo.val()) && campo.val().indexOf(' ')==-1) || campo.val().length==0 ) {}
			else
			{
				crearMensajeError(campo, 'Este campo sólo puede contener caracteres alfanuméricos\ny algunos símbolos de puntuación.');
				res = false;
			}
		}
		else if ( trim(condiciones[i]).toUpperCase() == "ALFANUMERICO" )
		{
			if ( /[0-9\w\!\$\&().,;:\-_áéíóúñÑÁÉÍÓÚüÜ\s]+/.test(campo.val()) || campo.val().length==0 ) {}
			else
			{
				crearMensajeError(campo, 'Este campo sólo puede contener caracteres alfanuméricos');
				res = false;
			}
		}
		else if ( trim(condiciones[i]).toUpperCase() == "CP" )
		{
			if ( /^[0-9]{5}$/.test(campo.val()) || campo.val().length==0 ) {}
			else
			{
				crearMensajeError(campo, 'Debe proporcionar un CP válido.');
				res = false;
			}
		}
		else if ( trim(condiciones[i]).toUpperCase() == "TELEFONO" )
		{
			if ( campo.attr('data-origen')!=undefined && campo.attr('data-origen').toUpperCase() == "MX" )
			{
				// para teléfonos de méxico la regla es que debe tener 10 digitos
				if ( /^[0-9(\+)\-#,\.\sa-zA-Z.]{10}$/.test(campo.val()) || campo.val().length==0 ) {}
				else
				{
					crearMensajeError(campo, 'Debe proporcionar un teléfono válido (máximo 10 caracteres).');
					res = false;
				}
			}
			else
			{
				if ( /[0-9(\+)\-#,\.\sa-zA-Z.]+/.test(campo.val()) || campo.val().length==0 ) {}
				else
				{
					crearMensajeError(campo, 'Debe proporcionar un teléfono válido.');
					res = false;
				}
			}
		}
		else if ( trim(condiciones[i]).toUpperCase() == "EMAIL" )
		{
			if ( /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test(campo.val()) || campo.val().length==0 ) {}
			else
			{
				crearMensajeError(campo, 'El correo electrónico no es válido.');
				res = false;
			}
		}
		else if ( trim(condiciones[i]).toUpperCase() == "NUMERICO" || trim(condiciones[i]).toUpperCase() == "NUMERO" )
		{
			if ( /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(campo.val()) || campo.val().length==0 ) {}
			else
			{
				crearMensajeError(campo, 'Este campo sólo puede contener números.');
				res = false;
			}
		}
		else if ( trim(condiciones[i]).toUpperCase() == "NOMBRE" || trim(condiciones[i]).toUpperCase() == "APELLIDO" || trim(condiciones[i]).toUpperCase() == "MUNICIPIO" || trim(condiciones[i]).toUpperCase() == "LOCALIDAD" )
		{
			if ( /[0-9(\+)áÁéÉíÍóÓúÚàèìòùÀÈÌÒÙ'çÇ\-#,\.\sa-zA-Z.]+/.test(campo.val()) || campo.val().length==0 ) {}
			else
			{
				crearMensajeError(campo, 'Este campo sólo puede contener números.');
				res = false;
			}
		}
		else if ( trim(condiciones[i]).toUpperCase() == "URL" )
		{
			if ( /https?:\/\/([-\w\'\.]+)+(:\d+)?(\/([\w\/_\.]*(\?\S+)?)?)?/.test(campo.val()) || campo.val()=='' ) {}
			else
			{
				crearMensajeError(campo, 'Este campo debe ser URL.');
				res = false;
			}
		}
		else if ( trim(condiciones[i]).toUpperCase() == "SEXO" )
		{
			if ( /\w/.test(campo.val()) ) {}
			else
			{
				crearMensajeError(campo, 'Este campo sólo puede contener números.');
				res = false;
			}
		}
		else if ( trim(condiciones[i]).toUpperCase() == 'TAMANHO' )
		{
			ocultarMensajeError(campo);
			if ( !jQuery.browser.msie )
			{
				// validamos el tamaño
				if ( campo.get(0).files.length > 0 )
				{
					var tam = campo.get(0).files[0].size;

					if ( tam > 30000000 )
					{
						crearMensajeError(campo, 'El archivo es demasiado grande.');
						res = false;
					}
				}
			}
		}
	}

	return res;
} // fin de la función validarCampo

function crearMensajeError(campo, mensaje)
{
	//campo.addClass('inputRequired');
	campo.addClass('error');
	campo.addClass('cruz');
	campo.removeClass('success');
	campo.removeClass('paloma');

	if ( jQuery(campo).hasClass( "isdate" ) )
	{
	}
	else
	{
		if( campo.is(':radio') || campo.is(':checkbox') )
		{
		}
		else
		{
			// reseteamos, en caso de que ya tenga
			campo.find('.validator').remove();

			// creamos el nuevo validador y lo agregamos
			/*var validador = protovalida.clone();
			validador.find('.mensaje').html( mensaje );
			campo.after( validador );*/
		}
	}
} // fin de la función crearMensajeError()


function ocultarMensajeError(ele)
{
	ele = jQuery(ele);
	//ele.removeClass('inputRequired');
	ele.removeClass('error');
	ele.removeClass('cruz');
	ele.addClass('success');
	ele.addClass('paloma');

	if ( ele.next().hasClass('validador' ) )
		ele.next().remove();
		//ele.find('.validador').remove();
}
