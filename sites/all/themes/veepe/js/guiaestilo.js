function btn_regresar(url)
{
  document.getElementById("flecha_regresar").style.display = "inline-block";
  document.getElementById("boton_regresar").style.display = "inline-block";
  document.getElementById("boton_regresar").href = url;
  document.getElementById("logoHeader").style.display = "none";
}

function input_checkbox(span, input)
{
  jQuery(span).click(function()
  {
    if(jQuery(input).is(':checked'))
    {
      jQuery(input).prop('checked', false);
		}
    else
    {
      jQuery(input).prop('checked', true);
		}
  });
}

/*function limpiar_input(elem)
{
	jQuery(elem).val('');
	jQuery(elem).removeClass('error');
	jQuery(elem).removeClass('cruz');
	jQuery(elem).removeClass('sucess');
	jQuery(elem).removeClass('paloma');
}*/

jQuery(document).ready(function ()
{
  //Checkbox *******************************************************************
  input_checkbox('#span_checkbox1', '#input_checkbox1');
  input_checkbox('#span_checkbox2', '#input_checkbox2');
  input_checkbox('#span_checkbox3', '#input_checkbox3');
  input_checkbox('#span_checkbox4', '#input_checkbox4');
  input_checkbox('#span_checkbox5', '#input_checkbox5');
  input_checkbox('#span_checkbox6', '#input_checkbox6');
  input_checkbox('#span_checkbox7', '#input_checkbox7');
  input_checkbox('#span_checkbox8', '#input_checkbox8');

  //Radio **********************************************************************
  input_checkbox('#span_radio1', '#input_radio1');
  input_checkbox('#span_radio2', '#input_radio2');
  input_checkbox('#span_radio3', '#input_radio3');
  input_checkbox('#span_radio4', '#input_radio4');

  //Tooltips *******************************************************************
  jQuery('.validador').tooltip();
  jQuery('[data-toggle="tooltip"]').tooltip();

  //Date Picker ****************************************************************
  jQuery('.datepicker').datepicker();

});
