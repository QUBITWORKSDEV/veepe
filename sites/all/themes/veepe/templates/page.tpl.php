<?php
if ($logged_in) {
	echo '<style>body { background-image:url(' . drupal_get_path('theme', 'veepe') . '/img/bg/mapa_fondo.jpg) }</style>';
}

$mostrarMenu = true;
if ( isset($_REQUEST["pass-reset-token"]) ) $mostrarMenu = false;
if ( strpos($_SERVER["REQUEST_URI"], "recuperar_nip")!==false ) $mostrarMenu = false;
if( esSoporte() ) $mostrarMenu = false;
?>


<!-- ******************************* DOCUMENTO ***************************** -->
<?php print $messages; ?>
<div id="documento" style="margin-top: 56px;">

	<!-- ******** header y navs ******** -->
	<!-- HEADER Y NAVS ******************************************************* -->
	<?php if ($logged_in) { ?>
		<header>
			<!-- logo veepe -->
			<span id="logoHeader" class="logoHeader"></span>

			<!-- boton regresar -->
			<div class="">
				<div id="flecha_regresar" class="flecha_regresar" style="position: absolute; margin-top: 4px; margin-left: 24px; width: 48px; height: 48px; line-height: 56px; display: none;"></div>
				<a id="boton_regresar" href="#" style="position: absolute; margin-left: 64px; color: white; line-height: 56px; text-decoration: none; display: none;">
					Regresar
				</a>
			</div>

			<!-- logout -->
			<a href="./?q=user/logout" class="pull-right" style="margin-right: 32px; color: white; line-height: 56px; text-decoration: none;">Cerrar sesión</a>
		</header>
	<?php } ?>

	<!-- MENU GENERAL ******************************************************** -->
	<?php if ($logged_in && $mostrarMenu) { ?>

	<div id="menuLateral" class="menu">
		<div class="opcion" data-target="">
			<div class="icono home"></div><div class="leyenda">INICIO</div><div class="flechita"></div>
		</div>

		<div class="opcion" data-target="perfil">
			<div class="icono perfil"></div><div class="leyenda">PERFIL</div><div class="flechita"></div>
		</div>

		<?php if (esAgv() || esSocio() || esSupervisor() ) { ?>
		<div class="opcion" data-target="empleados">
			<div class="icono empleados"></div><div class="leyenda">EMPLEADOS</div><div class="flechita"></div>
		</div>
		<?php } ?>

		<?php if (esAgv() || esAva()) { ?>
		<div class="opcion" data-target="clientes">
			<div class="icono empleados"></div><div class="leyenda">CLIENTES</div><div class="flechita"></div>
		</div>
		<?php } ?>

		<?php if (esSocio() || esSupervisor()) { ?>
		<div class="opcion" data-target="estacionamientos">
			<div class="icono estacionamientos"></div><div class="leyenda">ESTACIONAMIENTOS</div><div class="flechita"></div>
		</div>
		<?php } ?>
		
		
		<?php if (esSocio() || esSupervisor()) { ?>
		<div class="opcion" data-target="pensionados">
			<div class="icono pensionados"></div><div class="leyenda">PENSIONADOS</div><div class="flechita"></div>
		</div>
		<?php } ?>
		
	
		<?php if (esAgv() || esAva() || esSocio() || esSupervisor()) { ?>
		<div class="opcion" data-target="panel_estadisticas">
			<div class="icono estadisticas"></div><div class="leyenda">ESTADÍSTICAS</div><div class="flechita"></div>
		</div>
		<?php } ?>

		<?php if (esAgv() || esAva() || esSocio() || esSupervisor()) { ?>
		<div class="opcion" data-target="reportes">
			<div class="icono reportes"></div><div class="leyenda">REPORTES</div><div class="flechita"></div>
		</div>
		<?php } ?>

		<?php if (esSocio() || esSupervisor()) { ?>
		<div class="opcion" data-target="cpersonal">
			<div class="icono reportes"></div><div class="leyenda">CONTROL DE PERSONAL</div><div class="flechita"></div>
		</div>
		<?php } ?>

		<?php if (esAgv() || esAva()) { ?>
		<div class="opcion" data-target="semaforo">
			<div class="icono semaforo"></div><div class="leyenda">SEMÁFORO</div><div class="flechita"></div>
		</div>
		<?php } ?>

		<?php if (false) { ?>
		<div class="opcion" data-target="">
			<div class="icono ayuda"></div><div class="leyenda">AYUDA</div><div class="flechita"></div>
		</div>
		<?php } ?>

		<?php /*if (esAgv() || esAva() || esSocio() || esSupervisor()) {*/ ?>
		<?php if (false) { ?>
		<div class="opcion" data-target="">
			<div class="icono promociones"></div><div class="leyenda">PROMOCIONES</div><div class="flechita"></div>
		</div>
		<?php } ?>

	</div>

	<?php } ?>

	<!--CONTENIDO ************************************************************ -->
	<?php print render($page["content"]); ?>

	<!--FOOTER *************************************************************** -->
	<?php if ($logged_in) { ?>
		<footer class="">
		</footer>
	<?php } ?>

</div>

<script>
	"use strict";

// _____________________________________________________________________________________________________________ VARIABLES GLOBALES

// _____________________________________________________________________________________________________________ FUNCIONES GLOBALES

// _____________________________________________________________________________________________________________ JQUERY
	jQuery(document).ready(function ()
	{
		// _____________________________________________________________________ configuraciones
		jQuery('div.menu .opcion').click(function () {
			var target = jQuery(this).data('target');
			
			if ( target == null )
				target = '';
			else
			{
				var path = window.location.pathname.split('/');
				// estamos en la misma sección?
				if ( target.indexOf(path[path.length-1]) == -1 )
					window.location = './' + target;
			}
			
		});
		// _____________________________________________________________________ entrypt
	});// fin del document.ready
</script>
