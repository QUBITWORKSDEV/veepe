<?php

/**
 * @file
 * Default theme implementation to format an HTML mail.
 *
 * Copy this file in your default theme folder to create a custom themed mail.
 * Rename it to mimemail-message--[module]--[key].tpl.php to override it for a
 * specific mail.
 *
 * Available variables:
 * - $recipient: The recipient of the message
 * - $subject: The message subject
 * - $body: The message body
 * - $css: Internal style sheets
 * - $module: The sending module
 * - $key: The message identifier
 *
 * @see template_preprocess_mimemail_message()
 */
?>
<html style="padding:0px">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <?php if ($css): ?>
    <style type="text/css">
      <!--
      <?php print $css ?>
      -->
    </style>
    <?php endif; ?>
  </head>
  <body id="mimemail-body" <?php if ($module && $key): print 'class="'. $module .'-'. $key .'"'; endif; ?> style="background-color: white; color: white; text-align: center; font-family: sans-serif, Arial; padding:0px; margin:0px">
    <center><div id="center" style="width:600px; background-color: #090A19">

		<!-- header -->
		<div style="padding: 16px; border-bottom: 8px solid #00BFD6">
			<img src="http://veepe.mx/logo_header_mailing.png" height="152px" />
		</div>

      <div id="main" style="background-color: white; text-align: justify; padding: 24px 40px; border-bottom: 10px solid #414143; color: #414042">
        <?php print $body ?>
      </div>

		<!-- footer -->
		<div id="footer" style="background-color: #070a17; padding: 24px;">
			<img src="http://veepe.mx/logo_footer.png" />
		</div>

    </div></center>
  </body>
</html>

<!--

<html style="padding:0px">
	<head></head>
	<body style="background-color: white; color: white; text-align: center; font-family: sans-serif, Arial; padding:0px; margin:0px">
		<center><div style="width:600px; background-color: #090A19">
			<!-- header 
			<div style="padding: 16px; border-bottom: 8px solid #00BFD6">
				<img src="http://veepe.mx/logo_header_mailing.png" height="152px" />
			</div>
			
			<!-- cuerpo 
			<div style="background-color: white; text-align: justify; padding: 24px 40px; border-bottom: 10px solid #414143">
				<div style="font-weight: 100; font-size: 36px; color: #090A19">#TITULO#</div>
				<br>
				<div style="font-size: 18px; color: #414042">#CUERPO#</div>
			</div>
			
			<!-- footer 
			<div id="footer" style="background-color: #070a17; padding: 24px;">
				<img src="http://veepe.mx/logo_footer.png" />
			</div>
		</div></center>
	</body>
</html>

-->