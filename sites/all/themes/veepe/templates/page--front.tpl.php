<?php
global $user;

//print_r($user->roles);

if ( $user->uid )
{
	if ( in_array("admin_general_veepe", $user->roles) )
		drupal_goto("agv_inicio");

	else if ( in_array("admin_veepe", $user->roles) )
		drupal_goto("av_inicio");

	else if ( in_array("socio_veepe", $user->roles) )
		drupal_goto("socio_inicio");

	else if ( in_array("supervisor", $user->roles) )
		drupal_goto("supervisor_inicio");

	else if ( in_array("usuario", $user->roles) )
		drupal_goto("uf_inicio");

	else if ( in_array("jefe_estacionamiento", $user->roles) )
		drupal_goto("je_inicio");

	else if ( in_array("operador", $user->roles) )
		drupal_goto("op_inicio");
	
	else if ( in_array("soporte", $user->roles) )
		drupal_goto("soporte");
}
?>

<style>
	.footer {
		margin-top: 0px;
		padding-top: 0px;
		padding-bottom: 0x;
		border-top: none;
	}
</style>


<!-- ********************* DOCUMENTO *********************** -->
<?php print $messages; ?>
	<div id="overlayTerminos" class="overlay" style="padding: 80px">
		<button class="cmdCerrar" type="button" style="background-image: url('<?php echo drupal_get_path("theme", "veepe"); ?>/img/botones/b_close_n.svg'); width:30px; height: 30px; position: absolute; right: 20px; top:20px; cursor: pointer; background-color: transparent; border: none"></button>
		<div class="contenido" style="padding:20px 10px 0 0; color: white; text-align: justify; overflow: auto; max-height: 100%">
<b>Términos y Condiciones de los Servicios de Veepe Parking</b><br>
<br>
Los siguientes términos y condiciones aplican para el uso de la plataforma y aplicación móvil (en conjunto la “Aplicación”) proporcionados por [VEEPE PARKING S.A.P.I. de C.V.] (“Veepe Parking”). Usted es el único responsable de su conducta en la Aplicación y del cumplimiento de estos términos y condiciones.<br>
<br>
Al usar o explorar la Aplicación, usted expresamente reconoce que leyó, comprendió y aceptó estar obligado por estos términos y condiciones.<br>
<br>
Veepe Parking provee el servicio de localización y reserva de estacionamientos, contratación de servicios de pensión y lavado de vehículos, así como el pago en línea de los servicios referidos (los “Servicios”).<br>
<br>
1. Términos Generales.<br>
<br>
Al usar la Aplicación, usted está de acuerdo en que la información que proporcione a través de la Aplicación podrá ser usada como mejor determine Veepe Parking, en los términos aquí señalados.<br>
<br>
La descarga de la Aplicación es gratuita <br>
<br>
Los Servicios proporcionados por Veepe Parking podrán cambiar sin previo aviso o bien, Veepe Parking podrá dejar de proporcionar los Servicios tanto a usted como a la generalidad de los usuarios, sin previo aviso.<br>
<br>
Aún y cuando la intención de Veepe Parking es la prestar los Servicios, usted reconoce que existirán ocasiones en las cuales los Servicios puedan ser interrumpidos, incluyendo enunciativa más no limitativamente, por mantenimiento, actualizaciones, reparaciones o por fallas en los equipos, interrupciones de la señal o cualquier otra, por las cuales los usuarios no podrán emitir reclamación alguna.<br>
<br>
Usted reconoce que su relación con Veepe Parking no es confidencial, fiscal, ni ningún tipo de relación especial, y la aceptación del uso que Veepe Parking dé a la información que usted proporcione a través de la Aplicación, de conformidad con los presentes Términos y Condiciones, no lo hace diferente a la posición en la cual se encuentra cualquier otro usuario de los Servicios.<br>
<br>
Es probable que para proporcionar la información contenida en la Aplicación Veepe Parking hubiere contratado a terceros para realizar los estudios e investigaciones correspondientes, así como los dibujos, diseños, sonidos, videos, textos, o fotografías, que se muestren en la Aplicación. Veepe Parking , advierte que cualquier material o contenido que no sea de su titularidad, ni que haya sido desarrollado por Veepe Parking  , podrían no ser veraces o no estar actualizados, por lo que Veepe Parking no se hace responsable.<br>
<br>
Es importante señalar que los Servicios derivan en otros servicios prestados por terceros, es decir, los servicios particulares de estacionamiento, lavado y operación de vehículos es prestado por terceros ajenos a Veepe Parking (“Servicios de Terceros”) sobre los cuales Veepe Parking  no tiene control alguno. Por lo tanto, Veepe Parking no será responsable de forma alguna de los Servicios de Terceros.<br>
<br>
2. Derechos.<br>
<br>
Sujeto a lo establecido en los presentes Términos y Condiciones, Veepe Parking le otorga a usted el derecho intransferible y no exclusivo para acceder de forma remota y utilizar los Servicios que Veepe Parking ofrece mediante una cuenta de usuario.<br>
<br>
<br>
3. Responsabilidades del usuario. <br>
<br>
Mediante los presentes Términos y Condiciones, usted acepta que:<br>
<br>
Usted no utilizará el servicio de Veepe Parking para proporcionar información a terceros, sin autorización escrita por parte de Veepe Parking.<br>
<br>
Usted deberá acatar y seguir todas las normas de buena conducta socialmente aceptadas, así como todas las disposiciones legales aplicables.<br>
<br>
Usted deberá mantener y salvaguardar toda información marcada como confidencial.<br>
<br>
Los Servicios se proporcionan “tal cual” por lo cual Veepe Parking no da garantía alguna, ya sea expresa o implícita, que no se encuentre contenida en los presentes Términos y Condiciones, incluidas las garantías implícitas de comerciabilidad, idoneidad para un fin particular y no violación de disposiciones legales.<br>
<br>
Veepe Parking no será responsable por cualquier daño material, pérdida o robo del vehículo del usuario, toda vez que Veepe Parking funge como intermediario entre el usuario y los Servicios de Terceros para los Servicios, por lo cual Veepe Parking no puede garantizar que los Servicios de Terceros sean del agrado o satisfacción absoluta del usuario, ya que Veepe Parking no tiene control directo sobre los Servicios de Terceros.<br>
<br>
4. Prohibiciones del usuario.<br>
<br>
Usted acepta que no publicará, enviará por correo electrónico, pondrá a disposición ningún contenido ni usará la Aplicación:<br>
<br>
-Publicar información falsa, difamatoria o ilegal.<br>
<br>
-De forma que infrinja, viole o se apropie indebidamente de cualquier derecho de propiedad intelectual u otros derechos contractuales o de propiedad de terceros.<br>
<br>
-De forma que sea injurioso o difamatorio o de forma que sea de otro modo amenazante, abusiva, violenta, acosadora, maliciosa o dañina para cualquier persona o entidad o invasiva de la privacidad de otra persona.<br>
<br>
-De forma que sea perjudicial para los demás usuarios de cualquier manera.<br>
<br>
-Para suplantar a cualquier otra persona, o afirmar falsamente o de otro hacer declaraciones falsas de su afiliación con otra persona o entidad o para obtener acceso a la Aplicación sin autorización.<br>
<br>
-Para interferir o intentar interferir con el funcionamiento correcto de la Aplicación o impedir que otros usen la Aplicación o de una manera que interrumpa el flujo normal o el diálogo con un número excesivo de mensajes (ataque de inundación) a la Aplicación o que de otro modo afecte la capacidad de otras personas de usar la Aplicación.<br>
<br>
-Para usar cualquier medio manual o automatizado, incluidos agentes, robots, secuencias de comandos o arañas para tener acceso o administrar la cuenta de cualquier usuario o para supervisar o copiar la Aplicación o el contenido que esta incluye; <br>
<br>
-Para facilitar la distribución ilegal de contenido de información sensible o confidencial.<br>
<br>
-En una manera que incluya información personal o de identificación sobre otra persona sin el consentimiento explícito de esa persona; <br>
<br>
Asimismo, usted acepta no:<br>
<br>
-Acechar o acosar a persona alguna a través de la Aplicación.<br>
<br>
-Usar medios automatizados, incluidos arañas, robots, crawlers, herramientas de minería de datos o similares para descargar datos desde la Aplicación.<br>
<br>
-Intentar obtener acceso no autorizado a nuestros sistemas informáticos o participar en cualquier actividad que interrumpa, disminuya la calidad o interfiera con el rendimiento o perjudique la funcionalidad de la Aplicación.<br>
<br>
-Desarrollar, invocar o utilizar código para interrumpir, disminuir la calidad, interferir con el rendimiento, o perjudicar la funcionalidad de la Aplicación.<br>
<br>
Usted acepta no autorizar ni recomendar a un tercero a usar la Aplicación para facilitar cualquiera de las anteriores conductas prohibidas. Además, usted acepta que estos términos de servicio de la Aplicación entrarán en vigor para el beneficio de los usuarios y de los Servicio y que Veepe Parking tomar las medidas que consideren adecuadas, incluyendo la eliminación de su contenido y la inhabilitación de su cuenta, con el fin de mantener el cumplimiento de estos términos de servicio de la Aplicación.<br>
<br>
5. Registro.<br>
<br>
Para poder hacer uso de los Servicios, es indispensable que usted se registre en la aplicación lo cual podrá realizar mediante ingreso a la cesión de la red social denominada “Facebook” o a través de un correo electrónico, del cual el usuario es titular, y el llenado de un formulario.<br>
<br>
Será obligación de cada usuario registrarse proporcionando a Veepe Parking determinada información personal (nombre, edad, número de teléfono móvil, método de pago, datos del vehículo, entre otros) la cual deberá ser verás y será tratada de conformidad con el Aviso de Privacidad de Veepe Parking, mismo que puede encontrar en [*].<br>
<br>
6. Menores de edad. <br>
<br>
Los menores de edad (personas de menos de 18 años) pueden usar la Aplicación, siempre que obtengan el consentimiento de sus padres o tutores para el uso del servicio. Los menores que no cuenten con el consentimiento de sus padres o tutores no deben utilizar los Servicios, hasta que alcancen la edad de 18 años.<br>
<br>
Al usar la Aplicación, usted declara que es mayor de 18 años y en caso de ser menor de 18 años, usted declara que cuenta con la autorización expresa de sus padres o de quien ejerce la patria potestad sobre usted.<br>
<br>
7. Pagos.<br>
<br>
Usted realizará el pago por los Servicios mediante la Aplicación, ingresando una tarjeta de crédito o débito. La Aplicación se apoya en el servicio Banwire o cualquier plataforma para el procesamiento de pagos (el “Procesador de Pagos”), por lo cual Veepe Parking no será responsable en caso de que dicha aplicación no se encuentre funcional o presente fallas técnicas.<br>
<br>
El Procesador de Pagos es una entidad ajena a Veepe Parking por lo cual Veepe Parking no podrá ser responsable en el caso de que el Procesador de Pagos realice algún cargo indebido a los usuarios.<br>
<br>
Los datos de la/s tarjeta/s que ingrese deberán ser verdaderos y el titular deberá ser el usuario, Veepe Parking no será responsable en caso de que los usuarios hagan uso de información de tarjetas que hayan sido robadas u obtenidas por cualquier otro medio considerado ilegal. <br>
<br>
Cuando la tarjeta que haya introducido para el pago de los Servicios no cuente con fondos suficientes para cubrir los mismos Veepe Parking suspenderá la cuenta hasta que usted pague los Servicios adeudados.<br>
<br>
8. Contenido de Terceros.<br>
<br>
El contenido de los Servicios anunciados en la Aplicación son propiedad de terceros que por el simple hecho de anunciarlos en la Aplicación autorizan a Veepe Parking el uso de los mismos, mediante una licencia gratuita, no exclusiva de uso y reproducción del contenido proporcionado.<br>
<br>
Asimismo, Veepe Parking podrá incluir anuncios en la Aplicación los cuales podrán dirigir a páginas propiedad de terceros ajenos a Veepe. Parking En ningún momento Veepe Parking será responsable por el contenido de dichas páginas o vínculos.<br>
<br>
9. Sanciones.<br>
<br>
En el caso de que usted incumpla con los presentes Términos y Condiciones se podrá hacer acreedor a la suspensión temporal o definitiva de su cuenta, dicha sanción será impuesta por 
Veepe Parking, quien definirá la gravedad de la falta incurrida, así como la sanción correspondiente.<br>
<br>
10. Indemnización.<br>
<br>
Usted y cualquier usuario de los Servicios, acepta defender, indemnizar y mantener a salvo a 
Veepe Parking , sus filiales, subsidiarias, funcionarios, directores, empleados, agentes, socios, contratistas y licenciantes, frente a cualquier reclamación o demanda, incluidos los honorarios razonables de abogados, interpuesta por terceros, que se derive o surja de: (a) cualquier contenido de la Aplicación que envíe, publique, transmita o ponga a disposición de algún tercero; (b) el uso que haga de la Aplicación; (c) cualquier infracción por su parte del presente contrato; (d) cualquier medida tomada por Veepe Parking como parte de su investigación de un supuesto incumplimiento o como resultado de su detección o decisión de incumplimiento del presente contrato; o (e) su violación de los derechos de otros. <br>
<br>
De igual forma usted no puede interponer una acción legal contra Veepe Parking , sus filiales, subsidiarias, funcionarios, directores, empleados, agentes, socios, contratistas y licenciantes como resultado de una decisión por parte de Veepe Parking   de eliminar o rehusarse a procesar cualquier información o contenido, advertirle, suspender o terminar su acceso al Sitio, o tomar cualquier otra medida durante la investigación de un supuesto incumplimiento o como resultado de la conclusión por Veepe Parking  de la existencia de un incumplimiento del presente Contrato. <br>
<br>
La presente cláusula de renuncia e indemnización se aplican a todas las violaciones descritas o contempladas en el presente contrato. Dicha obligación subsistirá la terminación o expiración del presente contrato y/o el uso que haga de la Aplicación. Usted reconoce que es responsable de todo uso que haga de la Aplicación mediante su cuenta, y que el presente contrato aplica a cualquier uso que haga de su cuenta. Usted acuerda cumplir con lo previsto en el presente contrato y defender, indemnizar y mantener a salvo a Veepe Parking frente a cualquier reclamación o demanda que surja como consecuencia del uso de su cuenta, tanto si dicho uso está expresamente autorizado por el usuario como si no lo está.<br>
<br>
11. Resolución de Conflictos. <br>
<br>
El usuario al hacer uso de los Servicios, acepta de manera expresa, someterse en caso de cualquier controversia, a la jurisdicción de los tribunales federales de la Ciudad de México, así como las leyes federales aplicables para el caso concreto, renunciando expresamente a cualquier otra jurisdicción que por motivo de su nacionalidad o domicilio pudiera corresponder.<br><br><br>
		</div>
	</div>
<!doctype html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Estacionamiento Sin Complicaciones Veepe</title>
	<meta name="description" content="Estacionamiento y servicio de valet parking por medio de una app gratuita">
    	<meta name="keywords" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Favicon -->
	<link rel="shortcut icon" type="image/x-icon" href="img/favicon-veepe.ico">
	
	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
	
	<!-- css  -->
	<!-- Bootstrap CSS
	============================================ -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<!-- Icon Font CSS
	============================================ -->
	<link rel="stylesheet" href="css/material-design-iconic-font.min.css">
	<link rel="stylesheet" href="css/appup-icon.css">
	<!-- Animate CSS
	============================================ -->
	<link rel="stylesheet" href="css/animate.min.css">
	<!-- Owl Carousel CSS
	============================================ -->
	<link rel="stylesheet" href="css/slick.css">
	<!-- Magnific Popup CSS
	============================================ -->
	<link rel="stylesheet" href="css/magnific-popup.css">
	<!-- Style CSS
	============================================ -->
	<link rel="stylesheet" href="style.css">
	<!-- Responsive CSS 
	============================================ -->
	<link rel="stylesheet" href="css/responsive.css">
	<!-- Colors CSS 
	============================================ -->
    <link rel="stylesheet" href="css/color/color-1.css" title="color-1" media="screen" />
	<!-- Modernizer JS
	============================================ -->
	<script src="js/vendor/modernizr-2.8.3.min.js"></script>
	
</head>
<body>
<div class="main-wrapper dark-layout">
<!-- Pre Loader
============================================ -->
<div class="preloader bg-dark">
	<div class="loading-center">
		<div class="load ing-center-absolute">
			<div class="object object_one"></div>
			<div class="object object_two"></div>
			<div class="object object_three"></div>
		</div>
	</div>
</div>
<!-- Header Area
============================================ -->
<div id="header" class="header text-center bg-dark">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="logo float-left">
					<a href="index.html"><img src="img/aplicacion-estacionamiento-servicio-de-valet-parking-veepe-parking.png" alt="logo" /></a>
				</div>
				<div class="menu-wrapper">
					<button class="menu-toggle hamburger hamburger--arrowalt-r hidden-lg hidden-md">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
					<div class="main-menu">
						<nav>
							<ul>
								<li class="active"><a href="#hero">Inicio</a></li>
								<li><a href="#beneficios">Beneficios</a></li>
								<li><a href="#comoFunciona">Cómo Funciona</a></li>
								<li><a href="#pensionesCompartidas">Pensiones Compartidas</a></li>
								<li><a href="#pantallas">Pantallas</a></li>
								<li><a href="#contact">Contacto</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<span class="menu-overlay"></span>
<!-- Hero Area
============================================ -->
<div id="hero" class="hero-area bg-fixed text-center">
	<div class="container">
		<div class="row">
			<div class="hero-content">
				<img src="img/aplicacion-estacionamiento-servicio-de-valet-parking-veepe-parking.png" alt="logo" />
				<h1>ESTACIÓNATE SIN COMPLICACIONES</h1> <h1>DESCÁRGALA TOTALMENTE GRATIS</h1>
				<a href="https://play.google.com/store/apps/details?id=com.funktionell.veepeapp" target="_blank" > <img src="img/valet-df-estacionamiento-veepe-parking.png"></a> 
				<a href="https://itunes.apple.com/us/app/veepe/id1175662060?ls=1&mt=8" target="_blank"><img src="img/estacionamiento-publico-app-parquimetro-veepe-parking.png"></a>
			</div>
		</div>
	</div>
</div>

<!-- Feature Area
============================================ -->
<div id="beneficios" class="feature-area bg-dark-gray ptb-100">
	<div class="container">
		<div class="row">
			<div class="section-title title-black text-center col-xs-12">
				<h1>LOS BENEFICIOS DE ESTACIONARTE CON VEEPE</h1>
				
			</div>
			<div class="feature-wrap feature-left col-lg-4 col-lg-offset-0 col-md-6 col-md-offset-3 col-xs-12">
				<div class="single-feature fix">
					<div class="icon">
                    	<img class="icon2" src="img/valet-df-app-parquimetro-veepe-parking.png">
                    </div>
					<div class="content fix">
						<p>Encuentra rápidamente 
estacionamiento.</p>
					</div>
				</div>
				<div class="single-feature fix">
					<div class="icon">
                    	<img class="icon2" src="img/valet-df-app-parquimetro-veepe-parking.png">
                    </div>
					<div class="content fix">
						<p>Reserva tu lugar desde antes de salir de casa. </p>
					</div>
				</div>
				<div class="single-feature fix">
					<div class="icon">
                    	<img class="icon2" src="img/valet-df-app-parquimetro-veepe-parking.png">
                    </div>
					<div class="content fix">
					<p>Obtén mayor seguridad de servicio con tu ticket digital.</p>
					</div>
				</div>
				<div class="single-feature fix">
					<div class="icon">
                    	<img class="icon2" src="img/valet-df-app-parquimetro-veepe-parking.png">
                    </div>
                    <div class="content fix">
						<p>No más filas para pagar o esperar tu auto.</p>
					</div>
				</div>
			</div>
		  <div class="feature-image text-center col-lg-4 col-xs-12">
				<img src="img/app-estacionamiento-empresas-de-valet-parking-veepe-parking.png"alt="App Estacionamiento Empresas de Valet Parking Veepe Parking"/>
			</div>
			
			<div class="feature-wrap feature-right col-lg-4 col-lg-offset-0 col-md-6 col-md-offset-3 col-xs-12">
			  <div class="single-feature fix">
					<div class="icon">
                    	<img class="icon2" src="img/valet-df-app-parquimetro-veepe-parking.png">
                    </div>
					<div class="content fix">
						<p>Consulta el tiempo exacto a pagar en tu móvil.</p>
					</div>
				</div>
				<div class="single-feature fix">
				    <div class="icon">
                    	<img class="icon2" src="img/valet-df-app-parquimetro-veepe-parking.png">
                    </div>
					<div class="content fix">
						<p>Solicita tu auto desde el celular. </p>
					</div>
			  </div>
			<div class="single-feature fix">
                    		<div class="icon">
                    			<img class="icon2" src="img/valet-df-app-parquimetro-veepe-parking.png">
                    		</div>
				<div class="content fix">
					<p>Olvídate de pagar por boletos perdidos. </p>
				</div>
			</div>
		  </div>
		</div>
	</div>
</div>
<!-- About App Area
============================================ -->
<div id="comoFunciona" class="about-app-area bg-dark ptb-100">
	<div class="container">
		<div class="row">
			<div class="section-title title-black text-center col-xs-12">
				<h1>Fácil de usar</h1>
			</div>
			<div class="about-app-image text-right col-lg-6 col-md-5 col-xs-12">
				<img src="img/estacionamiento-aplicacion-parquimetro-veepe-parking.png" alt="Estacionamiento Aplicacion Parquimetro Veepe Parking" />
			</div>
			<div class="about-app-content text-left col-lg-6 col-md-7 col-xs-12">
				<div class="single-about-app fix">
				  <div class="icon"> <img class="icon3" src="img/parking-aeropuerto-servicio-de-valet-parking-veepe-parking.png"> </div>
				  <div class="content fix">
					<h4>Abre VEEPE</h4>
						<p>Regístrate o inicia sesión con Facebook y comienza a disfrutar del 
beneficio de estacionarte sin complicaciones. </p>
				  </div>
			  </div>
				<div class="single-about-app fix">
					<div class="icon"> <img class="icon4" src="img/valet-df-estacionamiento-publico-veepe-parking.png"> </div>
					<div class="content fix">
						<h4>Encuentra estacionamiento</h4>
						<p>Busca en el mapa tu ubicación y selecciona el estacionamiento de la 
red VEPEE de tu preferencia y dirígete hacia el. </p>
					</div>
				</div>
				<div class="single-about-app fix">
					<div class="icon"> <img class="icon5" src="img/aplicacion-estacionamiento-parking-valet-veepe-parking.png"> </div>
					<div class="content fix">
						<h4>Deja tu coche en manos seguras</h4>
						<p>Nuestro Valet recibirá tu vehículo, obtendrás un registro fotográfico y 
un listado de pertenencias por medio de tu Ticket Digital en VEEPE, 
¡No es necesario esperar! </p>
					</div>
				</div>
               <div class="single-about-app fix">
				<div class="icon"> <img class="icon6" src="img/estacionamiento-aeropuerto-empresas-de-valet-parking-veepe-parking.png"> </div>
					<div class="content fix">
						<h4>Paga electrónicamente</h4>
						<p>Elije la forma de pago que más te convenga, ya sea débito o crédito, 
VEEPE es totalmente seguro.</p>
					</div>
			  </div>
                <div class="single-about-app fix">
					<div class="icon"> <img class="icon7" src="img/parquimetro-estacionamiento-df-veepe-parking.png"> </div>
					<div class="content fix">
						<h4>Olvídate de esperar</h4>
						<p>Solicita tu auto desde VEEPE con hasta 15 minutos de anticipación, 
¡No esperes más en el Valet!. </p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Team Area
============================================ -->
<div id="pensionesCompartidas" class="team-area bg-fixed ptb-100">
	<div class="container">
		<div class="row">
			<div class="section-title title-white text-center col-xs-12">
				<h1>Las Pensiones compartidas VEEPE</h1>
			</div>
        </div>
        <div class="row">
        	<div class="col-lg-6 col-sm-6 content-pensiones">
            	<div class="single-about-app fix">
                        <div class="icon"> <img class="icon8" src="img/app-parquimetro-valet-df-veepe-parking.png"> </div>
             	</div>
                <div class="content fix">
                    <h4 style="line-height: 1.4em";>No importa donde te encuentres la pensión compartida Veepe te permite 
dejar tu auto en cualquiera de los estacionamientos de nuestra red.</h4>
                    <hr size="1" width="450" style="border-color: #00BDD6 !important;">

                    <p>Estaciónate en cualquier lugar de la red VEEPE 
con el modo pensión compartida. </p>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 content-img">
            	<img src= "img/parking-valet-estacionamiento-publico-veepe-parking.png"alt="parking valet estacionamiento publico veepe parking">
            </div>
        </div>
	</div>
</div>

<!-- Screenshot Area
============================================ -->
<div id="pantallas" class="screenshot-area bg-dark ptb-100">
	<div class="container">
		<div class="row">
			<div class="section-title title-black text-center col-xs-12">
				<h1>Pantallas</h1>
				
			</div>
			<div class="col-xs-12">
				<div class="screenshot-slider">
					<div class="single-screenshot"><div class="image"><img src="img/servicio-de-valet-parking-valet-df-veepe-parking.png" alt="Servicio de Valet Parking Valet DF Veepe Parking.png" /></div></div>
					<div class="single-screenshot"><div class="image"><img src="img/estacionamiento-aeropuerto-app-parquimetro-veepe-parking.png" alt="estacionamiento aeropuerto app parquimetro veepe parking" /></div></div>
					<div class="single-screenshot"><div class="image"><img src="img/parking-aeropuerto-parquimetro-veepe-parking.png" alt="parking aeropuerto parquimetro veepe parking" /></div></div>
					<div class="single-screenshot"><div class="image"><img src="img/parking-ver-mapa-veepe-parking.png" alt="parking ver mapa veepe parking" /></div></div>
                    <div class="single-screenshot"><div class="image"><img src="img/servicio-valet-parking-servicio-activo-veepe.jpg" alt="Servicio-valet parking servicio activo veepe.jpg" /></div></div>
                    <div class="single-screenshot"><div class="image"><img src="img/apliacion-valet-df-entrega-en-proceso-veepe.jpg" alt="Apliacion valet df entrega en proceso veepe" /></div></div>
                    <div class="single-screenshot"><div class="image"><img src="img/servicio-estacionamiento-ticket-principal-veepe.jpg" alt="Servicio estacionamiento ticket principal veepe" /></div></div>
                    <div class="single-screenshot"><div class="image"><img src="img/parking-app-codigo-veepe.jpg" alt="Parking app codigo veepe" /></div></div>
                    <div class="single-screenshot"><div class="image"><img src="img/servicio-valet-parking-calificación-veepe.jpg" alt="Servicio valet parking calificación veepe.jpg" /></div></div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Testimonial Area
============================================ -->
<div id="testimonial" class="testimonial-area #testimonial ptb-100">
	<div class="container">
		<div class="row">
			<div class="section-title title-black text-center col-xs-12">
				<h1>Comentarios de nuestros usuarios</h1>
			</div>
			<div class="testimonial-slider">
				<div class="single-testimonial">
					<div class="st-wrap fix">
						<div class="testimonial-image float-left"><img src="img/aplicacion-parquimetro-estacionamiento-aeropuerto-veepe-parking.png" alt="" /></div>
						<div class="testimonial-content fix">
							<h3>Saúl A</h3>
							<p>Me encantó el diseño y los beneficios. Excelente aplicación.</p>
						</div>
					</div>
				</div>
				<div class="single-testimonial">
					<div class="st-wrap fix">
						<div class="testimonial-image float-left"><img src="img/persona-masculina-app-parquimetro-valet-df-veepe-parking.png" alt="" /></div>
						<div class="testimonial-content fix">
							<h3>Miguel Gómez</h3>
							<p>Contrate mi pensión y me encanto.</p>
						</div>
					</div>
				</div>
				<div class="single-testimonial">
					<div class="st-wrap fix">
						<div class="testimonial-image float-left"><img src="img/persona-femenina-estacionamiento-df-parking-valet-veepe-parking.png" alt="" /></div>
						<div class="testimonial-content fix">
							<h3>Ale Herrera</h3>
							<p>No espere mi coche y llego a tiempo. Muy buena.</p>
						</div>
					</div>
				</div>
                <div class="single-testimonial">
					<div class="st-wrap fix">
						<div class="testimonial-image float-left"><img src="img/persona-masculina-app-parquimetro-valet-df-veepe-parking.png" alt="" /></div>
						<div class="testimonial-content fix">
							<h3>José R.</h3>
							<p>Esta maravillo no tener que cargar efectivo.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Contact Area
============================================ -->
<div id="subscribe" class="subscribe-area bg-dark pt-100">
	<div class="container">
		<div class="row">
			<div class="section-title title-black text-center col-xs-12">
				<h1>¿Si necesitas más información?</h1>
			</div>
        
            <div id="contact" class="contact-area bg-dark ptb-100">
                <div class="container">
                    <div class="row">
                        <div class="contact-info col-md-4 col-md-offset-1 col-sm-8 col-sm-offset-2 col-xs-12">
                            <h2>Detalles de Contacto:</h2>
                            <!--<div><span>Ubicación:</span><p>Newton 293</p></div>-->
                            <!--<div><span>Llama ahora:</span><p>(+88) 376 7234 7809 <br />(+88) 456 8542 4789</p></div>-->
                            <div><span>Mensaje:</span><p><a href="#">info@veepe.mx</a></p></div>
                            <div><span>Facebook :</span><p><!--<i class="zmdi zmdi-time">--><a href="https://www.facebook.com/VeePe-1131041506933079" target="_blank"> Veepe </a></p></div>
                        </div>
                        <div class="contact-form col-md-5 col-md-offset-1 col-sm-8 col-sm-offset-2 col-xs-12">
                            <h2>Contáctanos:</h2>
                            <form id="contact-form" action="mail.php" method="post">
                                <input name="name" type="text" placeholder="Nombre" />
                                <input name="number" type="text" placeholder="Teléfono" />
                                <input name="email" type="email" placeholder="Email" />
                                <textarea name="message" placeholder="Mensaje"></textarea>
                                <p class="form-messege"></p>
                                <div id="buttonFormSend"><button class="submit" type="submit">enviar<i class="zmdi zmdi-mail-send"></i></button></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
		</div>
    </div>
</div>

<!-- Download Area
============================================ -->
<div id="download" class="download-area bg-fixed #download ptb-100">
	<div class="container">
		<div class="row">
			<div class="section-title title-white text-center col-xs-12">
				<h1>Descarga la App</h1>
			</div>
			<div class="download-links text-center col-xs-12">
            <a href="https://play.google.com/store/apps/details?id=com.funktionell.veepeapp" target="_blank" > <img src="img/parking-valet-app-estacionamiento-veepe-parking.png"></a> 
	    <a href="https://itunes.apple.com/us/app/veepe/id1175662060?ls=1&mt=8" target="_blank"><img src="img/aplicacion-estacionamiento-parking-valet-veepe-parking-1.png"></a>
				<!--<a href="#">
                    <i class="zmdi zmdi-windows"></i>
					<div class="text fix">
						<span>Available for</span>
						<h4>Windows Phone</h4>
					</div>
				</a>-->
				<!--<a href="#">
					<i class="zmdi zmdi-android"></i>
					<div class="text fix">
						<span>Available in</span>
						<h4>Android Market</h4>
					</div>
				</a>
				<a href="#">
					<i class="zmdi zmdi-cloud-download"></i>
					<div class="text fix">
						<span>Available to</span>
						<h4>Download Now</h4>
					</div>
				</a>-->
			</div>
		</div>
	</div>
</div>
<!-- Footer Area
============================================ -->
<div id="footer" class="footer-area bg-dark">
	<div class="container">
		<div class="row">
        	<div class="footer-logo">
				<div class="footer-email text-center col-md-4 col-xs-12">
				<div class="mail-icons fix">
					<!--<a href="#"><i class="zmdi zmdi-email"></i></a>
					<a href="#"><i class="zmdi zmdi-rss"></i></a>-->
				</div>
				<img src="img/aplicacion-parquimetro-veepe-parking.png" alt="aplicacion parquimetro veepe parking">
			</div>
           </div>
           <div class="footer-social">
           		<div class="footer-social text-center col-md-4 col-xs-12">
                <h4>Síguenos</h4>
              <a href="https://www.facebook.com/VeePe-1131041506933079" target="_blank"><i class="zmdi zmdi-facebook"></i></a>
				<!--<a href="#"><i class="zmdi zmdi-twitter"></i></a>
				<a href="#"><i class="zmdi zmdi-linkedin"></i></a>
				<a href="#"><i class="zmdi zmdi-dribbble"></i></a>
				<a href="#"><i class="zmdi zmdi-dropbox"></i></a>-->
				</div>
           </div>
	   <div id="terminos">
		<a href="http://veepe.mx/terminos">Términos y condiciones</a>
	   </div>
        	<div class="copyright">
				<div class="footer-copyright text-center col-md-6 col-xs-12"><p>Copyrights 2017 &copy; VEEPE-Hecho con <img src="img/estacionamiento-valet-veepe-parking.png"> <a href="http://qubitworks.com/" target="_blank">Qubit</a> </p></div>
            </div>
		</div>
	</div>
</div>

</div>

<!-- JS -->

<!-- jQuery JS
============================================ -->
<script src="js/vendor/jquery-1.12.0.min.js"></script>
<!-- Bootstrap JS
============================================ -->
<script src="js/bootstrap.min.js"></script>
<!-- One Page Nav JS
============================================ -->
<script src="js/jquery.nav.js"></script>
<!-- Owl Carousel JS
============================================ -->
<script src="js/slick.min.js"></script>
<!-- Smooth Scroll JS
============================================ -->
<script src="js/smoothscroll.js"></script>
<!-- ScrollUP JS
============================================ -->
<script src="js/jquery.scrollup.min.js"></script>
<!-- Ajaxchimp JS
============================================ -->
<script src="js/jquery.ajaxchimp.min.js"></script>
<!-- Ajax Mail JS
============================================ -->
<script src="js/ajax-mail.js"></script>
<!-- Ccountdown JS
============================================ -->
<script src="js/jquery.ccountdown.js"></script>
<!-- Knob JS
============================================ -->
<script src="js/jquery.knob.min.js"></script>
<!-- textillate JS
============================================ -->
<script src="js/jquery.textillate.js"></script>
<script src="js/jquery.lettering.js"></script>
<!-- Youtube Background JS
============================================ -->
<script src="js/jquery.mb.YTPlayer.min.js"></script>
<!-- Vide JS
============================================ -->
<script src="js/jquery.vide.min.js"></script>
<!-- Backstretch JS
============================================ -->
<script src="js/jquery.backstretch.min.js"></script>
<!-- Velocity JS
============================================ -->
<script src="js/jquery.velocity.min.js"></script>
<!-- Kenburnsy JS
============================================ -->
<script src="js/jquery.kenburnsy.min.js"></script>
<!-- Magnific Popup JS
============================================ -->
<script src="js/jquery.magnific-popup.min.js"></script>	
<!-- Google Map APi
============================================ -->
<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBuU_0_uLMnFM-2oWod_fzC0atPZj7dHlU"></script>
<script src="js/map.js"></script>-->
<!-- Main JS
============================================ -->
<script src="js/main.js"></script>

</body>
<script>
	"use strict";
	// _____________________________________________________________________________________________________________ VARIABLES GLOBALES
	
	// _____________________________________________________________________________________________________________ FUNCIONES GLOBALES
	
	// _____________________________________________________________________________________________________________ JQUERY
	jQuery(document).ready(function ()
	{
		// _____________________________________________________________________ configuraciones
		jQuery('#lnkTerminos').click(function(){
			jQuery('#overlayTerminos').show();
			jQuery('body').css('overflow', 'hidden');
		});
		
		jQuery('#overlayTerminos .cmdCerrar').click(function(){
			jQuery('#overlayTerminos').hide();
			jQuery('body').css('overflow', 'auto');
		});
		
		// _____________________________________________________________________ entrypt
	});// fin del document.ready
</script>
</html>
